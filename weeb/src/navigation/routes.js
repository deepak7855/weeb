import * as React from 'react';
import Menu from '../screens/Menu'
import ChatList from '../screens/ChatList';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Home from '../screens/HomeScreen/Home';
import HomeTabs from '../screens/HomeTabs';
import Search_Tab from '../screens/Search/Search_Tab';
import Maintenance from '../screens/Maintenance';
import PayRent from '../screens/PayRent';
import Profile from '../screens/Profile/Profile';
import Signup from '../screens/Signup/Signup';
import Signin from '../screens/Signin/Signin';
import VerifyOtp from '../screens/VerifyOtp/VerifyOtp';
import ForgetPassword from '../screens/ForgetPassword/ForgetPassword';
import Resetpassword from '../screens/ResetPassword/Resetpassword';
import SplashScreen from '../screens/Splash/Splash';
import MyDocuments from '../screens/MyDocuments';
import UploadDocuments from '../screens/UploadDocuments';
import DocumentTabs from '../screens/DocumentTabs';
import PropertyDetails from '../screens/PropertyDetails/PropertyDetails';
import BookingRequestTopTab from '../screens/BookingRequestTopTab';
import MyRentProperty from '../screens/MyRentProperty';
import PaymentDetails from '../screens/PaymentDetails';
import Payment from '../screens/Payment';
import Chat from '../screens/Chat';
import MyTransactions from '../screens/MyTransactions';
import Faq from '../screens/Faq';
import ChangePassword from '../screens/ChangePassword';
import SelectCardType from '../screens/PaymentMethod/SelectCardType';
import PaymentMethods from '../screens/PaymentMethod/PaymentMethods';
import AddNewCard from '../screens/PaymentMethod/AddNewCard'
import Notification from '../screens/Notification/Notification'
import SelectPropertyType from '../screens/AddProperty/SelectPropertyType';
import ProperityInformation from '../screens/AddProperty/ProperityInformation';
import ProperityRoomInfomation from '../screens/AddProperty/ProperityRoomInfomation';
import SelectFurnitures from '../screens/AddProperty/SelectFurnitures';
import ProperityCharges from '../screens/AddProperty/ProperityCharges';
import AddProperityMedia from '../screens/AddProperty/AddProperityMedia';
import OpenNotification from '../screens/Notification/OpenNotification'
import Settings from '../screens/Settings/Settings';
import ListedPropertyTabs from '../screens/ListedPropertyTabs'
import ShowAgreementsDocuments from '../screens/Agreements/ShowAgreementsDocuments';
import DigitalAgreement from '../screens/Agreements/DigitalAgreement';
import AddAgreement from '../screens/Agreements/AddAgreement';
import ShowTenantList from '../screens/Tenant/ShowTenantList';
import AddExitingTenant from '../screens/Tenant/AddExitingTenant';
import SelectProperityForExitingTenant from '../screens/Tenant/SelectProperityForExitingTenant';
import SideDrawer from '../screens/Drawer/Drawer';
import WelcomeBank from '../screens/bankAccount/WellcomeAddBank';
import AddBankList from '../screens/bankAccount/AddBankList';
import EditAddBank from '../screens/bankAccount/EditAddBank'
import Maintenance_16_2 from '../screens/Maintenance_16_2';
import Maintenance_16_4 from '../screens/Maintenance_16_4';
import Maintenance_16_3 from '../screens/Maintenance_16_3';
import InActiveProperty from '../screens/InActiveProperty'
import WellcomeAddBank from '../screens/bankAccount/WellcomeAddBank';
import MyRentel from '../screens/MyRentale/MyRentel'
import EditProfile from '../screens/Profile/EditProfile';
import Paymentmethods from '../screens/Paymentmethods';
import BookingRequest from '../screens/BookingRequest';
import BookingAccepted from '../screens/BookingAccepted';
import BookingDeclined from '../screens/BookingDeclined';
import Agreement from '../screens/Agreement'
import OwnerDocument from '../screens/OwnerDocument'
import Signature from '../screens/Agreements/Signature'

import TermAndPrivacy from '../screens/TermAndPrivacy';

import FavoriteList from '../screens/Favorite/TenantFavoritePropertyList';
import ChangeLanguage from '../screens/ChangeLanguage/ChangeLanguage';
const ScreenStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const MainStack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();
const DrawerStack = () => (
    <Drawer.Navigator
        drawerType={'slide'}
        drawerStyle={{
            width: '100%',
        }}
        drawerType="front"
        options={{ headerShown: false }}
        drawerContent={props => <SideDrawer {...props} />
        }
    >
        <Drawer.Screen name="HomeTabs" component={HomeTabs}  />
    </Drawer.Navigator>
)

const ScreenStackNavigator = () => {
    return (
      <ScreenStack.Navigator initialRouteName="SplashScreen">
        <ScreenStack.Screen
          name="DrawerStack"
          component={DrawerStack}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="Menu"
          component={Menu}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="TermAndPrivacy" component={TermAndPrivacy} />
        <ScreenStack.Screen name="OwnerDocument" component={OwnerDocument} />
        <ScreenStack.Screen name="Agreement" component={Agreement} />
        <ScreenStack.Screen
          name="Signature"
          component={Signature}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="BookingRequest"
          component={BookingRequest}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="BookingAccepted"
          component={BookingAccepted}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="BookingDeclined"
          component={BookingDeclined}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="AddNewCard" component={AddNewCard} />
        <ScreenStack.Screen
          name="Maintenance_16_2"
          component={Maintenance_16_2}
        />
        <ScreenStack.Screen
          name="Maintenance_16_3"
          component={Maintenance_16_3}
        />
        <ScreenStack.Screen
          name="Maintenance_16_4"
          component={Maintenance_16_4}
        />
          <ScreenStack.Screen
          name="FavoriteList"
          component={FavoriteList}
        />
        <ScreenStack.Screen name="PaymentMethods" component={PaymentMethods} />
        <ScreenStack.Screen name="AddBankList" component={AddBankList} />
        <ScreenStack.Screen name="SelectCardType" component={SelectCardType} />
        <ScreenStack.Screen name="EditAddBank" component={EditAddBank} />
        <ScreenStack.Screen
          name="WelcomeBank"
          component={WelcomeBank}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="Chat" component={Chat} />
        <ScreenStack.Screen name="MyTransactions" component={MyTransactions} />
        <ScreenStack.Screen name="ChatList" component={ChatList} />
        <ScreenStack.Screen name="MyRentProperty" component={MyRentProperty} />
        <ScreenStack.Screen name="PaymentDetails" component={PaymentDetails} />
        <ScreenStack.Screen name="Payment" component={Payment} />
        <ScreenStack.Screen
          name="Signin"
          component={Signin}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="ForgetPassword"
          component={ForgetPassword}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="Resetpassword"
          component={Resetpassword}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="Notification" component={Notification} />
        <ScreenStack.Screen
          name="OpenNotification"
          component={OpenNotification}
        />
        <ScreenStack.Screen
          name="SelectPropertyType"
          component={SelectPropertyType}
        />
        <ScreenStack.Screen
          name="ProperityInformation"
          component={ProperityInformation}
        />
        <ScreenStack.Screen
          name="ProperityCharges"
          component={ProperityCharges}
        />
        <ScreenStack.Screen
          name="AddProperityMedia"
          component={AddProperityMedia}
        />
        <ScreenStack.Screen
          name="ListedPropertyTabs"
          component={ListedPropertyTabs}
        />
        <ScreenStack.Screen
          name="InActiveProperty"
          component={InActiveProperty}
        />
        <ScreenStack.Screen name="EditProfile" component={EditProfile} />
        <ScreenStack.Screen
          name="WellcomeAddBank"
          component={WellcomeAddBank}
        />
        <ScreenStack.Screen name="Faq" component={Faq} />
        <ScreenStack.Screen name="MyRentel" component={MyRentel} />
        <ScreenStack.Screen name="Paymentmethods" component={Paymentmethods} />
        <ScreenStack.Screen name="ChangePassword" component={ChangePassword} />
        <ScreenStack.Screen name="Home" component={Home} />
        <ScreenStack.Screen name="Search_Tab" component={Search_Tab} />
        <ScreenStack.Screen name="Maintenance" component={Maintenance} />
        <ScreenStack.Screen
          name="PayRent"
          component={PayRent}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="Profile" component={Profile} />
        <ScreenStack.Screen
          name="VerifyOtp"
          component={VerifyOtp}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="MyDocuments" component={MyDocuments} />
        <ScreenStack.Screen
          name="UploadDocuments"
          component={UploadDocuments}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="DocumentTabs" component={DocumentTabs} />
        <ScreenStack.Screen name="AddAgreement" component={AddAgreement} />
        <ScreenStack.Screen
          name="PropertyDetails"
          component={PropertyDetails}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen
          name="BookingRequestTopTab"
          component={BookingRequestTopTab}
        />
        <ScreenStack.Screen
          name="SelectFurnitures"
          component={SelectFurnitures}
        />
        <ScreenStack.Screen
          name="ShowAgreementsDocuments"
          component={ShowAgreementsDocuments}
        />
        <ScreenStack.Screen
          name="DigitalAgreement"
          component={DigitalAgreement}
          options={{headerShown: false}}
        />
        <ScreenStack.Screen name="Settings" component={Settings} />
        <ScreenStack.Screen name="ShowTenantList" component={ShowTenantList} />
        <ScreenStack.Screen
          name="ProperityRoomInfomation"
          component={ProperityRoomInfomation}
        />
        <ScreenStack.Screen
          name="AddExitingTenant"
          component={AddExitingTenant}
        />
        <ScreenStack.Screen
          name="SelectProperityForExitingTenant"
          component={SelectProperityForExitingTenant}
        />
          <ScreenStack.Screen
          name="ChangeLanguage"
          component={ChangeLanguage}
        />
      </ScreenStack.Navigator>
    );
}







function Routes() {
    return (
        <NavigationContainer>
            <Stack.Navigator >
                <Stack.Screen name="ScreenStackNavigator" component={ScreenStackNavigator} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
export function handleNavigation(nav) {
    switch (nav.type) {
        case 'push':
            nav.navigation.navigate(nav.page, nav.passProps);
            break;
        case 'pushTo':
            nav.navigation.push(nav.page, nav.passProps);
            break;
        case 'setRoot':
            nav.navigation.reset({ index: 0, routes: [{ name: nav.page }] });
            break;
        case 'pop':
            nav.navigation.goBack(null);
            break;
            case 'popToTop':
                nav.navigation.goBack(null);
                break;
        case 'popTo':
            break;
    }
}
export default Routes;
