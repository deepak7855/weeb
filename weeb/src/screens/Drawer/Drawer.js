import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  Keyboard,
  View,
  Image,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Platform,
  DeviceEventEmitter,
  Dimensions,
} from 'react-native';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import Helper from '../../Lib/Helper';
import images from '../../Lib/Images';
import {handleNavigation} from '../../navigation/routes';
import {Constant, screenDimensions} from '../../Lib/Constant';
import {
  ApiCall,
  ActivityIndicatorApp,
  LoaderForList,
  ApiUrl,
} from '../../Api/index';
import {Network, AlertMsg} from '../../Lib/index';
import { translate } from "../../Language";

export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelectType: Helper.userData?.user_type == 'OWNER' ? 'OWNER' : 'TENANT',
      selectedAvtar: Helper.userData?.profile_picture
        ? Helper.userData?.profile_picture
        : '',
      name: Helper.userData?.name ? Helper.userData?.name : '',
      email: Helper.userData?.email ? Helper.userData?.email : '',
      phoneNumber: Helper.userData?.mobile_number
        ? Helper.userData?.mobile_number
        : '',
      city: Helper.userData?.city ? Helper.userData?.city : '',
      userCount: '',
      render: false,
    };
  }
  componentDidMount() {
    this.getUserTypeStorge();
    console.log(
      'Helper.userData?.user_type mount ',
      Helper.userData?.user_type,
      this.state.isSelectType,
    );
    this.userCount = DeviceEventEmitter.addListener('user_count', (data) => {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          ApiCall.ApiMethod({Url: 'get-property-counts', method: 'GET'}).then(
            (res) => {
              //  console.log('user count', res);
              if (res?.status) {
                this.setState({
                  userCount: res?.data,
                });
              }
            },
          );
        }
      });
    });
    this.getUserType = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
       // alert(data?.type);
        this.setState({
          selectedAvtar: Helper.userData?.profile_picture
            ? Helper.userData?.profile_picture
            : '',
          name: Helper.userData?.name ? Helper.userData?.name : '',
          email: Helper.userData?.email ? Helper.userData?.email : '',
          phoneNumber: Helper.userData?.mobile_number
            ? Helper.userData?.mobile_number
            : '',
          city: Helper.userData?.city ? Helper.userData?.city : '',
          isSelectType: data?.type == 'OWNER' ? 'OWNER' : 'TENANT',
        });
      },
    );
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getUserTypeStorge();
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          ApiCall.ApiMethod({Url: 'get-property-counts', method: 'GET'}).then(
            (res) => {
              console.log('user count', res)
              if (res?.status) {
                this.setState({
                  userCount: res?.data,
                });
              }
            },
          );
        }
      });
      this.setState({
        selectedAvtar: Helper.userData?.profile_picture
          ? Helper.userData?.profile_picture
          : '',
        name: Helper.userData?.name ? Helper.userData?.name : '',
        email: Helper.userData?.email ? Helper.userData?.email : '',
        phoneNumber: Helper.userData?.mobile_number
          ? Helper.userData?.mobile_number
          : '',
        city: Helper.userData?.city ? Helper.userData?.city : '',
        isSelectType: Helper.userType == 'OWNER' ? 'OWNER' : 'TENANT',
      });
    });

    this.messageRecvied = DeviceEventEmitter.addListener('messageCount',(data)=>{
      if(data){
        ApiCall.ApiMethod({Url: 'get-property-counts', method: 'GET'}).then(
          (res) => {
            console.log('user count', res)
            if (res?.status) {
              this.setState({
                userCount: res?.data,
              });
            }
          },
        );
      }
    })

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
    // ApiCall.ApiMethod('get-profile','GET').then((response) => {
    //     //console.log('user profile data', response)
    // })

    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        ApiCall.ApiMethod({Url: 'get-property-counts', method: 'GET'}).then(
          (res) => {
            console.log('user count', res);
            if (res?.status) {
              this.setState({
                userCount: res?.data,
              });
            }
          },
        );
      }
    });
  }
  getUserTypeStorge = () => {
    Helper.getData(Constant.USER_TYPE).then((data) => {
      // alert(data)
      console.log('user type', data);
      Helper.userType = data;
      Helper.userData['user_type'] = data;
      // this.setState({
      //   render:!this.state.render
      // })
      return data;
    });
  };
  componentWillUnmount() {
    this._unsubscribe();
    this.userCount.remove();
    this.getUserType.remove();
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  MyRentProperty = (value) => {
    this.props.navigation.closeDrawer();
    Helper.showDocument = false;
    handleNavigation({
      type: 'push',
      page: value,
      navigation: this.props.navigation,
      passProps: {type: 'document'},
    });
  };

  onSelectTypeUser = (value) => {
    Network.isNetworkAvailable().then((isConnected) => {
      this.setState({isSelectType: value}, () => {
        DeviceEventEmitter.emit(Constant.USER_TYPE, {
          type: this.state.isSelectType,
        });
        Helper.userType = this.state.isSelectType;
        Helper.userData['user_type'] = this.state.isSelectType;
        Helper.setData(Constant.USER_TYPE, this.state.isSelectType);
        Helper.setData(Constant.USER_DATA, Helper.userData);
        if (isConnected) {
          ApiCall.ApiMethod({
            Url: 'set-user-type',
            method: 'POST',
            data: {type: value},
          })
            .then((res) => {
              console.log('user status changes', res);
              if (res?.status) {
                return true;
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
      });
    });
  };

  termsandcondition() {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'Terms and Conditions',
        url: ApiUrl?.TermAndPrivacy,
      },
      navigation: this.props.navigation,
    });
  }

  openPrivacy = () => {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'Privacy Policy',
        url: ApiUrl?.PrivacyPolicy,
      },
      navigation: this.props.navigation,
    });
  };

  openAboutus = () => {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'About Weeb',
        url: ApiUrl?.AboutUs,
      },
      navigation: this.props.navigation,
    });
  };
  openHelpAndSupport = () => {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'Help & Support',
        url: ApiUrl?.HelpAndSupport,
      },
      navigation: this.props.navigation,
    });
  };

  render() {
    //console.log('Helper.userData?.user_type ',Helper.userData?.user_type,this.state.isSelectType )
    return (
      <View style={styles.containView}>
        <SafeAreaView style={{backgroundColor: Colors.lightMint}} />
        <View style={{height: 186, backgroundColor: Colors.lightMint}}>
          <View style={styles.mainView}>
            <View style={{flex: 0.2, alignItems: 'center'}}>
              <Image
                style={[styles.profile, {marginTop: 5}]}
                source={
                  this.state.selectedAvtar
                    ? {uri: this.state.selectedAvtar}
                    : images.tenant_profile
                }
              />
            </View>
            <View style={{flex: 0.6, left: 5}}>
              <Text style={styles.nameText}>{this.state.name}</Text>
              <Text style={styles.mobText}>{this.state.email}</Text>
              <Text style={styles.mobText}>{this.state.phoneNumber}</Text>
              <Text style={{fontSize: 12, marginTop: 17}}>Switch Account</Text>
              {/* <View style={styles.typeView}>
                                <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={() => { this.onSelectTypeUser('TENANT') }}>
                                    <Text style={this.state.isSelectType == 'TENANT' ? styles.actTenText : styles.tenText}>TENANT</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.onSelectTypeUser('OWNER') }} style={{ justifyContent: "center", alignItems: 'center' }}>
                                    <Text style={this.state.isSelectType == 'OWNER' ? styles.actOwnerText : styles.ownerText}>OWNER</Text>
                                </TouchableOpacity>
                            </View> */}
              <View style={styles.typeView}>
                <TouchableOpacity
                  style={[
                    this.state.isSelectType == 'TENANT'
                      ? styles.actTenText
                      : styles.tenText,
                    {justifyContent: 'center', alignItems: 'center'},
                  ]}
                  onPress={() => {
                    this.onSelectTypeUser('TENANT');
                  }}>
                  <Text style={{fontSize: 11, fontFamily: Fonts.segoeui}}>
                    TENANT
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.onSelectTypeUser('OWNER');
                  }}
                  style={[
                    this.state.isSelectType == 'OWNER'
                      ? styles.actOwnerText
                      : styles.ownerText,
                    {justifyContent: 'center', alignItems: 'center'},
                  ]}>
                  <Text style={{fontSize: 11, fontFamily: Fonts.segoeui}}>
                    OWNER
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex: 0.2, alignItems: 'flex-end'}}>
              <TouchableOpacity
                style={{width: 30, alignItems: 'center'}}
                onPress={() => {
                  this.props.navigation.closeDrawer();
                }}>
                <Image style={styles.closeIcon} source={images.close} />
              </TouchableOpacity>
              <Text style={[styles.veiText, {top: 7, right: 5}]}>Verified</Text>
              <View
                style={{
                  marginTop: Helper.hasNotch
                    ? 57
                    : Platform.OS === 'ios'
                    ? 57
                    : 69,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.MyRentProperty('ChatList');
                  }}
                  style={styles.chatView}>
                  <Image
                    style={styles.chatIcon}
                    source={images.chat_circle_btn}
                  />
                  <Text style={styles.chatText}>Chat</Text>
                  <View style={styles.txt_Chat}>
                    <Text
                      style={{
                        fontFamily: Fonts.segoeui,
                        fontSize: 10,
                        color: Colors.whiteTwo,
                      }}>
                      {this.state.userCount?.message_count}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <ScrollView
          style={{paddingTop: 10}}
          showsVerticalScrollIndicator={false}>
          {this.state.isSelectType == 'TENANT' ? (
            <TouchableOpacity
              onPress={() => {
                this.MyRentProperty('MyRentProperty');
              }}
              style={styles.view_Menu}>
              <Image
                style={styles.icon_menu}
                source={images.my_rent_property}
              />
              <View style={{flex: 1, marginLeft: 21}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.txt_Label}>{translate("MyRentProperty")}</Text>
                  <View style={styles.rentProperty}>
                    <Text
                      style={{
                        fontFamily: Fonts.segoeui,
                        fontSize: 10,
                        color: Colors.pinkishGrey,
                      }}>
                      {this.state.userCount
                        ? this.state.userCount?.tenant_rented_count
                        : ''}
                    </Text>
                  </View>
                </View>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : (
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.MyRentProperty('ListedPropertyTabs');
                }}
                style={styles.view_Menu}>
                <Image style={styles.icon_menu} source={images.list_Property} />
                <View style={{flex: 1, marginLeft: 21}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.txt_Label}>{translate("ListedProperty")}</Text>
                    <View style={styles.rentProperty}>
                      <Text
                        style={{
                          fontFamily: Fonts.segoeui,
                          fontSize: 10,
                          color: Colors.pinkishGrey,
                        }}>
                        {this.state.userCount
                          ? this.state.userCount?.properties_count
                          : ''}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.boder_grey}></View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.MyRentProperty('MyRentel');
                }}
                style={styles.view_Menu}>
                <Image
                  style={styles.icon_menu}
                  source={images.my_rent_property}
                />
                <View style={{flex: 1, marginLeft: 21}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.txt_Label}>{translate("MyRentals")}</Text>
                    <View style={styles.rentProperty}>
                      <Text
                        style={{
                          fontFamily: Fonts.segoeui,
                          fontSize: 10,
                          color: Colors.pinkishGrey,
                        }}>
                        {this.state.userCount
                          ? this.state.userCount?.owner_rented_count
                          : ''}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.boder_grey}></View>
                </View>
              </TouchableOpacity>
            </View>
          )}

          <TouchableOpacity
            onPress={() => {
              this.MyRentProperty('BookingRequestTopTab');
            }}
            style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.booking_request} />
            <View style={{flex: 1, marginLeft: 21}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.txt_Label}>{translate("BookingRequest")}</Text>
                {this.state.isSelectType == 'TENANT' ? (
                  <View style={styles.rentProperty}>
                    <Text
                      style={{
                        fontFamily: Fonts.segoeui,
                        fontSize: 10,
                        color: Colors.pinkishGrey,
                      }}>
                      {this.state.userCount
                        ? this.state.userCount?.booking_tenant_count
                        : ''}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.BookRequest}>
                    <Text
                      style={{
                        fontFamily: Fonts.segoeui,
                        fontSize: 10,
                        color: Colors.whiteTwo,
                        textAlign: 'center',
                      }}>
                      {this.state.userCount
                        ? this.state.userCount?.booking_owner_count
                        : ''}
                    </Text>
                  </View>
                )}
              </View>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          {this.state.isSelectType == 'TENANT' ? (
            <TouchableOpacity
              onPress={() => {
                this.MyRentProperty('DocumentTabs');
              }}
              style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.doctounts} />
              <View style={{flex: 1, marginLeft: 21}}>
                <Text style={styles.txt_Label2}>{translate("Documents")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                this.MyRentProperty('OwnerDocument');
              }}
              style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.doctounts} />
              <View style={{flex: 1, marginLeft: 21}}>
                <Text style={styles.txt_Label2}>{translate("Documents")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          )}

          {this.state.isSelectType == 'TENANT' ? (
            <TouchableOpacity
              onPress={() => {
                this.MyRentProperty('MyTransactions');
              }}
              style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.my_transactions} />
              <View style={{flex: 1, marginLeft: 21}}>
                <Text style={styles.txt_Label2}>{translate("MyTransactions")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                this.MyRentProperty('ShowTenantList');
              }}
              style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.add_tanant} />
              <View style={{flex: 1, marginLeft: 21}}>
                <Text style={styles.txt_Label2}>{translate("AddTenant")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          )}
          {this.state.isSelectType == 'TENANT' ? (
            <TouchableOpacity
              style={styles.view_Menu}
              onPress={() =>
                handleNavigation({
                  type: 'push',
                  page: 'FavoriteList',
                  navigation: this.props.navigation,
                })
              }>
              <Image
                style={[styles.icon_menu, {tintColor: Colors.lightMint}]}
                source={images.favorite}
              />
              <View style={{flex: 1, marginLeft: 21}}>
                <Text style={styles.txt_Label2}>{translate("Favorite")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            style={styles.view_Menu}
            onPress={() => this.openAboutus()}>
            <Image style={styles.icon_menu} source={images.about_weeb} />
            <View style={{flex: 1, marginLeft: 21}}>
              <Text style={styles.txt_Label2}>{translate("AboutWeeb")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.MyRentProperty('Faq');
            }}
            style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.faq} />
            <View style={{flex: 1, marginLeft: 21}}>
              <Text style={styles.txt_Label2}>{translate("FAQs")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.view_Menu}
            onPress={() => this.openHelpAndSupport()}>
            <Image style={styles.icon_menu} source={images.customer_support} />
            <View style={{flex: 1, marginLeft: 21}}>
              <Text style={styles.txt_Label2}>{translate("Help&Support")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.view_Menu}
            onPress={() => {
              this.termsandcondition();
            }}>
            <Image style={styles.icon_menu} source={images.terms} />
            <View style={{flex: 1, marginLeft: 21}}>
              <Text style={styles.txt_Label2}>{translate("Terms&Conditions")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.view_Menu, {marginBottom: 50}]}
            onPress={() => {
              this.openPrivacy();
            }}>
            <Image style={styles.icon_menu} source={images.privacy} />
            <View style={{flex: 1, marginLeft: 21}}>
              <Text style={styles.txt_Label2}>{translate("PrivacyPolicy")}</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
      // </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  containView: {flex: 1},
  mainView: {flexDirection: 'row', marginTop: 30, marginHorizontal: 12},
  profile: {height: 56, width: 56, borderRadius: 56},
  nameText: {
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackThree,
  },
  mobText: {
    fontSize: 13,
    fontFamily: Fonts.segoeui,
    color: Colors.brownishGrey,
  },
  closeIcon: {height: 13, width: 13, resizeMode: 'contain', marginTop: 9},
  veiText: {fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.dustyOrange},
  tenText: {
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    width: 70,
    paddingVertical: 2,
    textAlign: 'center',
    color: 'black',
    backgroundColor: Colors.whiteFive,
  },
  actTenText: {
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    width: 70,
    paddingVertical: 2,
    textAlign: 'center',
    backgroundColor: Colors.darkSeafoamGreen,
    color: 'white',
  },
  ownerText: {
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    width: 70,
    paddingVertical: 2,
    textAlign: 'center',
    color: Colors.greyishBrown,
    backgroundColor: Colors.whiteFive,
  },
  actOwnerText: {
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    width: 70,
    paddingVertical: 2,
    textAlign: 'center',
    color: 'white',
    backgroundColor: Colors.darkSeafoamGreen,
  },
  typeView: {
    flexDirection: 'row',
    borderColor: Colors.warmGrey,
    marginTop: 6,
    width: 141,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 0.5,
  },
  chatView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    paddingHorizontal: 8,
    borderRadius: 2,
    borderColor: Colors.darkSeafoamGreen,
    backgroundColor: Colors.whiteTwo,
  },
  chatIcon: {height: 12, width: 12, resizeMode: 'contain', margin: 2},
  chatText: {
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
    margin: 2,
  },
  view_Menu: {flexDirection: 'row', marginHorizontal: 15, marginTop: 15},
  icon_menu: {width: 29, height: 29, resizeMode: 'contain'},
  txt_Label: {
    flex: 1,
    fontFamily: Fonts.segoeui,
    fontSize: 16,
    color: Colors.black,
    lineHeight: 25,
  },
  rentProperty: {
    justifyContent: 'center',
    alignItems: 'center',
    top: Platform.OS == 'ios' ? 5 : 0,
    height: 22,
    borderColor: Colors.pinkishGrey,
    borderRadius: 15,
    borderWidth: 1,
    paddingHorizontal: 24,
  },
  boder_grey: {
    backgroundColor: Colors.warmGrey,
    height: 0.5,
    marginTop: 17,
    opacity: 0.5,
  },
  BookRequest: {
    justifyContent: 'center',
    width: 26,
    height: 26,
    paddingVertical: 3,
    backgroundColor: Colors.cherryRed,
    borderRadius: 26,
  },
  txt_Label2: {
    fontFamily: Fonts.segoeui,
    fontSize: 16,
    color: Colors.black,
    lineHeight: 25,
  },
  txt_Chat: {
    backgroundColor: Colors.cherryRed,
    paddingHorizontal: 6,
    borderRadius: 20,
    position: 'absolute',
    right: 0,
    top: -10,
  },
});
