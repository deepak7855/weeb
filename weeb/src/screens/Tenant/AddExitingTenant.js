import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import { Validation } from '../../Lib/index'
import { Inputs, MobileInputs, CountryCodePickerModal } from '../../Components/Common/index'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { handleNavigation } from '../../navigation/routes'
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import { translate } from '../../Language';
export default class AddExitingTenant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            mobile: '',
            countryCode: '+965',
            modalVisibleCode: false,
            countryCodeWithoutPlus: '965',
        };
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Add Existing Tenant',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    renderTitle = (bold, label) => {
        return (
            <Text
                style={{
                    fontSize: bold ? 16 : 14,
                    color: Colors.blackThree,
                    fontFamily: bold ? Fonts.segoeui_bold : Fonts.segoeui,
                    marginHorizontal: 12, marginTop: 10
                }}>
                {label}
            </Text>
        )
    }
    nextScreen() {
        if (Validation.checkAlphabat('Full Name', 3, 25, this.state.name) &&
            Validation.checkEmail('Email', this.state.email) &&
            Validation.checkPhoneNumber('Phone Number', 7, 13, this.state.mobile)) {
            handleNavigation({ type: 'push', page: 'SelectProperityForExitingTenant', passProps: { tenantData: { 'name': this.state.name, 'email': this.state.email, 'country_code': this.state.countryCode, 'mobile_number': this.state.mobile } }, navigation: this.props.navigation })

        }

    }
    callCounteryCodeApi = (dial_code) => {
        this.setState({
            countryCode: dial_code,
            modalVisibleCode: false,
            countryCodeWithoutPlus: dial_code.substring(1),
        })
    }
    render() {
        return (
            <View style={styles.Container}>
                <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}>
                    <View style={{ marginHorizontal: 20, marginTop: 9 }}>
                        {this.renderTitle(true, translate("TenantDetails"))}
                        <Inputs
                            labels={translate("FullName")}
                            labelsize={12}
                            labelcolor={Colors.black}
                            width={'80%'}
                            backgroundcolor={'white'}
                            color={Colors.brownishGrey}
                            rightimage={Images.edit_pofile}
                            placeholder={translate("FullName")}
                            marginleft={-10}
                            keyboard={'default'}
                            maxlength={20}
                            onChangeText={(text) => { this.setState({ name: text }) }}
                            value={this.state.name}
                            returnKeyType={'next'}
                            setfocus={(input) => {
                                this.name = input;
                            }}
                            getfocus={() => {
                                this.email.focus();
                            }}
                            bluronsubmit={false}
                        />

                        <Inputs
                            labels={translate("EmailAddress")}
                            labelsize={12}
                            labelcolor={Colors.black}
                            width={'80%'}
                            backgroundcolor={'white'}
                            color={Colors.brownishGrey}
                            rightimage={Images.email}
                            placeholder={translate("EmailAddress")}
                            marginleft={-10}
                            keyboard={'email-address'}
                            maxlength={30}
                            onChangeText={(text) => { this.setState({ email: text }) }}
                            value={this.state.email}
                            returnKeyType={'next'}
                            setfocus={(input) => {
                                this.email = input;
                            }}
                            getfocus={() => {
                                this.mobile.focus();
                            }}
                            bluronsubmit={false}
                        />
                        <Text style={{ marginTop: 20, marginHorizontal: 10, fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.black }}>{translate("MobileNumber")}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <View style={{ flex: 0.4 }}>
                                <TouchableOpacity style={[styles.countryView, { backgroundColor: Colors.whiteTwo }]} onPress={() => this.setState({ modalVisibleCode: true })}>
                                    <Image style={{ height: 12, width: 12, resizeMode: 'contain' }} source={images.mobile} />
                                    <Text>{this.state.countryCode}</Text>
                                    <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={images.drop_arrow} />
                                </TouchableOpacity>

                            </View>
                            <CountryCodePickerModal
                                visible={this.state.modalVisibleCode}
                                onRequestClose={() => { this.setState({ modalVisibleCode: false }) }}
                                onPress={() => { this.setState({ modalVisibleCode: false }) }}
                                callCounteryCodeApi={(dial_code) => { this.callCounteryCodeApi(dial_code) }}
                            />
                            <View style={{ flex: 0.6, alignItems: 'center', top: -10 }}>
                                <Inputs
                                    width={'90%'}
                                    backgroundcolor={'white'}
                                    color={Colors.brownishGrey}
                                    placeholder={'968-926-0227'}
                                    keyboard={'number-pad'}
                                    onChangeText={(text) => this.setState({ mobile: text })}
                                    value={this.state.mobile}
                                    setfocus={(input) => {
                                        this.mobile = input;
                                    }}
                                    returnKeyType={'done'}
                                    bluronsubmit={true}
                                    maxlength={10}
                                />
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>

                <TouchableOpacity
                    onPress={() =>
                        this.nextScreen()
                    }
                >

                    <View style={{ backgroundColor: Colors.darkSeafoamGreen, height: 1 }}></View>
                    <View style={styles.submitButton}>
                        <Text style={styles.buttonText}>{translate("Continue")}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: 'white'
    },
    submitButton: {
        height: 55,
        justifyContent: 'center',
        alignItems: "center",
        // borderWidth: 1,
    },
    buttonText: {
        fontSize: 18,
        fontFamily: Fonts.segui_semiBold,
        color: Colors.darkSeafoamGreen
    },
    countryView: {
        marginHorizontal: 8, marginTop: 10, paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderWidth: 1.5, borderRadius: 4,
        borderColor: Colors.whiteEight
    }
})