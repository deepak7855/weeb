import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity,RefreshControl } from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import { Inputs, FlootingButton, GoogleApiAddressList,ProgressiveImage } from '../../Components/Common/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { Helper, Network, AlertMsg } from '../../Lib/index'
import {ApiCall, LoaderForList} from '../../Api/index';
import { translate } from '../../Language';
export default class SelectProperityForExitingTenant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      openId: '',
      opener: false,
      houseData: [],
      dataOfFilter: [
        {
          iconChecked: Images.filter_check,
          text: 'Full House',
          selected: true,
        },
        {
          iconChecked: Images.filter_check,
          text: 'Apartment in House ',
          selected: true,
        },
        {iconChecked: Images.filter_check, text: 'Chalet', selected: true},
        {
          iconChecked: Images.filter_check,
          text: 'Apartment in Building',
          selected: true,
        },
      ],
      modalVisibleForLocationFilter: false,
      lat: '',
      long: '',
      address: '',
      tenantData: props.route.params?.tenantData,
      isLoading: false,
      emptymessage: false,
      refreshing: false,
      next_page_url: '',
      currentPage: 1,
      propertyId: '',
      propertyType: '',
      checkInd: -1,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Add Existing Tenant',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    this.getOwnerProperty();
  }

  getOwnerProperty() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let data = {};
          ApiCall.ApiMethod({
            Url: 'active-property-list' + '?page=' + this.state.currentPage,
            method: 'POST',
            data: {
              property_type: this.state.propertyType,
              lat: this.state.lat,
              lng: this.state.long,
            },
          })
            .then((res) => {
              console.log('get owner property', res);
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res?.status) {
                if (res?.data) {
                this.setState({
                  houseData: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.next_page_url,
                });
                return;  
                } else {
              this.setState({
                isLoading: false,
                emptymessage: true,
                houseData: [],
                next_page_url: res?.next_page_url,
              });
              return    
                }
                
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                houseData: [],
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                houseData: [],
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  // ShowTenantList

  openFilter = () => {
    this.setState({opener: !this.state.opener});
  };

  selectItemGroup(value) {
    if (this.state.checkInd == value) {
      this.setState({checkInd: -1});
      this.setState({opener: false});
    } else {
      this.setState({checkInd: value});
      //alert(this.state.dataOfFilter[value]?.text);
      this.setState(
        {
          opener: false,
          propertyType: this.state.dataOfFilter[value]?.text,
        },
        () => {
          this.getOwnerProperty();
        },
      );
    }
  }

  toSelectSpecializatio = (index) => {
    let NewVar = [...this.state.dataOfFilter];
    NewVar[index].selected = !NewVar[index].selected;
    this.setState({dataOfFilter: NewVar});
  };
  renderTitle = (bold, label) => {
    return (
      <Text
        style={{
          fontSize: bold ? 16 : 14,
          color: Colors.blackThree,
          fontFamily: bold ? Fonts.segoeui_bold : Fonts.segoeui,
          marginHorizontal: 12,
          marginTop: 10,
        }}>
        {label}
      </Text>
    );
  };
  getdata = (index) => {
    if (this.state.houseData[index]?.id) {
      console.log('property id', this.state.houseData[index]?.id);
      this.setState(
        {openId: index, propertyId: this.state.houseData[index]?.id},
        () => {
          console.log('data get', index, this.state.openId);
        },
      );
    }
  };
  renderProperityView = (data) => {
    // console.log('property data', data?.item,data?.index);
    return (
      <TouchableOpacity
        style={
          this.state.openId === data?.index
            ? styles.mainViewChecked
            : styles.mainview
        }
        onPress={() => {
          this.getdata(data?.index);
        }}>
        <ProgressiveImage
          source={{
            uri: data.item?.property?.propertyphotos
              ? data.item?.property?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.Image}
          resizeMode="cover"
        />
        {/* <Image source={Images.propert_requested} style={styles.Image} /> */}
        <View style={styles.TextViewCss}>
          <Text numberOfLines={1} style={styles.LighthouseTxt}>
            {data?.item?.title}
          </Text>
          <View style={styles.locationCss}>
            <Image source={Images.location} style={styles.locationImage} />
            <Text numberOfLines={1} style={styles.DhabiTxt}>
              {data?.item?.location}
            </Text>
          </View>
          <Text numberOfLines={1} style={styles.MTxt}>
            {data?.item?.rent}{' '}
            <Text
              style={{
                color: Colors.marigold,
                fontFamily: Fonts.segui_semiBold,
              }}>
              {' '}
              {data?.item?.type}
            </Text>
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  handleAddressForFilter = (data) => {
    console.log('property location', data);

    //this.setState({ addressFilter: data?.addressname, latFilter: data?.lat, longFilter: data?.long })
    this.setState({
      address: data?.addressname,
      lat: data?.lat,
      long: data?.long,
    });

    this.getOwnerProperty();
    // this.callPropertyList({ lat: data?.lat, long: data?.long, already: true })
  };

  addTenant() {
    if (!this.state.propertyId) {
      Helper.showToast('Please select property.');
      return;
    }
    Network.isNetworkAvailable()
      .then((isConnected) => {
        let data = this.state.tenantData;
        data['property_id'] = this.state.propertyId;
        if (isConnected) {
          Helper.mainApp.showLoader();
          ApiCall.ApiMethod({Url: 'add-tenant', method: 'POST', data: data})
            .then((res) => {
              Helper.mainApp.hideLoader();
              console.log('add tenant data', res);
              Helper.showToast(res?.message);
              if (res?.status) {
                this.setState({
                  propertyId: '',
                });
                this.props.navigation.navigate('ShowTenantList');
              }
            })
            .catch((err) => {
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {
          currentPage: 1,
          refreshing: false,
          propertyType: '',
          lat: '',
          long: '',
          address: '',
        },
        () => {
          this.getOwnerProperty();
        },
      );
    }, 2000);
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getTenantListData();
      });
    }
  };
  render() {
    //console.log('tenatn data', this.state.tenantData);
    return (
      <View style={styles.Container}>
        <KeyboardAwareScrollView>
          <View style={{marginHorizontal: 15}}>
            {this.renderTitle(true, translate("SelectProperty"))}
            <TouchableOpacity
              hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
              style={styles.searchViewFilter}
              onPress={() =>
                this.setState({modalVisibleForLocationFilter: true})
              }>
              <Image style={styles.searchIcon} source={images.search_icon} />
              <View
                style={styles.inputStyle}
                placeholderTextColor={Colors.pinkishGrey}
                onChangeText={(text) => {
                  this.setState({search: text});
                }}
                value={this.state.address}
                placeholder="Search by Location or Address"
                editable={false}
                // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: Fonts.segoeui,
                    color: Colors.pinkishGrey,
                  }}>
                  {' '}
                  {this.state.address
                    ? this.state.address
                    : translate("SearchbyLocationorAddress")}
                </Text>
              </View>
            </TouchableOpacity>
            <GoogleApiAddressList
              modalVisible={this.state.modalVisibleForLocationFilter}
              hideModal={() => {
                this.setState({modalVisibleForLocationFilter: false});
              }}
              onSelectAddress={this.handleAddressForFilter}
            />
            {/* <Inputs
                            width={'87%'}
                            backgroundcolor={Colors.whiteTwo}
                            color={Colors.brownishGrey}
                            rightimage={Images.search_icon}
                            placeholder={'Search by Location or Address'}
                            keyboard={'default'}
                            onChangeText={(text) => { this.setState({ search: text }) }}
                            value={this.state.search}
                            returnKeyType={'done'}
                            setfocus={(input) => {
                                this.search = input;
                            }}
                            getfocus={() => {
                                this.search.focus();
                            }}
                        /> */}
            <FlatList
              data={this.state.houseData}
              renderItem={this.renderProperityView}
              keyExtractor={(item, index) => item + index}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.onRefresh()}
                />
              }
              extraData={this.state}
              ItemSeparatorComponent={() => (
                <View style={styles.seperator}></View>
              )}
              ListFooterComponent={() => {
                return this.state.isLoading ? <LoaderForList /> : null;
              }}
              onEndReached={this.onScroll}
              onEndReachedThreshold={0.5}
              keyboardShouldPersistTaps={'handled'}
              refreshing={this.state.refreshing}
              ListEmptyComponent={() => (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: Fonts.segoeui,
                      color: Colors.cherryRed,
                      textAlign: 'center',
                    }}>
                    {this.state.houseData ? null : 'No Property.'}
                  </Text>
                </View>
              )}
            />
          </View>
        </KeyboardAwareScrollView>
        <FlootingButton
          toSelectSpecializatio={(index) => this.selectItemGroup(index)}
          opener={this.state.opener}
          openFilter={() => this.openFilter()}
          closeFilter={() => {
            this.openFilter();
          }}
          filterlist={this.state.dataOfFilter}
          checkInd={this.state.checkInd}
        />
        <TouchableOpacity
          onPress={() => this.addTenant()}
          disabled={
            this.state.openId === 0 ? false : this.state.openId ? false : true
          }>
          <View style={{backgroundColor: Colors.whiteFive, height: 1}}></View>
          <View style={styles.submitButton}>
            <Text
              style={[
                styles.buttonText,
                {
                  color:
                    this.state.openId === 0
                      ? Colors.darkSeafoamGreen
                      : this.state.openId
                      ? Colors.darkSeafoamGreen
                      : Colors.pinkishGrey,
                },
              ]}>
              {translate("AddNow")}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainview: { margin: 10, borderWidth: 0.3, borderRadius: 5, paddingHorizontal: 15, paddingVertical: 10, backgroundColor: Colors.whiteTwo, elevation: .3, flexDirection: 'row', alignItems: 'center', borderColor: Colors.whiteFive },
    mainViewChecked: { margin: 10, borderWidth: 0.3, borderRadius: 5, paddingHorizontal: 15, paddingVertical: 10, backgroundColor: Colors.lightMint, elevation: .3, flexDirection: 'row', alignItems: 'center', borderColor: Colors.whiteFive },
    Image: { height: 60, width: 70, resizeMode: 'contain' },
    LighthouseTxt: { fontSize: 14, color: Colors.blackTwo, top: -2, lineHeight: 25, fontFamily: Fonts.segoeui_bold, width: "95%" },
    locationCss: { flexDirection: 'row', alignItems: 'center' },
    TextViewCss: { flex: 1, marginHorizontal: 20 },
    locationImage: { height: 12, width: 10, resizeMode: 'contain', marginRight: 5, tintColor: Colors.brownishGreyTwo, top: 2 },
    DhabiTxt: { fontSize: 12, lineHeight: 22, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, width: "95%" },
    MTxt: { color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui_bold, fontSize: 12 },
    flottingButton: {
        // position: "absolute",
        bottom: 10,
        height: 57,
        width: 57,
        right: 10,
    },
    submitButton: {
        height: 55,
        justifyContent: 'center',
        alignItems: "center",
        // borderWidth: 1,
    },
    buttonText: {
        fontSize: 18,
        fontFamily: Fonts.segui_semiBold,
        // color: Colors.darkSeafoamGreen
    },
    searchViewFilter: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 3,
        height: 50,
        backgroundColor: Colors.whiteTwo,
        marginTop: 20,
        borderRadius: 4,
        borderWidth: 0.5,
    },
    searchIcon: { height: 12, width: 12, resizeMode: 'contain', marginLeft: 14 },
    inputStyle: {
        justifyContent: 'center',
        marginLeft: 10,
        fontSize: 12,
        fontFamily: Fonts.segoeui,
        color: Colors.pinkishGrey,
        width: '85%',
        height: Platform.OS === 'ios' ? 30 : 50,
    },
})