import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Linking,
  Platform,
  RefreshControl,
  DeviceEventEmitter,
} from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import { Inputs, ProgressiveImage } from '../../Components/Common/index';
import { handleNavigation } from '../../navigation/routes'
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { Helper, Network, AlertMsg } from '../../Lib/index'
import { ApiCall, LoaderForList } from '../../Api/index';
import moment from 'moment';
import { translate } from '../../Language';
export default class ShowTenantList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      tenantListData: [],
      isLoading: false,
      refreshing: false,
      next_page_url: '',
      currentPage: 1,
      emptymessage: false,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Add Tenant',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getTenantListData();
    });

    this.listner = DeviceEventEmitter.addListener(
      'notificaionCount',
      (data) => {
       // alert(data);
        // console.log('noti count in booking request',data)
        AppHeader({
          ...this,
          leftHeide: false,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          title: 'Add Tenant',
          searchClick: () => {
            this.notification();
          },
          searchIcon:
            data > 0 ? images.notification_goup : images.notification_icon,
          search: true,
        });
      },
    );
  }

  componentWillUnmount() {
    this._unsubscribe();
  }
  onChats = (item) => {
    //console.log(item?.user, 'item');
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {
        userId:item?.user?.id,
      },
      navigation: this.props.navigation,
    });
    // handleNavigation({ type: 'push', page: 'Chat', navigation: this.props.navigation });
  };
  getTenantListData() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          ApiCall.ApiMethod({
            Url: 'tenant-list' + '?page=' + this.state.currentPage,
            method: 'POST',
            data: {name: this.state.search},
          })
            .then((res) => {
              // console.log('get tenant list data', res)
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res) {
                this.setState({
                  tenantListData: res?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  callTenant(number) {
    let phoneNumber = '';
    if (Platform.OS == 'android') {
      phoneNumber = `tel:$${+number}`;
    } else {
      phoneNumber = `telprompt:$${+number}`;
    }
    Linking.openURL(phoneNumber);
  }

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getTenantListData();
      });
    }
  };
  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: this.state.currentPage, refreshing: false, name: ''},
        () => {
          this.getTenantListData();
        },
      );
    }, 2000);
  };
  renderUserData = (item) => {
   // console.log('user data', item.item);
    return (
      <View style={styles.flatListMainView}>
        <View style={styles.flatUserInfoView}>
          <ProgressiveImage
            source={{
              uri: item.item?.user ? item.item?.user?.profile_picture : '',
            }}
            style={styles.flatListUserPic}
            resizeMode="cover"
          />
          {/* <Image source={item.item.userprofile} style={styles.flatListUserPic} /> */}
          <View style={{flex: 1, paddingLeft: 10}}>
            <Text style={styles.userNameLabel}>{item.item?.user?.name}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={Images.tick} style={{width: 6, height: 4}} />
              <Text
                style={{
                  marginHorizontal: 5,
                  fontSize: 12,
                  fontFamily: Fonts.segoeui,
                  color: Colors.black,
                }}>
                {item.item?.user?.mobile_number}
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={Images.tick} style={{width: 6, height: 4}} />
              <Text
                style={{
                  marginHorizontal: 5,
                  fontSize: 12,
                  fontFamily: Fonts.segoeui,
                  color: Colors.black,
                }}>
                {item.item?.user?.email}
              </Text>
            </View>
          </View>

          <View style={{}}>
            {item.item?.status === 1 ? (
              <View style={{justifyContent: 'space-between', flex: 1}}>
                <TouchableOpacity
                  style={{
                    height: 20,
                    width: 23,
                    resizeMode: 'contain',
                    alignSelf: 'flex-end',
                  }}>
                  {/* <Image
                    source={Images.more_icon}
                    style={{
                      height: 20,
                      width: 23,
                      resizeMode: 'contain',
                      alignSelf: 'flex-end',
                    }}
                  /> */}
                </TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={Images.clock}
                    style={{width: 9, height: 9, resizeMode: 'contain'}}
                  />
                  <Text
                    style={{
                      marginHorizontal: 5,
                      fontSize: 10,
                      fontFamily: Fonts.segoeui,
                      color: Colors.pinkishGrey,
                    }}>
                    {moment(item.item.created_at).format('YYYY-MM-DD')}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}>
                  <View
                    style={{
                      height: 5,
                      width: 5,
                      borderRadius: 10,
                      backgroundColor: Colors.dustyOrange,
                      marginTop: 3,
                    }}
                  />
                  <Text
                    style={{
                      marginHorizontal: 5,
                      fontSize: 12,
                      fontFamily: Fonts.segoeui,
                      color: Colors.dustyOrange,
                    }}>
                    {translate("RequestSent")}
                  </Text>
                </View>
              </View>
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}>
                  <TouchableOpacity onPress={()=>{this.onChats(item?.item)}}>
                    <Image
                      source={Images.owner_chat}
                      style={{
                        width: 31,
                        height: 30,
                        resizeMode: 'contain',
                        marginRight: 16,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.callTenant(item.item?.user?.mobile_number)
                    }>
                    <Image
                      source={Images.owner_call}
                      style={{width: 31, height: 30, resizeMode: 'contain'}}
                    />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    marginTop: 5,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                  }}>
                  <View
                    style={{
                      height: 5,
                      width: 5,
                      borderRadius: 10,
                      backgroundColor: Colors.darkSeafoamGreen,
                      marginTop: 3,
                    }}
                  />
                  <Text
                    style={{
                      marginHorizontal: 5,
                      fontSize: 12,
                      fontFamily: Fonts.segoeui,
                      color: Colors.darkSeafoamGreen,
                    }}>
                    {translate("Connected")}
                  </Text>
                </View>
              </View>
            )}
          </View>
        </View>

        <View style={styles.flatListUserAddress}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <ProgressiveImage
                source={{
                  uri: item.item?.property?.propertyphotos
                    ? item.item?.property?.propertyphotos[0]?.imgurl
                    : '',
                }}
                style={{
                  width: 40,
                  height: 34,
                  borderRadius: 6,
                  resizeMode: 'contain',
                }}
                resizeMode="cover"
              />
              {/* <Image source={Images.apartmentin_house} style={{ width: 40, height: 34, borderRadius: 6, resizeMode: 'contain' }} /> */}
            </View>
            <View style={{marginHorizontal: 10}}>
              <Text>{item.item?.property?.title}</Text>
              <View
                style={{
                  marginTop: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  width: '90%',
                }}>
                <Image
                  source={Images.location}
                  style={{width: 7, height: 9, resizeMode: 'center'}}
                />
                <Text
                  numberOfLines={1}
                  style={{
                    marginHorizontal: 5,
                    fontSize: 12,
                    fontFamily: Fonts.segoeui,
                    color: Colors.brownishGrey,
                  }}>
                  {item.item?.property?.location}
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            height: 1,
            backgroundColor: Colors.warmGrey,
            opacity: 0.2,
            marginVertical: 10,
          }}></View>
      </View>
    );
  };

  searchTenant(text) {
    this.setState({search: text}, () => {
      this.getTenantListData();
    });
  }

  render() {
    return (
      <View style={styles.Container}>
        <View style={{height: 70, backgroundColor: Colors.lightMint}}>
          <View
            style={{justifyContent: 'center', alignItems: 'center', top: -20}}>
            <Inputs
              width={'87%'}
              backgroundcolor={Colors.whiteTwo}
              color={Colors.brownishGrey}
              rightimage={Images.search_icon}
              placeholder={translate("SearchbyTenantName")}
              keyboard={'default'}
              onChangeText={(text) => {
                this.searchTenant(text);
              }}
              value={this.state.search}
              returnKeyType={'done'}
              setfocus={(input) => {
                this.search = input;
              }}
              getfocus={() => {
                this.search.focus();
              }}
            />
          </View>
        </View>
        <FlatList
          data={this.state.tenantListData}
          renderItem={this.renderUserData}
          keyExtractor={(item, index) => item + index}
          extraData={this.state}
          ItemSeparatorComponent={() => <View style={styles.seperator}></View>}
          ListFooterComponent={() => {
            return this.state.isLoading ? <LoaderForList /> : null;
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          keyboardShouldPersistTaps={'handled'}
          refreshing={this.state.refreshing}
          ListEmptyComponent={() => (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: Fonts.segoeui,
                  color: Colors.cherryRed,
                  textAlign: 'center',
                }}>
                {this.state.emptymessage == false ? null : 'No Tenant.'}
              </Text>
            </View>
          )}
        />
        <TouchableOpacity
          onPress={() =>
            handleNavigation({
              type: 'push',
              page: 'AddExitingTenant',
              navigation: this.props.navigation,
            })
          }>
          <View style={{backgroundColor: Colors.whiteFive, height: 1}}></View>
          <View style={styles.submitButton}>
            <Image
              source={Images.floting_plus}
              style={{
                height: 30,
                width: 30,
                resizeMode: 'contain',
                marginRight: 10,
              }}
            />
            <Text style={styles.buttonText}>{translate("AddExistingTenant")}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: 'white'
    },
    userNameLabel: {
        fontSize: 16,
        fontFamily: Fonts.segui_semiBold,
        color: Colors.black
    },
    submitButton: {
        height: 55,
        justifyContent: 'center',
        alignItems: "center",
        // borderWidth: 1,
        flexDirection: 'row'
    },
    buttonText: {
        fontSize: 18,
        fontFamily: Fonts.segui_semiBold,
        color: Colors.darkSeafoamGreen
    },
    flatListMainView: {
        marginHorizontal: 10, flex: 1, marginVertical: Helper.hasNotch ? 6 : 0, top: 7
    },
    flatUserInfoView: {
        flexDirection: 'row', alignItems: 'center'
    },
    flatListUserPic: {
        height: 60, width: 60, borderRadius: 60 / 2
    },
    flatListUserAddress: {
        backgroundColor: "#fdfdfd", padding: 10, borderStyle: 'dotted', borderColor: Colors.pinkishGrey, borderWidth: 1, borderRadius: 2, marginTop: 15, flexDirection: 'row',
    }
})