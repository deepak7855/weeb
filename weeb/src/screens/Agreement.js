import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  TouchableHighlight,
  ScrollView,
  TextInput,
} from 'react-native';
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import {Helper, Network, AlertMsg, downloadFile} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import {ProgressiveImage} from '../Components/Common/index';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {translate} from '../Language'
export default class Agreement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataOfAreement: [],
      dataOfFilter: [
        {
          iconChecked: images.filter_check,
          text: 'Full House',
          selected: true,
        },
        {
          iconChecked: images.filter_check,
          text: 'Apartment in House ',
          selected: true,
        },
        {iconChecked: images.filter_check, text: 'Chalet', selected: true},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in Building',
          selected: true,
        },
      ],
      dataOfDownloadAgreement: [
        {
          icon: images.property_pdf,
          text1: 'Agreement part -1',
          text2: '15 Jan 2021 | 10:15 pm',
          iconDownload: images.download_arrow,
        },
        {
          icon: images.jpg_doc,
          text1: 'Agreement part -2',
          text2: '15 Jan 2021 | 10:15 pm',
          iconDownload: images.download_arrow,
        },
      ],
      selectorOfAppartment: '',
      openerOfDetails: false,
      opener: false,
      checkInd: -1,
      currentPage: 1,
      emptymessage: false,
      propertyType: '',
      propertyName: '',
      isLoading: false,
      refreshing: false,
      next_page_url: '',
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.getRentedProperty();
    });
    // this.getRentedProperty();
  }

  getRentedProperty() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let data = {};
          ApiCall.ApiMethod({
            Url: 'tenant-rented-properties' + '?page=' + this.state.currentPage,
            method: 'POST',
            data: {
              type: this.state.propertyType,
              name: this.state.propertyName,
              //   lat: this.state.lat,
              //   lng: this.state.long,
            },
          })
            .then((res) => {
              //   console.log(
              //     'get tenant property list',
              //     res?.data?.data
              //   );
              //   Helper.mainApp.hideLoader();
              //   // Helper.showToast(res?.message)
              if (res?.status) {
                this.setState({
                  dataOfAreement: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  openAppartment = (item) => {
    this.setState({selectorOfAppartment: item.id});
  };
  openDetails = () => {
    this.setState({openerOfDetails: !this.state.openerOfDetails});
  };
  openFilter = () => {
    this.setState({opener: !this.state.opener});
  };

  downloadDoc(item, index) {
    console.log('download item', item, index);
    downloadFile(item);
  }

  renderOfPropertyDetails = ({item, index}) => {
  //  console.log('agreements items', item);
    return (
      <View style={styles.viewProDetail}>
        <Image
          source={
            item.split('.').pop() == 'jpg'
              ? images.agreement_jpg
              : images.agreement_pdf
          }
          style={styles.iconPDF}
        />
        {/* <Image style={styles.iconPDF} source={item.icon} /> */}
        <View style={styles.view6}>
          <Text style={styles.textpart1}>{`Agreement${index}`}</Text>
          {/* <Text style={styles.textpart2}>{item.text2}</Text> */}
        </View>
        <TouchableOpacity onPress={() => this.downloadDoc(item, index)}>
          <Image style={styles.iconDownloads} source={images.download_arrow} />
        </TouchableOpacity>
      </View>
    );
  };
  renderOfFilter = ({item, index}) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.selectItemGroup(index)}
          style={styles.ViewRenderFilter}>
          <Image
            style={
              this.state.checkInd == index
                ? styles.checkIcon
                : styles.uncheckIcon
            }
            source={item.iconChecked}
          />
          <Text
            style={
              this.state.checkInd == index
                ? styles.selectedText
                : styles.unselectText
            }>
            {item.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  renderOfProperty = ({item}) => {
    //  console.log('property details', item?.property?.agreements[0]?.media_url);
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.openAppartment(item);
            this.openDetails(item);
          }}
          style={
            this.state.selectorOfAppartment == item.id
              ? styles.view11
              : styles.view1
          }>
          <ProgressiveImage
            source={{
              uri: item?.property?.propertyphotos
                ? item?.property?.propertyphotos[0]?.imgurl
                : '',
            }}
            style={styles.iconHome}
            resizeMode="cover"
          />
          {/* <Image style={styles.iconHome} source={item.icon} /> */}
          <View style={{marginLeft: 13}}>
            <Text style={styles.textTitle}>{item?.property?.title}</Text>
            <View style={styles.view3}>
              <Image style={styles.iconLocation} source={images.location} />
              <Text style={styles.textAddress}>{item.property?.location}</Text>
            </View>
            <View style={styles.view4}>
              <Text style={styles.textRate}>{item.rent}</Text>
              <Text style={[styles.textRate, {fontFamily: Fonts.segoeui}]}>
                /m
              </Text>
              <View style={styles.dot} />
              <Text style={styles.textPropType}>{item?.property?.type}</Text>
              {this.state.selectorOfAppartment == item.id &&
              this.state.openerOfDetails ? (
                <Image style={styles.iconMore} source={images.more_hr_icon} />
              ) : null}
            </View>
          </View>
        </TouchableOpacity>
        {this.state.selectorOfAppartment == item.id &&
        this.state.openerOfDetails ? (
          <View style={styles.view5}>
            <Image style={styles.iconDown} source={images.up_triangle} />
            <FlatList
              data={item?.property?.agreements[0]?.media_url}
              renderItem={this.renderOfPropertyDetails}
              ItemSeparatorComponent={() => (
                <View style={styles.seperatorOfFilter} />
              )}
            />
          </View>
        ) : null}
      </View>
    );
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getRentedProperty();
      });
    }
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {
          currentPage: 1,
          refreshing: false,
          propertyType: '',
          propertyName: '',
        },
        () => {
          this.getRentedProperty();
        },
      );
    }, 2000);
  };

  searchProperty(val) {
    this.setState(
      {
        propertyName: val,
      },
      () => {
        this.getRentedProperty();
      },
    );
  }

  selectItemGroup(value) {
    if (this.state.checkInd == value) {
      this.setState({checkInd: -1});
      // DeviceEventEmitter.emit('Listed-Property-Filter', { type: '' });
      this.setState({opener: false});
    } else {
      this.setState({checkInd: value});
      // DeviceEventEmitter.emit('Listed-Property-Filter', { type: this.state.dataOfFilter[value]?.text });
      this.setState(
        {
          opener: false,
          propertyType: this.state.dataOfFilter[value]?.text,
        },
        () => {
          this.getRentedProperty();
        },
      );
    }
  }

  render() {
    //console.log('state', this.state.dataofAreement);
    return (
      <View style={styles.container}>
        <View style={styles.viewTop}>
          <Image style={styles.searchIcon} source={images.search_icon} />
          <TextInput
            placeholder={'Search Property'}
            placeholderTextColor={Colors.pinkishGrey}
            style={styles.textInputStyl}
            onChangeText={(val) => this.searchProperty(val)}
          />
        </View>
        <View style={styles.view2}>
          <Text style={styles.textYouHave}>
            {translate("Youhave")}{' '}
            <Text style={styles.textRed}>
              {this.state.dataOfAreement ? this.state.dataOfAreement.length : 0}{' '}
              {translate("rentproperty")}
            </Text>
          </Text>
        </View>
        <KeyboardAwareScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          containerContentStyle={{flexGrow: 1}}>
          <View>
            <FlatList
              data={this.state.dataOfAreement}
              renderItem={this.renderOfProperty}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.onRefresh()}
                />
              }
              extraData={this.state}
              ItemSeparatorComponent={() => (
                <View style={styles.seperator}></View>
              )}
              ListFooterComponent={() => {
                return this.state.isLoading ? <LoaderForList /> : null;
              }}
              onEndReached={this.onScroll}
              onEndReachedThreshold={0.5}
              keyboardShouldPersistTaps={'handled'}
              refreshing={this.state.refreshing}
              ListEmptyComponent={() => (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: Fonts.segoeui,
                      color: Colors.cherryRed,
                      textAlign: 'center',
                    }}>
                    {this.state.emptymessage == false ? null : 'No Property.'}
                  </Text>
                </View>
              )}
            />
          </View>
        </KeyboardAwareScrollView>
        <TouchableOpacity
          onPress={() => this.openFilter()}
          style={styles.filterTouch}>
          <Image style={styles.iconFilter} source={images.property_filter} />
        </TouchableOpacity>
        {this.state.opener ? (
          <View style={styles.filteView}>
            <View
              style={[
                styles.viewTopTextOfFilter,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.textPropTypeOfFilter}>{translate("PropertyType")}</Text>
              <TouchableOpacity
                style={{height: 25, width: 25, alignItems: 'center'}}
                onPress={() => {
                  this.openFilter();
                }}>
                <Image
                  style={{
                    height: 12,
                    width: 12,
                    resizeMode: 'contain',
                    marginTop: 7,
                  }}
                  source={images.close}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              data={this.state.dataOfFilter}
              renderItem={this.renderOfFilter}
              ItemSeparatorComponent={() => (
                <View style={styles.filterSeperator} />
              )}
            />
          </View>
        ) : null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 10 },
    viewTop: { flexDirection: 'row', marginTop: 17, borderWidth: 1, borderRadius: 4, marginHorizontal: 11, alignItems: 'center', paddingHorizontal: 13, borderColor: Colors.whiteFive },
    searchIcon: { width: 11, height: 11, resizeMode: 'contain' },
    textInputStyl: {fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40},
    iconMore: { width:10, height: 4, resizeMode: 'contain' ,position :'absolute',right:10},
    view2: { marginLeft: 11, paddingTop: 12, },
    textYouHave: { fontSize: 14, lineHeight: 25, color: Colors.blackTwo, fontFamily: Fonts.segoeui },
    textRed: { fontSize: 14, lineHeight: 25, color: Colors.cherryRed, fontFamily: Fonts.segoeui },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold },
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { fontSize: 12, lineHeight: 16,color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 ,width:'85%'},
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, lineHeight: 16, color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui_bold },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, lineHeight: 16, color: Colors.marigold, fontFamily: Fonts.segoeui_bold, },
    iconDown:{ width: 35, height: 20, resizeMode: 'contain', right: 40, top: -15, position: 'absolute'},
    view5: { marginHorizontal: 11, paddingHorizontal: 15, borderWidth: 1, borderColor: Colors.whiteFive, borderRadius: 4, top: -5 },
    seperatorOfFilter: { height:0.5, backgroundColor: Colors.warmGrey, opacity: 0.3 },
    filterTouch: { position: 'absolute', bottom: 21, right: 13 },
    iconFilter: { width: 56, height: 56, resizeMode: 'contain' },
    filteView: { position: 'absolute', bottom: 90, right: 12, backgroundColor: Colors.whiteTwo, elevation: 5, width: 200, borderRadius: 10 },
    viewTopTextOfFilter: { paddingVertical: 10, paddingHorizontal: 20, backgroundColor: Colors.whiteNine, borderBottomColor: Colors.whiteFive, borderBottomWidth: 1 },
    textPropTypeOfFilter: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    filterSeperator: { borderWidth: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.05 },
    ViewRenderFilter: { flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 23, backgroundColor: Colors.whiteTwo, alignItems: 'center' },
    viewProDetail: { flexDirection: 'row', paddingVertical: 15, alignItems: 'center' },
    iconPDF: { width: 30, height: 30, resizeMode: 'contain' },
    view6: { marginLeft: 12, flex: 0.95 },
    textpart1: { fontSize: 12, lineHeight: 15, fontFamily: Fonts.segoeui },
    textpart2: { fontSize: 8, lineHeight: 25, color: Colors.pinkishGrey, fontFamily: Fonts.segoeui },
    iconDownloads: { width: 17, height: 17, resizeMode: 'contain' },
    checkIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.darkSeafoamGreen },
    uncheckIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.pinkishGrey },
    unselectText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.pinkishGrey },
    selectedText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.black },
    view1: { flexDirection: 'row', marginHorizontal: 11, marginVertical: 7, borderWidth: 1, borderRadius: 4, borderColor: Colors.whiteFive, paddingLeft: 15, paddingVertical: 15, },
    view11: { flexDirection: 'row', marginHorizontal: 11, marginVertical: 7, borderWidth: 1, borderRadius: 4, borderColor: Colors.whiteFive, paddingLeft: 15, paddingVertical: 15, backgroundColor: Colors.lightMint }
})