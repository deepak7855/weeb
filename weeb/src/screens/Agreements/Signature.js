import React, { Component, useRef } from 'react';
import { View, Image, StyleSheet, TouchableOpacity, Modal } from 'react-native';
import Colors from '../../Lib/Colors';
import { screenDimensions } from '../../Lib/Constant';
import images from '../../Lib/Images';
import SignatureScreen from 'react-native-signature-canvas';
import { WebView } from "react-native-webview"
import { handleNavigation } from '../../navigation/routes';
import RNFetchBlob from 'rn-fetch-blob';
const Signature = ({ visible, onCallBack }) => {
    const ref = useRef();

    const handleSignature = signature => {
        //console.log(signature);
        var Base64Code = signature.split('data:image/png;base64,'); //base64Image is my image base64 string

        // const dirs = RNFetchBlob.fs.dirs;

        // var path = dirs.DCIMDir + '/image.png';
        // RNFetchBlob.fs
        //   .writeFile(path, RNFetchBlob.base64.encode(Base64Code[1]), 'base64')
        //   .then((res) => {
        //       console.log('File : ', res);
        //   });
      onCallBack(signature, Base64Code[1]);
    // props.navigation.goBack()
    };

    const handleEmpty = () => {
        console.log('Empty');
    }

    const handleClear = () => {
        console.log('clear success!');
    }

    const handleEnd = () => {
        // ref.current.readSignature();
    }

    const resetSign = () => { }
    const goBack = () => { }

    return (
        <Modal
            visible={visible}
            animationType={'slide'}
            onRequestClose={() => onCallBack(false)}
        >
            <View style={styles.container}>
                <SignatureScreen
                    ref={ref}
                    onEnd={handleEnd}
                    onOK={handleSignature}
                    onEmpty={handleEmpty}
                    onClear={handleClear}
                    autoClear={false}
                    descriptionText={''}
                    //imageType={'image/jpeg'}
                />
            </View>
        </Modal>
    );
}

export default Signature

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
    signature: { width: screenDimensions.WindowWidth, height: screenDimensions.WindowHeight },
    icon_Refresh: { height: 40, width: 40, resizeMode: 'contain' },
    buttonStyle: { marginHorizontal: 20 }

});
