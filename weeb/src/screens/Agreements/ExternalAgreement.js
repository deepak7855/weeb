import React from 'react';
import {SafeAreaView,View,Text,TouchableOpacity,StyleSheet} from 'react-native';
import images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import {remove} from 'lodash';
import {handleNavigation} from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import { translate } from '../../Language';

export default class DigitalAgreement extends React.Component {

    constructor(props) {
        super(props)
        this.state={

        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => {
              this.goBack();
            },
            title: 'Add Agreement',
            rightHide: true,
          });
    }
    render(){
        return(
        <SafeAreaView style={styles.container}>
                <Text>{translate("ExtrnalAGremeent")}</Text>
        </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    }
})