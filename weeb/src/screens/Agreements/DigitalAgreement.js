import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import {Inputs, Picker, LargeTextInput} from '../../Components/Common/index';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Helper, Network, AlertMsg, downloadFile} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {ConsoleLog} from '../../Components/Utils';
import {Constant} from '../../Lib/Constant';
import { translate } from '../../Language';
export default class DigitalAgreement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      houseData: [
        {
          label: 'Full House',
          value: 'Full House',
        },
        {
          label: 'Apartment in House ',
          value: 'Apartment in House ',
        },
        {
          label: 'Chalet',
          value: 'Chalet',
        },
        {
          label: 'Apartment in Building',
          value: 'Apartment in Building',
        },
      ],
      propertyType: '',
      NumberOfRooms: '',
      NumberOfBalcony: '',
      propertyAddress: '',
      ownerName: '',
      ownerAddress: '',
      tenantName: '',
      tenantAddress: '',
    };
  }
  renderTitle = (bold, number, label) => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text
          style={{
            fontSize: 14,
            color: Colors.brownishGrey,
            fontFamily: Fonts.segoeui_bold,
            marginHorizontal: 12,
            marginTop: 10,
          }}>
          {number}
        </Text>
        <Text
          style={{
            fontSize: bold ? 16 : 14,
            color: Colors.blackThree,
            fontFamily: bold ? Fonts.segoeui_bold : Fonts.segoeui,
            marginHorizontal: 8,
            marginTop: 10,
          }}>
          {label}
        </Text>
      </View>
    );
  };

  checkValidation = () =>{
    if(!this.state.propertyType){
      Helper.showToast(translate('Pleaseselectpropertytype'))
      return false
    }
    if(!this.state.NumberOfRooms){
      Helper.showToast(translate('Pleaseentertotalrooms'))
      return false
    }
    if(!this.state.NumberOfBalcony){
      Helper.showToast(translate('Pleaseentertotalbalcony'))
      return false
    }
   
    
    if(!this.state.propertyAddress){
      Helper.showToast('Please enter property address')
      return false
    }
    if(!this.state.ownerName){
      Helper.showToast('Please enter owner name')
      return false
    }
    if(!this.state.ownerAddress){
      Helper.showToast('Please enter owner address')
      return false
    }
    if(!this.state.tenantName){
      Helper.showToast('Please enter tenant name')
      return false
    }
    if(!this.state.tenantAddress){
      Helper.showToast('Please enter tenant address')
      return false
    }

    this.createPDF()
  }

  createPDF = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        const data = {
          property_type: this.state.propertyType,
          rooms: this.state.NumberOfRooms,
          balcony: this.state.NumberOfBalcony,
          property_address: this.state.propertyAddress,
          owner_name: this.state.ownerName,
          owner_address: this.state.ownerAddress,
          tenant_name: this.state.tenantName,
          tenant_address: this.state.tenantAddress,
        };
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({Url: 'generate-pdf', method: 'POST', data: data}).then((res)=>{
          Helper.mainApp.hideLoader();
              console.log('pdf genrate res',res);
              Helper.showToast(res?.message)
              if(res?.status){
                DeviceEventEmitter.emit('DigitalAgreementPdfUrl', res?.data?.pdf);
                this.props.navigation.goBack()
              }
        }).catch((err)=>{
          Helper.mainApp.hideLoader();
          console.log('api err',err)
        })
      }
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView>
          {this.renderTitle(true, '01', 'Fill Property Details')}
          <Picker
            selectedPickerValue={(val) => {
              this.setState({propertyType: val});
            }}
            labels={'Property Type'}
            // width={'30%'}
            data={this.state.houseData}
            labelfontfamily={Fonts.segoeui}
            labelsize={12}
            backgroundcolor={'white'}
            color={Colors.black}
            leftviewWidth={'100%'}
            leftimage={Images.drop_arrow}
            placeholder={'Select Property Type'}
          />

          <Inputs
            labels={'Number of Rooms'}
            width={'95%'}
            labelfontfamily={Fonts.segoeui}
            labelsize={12}
            backgroundcolor={'white'}
            color={Colors.black}
            placeholder={'Enter Number of Rooms'}
            placeholderTextColor={Colors.brownishGrey}
            bluronsubmit={false}
            onChangeText={(text) => {
              this.setState({NumberOfRooms: text});
            }}
            keyboard={'number-pad'}
          />

          <Inputs
            labels={'Number of Balcony'}
            width={'95%'}
            labelfontfamily={Fonts.segoeui}
            labelsize={12}
            backgroundcolor={'white'}
            color={Colors.black}
            placeholder={'Enter Number of Balcony'}
            placeholderTextColor={Colors.brownishGrey}
            bluronsubmit={false}
            onChangeText={(text) => {
              this.setState({NumberOfBalcony: text});
            }}
            keyboard={'number-pad'}
          />

          <LargeTextInput
            fontsize={12}
            fontfamily={Fonts.segoeui}
            height={85}
            maxlength={500}
            discription={'Address'}
            placeholder={'Enter Address'}
            maxlengthtext={true}
            changetext={(text) => {
              this.setState({propertyAddress: text});
            }}
          />

          {this.renderTitle(true, '02', 'Fill Owner Details')}

          <Inputs
            labels={'Owner Name'}
            width={'95%'}
            labelfontfamily={Fonts.segoeui}
            labelsize={12}
            backgroundcolor={'white'}
            color={Colors.black}
            placeholder={'Enter Owner Name'}
            placeholderTextColor={Colors.brownishGrey}
            bluronsubmit={true}
            onChangeText={(text) => {
              this.setState({ownerName: text});
            }}
          />

          <LargeTextInput
            fontsize={12}
            fontfamily={Fonts.segoeui}
            height={85}
            maxlength={500}
            discription={'Address'}
            placeholder={'Address'}
            maxlengthtext={true}
            changetext={(text) => {
              this.setState({ownerAddress: text});
            }}
          />

          {this.renderTitle(true, '02', 'Fill Tenant Details')}

          <Inputs
            labels={'Tenant Name'}
            width={'95%'}
            labelfontfamily={Fonts.segoeui}
            labelsize={12}
            backgroundcolor={'white'}
            color={Colors.black}
            placeholder={'Enter Tenant Name'}
            placeholderTextColor={Colors.brownishGrey}
            bluronsubmit={true}
            onChangeText={(text) => {
              this.setState({tenantName: text});
            }}
          />

          <LargeTextInput
            fontsize={12}
            fontfamily={Fonts.segoeui}
            height={85}
            maxlength={500}
            discription={'Address'}
            placeholder={'Address'}
            maxlengthtext={true}
            changetext={(text) => {
              this.setState({tenantAddress: text});
            }}
          />
        </KeyboardAwareScrollView>
        <TouchableOpacity
          onPress={() => this.checkValidation()}
          style={styles.submitButton}>
          <Text style={styles.buttonText}>{translate("Submit")}</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginHorizontal: 10,
  },
  submitButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 1,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.darkSeafoamGreen,
  },
});
