import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  DeviceEventEmitter,
  FlatList,
  Alert,
  TouchableOpacityBase,
  Modal,
} from 'react-native';
import images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import {screenDimensions} from '../../Lib/Constant';
import CameraController from '../../Lib/CameraController';
import {remove} from 'lodash';
import {handleNavigation} from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import {ProgressiveImage} from '../../Components/Common/index';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import Signature from './Signature';
import DocumentPicker from 'react-native-document-picker';
import {Helper, Network, AlertMsg, downloadFile} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {ConsoleLog} from '../../Components/Utils';
import {Constant} from '../../Lib/Constant';
import { translate } from '../../Language';
let agreementFileArray = [];

export default class AddAgreement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      render: false,
      addAgreementState: 0,
      signature: false,
      showUploadList: false,
      visible: false,
      selectSignature: images.signature_box,
      propertyData: props.route.params?.propertyData,
      userSelectedDoc: [],
      userSignature: '',
      viewAgreement: props.route.params?.view,
      propertyAgreement: '',
      userSelectedDocApi: [],
      deleteDocId: [],
      signatureUrl: '',
      userType: Helper.userType == 'OWNER' ? true : null,
      totalAgreement: 0,
      modalVisible: false,
      digitalAgreement: false,
      externalAgreement: false,
      pdfUrl: props.route.params?.pdfUrl,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Add Agreement',
      rightHide: true,
    });
  }

  componentDidMount() {
    
    if (this.state.viewAgreement) {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          Helper.mainApp.showLoader();
          //console.log('add agreement data', data);
          ApiCall.ApiMethod({
            Url: 'get-property-agreement',
            method: 'POST',
            data: {property_id: this.state.propertyData?.property_id},
          })
            .then((res) => {
              console.log(
                `view agreement res ${this.state.userType}`,
                res,
                this.state.propertyData?.property_id,
              );
              // res?.data?.map((item) => {
              //   console.log('media',item)
              // })
              Helper.mainApp.hideLoader();
              //Helper.showToast(res?.message);
              if (res?.status) {
                this.setState(
                  {
                    propertyAgreement: res?.data,
                    totalAgreement: res?.data?.length,
                  },
                  () => {
                    this.setState({
                      signatureUrl: this.state.propertyAgreement[0]
                        ?.signature_url,
                    });
                  },
                );
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
      });
    }

    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        this.setState({
          userType: data?.type == 'OWNER' ? true : null,
        });
      },
    );
    this.listner2 = DeviceEventEmitter.addListener(
      'DigitalAgreementPdfUrl',
      (data) => {
        console.log('pdf url', data);
        this.state.userSelectedDoc.push(data);
        agreementFileArray.push({
          id: this.state.addAgreementState + 1,
          type: 'pdf',
        });
        this.setState({
          addAgreementState: this.state.addAgreementState + 1,
          pdfUrl:data
        });
        return true
      },
    );
  }

  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
    }
    this.listner2.remove();
  }

  goBack = () => {
    agreementFileArray = [];
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  goSignature() {
    handleNavigation({
      type: 'push',
      page: 'Signature',
      navigation: this.props.navigation,
    });
  }

  handleSignature = () => {
    this.setState({signature: true});
  };
  saveSign() {
    // this.refs["sign"].saveImage();
    this.props.navigation.navigate('DigitalAgreement');
  }

  resetSign() {
    this.refs['sign'].resetImage();
  }
  Item = ({title}) => (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
  AddAgreementFunction = () => {
    let y = Math.random();
    let type = '';
    if (y < 0.5) y = 0;
    else y = 1;
    if (y === 1) {
      type = 'pdf';
    } else {
      type = 'pic';
    }
    agreementFileArray.push({id: this.state.addAgreementState + 1, type: type});
    this.setState({
      addAgreementState: this.state.addAgreementState + 1,
    });
  };

  openGalleryPicker = async () => {
    Alert.alert(
      'Weeb',
      'Please Select File',
      [
        {
          text: 'Pdf',
          onPress: () => this.openDocumentPicker(),
        },
        {
          text: 'Media',
          onPress: () => this.changeImage(),
        },
      ],
      {cancelable: true},
    );
  };
  changeImage = async (type) => {
    await CameraController.open((response) => {
      console.log(response.path)
      if (response.path) {
        this.state.userSelectedDoc.push(response?.path);
        agreementFileArray.push({
          id: this.state.addAgreementState + 1,
          type: 'pic',
        });
        this.setState({
          addAgreementState: this.state.addAgreementState + 1,
        });
      }
    });
  };
  openDocumentPicker = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      console.log('exteenal pdf url', res?.uri);
      this.state.userSelectedDoc.push(res?.uri);
      agreementFileArray.push({
        id: this.state.addAgreementState + 1,
        type: 'pdf',
      });
      this.setState({
        addAgreementState: this.state.addAgreementState + 1,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };
  deleteDoc = (docid, indexs) => {
    //console.log('array of doc', this.state.userSelectedDoc[indexs]);
    const userdoc = [...this.state.userSelectedDoc];
    const index = agreementFileArray.indexOf(docid);
    remove(agreementFileArray, function (n) {
      return n.id === docid;
    });
    remove(this.state.userSelectedDoc, function (n) {
      return n === userdoc[indexs];
    });
    this.setState(
      {
        render: !this.state.render,
      },
      () => {
        // console.log(
        //   'after state update',
        //   this.state.userSelectedDoc,
        //   agreementFileArray,
        // );
      },
    );
  };

  onCallBackAction = (res, sigpath) => {
    //console.log('onCallBackAction  res', res );
    //console.log('signpath', sigpath);
    this.setState({
      visible: false,
      selectSignature: res ? {uri: res} : this.state.selectSignature,
      userSignature: res,
    });
  };

  renderItem = ({item, index}) => {
    // console.log('index', this.state.userSelectedDoc[index]);
    return (
      <View style={{marginHorizontal: 7, marginTop: 48}}>
        <View
          style={{
            height: 100,
            width: 100,
            borderColor: Colors.whiteFive,
            borderWidth: 1,
            justifyContent: 'center',
            borderRadius: 4,
            alignItems: 'center',
          }}>
          <View style={{margin: 10}}>
            <Image
              source={
                item.type === 'pic'
                  ? this.state.userSelectedDoc[index]
                    ? {uri: this.state.userSelectedDoc[index]}
                    : images.agreement_jpg
                  : images.agreement_pdf
              }
              style={{width: 46, height: 46, resizeMode: 'contain'}}
            />
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: Colors.whiteFive,
              height: 1,
            }}></View>
          <TouchableOpacity
            onPress={() => this.deleteDoc(item.id, index)}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 110,
              height: 20,
            }}>
            <Image
              source={images.delete_id}
              style={{width: 12, height: 12, resizeMode: 'contain'}}
            />
          </TouchableOpacity>
        </View>
        {/* <Text
          style={[
            styles.AddAgreementTxt,
            {marginTop: 5, color: Colors.pinkishGrey},
          ]}>
          {'15 Jan 2021 | 10:15 pm'}
        </Text> */}
      </View>
    );
  };
  downloadDoc(item, index) {
    console.log('download item', item?.media_url[0], index);
    downloadFile(item?.media_url[0]);
  }

  deleteUploadedAgreements(items) {
    console.log('delete item', this.state.propertyAgreement);
    this.state.propertyAgreement.map((itemm, index) => {
      console.log('property item', itemm?.property_media, index);
      itemm?.property_media.map((item, index) => {
        //console.log('item', item)
        if (items?.id == item?.id) {
          this.setState(
            {
              deleteDocId: item?.id,
            },
            () => {
              for (var i = 0; i < this.state.propertyAgreement.length; i++) {
                if (
                  this.state.propertyAgreement[i]?.property_media[0]?.id ===
                  item?.id
                ) {
                  this.state.propertyAgreement.splice(i, 1);
                }
                console.log(
                  'this.state.propertyagreements',
                  this.state.propertyAgreement,
                );
                this.setState(
                  {
                    render: !this.state.render,
                    // totalAgreement:this.state.propertyAgreement.length,
                  },
                  () => {
                    // console.log(
                    //   'after state update',
                    //   this.state.userSelectedDoc,
                    //   agreementFileArray,
                    // );
                  },
                );
              }
            },
          );
        }
      });
    });
  }
  downloadRenderItem = ({item, index}) => {
    console.log(item?.property_media[0]?.media_url[0])
    return (
      <View style={{flexDirection: 'row'}}>
        {item?.property_media.length > 0
          ? item?.property_media.map((item) => {
               console.log('itemmmmm', item);
              return (
                <View style={{marginHorizontal: 7, marginTop: 48}}>
                  <View
                    style={{
                      height: 100,
                      width: 100,
                      borderColor: Colors.whiteFive,
                      borderWidth: 1,
                      justifyContent: 'center',
                      borderRadius: 4,
                      alignItems: 'center',
                    }}>
                    <View style={{margin: 10}}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity
                          hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                          onPress={() =>
                            Alert.alert(
                              'Weeb',
                              'Are you sure you want to remove agreement?',
                              [
                                {
                                  text: 'Yes',
                                  onPress: () =>
                                    this.deleteUploadedAgreements(item),
                                },
                                {
                                  text: 'No',
                                  //onPress: () => this.changeImage(),
                                },
                              ],
                              {cancelable: true},
                            )
                          }>
                          <Image
                            source={images.close}
                            style={{
                              width: 7,
                              height: 7,
                              resizeMode: 'contain',
                              top: 3,
                              left: 40,
                            }}
                          />
                        </TouchableOpacity>
                        <ProgressiveImage
              source={
                item?.media_url[0].split('.').pop() == 'jpg'
                  ?item?.media_url.length >0 ?{uri:item?.media_url[0]} :images.agreement_jpg
                  : images.agreement_pdf
              }
              style={{width: 46, height: 46, resizeMode: 'contain'}}
              resizeMode="cover"
            />
                        <View
                          style={{
                            width: '100%',
                            backgroundColor: Colors.whiteFive,
                            height: 1,
                          }}></View>
                        <TouchableOpacity
                          onPress={() => this.downloadDoc(item, index)}
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: 110,
                            height: 20,
                          }}>
                          <Image
                            source={images.download_arrow}
                            style={{
                              width: 12,
                              height: 12,
                              resizeMode: 'contain',
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })
          : null}
      </View>
    );
  };

  addAgreement() {
    if(!this.state.userSignature){
      Helper.showToast(translate('Pleaseuploadyoursignature'));
      return false;
    }
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        let data = new FormData();
        
        data.append('property_id', this.state.propertyData?.property_id);
        data.append('digital_signature', this.state.userSignature);
        data.append('media_count', this.state.userSelectedDoc.length);
        if (this.state.pdfUrl) {
          data.append('pdfurl', this.state.pdfUrl);
          // data.append(`media`, {
          //   uri: this.state.pdfUrl,
          //   name:
          //     agreementFileArray[index].type == 'pic' ||
          //     agreementFileArray[index].type == 'PIC'
          //       ? 'test.jpg'
          //       : 'test.pdf',
          //   type: 'image/jpg',
          // });
        } else {
          for (
            let index = 0;
            index < this.state.userSelectedDoc.length;
            index++
          ) {
            console.log('this.state.userSelectedDoc-----------', this.state.userSelectedDoc,agreementFileArray[index]);
            data.append(`media_${index + 1}`, {
              uri: this.state.userSelectedDoc[index],
              name:
                agreementFileArray[index].type == 'pic' ||
                agreementFileArray[index].type == 'PIC'
                  ? 'test.jpg'
                  : 'test.pdf',
              type: 'image/jpg',
            });
            data.append(
              `media_type_${index + 1}`,
              agreementFileArray[index].type == 'pic' ||
                agreementFileArray[index].type == 'PIC'
                ? 'jpg'
                : 'pdf',
            );
          }
        }

        Helper.mainApp.showLoader();
        console.log('add agreement data', data);
        ApiCall.ApiMethod({
          Url: 'add-property-agreement',
          method: 'POSTUPLOAD',
          data: data,
        })
          .then((res) => {
            console.log('add agreement res', res);
            Helper.mainApp.hideLoader();
            //Helper.showToast(res?.message);
            if (res?.status) {
              this.setState(
                {
                  userSelectedDoc: [],
                  userSignature: '',
                  propertyAgreement: '',
                  addAgreementState: 0,
                },
                () => {
                  (agreementFileArray = []), this.goBack();
                },
              );
            }
          })
          .catch((err) => {
            console.log(err);
            Helper.showToast(AlertMsg.error.ApiFail)
          });
      }else{
        Helper.showToast(AlertMsg.error.NETWORK)
      }
    }).catch((err)=>{
        console.log(err)
        Helper.showToast(AlertMsg.error.NETWORK)
    })
  }

  updateAgreement() {
    if( this.state.userSelectedDoc.length == 0){
      Helper.showToast( translate('Pleaseuploadagreement'));
      console.log('call')
      return false;
    }

    
    if (
      this.state.userSelectedDoc.length + this.state.propertyAgreement.length >
      this.state.totalAgreement
    ) {
      Helper.showToast(translate('Youcannotuploadmoreagreement'));
      console.log('call')
      return false;
    }
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        let data = new FormData();
        this.state.userSelectedDoc.map((item, index) => {
          data.append('property_media_id', this.state.deleteDocId);
        });

        if (this.state.pdfUrl) {
          //data.append('media_count',this.state.userSelectedDoc.length);
          data.append('pdfurl', this.state.pdfUrl);
          data.append(`media`, {
            uri: 'https://ibb.co/KjgXjgK',
            name:'test.jpg',
            type: 'image/jpg',
          });
        } else {
          for (
            let index = 0;
            index < this.state.userSelectedDoc.length;
            index++
          ) {
            data.append(`media`, {
              uri: this.state.userSelectedDoc[index],
              name:
                agreementFileArray[index].type == 'pic' ||
                agreementFileArray[index].type == 'PIC'
                  ? 'test.jpg'
                  : 'test.pdf',
              type: 'image/jpg',
            });
          }
        }
        Helper.mainApp.showLoader();
        console.log('add agreement data', data);
        ApiCall.ApiMethod({
          Url: 'update-property-agreement',
          method: 'POSTUPLOAD',
          data: data,
        })
          .then((res) => {
            console.log('update agreement res', res);
            Helper.mainApp.hideLoader();
            //Helper.showToast(res?.message);
            if (res?.status) {
              this.setState(
                {
                  userSelectedDoc: [],
                  userSignature: '',
                  propertyAgreement: '',
                  addAgreementState: 0,
                  deleteDocId: [],
                },
                () => {
                  (agreementFileArray = []), this.goBack();
                },
              );
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }else{
        Helper.showToast(AlertMsg.error.NETWORK)
      }
    }).catch((err)=>{
      Helper.showToast(AlertMsg.error.NETWORK)
console.log(err)
    })
  }

  renderButton = () => {
    return this.state.viewAgreement && this.state.propertyData ? (
      <View style={{flex: 1, flexDirection: 'row'}}>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => {
            this.goBack();
          }}>
          <Text style={[styles.button, {right: 20}]}>Cancel</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => {
            this.updateAgreement();
          }}>
          <Text
            style={this.state.signature ? styles.buttonGreen : styles.button}>
            Update
          </Text>
        </TouchableOpacity>
      </View>
    ) : (
      <>
        <View style={{marginBottom: 10}}>
          <Text style={styles.TabTxt}>Tap to box for signature</Text>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => {
              this.goBack();
            }}>
            <Text style={[styles.button, {right: 20}]}>Cancel</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => {
              this.addAgreement();
            }}>
            <Text
              style={this.state.signature ? styles.buttonGreen : styles.button}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
      </>
    );
  };

  selectAgreementType = () => {
    return (
      // <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: !this.state.modalVisible});
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Add Agreement</Text>
            <View style={{marginTop: 30, marginBottom: 30}}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: Fonts.segoeui,
                  color: Colors.black,
                }}>
                Select Agreement Type
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    digitalAgreement: !this.state.digitalAgreement,
                    externalAgreement: false,
                  })
                }>
                <Image
                  source={
                    this.state.digitalAgreement
                      ? images.check_charges
                      : images.uncheck_charges
                  }
                  resizeMode={'contain'}
                  style={{height: 25, width: 25}}
                />
              </TouchableOpacity>
              <View
                style={{
                  marginLeft: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: Fonts.segoeui,
                    color: Colors.warmGrey,
                  }}>
                  Digital Agreement
                </Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 30,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    externalAgreement: !this.state.externalAgreement,
                    digitalAgreement: false,
                  })
                }>
                <Image
                  source={
                    this.state.externalAgreement
                      ? images.check_charges
                      : images.uncheck_charges
                  }
                  resizeMode={'contain'}
                  style={{height: 25, width: 25}}
                />
              </TouchableOpacity>
              <View
                style={{
                  marginLeft: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: Fonts.segoeui,
                    color: Colors.warmGrey,
                  }}>
                  External Agreement
                </Text>
              </View>
            </View>

            <View
              style={{
                marginTop: 50,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={styles.buttonOpen}
                onPress={() =>
                  this.setState({modalVisible: !this.state.modalVisible})
                }>
                <Text
                  style={{
                    fontsize: 16,
                    fontFamily: Fonts.segoeui,
                    color: Colors.blackTwo,
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonClose}
                onPress={() => this.AddAgreementFunction()}>
                <Text
                  style={{
                    fontsize: 16,
                    fontFamily: Fonts.segoeui,
                    color: Colors.black,
                  }}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      // </View>
    );
  };

  AddAgreementFunction = () => {
    if (this.state.digitalAgreement) {
      this.setState({modalVisible: !this.state.modalVisible});
      this.props.navigation.navigate('DigitalAgreement');
      return;
    }
    if (this.state.externalAgreement) {
      this.setState({modalVisible: !this.state.modalVisible});
      this.openGalleryPicker();
      return;
    }
  };

  uploadAgreementModal = () =>{
    if (
      this.state.propertyAgreement.length == this.state.totalAgreement
    ) {
      Helper.showToast(translate('Youcannotuploadmoreagreement'));
      return false;
    }
    this.setState({modalVisible: true})
  }

  render() {
    return (
      <View style={styles.container}>
        {this.selectAgreementType()}
        <Signature
          visible={this.state.visible}
          onCallBack={this.onCallBackAction}
        />
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.mainview}>
            <ProgressiveImage
              source={{
                uri: this.state.propertyData?.property?.propertyphotos[0]
                  ?.imgurl
                  ? this.state.propertyData?.property?.propertyphotos[0]?.imgurl
                  : '',
              }}
              style={styles.Image}
              resizeMode="cover"
            />
            {/* <Image source={images.propert_requested} style={styles.Image} /> */}
            <View style={styles.TextViewCss}>
              <Text numberOfLines={1} style={styles.LighthouseTxt}>
                {this.state.propertyData?.property?.title}
              </Text>
              <View style={styles.locationCss}>
                <Image source={images.location} style={styles.locationImage} />
                <Text numberOfLines={1} style={styles.DhabiTxt}>
                  {this.state.propertyData?.property?.location}
                </Text>
              </View>
              <Text numberOfLines={1} style={styles.MTxt}>
                KWD {this.state.propertyData?.property?.rent}/m .{' '}
                <Text
                  style={{
                    color: Colors.marigold,
                    fontFamily: Fonts.segui_semiBold,
                  }}>
                  {' '}
                  {this.state.propertyData?.property?.type}
                </Text>
              </Text>
            </View>
          </View>

          {this.state.propertyAgreement &&
          this.state.propertyAgreement.length == 0 &&
          !this.state.viewAgreement ? (
            <Text
              style={[
                styles.AgreementTxt,
                {color: Colors.cherryRed, marginLeft: 20},
              ]}>
              {translate("Agreementnotfound")}
            </Text>
          ) : null}
          {this.state.viewAgreement && this.state.propertyData ? (
            <View style={{margin: 15, flexDirection: 'row'}}>
              {this.state.userType ? (
                <View style={{flexDirection: 'column'}}>
                  <Text style={styles.AgreementTxt}>{translate("Agreement")}</Text>
                  <TouchableOpacity
                    onPress={
                      () => this.uploadAgreementModal()}
                      >
                    <Image
                      source={images.agreement}
                      style={styles.agreementImg}
                    />
                  </TouchableOpacity>
                  <Text style={styles.AddAgreementTxt}>{translate("AddAgreement")}</Text>
                </View>
              ) : null}
              {this.state.propertyAgreement.length == 0 ? null : (
                <FlatList
                  data={this.state.propertyAgreement}
                  renderItem={this.downloadRenderItem}
                  horizontal={true}
                  keyExtractor={(item, index) => item + index}
                  showsHorizontalScrollIndicator={false}
                />
              )}

              {this.state.addAgreementState ? (
                <FlatList
                  data={agreementFileArray}
                  renderItem={this.renderItem}
                  horizontal={true}
                  keyExtractor={(item, index) => item + index}
                  showsHorizontalScrollIndicator={false}
                />
              ) : null}
            </View>
          ) : (
            <View style={{margin: 15, flexDirection: 'row'}}>
              <View style={{flexDirection: 'column'}}>
                <Text style={styles.AgreementTxt}>{translate("Agreement")}</Text>
                <TouchableOpacity
                  onPress={
                    () => this.setState({modalVisible: true})
                    //this.openGalleryPicker()
                  }>
                  <Image
                    source={images.agreement}
                    style={styles.agreementImg}
                  />
                </TouchableOpacity>
                <Text style={styles.AddAgreementTxt}>{translate("AddAgreement")}</Text>
              </View>

              <FlatList
                data={agreementFileArray}
                renderItem={this.renderItem}
                horizontal={true}
                keyExtractor={(item, index) => item + index}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          )}

          <View style={{marginHorizontal: 15, marginTop: 15}}>
            <Text style={styles.AgreementTxt}>{translate("DigitalSignature")}</Text>
            {this.state.viewAgreement && this.state.propertyData ? (
              <View style={styles.signatureView}>
                {this.state.signatureUrl ? (
                  <ProgressiveImage
                    source={{
                      uri: this.state.signatureUrl
                        ? this.state.signatureUrl
                        : '',
                    }}
                    style={{
                      width: screenDimensions.WindowWidth - 30,
                      height: 400,
                    }}
                    resizeMode="cover"
                  />
                ) : (
                  <Text style={{fontsize: Fonts.segoeui}}>
                    {translate("NoSignatureUploaded")}
                  </Text>
                )}
              </View>
            ) : (
              <TouchableOpacity
                onPress={() => this.setState({visible: true})}
                style={styles.signatureView}>
                <Image
                  resizeMode={'contain'}
                  style={{
                    width: screenDimensions.WindowWidth - 30,
                    height: 200,
                  }}
                  // source={images.signature_box}
                  source={this.state.selectSignature}
                />
              </TouchableOpacity>
            )}
            {this.state.userType ? this.renderButton() : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo},
  mainview: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: Colors.whiteTwo,
    elevation: 0.3,
    flexDirection: 'row',
    alignItems: 'center',
  },
  Image: {height: 60, width: 70, resizeMode: 'contain'},
  LighthouseTxt: {
    fontSize: 14,
    color: Colors.blackTwo,
    top: -2,
    lineHeight: 25,
    fontFamily: Fonts.segoeui_bold,
    width: '95%',
  },
  locationCss: {flexDirection: 'row', alignItems: 'center'},
  TextViewCss: {flex: 1, marginHorizontal: 20},
  locationImage: {
    height: 12,
    width: 10,
    resizeMode: 'contain',
    marginRight: 5,
    tintColor: Colors.brownishGreyTwo,
    top: 2,
  },
  DhabiTxt: {
    fontSize: 12,
    lineHeight: 22,
    color: Colors.brownishGreyTwo,
    fontFamily: Fonts.segoeui,
    width: '95%',
  },
  MTxt: {
    color: Colors.darkSeafoamGreen,
    fontFamily: Fonts.segoeui_bold,
    fontSize: 12,
  },
  AgreementTxt: {
    fontSize: 16,
    color: Colors.black,
    fontFamily: Fonts.segui_semiBold,
  },
  signature: {marginHorizontal: 15},
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    backgroundColor: Colors.whiteTwo,
    borderWidth: 1,
    borderColor: Colors.whiteFive,
    marginHorizontal: -15,
  },
  button: {
    color: Colors.brownishGreyTwo,
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
  },
  buttonGreen: {
    color: Colors.darkSeafoamGreen,
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
  },
  TextInput: {
    borderWidth: 1,
    borderColor: Colors.whiteFive,
    fontSize: 24,
    textAlign: 'center',
  },
  agreementImg: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    marginBottom: 5,
    marginTop: 20,
  },
  signatureView: {
    flex: 1,
    marginTop: 30,
    borderWidth: 1,
    borderColor: Colors.whiteFive,
  },
  AddAgreementTxt: {
    fontSize: 8,
    color: Colors.brownishGreyTwo,
    fontFamily: Fonts.segoeui,
  },
  TabTxt: {
    fontSize: 12,
    textAlign: 'center',
    color: Colors.greyishBrown,
    marginTop: 10,
    fontFamily: Fonts.segoeui,
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    // alignItems: "center",
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '85%',
    //height:200
  },
  buttonOpen: {
    backgroundColor: Colors.white,
    width: '40%',
    borderWidth: 1,
    borderColor: Colors.warmGrey,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
  },
  buttonClose: {
    backgroundColor: Colors.lightMint,
    width: '40%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 25,
    textAlign: 'center',
    fontFamily: Fonts.segoeui_bold,
    color: Colors.black,
    fontSize: 20,
  },
});
