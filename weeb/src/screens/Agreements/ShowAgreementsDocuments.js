import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    FlatList,
    TouchableOpacity,
    Linking
} from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../../Components/AppHeader';
import { handleNavigation } from '../../navigation/routes';
import images from '../../Lib/Images';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import {Helper, Network, AlertMsg, downloadFile,CallTenant} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {ProgressiveImage} from '../../Components/Common/index';
export default class ShowAgreementsDocuments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      agreements: [
        {
          rightIcon: Images.agreement_pdf,
          label: 'Agreement part -1',
          time: '15 Jan 2021 | 10:15 pm',
          leftIcon: Images.download_arrow,
        },
        {
          rightIcon: Images.agreement_jpg,
          label: 'Agreement part -2',
          time: '15 Jan 2021 | 10:15 pm',
          leftIcon: Images.download_arrow,
        },
      ],
      signature: [
        {
          rightIcon: Images.agreement_jpg,
          label: 'Signature',
          time: '15 Jan 2021 | 10:15 pm',
          //  leftIcon: Images.download_arrow
        },
      ],
      govermentid: [
        {
          rightIcon: Images.agreement_jpg,
          label: 'Front ID',
          time: '15 Jan 2021 | 10:15 pm',
          //  leftIcon: Images.download_arrow
        },
        {
          rightIcon: Images.agreement_jpg,
          label: 'Back ID',
          time: '15 Jan 2021 | 10:15 pm',
          //  leftIcon: Images.download_arrow
        },
      ],
      propertyData: [],
        propertyId: props.route.params?.propertyid,
      currentPage:1,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Documents',
      rightHide: true,
    });
  }

  componentDidMount() {
    this.getPropertyAgreement();
  }
  onCall = () => {
     console.log('call item', this.state.propertyData?.user?.mobile_number);
  // CallTenant(this.state.propertyDetailsData?.user?.mobile_number);
 };
  getPropertyAgreement() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let data = {};
          ApiCall.ApiMethod({
            Url:
              'owner-rented-property-detail' +
              '?page=' +
              this.state.currentPage,
            method: 'POST',
            data: {
              property_id: this.state.propertyId,
            },
          })
            .then((res) => {
              console.log(
                'get owner details property',
                res?.data,
              );
              //   Helper.mainApp.hideLoader();
              //   // Helper.showToast(res?.message)
              if (res?.status) {
                this.setState({
                  propertyData: res?.data,
                  isLoading: false,
                  emptymessage: false,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                propertyData: [],
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                propertyData: [],
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  rednerHouseDetails = () => {
    return (
      <View style={styles.mainview}>
        <ProgressiveImage
          source={{
            uri: this.state.propertyData?.propertyphotos
              ? this.state.propertyData?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.Image}
          resizeMode="cover"
        />
        {/* <Image source={Images.propert_requested} style={styles.Image} /> */}
        <View style={styles.TextViewCss}>
          <Text numberOfLines={1} style={styles.LighthouseTxt}>
            {this.state.propertyData?.title}
          </Text>
          <View style={styles.locationCss}>
            <Image source={Images.location} style={styles.locationImage} />
            <Text numberOfLines={1} style={styles.DhabiTxt}>
              {this.state.propertyData?.location}
            </Text>
          </View>
          <Text numberOfLines={1} style={styles.MTxt}>
            {this.state.propertyData?.rent}/m .{' '}
            <Text
              style={{
                color: Colors.marigold,
                fontFamily: Fonts.segui_semiBold,
              }}>
              {' '}
              {this.state.propertyData?.propertyType}
            </Text>
          </Text>
        </View>
      </View>
    );
  };

  downloadDoc(item,index) {
    console.log('download item', item, index)
    downloadFile(item);
}

  renderDocuments = ({ item,index }) => {
     console.log('tenant doc',item)
    return (
      <View
        style={{
          margin: 15,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            source={
              item.split('.').pop() == 'jpg'
                ? images.agreement_jpg
                : images.agreement_pdf
            }
            style={{width: 30, height: 30, resizeMode: 'contain'}}
          />
          <View style={{marginHorizontal: 15}}>
            <Text
              style={{
                fontSize: 12,
                fontFamily: Fonts.segoeui,
                color: Colors.black,
              }}>
              {`Agreement  ${index}`}
            </Text>
            {/* <Text
              style={{
                fontSize: 8,
                fontFamily: Fonts.segoeui,
                color: Colors.pinkishGrey,
              }}>
              15 Jan 2021 | 10:15 pm
            </Text> */}
          </View>
        </View>
        {this.state.propertyData?.agreements &&
        this.state.propertyData?.agreements[0]?.media_url ? (
            <TouchableOpacity
            onPress={()=>{this.downloadDoc(item, index);}}
            >
            <Image
              source={images?.download_arrow}
              style={{width: 17, height: 17, resizeMode: 'contain'}}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  };

  renderTitle = (bold, label) => {
    return (
      <Text
        style={{
          fontSize: bold ? 16 : 14,
          color: Colors.black,
          fontFamily: bold ? Fonts.segoeui_bold : Fonts.segoeui,
          marginHorizontal: 12,
          marginTop: 0,
        }}>
        {label}
      </Text>
    );
  };

  callTenant(number) {
    console.log(this.state.propertyData?.bookings[0]?.user?.mobile_number)
    let phoneNumber = '';
    if (Platform.OS == 'android') {
      phoneNumber = `tel:$${+number}`;
    } else {
      phoneNumber = `telprompt:$${+number}`;
    }
    Linking.openURL(phoneNumber);
  }
  onChateClick = () => {
   //console.log(this.state.propertyData?.bookings?.user);
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {userId: this.state.propertyData?.bookings[0]?.user?.id},
      navigation: this.props.navigation,
    });
  };
  renderTenantInfo = (
    profileimage,
    name,
    messageicon,
    callicon,
    navigation,
  ) => {
    return (
      <View
        style={{
          margin: 15,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ProgressiveImage
            source={{
              uri: this.state.propertyData?.bookings
                ? this.state.propertyData?.bookings[0]?.user?.profile_picture
                : '',
            }}
            style={{width: 30, height: 30, resizeMode: 'contain'}}
            resizeMode="cover"
          />
          {/* <Image
            source={profileimage}
            style={{width: 30, height: 30, resizeMode: 'contain'}}
          /> */}
          <View style={{marginHorizontal: 15}}>
            <Text
              style={{
                fontSize: 14,
                fontFamily: Fonts.segui_semiBold,
                color: Colors.black,
              }}>
              {this.state.propertyData?.bookings
                ? this.state.propertyData?.bookings[0]?.user?.name
                : ''}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={Images.location_pin}
                style={{width: 5, height: 8, resizeMode: 'contain'}}
              />
              <Text
                style={{
                  marginHorizontal: 5,
                  fontSize: 10,
                  fontFamily: Fonts.segoeui,
                  color: Colors.brownishGrey,
                }}>
                {this.state.propertyData?.bookings
                  ? this.state.propertyData?.bookings[0]?.user?.city
                  : ''}
              </Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => {
              this.onChateClick()
            }}>
            <Image
              source={messageicon}
              style={{width: 32, height: 30, resizeMode: 'contain'}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.callTenant(
                this.state.propertyData?.bookings[0]?.user?.mobile_number,
              )
            }>
            <Image
              source={callicon}
              style={{
                marginHorizontal: 7,
                width: 32,
                height: 30,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderSignature(label) {
    return (
      <View
        style={{
          margin: 15,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            source={images.agreement_jpg}
            style={{width: 30, height: 30, resizeMode: 'contain'}}
          />
          <View style={{marginHorizontal: 15}}>
            <Text
              style={{
                fontSize: 12,
                fontFamily: Fonts.segoeui,
                color: Colors.black,
              }}>
              {label}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  render() {
    console.log(
      '  propertyData: props.route.params?.propertyData,',
      this.state.propertyData?.agreements
        ? this.state.propertyData?.bookings[0]?.user
        : '',
    );
    return (
      <View style={styles.container}>
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          {this.rednerHouseDetails()}
          {this.renderTitle(true, 'Property Document')}
          {this.state.propertyData?.agreements &&
        this.state.propertyData?.agreements[0]?.media_url ?this.renderTitle(false, 'Agreement'):null}
          
          <View>
            <FlatList
              data={
                this.state.propertyData?.agreements
                  ? this.state.propertyData?.agreements[0]?.media_url
                  : []
              }
              renderItem={this.renderDocuments}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          {this.renderTitle(false, 'Signature')}
          {this.renderSignature('Signature')}
          {/* <View>
            <FlatList
              data={this.state.propertyData?.agreements}
              renderItem={this.renderDocuments}
              keyExtractor={(item, index) => index.toString()}
            />
          </View> */}

          {this.renderTitle(true, 'Tenant Info')}
          {this.renderTenantInfo(
            Images.profile_img,
            'Basheer Khan',
            Images.owner_chat,
            Images.owner_call,
          )}

          {/* {this.renderTitle(false, 'Government ID')}
          <View>
            <FlatList
              data={this.state.govermentid}
              renderItem={this.renderDocuments}
              keyExtractor={(item, index) => index.toString()}
            />
          </View> */}

          {this.renderTitle(false, 'Signature')}
          {this.renderSignature('Signature')}
          {/* <View>
            <FlatList
              data={this.state.propertyData?.agreements}
              renderItem={this.renderDocuments}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>  */}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    mainview: {
        paddingHorizontal: 15,
        paddingVertical: 20,
        backgroundColor: Colors.whiteTwo,
        elevation: 0.3,
        flexDirection: 'row',
        alignItems: 'center',
    },
    Image: { height: 60, width: 70, resizeMode: 'contain' },
    LighthouseTxt: {
        fontSize: 14,
        color: Colors.blackTwo,
        top: -2,
        lineHeight: 25,
        fontFamily: Fonts.segoeui_bold,
        width: '95%',
    },
    locationCss: { flexDirection: 'row', alignItems: 'center' },
    TextViewCss: { flex: 1, marginHorizontal: 20 },
    locationImage: {
        height: 12,
        width: 10,
        resizeMode: 'contain',
        marginRight: 5,
        tintColor: Colors.brownishGreyTwo,
        top: 2,
    },
    MTxt: {
        color: Colors.darkSeafoamGreen,
        fontFamily: Fonts.segoeui_bold,
        fontSize: 12,
    },
});
