import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Images from '../../Lib/Images';
import Fonts from '../../Lib/Fonts';
import { Inputs, GeoLocation, CircleButton } from '../../Components/Common/index';
import Colors from '../../Lib/Colors';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { handleNavigation } from '../../navigation/routes';
import images from '../../Lib/Images';
import { Validation, Network, AlertMsg } from '../../Lib/index'
import { configureGoogleLogin, googleLogin, FacebookLogin, CountryCodePickerModal } from '../../Components/Common/index'
import { ApiCall ,ApiUrl} from '../../Api/index';
import Helper from '../../Lib/Helper';
import { Constant } from '../../Lib/Constant'; 
import {AppleButton,appleAuth} from '@invertase/react-native-apple-authentication';
import { translate } from '../../Language';
export default class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      termCheck: false,
      name: '',
      email: '',
      mobile: '',
      password: '',
      conPassword: '',
      countryCode: '+965',
      modalVisibleCode: false,
      countryCodeWithoutPlus: '965',
      city: '', 
    };
  }
  componentDidMount() { 
    configureGoogleLogin();
  }
   
  goSignIn = () => {
    handleNavigation({
      type: 'push',
      page: 'Signin',
      navigation: this.props.navigation,
    });
  };

  googleLogin = () => {
    googleLogin((result) => {
      if (result) {
        //hit the social login api
        Helper.mainApp.showLoader();
        ApiCall.socialLogin(result).then((res) => {
          if (res.status) {
            Helper.mainApp.hideLoader();
            Helper.setData(Constant.USER_DATA, res?.data);
            Helper.setData(Constant.TOKEN, res?.token);
            Helper.userData = res?.data;
            Helper.token = res?.token;
            handleNavigation({
              type: 'setRoot',
              page: 'DrawerStack',
              navigation: this.props.navigation,
            });
          } else {
            Helper.mainApp.hideLoader();
          }
        });
      } else {
        Helper.mainApp.hideLoader();
        console.log('user google data not found signup');
      }
    });
  };
  faceBookLogin = () => {
    FacebookLogin((result) => {
      if (result) {
        Helper.mainApp.showLoader();
        ApiCall.socialLogin(result).then((res) => {
          if (res?.status) {
            Helper.mainApp.hideLoader();
            Helper.setData(Constant.USER_DATA, res?.data);
            Helper.setData(Constant.TOKEN, res?.token);
            Helper.userData = res?.data;
            Helper.token = res?.token;
            handleNavigation({
              type: 'setRoot',
              page: 'DrawerStack',
              navigation: this.props.navigation,
            });
          } else {
            Helper.mainApp.hideLoader();
          }
        });
      } else {
        Helper.mainApp.hideLoader();
        console.log('fb  data not found signup');
      }
    });
  };

  appleLogin = async () => {
    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      let {
        user,
        email,
        fullName,
        nonce,
        identityToken,
        realUserStatus /* etc */,
      } = appleAuthRequestResponse;

      if (user) {
        console.log(
          'apple login data',
          user,
          email,
          fullName,
          nonce,
          identityToken,
          realUserStatus,
        );
         Helper.mainApp.showLoader();
         let formdata = new FormData();
         formdata.append("social_type", 'APPLE');
         formdata.append("social_id", user);
         formdata.append("device_type", Helper.device_type);
         formdata.append("device_id", Helper.device_id);
         if (email) {
             formdata.append("email", email);
      }
         if (fullName) {
             formdata.append("name", fullName);
         }
        // if (json.picture.data.url) {
        //     formdata.append("profile_picture", json.picture.data.url);
        // }
         ApiCall.socialLogin(formdata).then((res) => {
           if (res?.status) {
            Helper.mainApp.hideLoader();
            Helper.setData(Constant.USER_DATA, res?.data);
            Helper.setData(Constant.TOKEN, res?.token);
            Helper.userData = res?.data;
            Helper.token = res?.token;
            handleNavigation({
              type: 'setRoot',
              page: 'DrawerStack',
              navigation: this.props.navigation,
            });
           } else {
             Helper.mainApp.hideLoader();
           }
         });
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  };


  userSignUp = () => {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          if (
            Validation.checkAlphabat('Full Name', 3, 25, this.state.name) &&
            Validation.checkEmail('Email', this.state.email) &&
            Validation.checkPhoneNumber(
              'Phone Number',
              7,
              13,
              this.state.mobile,
            ) &&
            Validation.checkAlphabat('City', 3, 25, this.state.city) &&
            Validation.checkNotNull('Password', 8, 15, this.state.password) &&
            Validation.checkMatch(
              'Password',
              this.state.password,
              this.state.conPassword,
            ) &&
            Validation.checkTrue('Terms and Conditions', this.state.termCheck)
          ) {
            let signUpData = {
              name: this.state.name,
              email: this.state.email,
              country_code: this.state.countryCodeWithoutPlus,
              mobile_number: this.state.mobile,
              city: this.state.city,
              password: this.state.password,
              device_id: Helper.device_id,
              device_type: Helper.device_type,
            };

            Helper.mainApp.showLoader();
            ApiCall.ApiMethod({Url: 'signup', method: 'POST', data: signUpData})
              .then((res) => {
                console.log('sign up res', res);
                if (!res?.verified && res?.status) {
                  Helper.showToast(
                    res?.message ? res?.message : AlertMsg?.success?.SIGNUP,
                  );
                  
                  Helper.mainApp.hideLoader();
                  this.props.navigation.navigate('VerifyOtp', {
                    screenName: '',
                    countrycode: this.state.countryCodeWithoutPlus,
                    phonenumber: this.state.mobile,
                  });
                  return true;
                } else {
                  Helper.showToast(
                    res?.message ? res?.message : AlertMsg.error.NETWORK,
                  );
                  Helper.mainApp.hideLoader();
                  return false;
                }
              })
              .catch((err) => {
                console.log('sign up api error', err);
                Helper.showToast(AlertMsg.error.NETWORK);
                Helper.mainApp.hideLoader();
                return false;
              });
          }
        } else {
          Helper.mainApp.hideLoader();
          Helper.showToast(AlertMsg.error.NETWORK);
          return false;
        }
      })
      .catch((err) => {
        Helper.mainApp.hideLoader();
        Helper.showToast(AlertMsg.error.NETWORK);
        return false;
      });
  };

  callCounteryCodeApi = (dial_code) => {
    this.setState({
      countryCode: dial_code,
      modalVisibleCode: false,
      countryCodeWithoutPlus: dial_code.substring(1),
    });
  };

  openTermAndCondition = () => {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'Terms and Conditions',
        url: ApiUrl.TermAndPrivacy,
      },
      navigation: this.props.navigation,
    });
    //  return <TermAndPrivacy title={'Terms and Conditions'} />

   // Linking.openURL('http://dev9server.com/weeb/terms-and-condition')
  };

  openPrivacy = () => {
    handleNavigation({
      type: 'push',
      page: 'TermAndPrivacy',
      passProps: {
        title: 'Privacy Policy',
        url: ApiUrl.PrivacyPolicy,
      },
      navigation: this.props.navigation,
    });
  };

  render() {
    const NameAndEmailAndMobile = () => {
      return (
        <View>
          <Inputs
            labels={translate("FullName")}
            labelsize={12}
            labelcolor={Colors.black}
            width={'80%'}
            backgroundcolor={'white'}
            color={Colors.brownishGrey}
            rightimage={Images.edit_pofile}
            placeholder={translate("FullName")}
            marginleft={-5}
            keyboard={'default'}
            onChangeText={(text) => {
              this.setState({name: text});
            }}
            value={this.state.name}
            setfocus={(input) => {
              this.name = input;
            }}
            getfocus={() => {
              this.email.focus();
            }}
            returnKeyType={'next'}
            bluronsubmit={false}
          />
          <Inputs
            labels={translate("EmailAddress")}
            labelsize={12}
            labelcolor={Colors.black}
            width={'80%'}
            backgroundcolor={'white'}
            color={Colors.brownishGrey}
            rightimage={Images.email}
            placeholder={translate("EmailAddress")}
            marginleft={-5}
            keyboard={'email-address'}
            onChangeText={(text) => this.setState({email: text})}
            value={this.state.email}
            setfocus={(input) => {
              this.email = input;
            }}
            getfocus={() => {
              this.number.focus();
            }}
            returnKeyType={'next'}
            bluronsubmit={false}
          />
          <Text
            style={{
              top: 10,
              marginHorizontal: 14,
              fontSize: 12,
              fontFamily: Fonts.segoeui,
              color: Colors.brownishGrey,
            }}>
            {translate("MobileNumber")}
          </Text>
          <View
            style={{
              top: 5,
              justifyContent: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{flex: 0.4}}>
                <TouchableOpacity
                  style={[
                    styles.countryView,
                    {backgroundColor: Colors.whiteTwo},
                  ]}
                  onPress={() => this.setState({modalVisibleCode: true})}>
                  <Image
                    style={{height: 12, width: 12, resizeMode: 'contain'}}
                    source={images.mobile}
                  />
                  <Text>{this.state.countryCode}</Text>
                  <Image
                    style={{height: 10, width: 10, resizeMode: 'contain'}}
                    source={images.drop_arrow}
                  />
                </TouchableOpacity>
              </View>
              <CountryCodePickerModal
                visible={this.state.modalVisibleCode}
                onRequestClose={() => {
                  this.setState({modalVisibleCode: false});
                }}
                onPress={() => {
                  this.setState({modalVisibleCode: false});
                }}
                callCounteryCodeApi={(dial_code) => {
                  this.callCounteryCodeApi(dial_code);
                }}
              />
              <View style={{flex: 0.6, top: -12}}>
                <Inputs
                  width={'95%'}
                  backgroundcolor={'white'}
                  color={Colors.brownishGrey}
                  placeholder={'968-926-0227'}
                  keyboard={'number-pad'}
                  onChangeText={(text) => this.setState({mobile: text})}
                  value={this.state.mobile}
                  setfocus={(input) => {
                    this.number = input;
                  }}
                  getfocus={() => {
                    this.city.focus();
                  }}
                  returnKeyType={'next'}
                  maxlength={10}
                  bluronsubmit={false}
                />
              </View>
            </View>
          </View>
        </View>
      );
    };

    const City = () => {
      return (
        <Inputs
          labels={translate("City")}
          width={'80%'}
          value={this.state.city}
          backgroundcolor={Colors.whiteTwo}
          color={Colors.brownishGrey}
          rightimage={Images.location}
          placeholder={this.state.city}
          // marginleft={-10}
          keyboard={'email-address'}
          maxlength={15}
          onChangeText={(text) => {
            this.setState({city: text});
          }}
          // leftimage={Images.arrow}
          modalVisible={this.state.modalVisible}
          showModal={() => {
            this.setState({modalVisible: true});
          }}
          hideModal={() => {
            this.setState({modalVisible: false});
          }}
          setfocus={(input) => {
            this.city = input;
          }}
          getfocus={() => {
            this.password.focus();
          }}
          modalBackIcon={Images.black_arrow_btn}
          returnKeyType={'next'}
          bluronsubmit={false}
        />
      );
    };

    const passowrd = () => {
      return (
        <View>
          <Inputs
            labels={translate("Password")}
            labelsize={12}
            labelcolor={Colors.black}
            width={'80%'}
            backgroundcolor={'white'}
            color={Colors.brownishGrey}
            rightimage={Images.lock}
            placeholder={'**********'}
            marginleft={-5}
            keyboard={'default'}
            onChangeText={(text) => {
              this.setState({password: text});
            }}
            value={this.state.password}
            password={true}
            setfocus={(input) => {
              this.password = input;
            }}
            getfocus={() => {
              this.conPassword.focus();
            }}
            returnKeyType={'next'}
            bluronsubmit={false}
          />
          <Inputs
            labels={translate("ConfirmPassword")}
            labelsize={12}
            labelcolor={Colors.black}
            width={'80%'}
            backgroundcolor={'white'}
            color={Colors.brownishGrey}
            rightimage={Images.lock}
            placeholder={'**********'}
            marginleft={-5}
            keyboard={'default'}
            onChangeText={(text) => {
              this.setState({conPassword: text});
            }}
            value={this.state.conPassword}
            password={true}
            setfocus={(input) => {
              this.conPassword = input;
            }}
            getfocus={() => {
              this.conPassword.focus();
            }}
            returnKeyType={'done'}
            bluronsubmit={true}
          />
        </View>
      );
    };

    const Terms = () => {
      return (
        <View
          style={{
            flexDirection: 'row',
            width: '90%',
            marginTop: 15,
            alignItems: 'center',
            marginLeft: 17,
          }}>
          <TouchableOpacity
            onPress={() => this.setState({termCheck: !this.state.termCheck})}>
            {this.state.termCheck ? (
              <Image source={Images.terms_check} style={styles.termImage} />
            ) : (
              <View
                style={{
                  height: 14,
                  width: 14,
                  borderRadius: 2,
                  borderWidth: 1,
                  marginRight: 10,
                }}></View>
            )}
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 10,
                fontFamily: Fonts.segoeui,
              }}
              numberOfLines={1}>
              {' '}
              {translate("Iagree")}{' '}
            </Text>
            <TouchableOpacity onPress={() => this.openTermAndCondition()}>
              <Text
                style={{
                  color: Colors.dustyOrange,
                  fontSize: 10,
                  fontFamily: Fonts.segoeui,
                }}
                numberOfLines={1}>
                {' '}
                {translate("TermsandConditions")}
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 10,
                fontFamily: Fonts.segoeui,
              }}>
              {' '}
              {translate("and")}{' '}
            </Text>
            <TouchableOpacity onPress={() => this.openPrivacy()}>
              <Text
                style={{
                  color: Colors.dustyOrange,
                  fontSize: 10,
                  fontFamily: Fonts.segoeui,
                }}
                numberOfLines={1}>
                {translate("PrivacyPolicy")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    };

    const Signup = () => {
      return (
        <CircleButton
          navigate={() => this.userSignUp()}
          //navigate={() => this.props.navigation.navigate('VerifyOtp', { screenName: '' })}
          fontsize={18}
          labelfonts={Fonts.segoeui_bold}
          colors={Colors.lightMint}
          arrowimage={Images.signup_arrow}
          label={translate("SignUp")}
        />
      );
    };

    return (
      <View style={styles.Container}>
        <KeyboardAwareScrollView
          bounces={false}
          alwaysBounceVertical={false}
          keyboardShouldPersistTaps={'handled'}>
          <Image source={Images.signup_bg} style={styles.signUpBg} />
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              // top: '10%',
            }}>
            <Image source={Images.sign_up_logo} style={styles.logo} />
            <Text style={styles.title}>{translate("Createyouraccount")}</Text>
            <View style={[styles.authBox, {backgroundColor: Colors.whiteFive}]}>
              {NameAndEmailAndMobile()}
              {City()}
              {passowrd()}
              {Terms()}
              {Signup()}
            </View>

            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 14, fontFamily: Fonts.segoeui}}>
                {translate("ConnectwithSocialAccount")}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              {Platform.OS === 'android' ? null : (
                // <TouchableOpacity>
                //   <Image style={styles.socialIcon} source={Images.apple_icon} />
                // </TouchableOpacity>
                <AppleButton
                buttonStyle={AppleButton.Style.BLACK}
               buttonType={AppleButton.Type.SIGN_UP}
                cornerRadius={25}
                style={styles.appleButton}
                onPress={() => this.appleLogin()}
              />
              )}
              <TouchableOpacity onPress={() => this.faceBookLogin()}>
                <Image source={Images.facebook} style={styles.socialIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.googleLogin()}>
                <Image
                  source={Images.google_hangouts}
                  style={styles.socialIcon}
                />
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', padding: 10, marginTop: 15}}>
              <Text style={{fontSize: 14, fontFamily: Fonts.segoeui}}>
                {translate("ExistingUsers")}
              </Text>
              <Text>{'  '}</Text>
              <TouchableOpacity onPress={() => this.goSignIn()}>
                <Text style={styles.signintext}>{translate("SignIn")}</Text>
              </TouchableOpacity>
            </View>

            <View style={{margin: 10}} />
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: Colors.whiteTwo
  },
  signUpBg: {
    height: 500,
    width: '100%',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    overflow: 'hidden',
    position: 'absolute',
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  logo: {
    resizeMode: 'contain',
    width: 70,
    height: 50,
    marginTop: 50,
    borderColor: 'black',
  },
  title: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 18,
    color: Colors.black,
    marginTop: 15
  },
  authBox: {
    padding: 10,
    width: '90%',
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 14,
    marginTop: 25,
    paddingTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    //elevation: 5,
    //borderWidth: 1,
  },
  termImage: {
    height: 14,
    width: 14,
    padding: 5,
    marginRight: 5,
    resizeMode: 'contain',
  },
  signUpImageIcon: {
    height: 13,
    width: 19,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    //  marginLeft: -10,
    //marginRight: 3,
  },
  signUpCircle: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: Colors.lightMint,
    justifyContent: 'center',
  },
  socialIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    margin: 10,
    marginTop: 15,
  },
  signintext: {
    textDecorationLine: 'underline',
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
  },
  countryView: {
    marginHorizontal: 8, marginTop: 5, paddingVertical: 8, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderWidth: 0.5, borderRadius: 4, borderBottomColor: Colors.pinkishGrey,
    borderTopColor: Colors.pinkishGrey,
    borderEndColor: Colors.pinkishGrey,
    borderLeftColor: Colors.pinkishGrey,
    borderRightColor: Colors.pinkishGrey,
    borderColor: Colors.pinkishGrey
  },
  appleButton: {
    height: 35,
    width: 35,
    marginLeft: 8,
    resizeMode: 'contain',
    marginTop:7
   },
});
