import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity,DeviceEventEmitter,Image,Keyboard, TextInput, FlatList, Alert,ScrollView, Platform, SafeAreaView, AppState} from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import Helper from '../Lib/Helper'
import { handleNavigation } from '../navigation/routes';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import ChatController from '../Lib/SocketManager';
import ActionSheet from 'react-native-actionsheet';
import { ProgressiveImage } from '../Components/Common/index';
import moment from 'moment';
import { translate } from '../Language';
import {Network,AlertMsg} from '../Lib/index';
import NetInfo from "@react-native-community/netinfo";
 let lodash = require('lodash'); 
//  lodash.compact(sendPropertyImage).join(',');
export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
      userId: props.route.params?.userId,
      message: '',
      allChatList: [],
      first_message_id: 0,
      change: false,
      isBlocked: false,
      mySelfPic: '',
      otherPic: '',
      last_message_id: -1,
      userName: '',
      blockText:'Block',
      KeyboardHeight:0,
      appState: AppState.currentState
    };
    this.flatList = null;
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Chat',
      searchClick: () => {
        this.moreOptionsIconClick();
      },
      searchIcon: images.more_icon,
      search: true,
    });
  }

  componentDidMount() {
      this.unsubscribe = NetInfo.addEventListener(state => {
        console.log("Connection type", state.type);
        console.log("Is connected?", state.isConnected);
        if(state.isConnected){
       
          //this.checkUserStatus();
        this.getMessageHistory('after');
        // this.getOtherUserProfile();
         //this.getmyProfile();
         return true
        }else{
          Helper.showToast(AlertMsg.error.NETWORK)
        }
      });
    AppState.addEventListener("change", this._handleAppStateChange);
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    this.checkUserStatus();
   this.getMessageHistory('after');
    this.getOtherUserProfile();
    this.getmyProfile();
    this.historylistener = DeviceEventEmitter.addListener(
      ChatController.events.get_user_thread_response,
      (data) => {
        //console.log('chat data',data)
        if (data.responseData.length > 0) {
          this.state.first_message_id = data.first_message_id;

          let newMessage = [];
          let allChatList = this.state.allChatList;
          var array = data.responseData;

          array.length > 0
            ? (this.state.last_message_id = array[array.length - 1].id)
            : null;

          for (let msg of data.responseData) {
            if (!allChatList.some((allChatList) => allChatList.id == msg.id)) {
              newMessage.push(msg);
            }
          }
          this.setState(
            {
              allChatList: [...this.state.allChatList, ...newMessage],
              // showLoader: false,
            },
            () => {
              console.log(
                'total message',
                this.state.allChatList.length,
                this.state.last_message_id,
              );
              this.readMessage();
            },
          );
        } else {
          this.setState({showLoader: false});
        }
      },
    );
    this.multi_userlistener = DeviceEventEmitter.addListener(
      ChatController.events.send_message_by_self,
      (data) => {
        if (this.state.allChatList.length == 0) {
          this.state.first_message_id = data.responseData.id;
        }
        let sameId = false;
        this.state.allChatList.map((item) => {
          if (item.id == data.responseData.id) {
            sameId = true;
          }
        });
        if (!sameId) {
          this.state.allChatList.unshift(data.responseData);
          this.readMessage();
          this.setState({change: !this.state.change}, () => {
            setTimeout(() => {
              this.flatList.scrollToIndex({animated: true, index: 0});
            }, 200);
          });
        }
      },
    );

    this.receive_messagelistener = DeviceEventEmitter.addListener(
      ChatController.events.recevied_message_by_others,
      (data) => {
        var sameId = false;
        this.state.allChatList.map((item) => {
          if (item.id == data.responseData.id) {
            sameId = true;
          }
        });
        if (!sameId) {
          let chatArray = this.state.allChatList;
          this.state.allChatList.unshift(data.responseData);
          this.readMessage();
          this.setState({change: !this.state.change}, () => {
            setTimeout(() => {
              this.flatList.scrollToIndex({animated: true, index: 0});
            }, 200);
          });
           DeviceEventEmitter.emit('messageCount',true)
        }
      },
    );
    this.check_User_Status = DeviceEventEmitter.addListener(
      ChatController.events.check_user_status_response,
      (data) => {
        //  console.log('user status', data?.responseData)
        if (data?.responseData?.is_blocked) {
          this.setState({
            isBlocked: true,
          });
        } else {
          this.setState({
            isBlocked: false,
          });
        }
        if (data?.responseData?.is_blocked_by_me) {
          this.setState({
            blockText: 'Unblock',
          });
        } else {
          this.setState({
            blockText: 'Block',
          });
        }
        //  this.setState({
        //    isBlocked:data?.responseData?.
        //  })
      },
    );
    this.user_block_unblock_response = DeviceEventEmitter.addListener(
      ChatController.events.user_block_unblock_response,
      (data) => {
        //  console.log('user status', data?.responseData)
        if (data) {
         this.checkUserStatus();
        }
      });
    this.get_other_profile_response = DeviceEventEmitter.addListener(
      ChatController.events.get_other_profile_response,
      (data) => {
        //console.log('myself status', data?.responseData?.userInfo?.id == Helper?.userData?.id)
        if (data?.responseData?.userInfo?.id == Helper?.userData?.id) {
          this.setState(
            {
              mySelfPic: data?.responseData?.userInfo?.profile_picture,
            },
            () => {
              // console.log('myself pic', this.state.mySelfPic);
            },
          );
        } else {
          this.setState(
            {
              otherPic: data?.responseData?.userInfo?.profile_picture,
              userName: data?.responseData?.userInfo?.name,
            },
            () => {
              console.log(
                'other user pic',
                data?.responseData?.userInfo?.profile_picture,
              );
              AppHeader({
                ...this,
                leftHeide: false,
                leftIcon: images.black_arrow_btn,
                leftClick: () => {
                  this.goBack();
                },
                title: this.state.userName,
                searchClick: () => {
                  this.moreOptionsIconClick();
                },
                searchIcon: images.more_icon,
                search: true,
              });
              //   console.log('myself pic', this.state.otherPic);
            },
          );
        }
      },
    );
  }
  componentWillUnmount() {
    this.multi_userlistener.remove();
    this.receive_messagelistener.remove();
    this.check_User_Status.remove();
    this.user_block_unblock_response.remove()
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    AppState.removeEventListener("change", this._handleAppStateChange);
    //this.unsubscribe.remove();
  }
  _keyboardDidShow(e) {
    this.setState({ KeyboardHeight: e.endCoordinates.height });
}
_keyboardDidHide() {
    this.setState({ KeyboardHeight: 0 })
}
_handleAppStateChange = nextAppState => {
  if (
    this.state.appState.match(/inactive|background/) &&
    nextAppState === "active"
  ) {
    console.log("App has come to the foreground!");
    Network.isNetworkAvailable().then((res)=>{
      if(!res){
      Helper.showToast(AlertMsg.error.NETWORK)
      }else{
       // alert('in')
       // this.checkUserStatus();
        this.getMessageHistory('after');
        // this.getOtherUserProfile();
         //this.getmyProfile();
      }
          })
   
  }
  this.setState({ appState: nextAppState });
  this.historylistener = DeviceEventEmitter.addListener(
    ChatController.events.get_user_thread_response,
    (data) => {
      //console.log('chat data',data)
      if (data.responseData.length > 0) {
        this.state.first_message_id = data.first_message_id;

        let newMessage = [];
        let allChatList = this.state.allChatList;
        var array = data.responseData;

        array.length > 0
          ? (this.state.last_message_id = array[array.length - 1].id)
          : null;

        for (let msg of data.responseData) {
          if (!allChatList.some((allChatList) => allChatList.id == msg.id)) {
            newMessage.push(msg);
          }
        }
        this.setState(
          {
            allChatList: [...this.state.allChatList, ...newMessage],
            // showLoader: false,
          },
          () => {
            console.log(
              'total message',
              this.state.allChatList.length,
              this.state.last_message_id,
            );
            this.readMessage();
          },
        );
      } else {
        this.setState({showLoader: false});
      }
    },
  );
};
  onEndReached = () => {
   // alert('call')
    if (
      this.state.first_message_id !=
      this.state.allChatList[this.state.allChatList.length - 1].id
    ) {
      this.getMessageHistory('before');
     }
  };
  readMessage() {
    // console.log('message_id', this.state.allChatList[0]?.conversation_id);
    let data = [];
    this.state.allChatList.map((item) => {
      // console.log('id', item)
      if (item?.is_read == 0 || item?.is_read == -1) {
        data.push(item?.id);
      }
    });

    // console.log('data', data, lodash.compact(data).join(','));
    if (this.state.allChatList && data.length > 0) {
      ChatController.callbackSocket(ChatController.events.read_message_update, {
        msg_id:
          this.state.allChatList && data.length > 0
            ? lodash.compact(data).join(',')
            : 0,
        is_read: 1,
      });
    }
  }
  sendMessage = () => {
    // if (this.state.isBlockedByMe || this.state.isBlockedByOther) {
    //   this.gotoUnBlockUser();
    // } else {
      Network.isNetworkAvailable().then((isConnected) => {
        if(isConnected){
          if (this.state.message.trim() != '') {
            let formdata = {
              user_id: Helper.userData.id,
              other_user_id: this.state.userId,
              message_type: 'TEXT',
              msg: this.state.message,
              thumb_image: '',
            };
            this.setState({message: ''});
            ChatController.callbackSocket(
              ChatController.events.send_message,
              formdata,
            );
          }
        }else{
          Helper.showToast(AlertMsg.error.NETWORK)
        }
      }).catch((err)=>{
        Helper.showToast(AlertMsg.error.NETWORK)
        console.log(err);
      })
   
    // }
  };
  getMessageHistory = (type) => {
    //this.setState({showLoader: true});
    let data = {
      user_id: Helper.userData.id,
      other_user_id: this.state.userId,
      type: 'TEXT',
      last_id: this.state.last_message_id,
      limit: 20,
      type: type,
    };
    ChatController.callbackSocket(ChatController.events.get_user_thread, data);
  };
  getmyProfile = () => {
    //this.setState({showLoader: true});
    let data = {
      user_id: Helper.userData.id,
    };
    ChatController.callbackSocket(
      ChatController.events.get_other_profile,
      data,
    );
  };
  getOtherUserProfile = () => {
    //this.setState({showLoader: true});
    let data = {
      user_id: this.state.userId,
    };
    ChatController.callbackSocket(
      ChatController.events.get_other_profile,
      data,
    );
  };
  checkUserStatus = () => {
    let data = {
      user_id: Helper.userData.id,
      other_user_id: this.state.userId,
    };
    ChatController.callbackSocket(
      ChatController.events.check_user_status,
      data,
    );
  };

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  RenderFeedCard = ({item}) => {
     //console.log(item);
    return (
      <View style={{marginTop: '3%', 
      flex: 1, marginHorizontal: 12, 
    }}
  >

        {/* {item.viewReceive ? (
          <View style={{flexDirection: 'row', flex: 1}}>
            <Image
              source={images.chat_Pro}
              style={{height: 41, width: 41, resizeMode: 'contain'}}
            />
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={styles.txt_MessageReceive}>{item.text}</Text>
              <Text style={styles.txt_MessageReceiveTime}>{item.time}</Text>
            </View>
          </View>
        ) : null} */}

        {item.user_id == Helper.userData.id ? (
          <View style={styles.view_MessageSend}>
            <View style={{flex: 1}}>
              <Text style={styles.txt_MessageSend}>{item?.msg}</Text>
              <Text style={styles.txt_MessageSendTime}>
                {moment(item?.updated_at).format('h:mm a')}{' '}
              </Text>
            </View>
            <ProgressiveImage
              // source={{
              //   uri:
              //     item.user_id == Helper.userData.id
              //       ? item?.user_info?.picture
              //       : item?.other_user_info?.picture,
              // }}
              source={{
                uri: this.state.mySelfPic,
              }}
              style={{
                height: 41,
                width: 41,
                resizeMode: 'contain',
                marginLeft: 7,
                borderRadius: 41,
              }}
              resizeMode="cover"
            />
          </View>
        ) : (
          <View style={{flexDirection: 'row', flex: 1}}>
            <ProgressiveImage
              // source={{
              //   uri:
              //     item.user_id == Helper.userData.id
              //       ? item?.other_user_info?.picture
              //       : item?.user_info?.picture,
              // }}
              source={{
                uri: this.state.otherPic,
              }}
              style={{
                height: 41,
                width: 41,
                resizeMode: 'contain',
                borderRadius: 41 / 2,
              }}
              resizeMode="cover"
            />
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={styles.txt_MessageReceive}>{item?.msg}</Text>
              <Text style={styles.txt_MessageReceiveTime}>
                {moment(item?.updated_at).format('h:mm a')}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  };

  moreOptionsIconClick = () => {
    this.ActionSheet.show();
  };
  getItemLayout = (data, index) => ({ length: 50, offset: 50 * index, index });
  blockUser = () => {
    ChatController.callbackSocket(
      ChatController.events.user_block_unblock,
      {
        user_id: Helper.userData.id,
        other_user_id: this.state.userId,
      },
    );
    return;
  } 
  render() {
    //console.log('other user id', this.state.userId?.id);
    return (
      <View style={{flex:1, backgroundColor: Colors.whiteTwo, marginBottom: Platform.OS == 'ios' && this.state.KeyboardHeight ? this.state.KeyboardHeight : 0}}>
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        <ActionSheet
          ref={(o) => (this.ActionSheet = o)}
          // title={'Are you sure you want to block?'}
          options={[this.state.blockText, 'Cancel']}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={(index) => {
            /* do something */
            if (index == 0) {
              Alert.alert(
                'Weeb',
                `Are you sure you want to ${this.state.blockText} this user?`,
                [
                  {
                    text: 'No',
                    //  onPress: () => this.openDocumentPicker(),
                  },
                  {
                    text: 'Yes',
                    onPress: () => this.blockUser(),
                  },
                ],
                {cancelable: true},
              );
            }
          }}
        />
        {/* <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'always'}
          // alwaysBounceVertical={false}
          // bounces={false}
        > */}
        {/* <View style={{
          marginBottom: Platform.OS == 'ios' && this.state.KeyboardHeight ? this.state.KeyboardHeight : 0
        }}> */}
        <FlatList
          style={{}}
          inverted
          data={this.state.allChatList}
          keyboardShouldPersistTaps="handled"
          renderItem={this.RenderFeedCard}
          ref={(ref) => (this.flatList = ref)}
          initialNumToRender={this.state.allChatList.length}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.01}
          ItemSeparatorComponent={() => <View style={{height: 13}} />}
        />
        {/* </View> */}
        {/* </KeyboardAwareScrollView> */}
        {this.state.isBlocked ? null : this.state.blockText == 'Unblock' ? (
          <View
            style={{
              // flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 9,
              justifyCenter: 'center',
              marginHorizontal: 12,
              bottom: Helper.hasNotch ? 7 : 0,
            }}>
            <Text
              style={{
                textalign: 'center',
                fontFamily: Fonts.segoeui,
                color: Colors.cherryRed,
                fontSize: 14,
              }}>
              {translate("YouBlockedthisuser")}
            </Text>
          </View>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 9,
              marginHorizontal: 12,
              bottom: Helper.hasNotch ? 7 : 0,
            }}>
            <TextInput
              style={styles.input_Message}
              placeholder={'Type here'}
              placeholderTextColor={Colors.greyish}
              onChangeText={(val) => {
                this.setState({message: val});
              }}
              value={this.state.message}
            />
            <TouchableOpacity onPress={() => this.sendMessage()}>
              <Image
                source={images.send_btn}
                style={{
                  height: Platform.OS === 'ios' ? 40 : 50,
                  width: Platform.OS === 'ios' ? 40 : 50,
                  marginLeft: 5,
                }}
              />
            </TouchableOpacity>
          </View>
        )}

        {/* <SafeAreaView /> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
    txt_Date: { alignSelf: 'center', fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.greyish },
    view_boder: { borderBottomWidth: 0.5, marginHorizontal: '6%', marginTop: 15, marginBottom: '8%', borderBottomColor: Colors.warmGrey, },
    input_Message: { flex: 1, borderRadius: 4, fontFamily: Fonts.segoeui, fontSize: 14, color: Colors.black, backgroundColor: Colors.whiteFour, paddingLeft: 20, paddingVertical: 10 },
    txt_MessageSend: { flex: 1, backgroundColor: Colors.darkSeafoamGreen, paddingHorizontal: 18, paddingVertical: 15, lineHeight: 21, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, fontFamily: Fonts.segoeui, color: Colors.black, fontSize: 14 },
    txt_MessageSendTime: { alignSelf: 'flex-end', marginTop: 8, fontSize: 12, color: Colors.greyish, fontFamily: Fonts.segoeui },
    view_MessageSend: { alignSelf: 'flex-end', flexDirection: 'row', flex: 1, marginTop: '0%', },
    txt_MessageReceive: { backgroundColor: Colors.lightMint, paddingVertical: 15, paddingHorizontal: 13, lineHeight: 21, borderTopRightRadius: 10, borderBottomRightRadius: 10, borderBottomLeftRadius: 10, fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.black },
    txt_MessageReceiveTime: { marginTop: 8, fontSize: 12, color: Colors.greyish, fontFamily: Fonts.segoeui },

});