import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList, Modal,DeviceEventEmitter,RefreshControl } from 'react-native';
import BookingRequestViews from '../Components/BookingRequestViews';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import ModalView from '../Components/ModalView';
import OpenDocument from '../Components/OpenDocument';
import Colors from '../Lib/Colors';
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import {Helper, Network, CallTenant} from '../Lib/index';
import { ApiCall,LoaderForList } from '../Api/index';
import { Constant } from '../Lib/Constant';
import Fonts from '../Lib/Fonts'
import { KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view' ;
import {translate} from '../Language'
let i  = 0;
export default class BookingRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestListItem: [],
      requestListItemOwner: [],
      modalVisibleAccept: false,
      modalVisibleDecline: false,
      modalVisibleDocument: false,
      userType: Helper.userType,
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
      front_id: '',
      back_id: '',
    };
  }

  componentDidMount() {
    //console.log('-----user type', Helper.userType);
    this.setState({
      userType: Helper.userType,
    },()=>{
     // this.getTenantBookingRequestData(true);
    });
    
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getTenantBookingRequestData(true);
    });
    this.listner = DeviceEventEmitter.addListener(
      'Call-getTenantBookingRequestData-Api',
      (data) => {
        this.getTenantBookingRequestData(true);
      },
    );
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  getTenantBookingRequestData = (already, loader) => {
    Network.isNetworkAvailable().then((isConnected) => {
      
      if (isConnected) {
        if (!this.state.refreshing) {
          this.setState({isLoading: true});
        }
        let url;
        const data = {
          status: 1,
        };
        if (this.state.userType == 'OWNER') {
          url = 'get-bookings-by-owner' + '?page=' + this.state.currentPage;
        } else {
          url = 'get-booking-by-tenant' + '?page=' + this.state.currentPage;
        }
        if (loader) {
          Helper.mainApp.showLoader();
        }
        // Helper.mainApp.showLoader()
      
        console.log('total render------in booking request',i++)
        ApiCall.ApiMethod({Url: url, method: 'POST', data: data})
          .then((res) => {
            if (res?.status) {
              if (this.state.userType == 'OWNER') {
                // alert(this.state.userType)
                Helper.mainApp.hideLoader();
                if (already) {
                  this.setState({
                    requestListItemOwner: [],
                  });
                }
                console.log('booking request  data', res?.data, url);
                if (res?.data?.data && res?.data?.data.length > 0) {
                  this.setState({
                    requestListItemOwner: [
                      ...this.state.requestListItemOwner,
                      ...res?.data?.data,
                    ],
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    emptymessage: false,
                  });
                  return;
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                    emptymessage: true,
                  });
                  return;
                }
              } else {
              // console.log('booking request tenat data', res?.data, url);
                //alert(this.state.userType)
                Helper.mainApp.hideLoader();
                if (already) {
                  this.setState({
                    requestListItem: [],
                  });
                }
                if (res?.data?.data && res?.data?.data.length > 0) {
                  this.setState({
                    requestListItem: [
                      ...this.state.requestListItem,
                      ...res?.data?.data,
                    ],
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    emptymessage: false,
                  });
                  return;
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                    emptymessage: true,
                  });
                  return;
                }
              }
            } else {
              this.setState({
                ext_page_url: res?.data?.next_page_url,
                isLoading: false,
                refreshing: false,
                emptymessage: true,
              });
              Helper.mainApp.hideLoader();
              //console.log('tenant fail data', res);
              return;
            }
          })
          .catch((err) => {
            Helper.mainApp.hideLoader();
           // console.log('api fail err', err);
          });
      }
    });
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: this.state.currentPage, refreshing: false},
        () => {
          this.getTenantBookingRequestData(true);
        },
      );
    }, 2000);
  };

  onScroll = () => {
    // console.log(
    //   'next page url',
    //   this.state.next_page_url,
    //   this.state.isLoading,
    // );
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getTenantBookingRequestData({loader: true});
      });
    }
  };

  openModelAccept = () => {
    return (
      <Modal
        animationType={'slide'}
        visible={this.state.modalVisibleAccept}
        transparent={true}
        onRequestClose={() => {
          this.setState({modalVisibleAccept: false});
        }}>
        <ModalView
          icon={images.success}
          title={'Accept'}
          color={Colors.kelleyGreen}
          subTitle={'Are you sure? \nYou want to Accept this Property'}
          onClickYess={() => {
            this.setState({modalVisibleAccept: false});
          }}
          onClickNo={() => {}}
          bttNoYess
        />
      </Modal>
    );
  };

  openModelDecline = () => {
    return (
      <Modal
        animationType={'slide'}
        visible={this.state.modalVisibleDecline}
        transparent={true}
        onRequestClose={() => {
          this.setState({modalVisibleDecline: false});
        }}>
        <ModalView
          icon={images.failed}
          title={'Decline'}
          color={Colors.cherryRed}
          subTitle={'Are you sure? \nYou want to Decline this Property'}
          onClickYess={() => {
            this.setState({modalVisibleDecline: false});
          }}
          onClickNo={() => {}}
          bttNoYess
        />
      </Modal>
    );
  };

  onDetails = (index) => {
    if (this.state.userType == 'OWNER') {
      handleNavigation({
        type: 'push',
        page: 'PropertyDetails',
        passProps: {bookingID: this.state.requestListItemOwner[index]?.id},
        navigation: this.props.navigation,
      });
    } else {
      // console.log(
      //   'tenant booking id',
      //   this.state.requestListItem[index]?.property?.id,
      // );
      handleNavigation({
        type: 'push',
        page: 'PropertyDetails',
        passProps: {propertyID:  this.state.requestListItem[index]?.property?.id},
        navigation: this.props.navigation,
      });
    }
  };

  onChats = (item) => {
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {
        userId:
          this.state.userType == 'OWNER'
            ? item?.user?.id
            : item?.property?.user?.id,
      },
      navigation: this.props.navigation,
    });
    // handleNavigation({ type: 'push', page: 'Chat', navigation: this.props.navigation });
  };

  onDocumentModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleDocument}
        onRequestClose={() => {
          this.setState({modalVisibleDocument: false});
        }}>
        <OpenDocument
          maodalHeid={() => {
            this.setState({modalVisibleDocument: false});
          }}
          front_id={this.state.front_id}
          back_id={this.state.back_id}
        />
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
      </Modal>
    );
  };
  acceptProperty = (index) => {
    // console.log(
    //   'accept id and status',
    //   this.state.requestListItemOwner[index],
    //   index,
    // );
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        const data = {
          booking_id: this.state.requestListItemOwner[index]?.id,
          status: 2,
        };
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({
          Url: 'accept-decline-booking',
          method: 'POST',
          data: data,
        })
          .then((res) => {
           // console.log('owner accepted res', res);
            Helper.mainApp.hideLoader();
            if (res?.status) {
              this.getTenantBookingRequestData(true, true);
              DeviceEventEmitter.emit('Call-Accept-Api', {
                type: 'Call-Accept-Api',
              });
            }
          })
          .catch((err) => {
            Helper.mainApp.hideLoader();
           // console.log('fail api', err);
          });
      }
    });
  };

  rejectProperty = (index) => {
    if (this.state.userType == 'OWNER') {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          const data = {
            booking_id: this.state.requestListItemOwner[index]?.id,
            status: 3,
          };
          Helper.mainApp.showLoader();
          ApiCall.ApiMethod({
            Url: 'accept-decline-booking',
            method: 'POST',
            data: data,
          })
            .then((res) => {
              Helper.mainApp.hideLoader();
              //console.log('owner reject property', res);
              if (res?.status) {
                this.getTenantBookingRequestData(true, true);
                DeviceEventEmitter.emit('Call-Reject-Api', {
                  type: 'Call-Reject-Api',
                });
              }
            })
            .catch((err) => {
              //console.log('api fail error');
            });
        }
      });
    } else {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          const data = {
            booking_id: this.state.requestListItem[index]?.id,
            status: 4,
          };
          Helper.mainApp.showLoader();
          ApiCall.ApiMethod({
            Url: 'accept-decline-booking',
            method: 'POST',
            data: data,
          })
            .then((res) => {
             // console.log('tenant reject property', res);
              Helper.mainApp.hideLoader();
              if (res?.status) {
                this.getTenantBookingRequestData(true, true);
              }else{
                Helper.showToast(translate('Somethingwentwrong,pleasetryagainlater'));
              }
            })
            .catch((err) => {
             // console.log('api err', err);
            });
        }
      });
    }
  };

  onCall = (item) => {
    //  console.log('call item', item?.user?.mobile_number);
    CallTenant(item?.user?.mobile_number);
  };
  renderRequestList = ({item, index}) => {
     //console.log('booking request data',item?.tenantdoc)
    return (
      <BookingRequestViews
        item={item}
        Button
        onClick1={() => {}}
        onClick2={() => {}}
        title={'View Details'}
        onClick1={() => {
          this.onDetails(index);
        }}
        subTitle={'Cancel'}
        onChat={() => {
          this.onChats(item);
        }}
        onCall={() => {
          this.onCall(item);
        }}
        AgreementBtt
        RejectProperty={() => {
          this.rejectProperty(index);
        }}
        titleOwner={'Decline'}
        onClickOwner={() => {
          this.setState({modalVisibleDecline: true});
        }}
        AcceptProperty={() => {
          this.acceptProperty(index);
        }}
        subTitleOwner={'Accept'}
        onClickOwner1={() => {
          this.setState({modalVisibleAccept: true});
        }}
        onClickAgreement={() => {
          this.setState(
            {
              //modalVisibleDocument: true,
              front_id: item?.tenantdoc[0]?.front_image_url,
              back_id: item?.tenantdoc[0]?.back_image_url,
            },
            () => {
              // console.log(
              //   'tenant doc',
              //   this.state.front_id,
              //   this.state.back_id,
              //   item?.tenantdoc,
              // );
              this.state.front_id && this.state.back_id
                ? this.setState({
                    modalVisibleDocument: true,
                  })
                : Helper.showToast(translate('Documentsnotuploaded'));
            },
          );
        }}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          bounces={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          //contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}>
          <FlatList
            style={{marginTop: 20}}
            data={
              this.state.userType == 'OWNER'
                ? this.state.requestListItemOwner
                : this.state.requestListItem
            }
            renderItem={this.renderRequestList}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={() => {
              return this.state.isLoading ? <LoaderForList /> : null;
            }}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false
                    ? null
                    : 'No request property'}
                </Text>
              </View>
            )}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
          />
          {this.openModelDecline()}
          {this.openModelAccept()}
          {this.onDocumentModal()}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.whiteTwo,},
});
