import React, { Component } from 'react'
import { Text, Image, View, StyleSheet, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native'
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import { Helper, Network, AlertMsg } from '../Lib/index';
import CameraController from '../Lib/CameraController';
import { ApiCall, LoaderForList } from '../Api/index';
import { handleNavigation } from '../navigation/routes';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import { translate } from '../Language';
export default class UploadDocuments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            frontid: '',
            profilePicture: '',
            backid: '',
        }
    }

    onSubmit = () => {
        Helper.showDocument = true;
        handleNavigation({ type: 'push', page: 'DocumentTabs', navigation: this.props.navigation, passProps: { 'type': 'uploadDocument' } });
    }

    frontidFunction = (type) => {
        CameraController.open((response) => {
            if (response.path) {
                this.setState({
                    frontid: response.path,
                    profilePicture: response.path
                });
            }
        });
    }
    backidFunction = (type) => {
        CameraController.open((response) => {
            if (response.path) {
                this.setState({
                    backid: response.path,
                    profilePicture: response.path
                });
            }
        });
    }

    SubmitId = () => {
        if (!this.state.frontid) {
            Helper.showToast(translate('Pleaseuploadfrontimage'))
            return false
        }
        if (!this.state.backid) {
          Helper.showToast(translate('Pleaseuploadbackimage'));
          return false;
        }
        Network.isNetworkAvailable().then((isConnected) => {
            if (isConnected) {
                let updateDoc = new FormData();
                if (this.state.frontid) {
                  updateDoc.append('front_image', {
                    uri: this.state.frontid,
                    name: 'front.jpg',
                    type: 'image/jpeg',
                  });
                }
                if (this.state.backid) {
                  updateDoc.append('back_image', {
                    uri: this.state.backid,
                    name: 'back.jpg',
                    type: 'image/jpeg',
                  });
                }
                Helper.mainApp.showLoader()
                ApiCall.ApiMethod({
                  Url: 'add-tenant-document',
                  method: 'POSTUPLOAD',
                  data: updateDoc,
                }).then((res) => {
                    Helper.mainApp.hideLoader();
                    console.log('tenat id is uploaded',res)
                    Helper?.showToast(res?.message)
                    if (res?.status) {
                        this.setState({ frontid: '', backid: '' }, () => {
                               this.props.navigation.goBack();
                        })
                    }
                }).catch((err) => {
                    Helper.mainApp.hideLoader();
                    console.log('err',err)
                });
            }
        })


    }
    render() {
        return (
          <View style={styles.container}>
            <StatusBarCustom
              backgroundColor={Colors.white}
              translucent={false}
            />

            <SafeAreaView style={styles.container}>
              <KeyboardAwareScrollView contentContainerStyle={{flex: 1}}>
                <Text style={styles.textTop}>{translate("UploadDocument")}</Text>
                <Text style={styles.textTop1}>{translate("AddFrontPartOfID")}</Text>
                <View style={styles.cardView}>
                  {this.state.frontid ? (
                    <TouchableOpacity
                      hitSlop={{right: 30, left: 30, top: 30, bottom: 30}}
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        right: -147,
                        top: 10,
                        zIndex: 5,
                        height: 15,
                        width: 15,
                        borderRadius: 15,
                        backgroundColor: Colors.cherryRed,
                      }}
                      onPress={() => this.setState({frontid: ''})}>
                      <Image
                        source={images.close}
                        style={{
                          tintColor: Colors.white,
                          height: 10,
                          width: 10,
                          borderRadius: 10,
                        }}
                      />
                    </TouchableOpacity>
                  ) : null}
                  <TouchableOpacity onPress={() => this.frontidFunction()}>
                    <Image
                      style={
                        this.state.frontid
                          ? styles.selectImgCard
                          : styles.imgCard
                      }
                      source={
                        this.state.frontid
                          ? {uri: this.state.frontid}
                          : images.front_back_id
                      }
                    />
                  </TouchableOpacity>
                  {this.state.frontid ? null : (
                    <>
                      <Text style={styles.textAdd}>{translate("ADD_GOVERNMENT_ID")}</Text>
                      <Text style={styles.textSide}>{translate("Front_ID")}</Text>
                    </>
                  )}
                </View>
                <Text style={styles.textTop1}>{translate("AddBackPartOfID")}</Text>
                <View style={styles.cardView}>
                  {this.state.backid ? (
                    <TouchableOpacity
                      hitSlop={{right: 30, left: 30, top: 30, bottom: 30}}
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        right: -150,
                        top: 10,
                        zIndex: 5,
                        height: 15,
                        width: 15,
                        borderRadius: 15,
                        backgroundColor: Colors.cherryRed,
                      }}
                      onPress={() => this.setState({backid: ''})}>
                      <Image
                        source={images.close}
                        style={{
                          tintColor: Colors.white,
                          height: 10,
                          width: 10,
                          borderRadius: 10,
                        }}
                      />
                    </TouchableOpacity>
                  ) : null}
                  <TouchableOpacity onPress={() => this.backidFunction()}>
                    <Image
                      style={
                        this.state.backid
                          ? styles.selectImgCard
                          : styles.imgCard
                      }
                      source={
                        this.state.backid
                          ? {uri: this.state.backid}
                          : images.front_back_id
                      }
                    />
                  </TouchableOpacity>
                  {this.state.backid ? null : (
                    <>
                      <Text style={styles.textAdd}>{translate("ADD_GOVERNMENT_ID")}</Text>
                      <Text style={styles.textSide}>{translate("Back_ID")}</Text>
                    </>
                  )}
                </View>
                {/* <View style={styles.viewNote}>
                  <Text style={styles.textNote}>Note:</Text>
                  <View style={styles.view2Fl}>
                    <View style={styles.dot} />
                    <Text style={styles.textDetails}>
                      It is a long established fact that a reader will be
                      distracted by the{'\n'} read able content of a page
                      layout.
                    </Text>
                  </View>
                </View> */}

                <View style={styles.viewButton}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()}
                    style={styles.buttonStyle}>
                    <Text style={styles.textCancel}>{translate("Cancel")}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.SubmitId()}
                    style={styles.buttonStyle}>
                    <Text style={styles.textSubmit}>{translate("Submit")}</Text>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </SafeAreaView>
          </View>
        );
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, },
    textTop: { marginLeft: 21, marginTop: 10, fontSize: 18, lineHeight: 25, color: Colors.black, fontFamily: Fonts.segoeui_bold },
    textTop1: { marginLeft: 21, marginTop: 15, fontSize: 14, lineHeight: 25, color: Colors.blackTwo, fontFamily: Fonts.segui_semiBold },
    cardView: { borderColor: Colors.whiteFive, borderWidth: 1, marginHorizontal: 21, borderRadius: 4, alignItems: 'center', paddingVertical: 25, marginTop: 5 },
    imgCard: { width: 85, height: 60, resizeMode: 'contain' },
    selectImgCard: { width: 300, height: 70 },
    textAdd: { fontSize: 11, lineHeight: 24, color: Colors.pinkishGrey, fontFamily: Fonts.segui_semiBold, marginTop: 10 },
    textSide: { fontSize: 16, lineHeight: 25, color: Colors.black, fontFamily: Fonts.segui_semiBold, marginTop: 10 },
    viewNote: { borderColor: Colors.whiteFive, borderWidth: 1, borderStyle: 'dotted', marginHorizontal: 21, borderRadius: 2, marginTop: 15, paddingBottom: 10 },
    textNote: { fontSize: 12, lineHeight: 12, color: Colors.cherryRed, fontFamily: Fonts.segoeui, marginLeft: 13, marginTop: 7 },
    textDetails: { fontSize: 10, lineHeight: 10, fontFamily: Fonts.segoeui, marginLeft: 8 },
    viewButton: { flexDirection: 'row', bottom: 0, position: 'absolute' },
    buttonStyle: { bottom: 5, paddingVertical: 15, flex: 0.5, borderWidth: 0.5, alignItems: 'center', borderColor: 'rgba(112,112,112,0.18)' },
    textCancel: { fontSize: 18, lineHeight: 22, color: Colors.brownishGreyTwo, fontFamily: Fonts.segui_semiBold },
    textSubmit: { fontSize: 18, lineHeight: 22, color: Colors.darkSeafoamGreen, fontFamily: Fonts.segui_semiBold },
    view2Fl: { flexDirection: 'row', marginHorizontal: 13, marginTop: 5 },
    dot: { width: 4, height: 4, borderRadius: 2, marginTop: 2, backgroundColor: Colors.pinkishGrey },
});