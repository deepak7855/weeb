import React from 'react';
import { Text, View, StyleSheet, ScrollView, Platform, Dimensions } from 'react-native';
// import colors from '../common/ColorsCode';
// import Header from '../common/AppHeader';
// import Fonts from '../assets/Fonts';
// import Helper from "../common/Helper";

import NetInfo from "@react-native-community/netinfo";
import AlertMsg from '../Lib/AlertMsg';
//import HTML from 'react-native-render-html';
import { WebView } from 'react-native-webview';
import Config from '../Lib/Config';
import AppHeader from '../Components/AppHeader';
import images from '../Lib/Images'
import { handleNavigation } from '../navigation/routes';
export default class TermAndPrivacy extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: props.route.params.title
        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.leftNavigation() },
            title: props.route.params.title,
            search: true
        });
    }
    leftNavigation = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }
   

    render() {
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: 'white' }}>
                   
                    <View style={{ paddingTop: 15 }}>
                        {/* <HTML html={this.state.page_data} /> */}
                        <WebView
                            source={{ uri:this.props.route.params.url }}
                            
                            style={{ marginVertical: 10, height: Dimensions.get('screen').height - 100, width: Dimensions.get('screen').width - 40 }}
                            //onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                            startInLoadingState
                            androidHardwareAccelerationDisabled={true}
                        />
                        {/* <WebView
                   source={{ uri:this.props.route.params.url }}
                    javaScriptEnabled={true}
                    originWhitelist={['*']}
                    domStorageEnabled={true}
                    androidHardwareAccelerationDisabled={true}
                    scrollEnabled
                    scalesPageToFit
                    zoomable={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    contentMode={'mobile'}
                    containerStyle={{ paddingHorizontal: 5, backgroundColor: '#fff'}}
                    style={{}}
                /> */}
                    </View>


                </ScrollView>
            </View>
        )
    }
};
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'white', paddingHorizontal: 25 },
})