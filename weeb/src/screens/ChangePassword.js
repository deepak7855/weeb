import React, { Component } from 'react';
import { View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../Components/AppHeader';
import { Inputs } from '../Components/Common';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import { Network, Validation, AlertMsg } from '../Lib/index';
import Helper from '../Lib/Helper'
import { ApiCall } from '../Api/index';
import { Constant } from '../Lib/Constant';
import { translate } from '../Language';
export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPass: '',
            nwePass: '',
            reEnterPass: '',
            isCurrentPassword: true,
            isNewPassword: true,
            isRe_enterPassword: true
        };
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Change Password',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    changePasswordCall = () => {
        if (Validation.checkNotNull('Current Password', 3, 25, this.state.currentPass) && Validation.checkNotNull('New Password', 3, 25, this.state.nwePass) && Validation.checkMatch('Password', this.state.nwePass, this.state.reEnterPass)) {
            if (!Validation.checkShouldNotBeMatch('Current and new Password', this.state.currentPass, this.state.nwePass)) return false;
            let changePassword = {
                'current_password': this.state.currentPass,
                'new_password': this.state.nwePass,
            }


            Network.isNetworkAvailable().then((isConnected) => {
                if (isConnected) {
                    Helper.mainApp.showLoader();
                    ApiCall.ApiMethod({ Url: 'change-password', method: 'POST', data: changePassword }).then((res) => {
                        console.log('change password res', res);
                        if (res.status) {
                            Helper.mainApp.hideLoader()
                            Helper.showToast(res?.message)
                            this.goBack();
                        } else {
                            Helper.mainApp.hideLoader()

                            Helper.showToast(res?.message)
                            return;
                        }
                        Helper.mainApp.hideLoader()
                    }).catch((err) => {

                        Helper.mainApp.hideLoader()
                        console.log('change password api fail', err);
                    })
                } else {
                    Helper.mainApp.hideLoader()
                    Helper.showToast(AlertMsg.error.NETWORK);
                }
            }).catch((err) => {
                console.log('err', err)
            })

        }
    }

    render() {
        const { isCurrentPassword, isNewPassword, isRe_enterPassword } = this.state
        return (
            <View style={styles.container}>
                <StatusBarCustom backgroundColor={Colors.lightMint10} translucent={false} />
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ flexGrow: 1 }} extraHeight={0} >
                    <View style={{ backgroundColor: Colors.lightMint, height: 150, justifyContent: 'center' }}>
                        <Image style={{ height: 100, width: "80%", resizeMode: 'contain', alignSelf: 'center', }} source={images.change_password_img} />
                    </View>
                    <View style={{ marginTop: 30, marginHorizontal: 15 }}>
                        <Inputs
                            labels={translate("CurrentPassword")}
                            width={'90%'}
                            backgroundcolor={Colors.whiteTwo}
                            onTbShow={() => { this.setState({ isCurrentPassword: !isCurrentPassword }) }}
                            leftimage={isCurrentPassword ? images.eye : images.eye_off}
                            color={Colors.brownishGrey}
                            placeholder={translate("EnterCurrentPassword")}
                            marginleft={-30}
                            keyboard={'default'}
                            onChangeText={(text) => { this.setState({ currentPass: text }) }}
                            value={this.state.currentPass}
                            password={isCurrentPassword}
                            setfocus={(input) => {
                                this.currentPass = input;
                            }}
                            getfocus={() => {
                                this.nwePass.focus();
                            }}
                            returnKeyType={'next'}
                            bluronsubmit={false}
                        />
                    </View>
                    <View style={{ marginHorizontal: 15 }}>
                        <Inputs
                            labels={translate("NewPassword")}
                            width={'90%'}
                            backgroundcolor={Colors.whiteTwo}
                            onTbShow={() => { this.setState({ isNewPassword: !isNewPassword }) }}
                            leftimage={isNewPassword ? images.eye : images.eye_off}
                            color={Colors.brownishGrey}
                            placeholder={translate("EnterNewPassword")}
                            marginleft={-30}
                            keyboard={'default'}
                            onChangeText={(text) => { this.setState({ nwePass: text }) }}
                            value={this.state.nwePass}
                            password={isNewPassword}
                            returnKeyType={'next'}
                            setfocus={(input) => {
                                this.nwePass = input;
                            }}
                            getfocus={() => {
                                this.reEnterPass.focus();
                            }}
                            bluronsubmit={false}
                        />
                    </View>
                    <View style={{ marginHorizontal: 15 }}>
                        <Inputs
                            labels={translate("ReEnterPassword")}
                            width={'90%'}
                            backgroundcolor={Colors.whiteTwo}
                            onTbShow={() => { this.setState({ isRe_enterPassword: !isRe_enterPassword }) }}
                            leftimage={isRe_enterPassword ? images.eye : images.eye_off}
                            color={Colors.brownishGrey}
                            placeholder={translate("EnterReEnterPassword")}
                            marginleft={-30}
                            keyboard={'default'}
                            onChangeText={(text) => { this.setState({ reEnterPass: text }) }}
                            value={this.state.reEnterPass}
                            password={isRe_enterPassword}
                            returnKeyType={'done'}
                            setfocus={(input) => {
                                this.reEnterPass = input;
                            }}
                            bluronsubmit={true}
                        />
                    </View>
                    <View style={styles.btnView}>
                        <TouchableOpacity onPress={() => { this.changePasswordCall() }}>
                            <Text style={styles.btnText}>{translate("ChangePassword")}</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
    btnView: { flex: 1, paddingVertical: 10, borderWidth: 0.3, width: '100%', borderColor: Colors.white, justifyContent: 'flex-end', bottom: 10 },
    btnText: { fontSize: 18, fontFamily: Fonts.segui_semiBold, color: Colors.darkSeafoamGreen, textAlign: 'center' }

});
