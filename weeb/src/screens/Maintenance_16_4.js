import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, FlatList, TouchableOpacity, ScrollView } from 'react-native'
import AppHeader from '../Components/AppHeader';
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import {Constant} from '../Lib/Constant';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import {
    ProgressiveImage,
  } from '../Components/Common/index';
import moment from 'moment';
import { translate } from '../Language';
export default class Maintenance_16_4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOfMaintenence_16_2: [
                { issueID: 'WB1464', time: '21 Jan 2021 | 10:15 pm', type: 'Water', detail: 'It is a long established fact that a reader will be distracted \nby the readable content of a page layout.', status: 'Status', progress: 'Incomplete', color: Colors.marigold ,color1:Colors.darkSeafoamGreen},
                { issueID: 'WB1464', time: '21 Jan 2021 | 10:15 pm', type: 'Electricity', detail: 'It is a long established fact that a reader will be distracted \nby the readable content of a page layout.', status: 'Status', progress: 'Inprocess', color: Colors.whiteTwo,backgroundColor:Colors.marigold,color1:Colors.darkSeafoamGreen },
                { issueID: 'WB1464', time: '21 Jan 2021 | 10:15 pm', type: 'Plumber', detail: 'It is a long established fact that a reader will be distracted \nby the readable content of a page layout.', status: 'Status', progress: 'Complete', color: Colors.whiteTwo,backgroundColor:Colors.marigold,backgroundColor1:Colors.darkSeafoamGreen,color1:Colors.whiteTwo },
            ],
            propertyData:this.props.route.params?.propertyData,
        }
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Maintenance',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    getRequest = () =>{
        Network.isNetworkAvailable().then((isConnected) => {
            if(isConnected){
             const data = {
                 property_id: this.props.route.params?.propertyData?.id,
               };
             ApiCall.ApiMethod({
                 Url: 'owner-rented-property-detail',
                 method: 'POST',
                 data: data,
               }).then((res) => {
                // Helper.mainApp.hideLoader();
               //  console.log('res',res?.data?.maintenance_request);
                 if (res?.status) {
                  
                   this.setState({
                     dataOfMaintenence_16_2: res?.data?.maintenance_request,
                   });
                 }
               });
            }
        }) 
    }

componentDidMount(){
    this.getRequest()
}

    goBack=()=>{
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }
    changeStatus=(id,status)=>{
        Network.isNetworkAvailable().then((isConnected) => {
            if(isConnected){
                Helper.mainApp.showLoader();
             const data = {
                request_id: id,
                status:status,
               };
             ApiCall.ApiMethod({
                 Url: 'update-maintenance-request-status',
                 method: 'POST',
                 data: data,
               }).then((res) => {
                Helper.mainApp.hideLoader();
                 console.log('res',res);
                 if (res?.status) {
                   Helper.showToast(translate('StatusisChanged'))
                   this.getRequest()
                 }
               }).catch((err)=>{
                   console.log(err)
                Helper.mainApp.hideLoader();
               })
            }
        }) 
    }
    renderOfCard = ({ item }) => {
        console.log('status item',item)
        return (
            <View>
                <View style={styles.view1Fl}>
                    <Text style={[styles.textIssue, { flex: 1 }]}>{translate("IssueID")}: {item?.id}</Text>
                    <Image style={styles.iconTime} source={images.clock} />
                    <Text style={styles.textIssue}>{moment(item?.created_at).format('YYYY-MM-DD h:mm a')}</Text>
                </View>
                <View style={styles.card}>
                    <View style={styles.view1Card}>
                        <Text style={styles.textTypeOfProp}>{item.type}</Text>
                    </View>
                    <Text style={styles.textDetails}>{item.description}</Text>
                    <View style={styles.viewStatus}>
                        <TouchableOpacity
                        onPress={() =>this.changeStatus(item?.id,1)}
                        style={[styles.TouchInprogress,{backgroundColor:item?.status == 1? Colors.marigold:Colors.whiteTwo}]}>
                            <Text style={[styles.textStatus,{color:item?.status == 1? Colors.white:item?.status == 1?Colors.marigold:Colors.marigold}]}>{translate("Inprogess")}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                           onPress={() =>this.changeStatus(item?.id,2)}
                        style={[styles.TouchComp,{backgroundColor:item?.status == 2? Colors.darkSeafoamGreen:Colors.whiteTwo}]}>
                            <Text style={[styles.textStatus,{color:item?.status == 2? Colors.white:item?.status == 1?Colors.darkSeafoamGreen:Colors.darkSeafoamGreen}]}>{translate("Complete")}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
    topBar = () => {
        return (
            <View style={{backgroundColor: Colors.lightMint, height:100}}>
                <View style={[styles.viewTop, {marginTop:0}]}>
                <ProgressiveImage
          source={{
            uri: this.state.propertyData?.propertyphotos
              ? this.state.propertyData?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.iconHome}
          resizeMode="cover"
        />
                <View style={{ marginLeft: 13 }}>
                    <Text style={styles.textTitle}>{this.state.propertyData?.title}</Text>
                    <View style={styles.view3}>
                        <Image style={styles.iconLocation} source={images.location} />
                        <Text style={styles.textAddress}>{this.state.propertyData?.location}</Text>
                    </View>
                    <View style={styles.view4}>
                        <Text style={styles.textRate}>KWD {this.state.propertyData?.rent}</Text>
                        <Text style={[styles.textRate, { fontFamily: Fonts.segoeui }]}>/m</Text>
                        <View style={styles.dot} />
                        <Text style={styles.textPropType}>{this.state.propertyData?.type}</Text>
                    </View>
                </View>
            </View>
            </View>
        )
    }
    render() {
        //console.log('propertyData',this.state.propertyData)
        return (
            <View style={styles.container}>
                {this.topBar()}
                <View style={styles.viewMaintenance}>
                    <Text style={styles.textMaint}>{translate("MaintenanceRequests")}</Text>
                    <Text style={styles.text150}>{this.state.dataOfMaintenence_16_2.length}</Text>
                </View>
                <FlatList
                    data={this.state.dataOfMaintenence_16_2}
                    renderItem={this.renderOfCard}
                    extraData={this.state} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 10 },
    viewMaintenance: { alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: 12, paddingTop: 10 },
    textMaint: { fontSize: 16, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, },
    text150: { fontSize: 16, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, },
    view1Fl: { marginTop: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 14 },
    textIssue: { fontFamily: Fonts.segoeui, fontSize: 12, color: Colors.brownishGreyTwo },
    iconTime: { width: 9, height: 9, resizeMode: 'contain' },
    card: { borderWidth: 1, marginHorizontal: 12, borderRadius: 4, borderColor: Colors.whiteFive, marginTop: 11 },
    view1Card: { backgroundColor: Colors.whiteNine, paddingLeft: 18, paddingTop: 7, paddingBottom: 12 },
    textTypeOfProp: { fontSize: 16, color: Colors.blackTwo, fontFamily: Fonts.segui_semiBold },
    textDetails: { marginLeft: 18, marginTop: 10, marginRight: 15, fontSize: 12, lineHeight: 22, fontFamily: Fonts.segoeui },
    viewStatus: { flexDirection: 'row', marginRight: 15, marginLeft: 18, marginVertical: 15,alignItems:'center' },
    textStatus: { fontSize: 12,fontFamily: Fonts.segui_semiBold, },
    viewTop: { flexDirection: 'row', paddingLeft: 14, paddingVertical: 25,  },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold },
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { fontSize: 12, lineHeight: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 },
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, fontFamily: Fonts.segoeui_bold, color: Colors.darkSeafoamGreen },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, color: Colors.marigold, fontFamily: Fonts.segui_semiBold },
    TouchInprogress:{ borderWidth: 0.5, borderColor: Colors.marigold,  borderRadius: 2,paddingHorizontal:20,paddingBottom:2},
    TouchComp:{ borderWidth: 0.5, borderColor: Colors.darkSeafoamGreen, marginLeft: 10, borderRadius: 2 ,paddingHorizontal:20,paddingBottom:2}
})