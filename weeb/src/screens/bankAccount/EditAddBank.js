import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import images from '../../Lib/Images';
import AppHeader from '../../Components/AppHeader';
import {handleNavigation} from '../../navigation/routes';
import RNPickerSelect from 'react-native-picker-select';
import CameraController from '../../Lib/CameraController';
import Helper from '../../Lib/Helper';
import { translate } from '../../Language';
import {ApiCall} from '../../Api/index';
import {Validation, AlertMsg, Network} from '../../Lib/index';
import {Constant} from '../../Lib/Constant';
import {DatePicker,ProgressiveImage} from '../../Components/Common/index';
import moment from 'moment';
function BankInput(props) {
  return (
    <View style={styles.inptMailViewOfCss}>
      <Text style={styles.labelOfCss}>{props.label}</Text>
      <View style={styles.inputOfCssView}>
        <TextInput
          style={styles.inputTextOfCss}
          placeholder={props.placeholder}
          placeholderTextColor={props.placeholderTextColor}
          keyboardType={props.keyboardType}
          returnKeyType={props.returnKeyType}
          maxLength={props.maxlength}
          onChangeText={props.onChangeText}
          value={props.value}
          secureTextEntry={props.secureTextEntry}
          ref={props.setfocus}
          onSubmitEditing={props.getfocus}
          editable={props.editable}
          fontFamily={props.fontFamily}
        />
      </View>
    </View>
  );
}

export default class EditAddBank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      branchName: this.props.route.params?.data
        ? this.props.route.params?.data?.branch_name
        : '',
      accountNumber: this.props.route.params?.data
        ? this.props.route.params?.data?.account_number
        : '',
      accountHolderName: this.props.route.params?.data
        ? this.props.route.params?.data?.account_name
        : '',
      iFSCCode: this.props.route.params?.data
        ? this.props.route.params?.data?.ifsc
        : '',
      isReadyBtt: false,
      type: this.props.route.params?.type ? this.props.route.params?.type : null,
      setSelectedValue: [
        {label: 'Icici Bank', value: 'Icici Bank'},
        {label: 'SBI Bank', value: 'SBI Bank'},
        {label: 'HDFC Bank', value: 'HDFC Bank'},
      ],
      selectedId: this.props.route.params?.data
        ? this.props.route.params?.data?.bank_name
        : '',
      commercialFront: '',
      commercialBack: this.props.route.params?.data ?this.props.route.params?.data?.documents?.files[1]:'',
      identityFront: '',
      identityBack:this.props.route.params?.data ?this.props.route.params?.data?.identification?.files[1]:'',
      commercialCardNumber: this.props.route.params?.data ?this.props.route.params?.data?.documents?.number:'',
      identityCardNumber: this.props.route.params?.data ?this.props.route.params?.data?.identification?.number:'',
      commercialCardIssueData: this.props.route.params?.data ?this.props.route.params?.data?.documents?.issuing_date:'',
      commercialCardExpirayData: this.props.route.params?.data ?this.props.route.params?.data?.documents?.expiry_date:'',
      identityCardIssueData: '',
      identityCardExpiryData: '',
      bankNameList: '',
      setCountryValue: [{label: 'KWD', value: 'KWD'}],
      selectedCountry:this.props.route.params?.data ?this.props.route.params?.data?.identification?.issuing_country:'',
      selectedIdentityIssueDate: this.props.route.params?.data ?this.props.route.params?.data?.identification?.issuing_date:'',
      selectedIdentityExpirayDate:this.props.route.params?.data ?this.props.route.params?.data?.identification?.expiry_date:'',
      identityFrontPic:this.props.route.params?.data ?this.props.route.params?.data?.identification?.files[0]:'',
      commecialselectedCountry: this.props.route.params?.data ?this.props.route.params?.data?.documents?.issuing_country:'',
      commecialFrontPic:this.props.route.params?.data ?this.props.route.params?.data?.documents?.files[0]:'',
      identitFrontPicId:'',
      identitBackPicId:'',
      commecialFrontPicId:'',
      commecialBackPicId:'',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Add Bank',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  componentDidMount() {
    this.getBankNameList();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.setState({type: this.props.route.params.type});
    });
  }

  getBankNameList = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        //Helper.mainApp.showLoader();
        ApiCall.ApiMethod({Url: 'get-bank-names', method: 'GET'})
          .then((res) => {
            //console.log('bank list api', res);
            Helper.mainApp.hideLoader();
            // Helper.showToast(res?.message);
            if (res?.status) {
              let tampbank = [];
              res?.data.map((list) => {
                // console.log('list',list)
                tampbank.push({label: list?.name, value: list?.name});
              });
              this.setState({
                bankNameList: tampbank,
              });
            }
          })
          .catch((err) => {
            console.log(err);
            Helper.mainApp.hideLoader();
          });
      }
    });
  };

  checkIsFilled = () => {
    return (
      this.state.branchName == '' ||
      this.state.accountNumber == '' ||
      this.state.accountHolderName == '' ||
      this.state.iFSCCode == '' ||
      this.state.identitBackPicId == '' ||
      this.state.commecialBackPicId == ''
    );
  };

  goAddBankList = () => {
    handleNavigation({
      type: 'push',
      page: 'AddBankList',
      navigation: this.props.navigation,
    });
  };

  addBank = () => {
  
    if (true) {
      Network.isNetworkAvailable()
        .then((isConnected) => {
          if (isConnected) {
// const addBank =  {
// 	"bank_name":this.state.selectedId,
// 	"branch_name":this.state.branchName,
// 	"account_name":this.state.accountHolderName,
// 	"account_number":this.state.accountNumber,
// 	"ifsc":this.state.iFSCCode,
// 	"is_edit":this.props.route.params.type == 'EDIT' ?this.props.route.params?.data?.id:0,
// 	"documents":{"type":"CommercialRegistration","number":this.state.commercialCardNumber,"issuing_country":this.state.commecialselectedCountry,"issuing_date":this.state.commercialCardIssueData,"expiry_date":this.state.commercialCardExpirayData,"front":this.state.commecialFrontPicId,"back":this.state.commecialBackPicId},
// 	"identification":{"type":"CommercialRegistration","number":this.state.identityCardNumber,"issuing_country":this.state.selectedCountry,"issuing_date":this.state.identityCardIssueData,"expiry_date":this.state.identityCardExpiryData,"front":this.state.identitFrontPicId,"back":this.state.identitBackPicId}
// }
            let addBank = new FormData();

            addBank.append('bank_name', this.state.selectedId);
            addBank.append('branch_name', this.state.branchName);
            addBank.append('account_name', this.state.accountHolderName);
            addBank.append('account_number', this.state.accountNumber);
            addBank.append('ifsc', this.state.iFSCCode);
            if (this.props.route.params.type == 'EDIT') {
              addBank.append('is_edit', this.props.route.params?.data?.id);
            } else {
              addBank.append('is_edit', 0);
            }
            // const CommercialRegistration={
            //   type:"CommercialRegistration",
            //  number:this.state.commercialCardNumber, 
            //  issuing_country:this.state.commecialselectedCountry,
            //   issuing_date:this.state.commercialCardIssueData, 
            //   expiry_date:this.state.commercialCardExpirayData, 
            //   front:this.state.commecialFrontPicId, 
            //   back:this.state.commecialBackPicId,
            // }
            addBank.append('documents[type]',"CommercialRegistration");
            addBank.append('documents[number]',this.state.commercialCardNumber);
            addBank.append('documents[issuing_country]',this.state.commecialselectedCountry);
            addBank.append('documents[issuing_date]',this.state.commercialCardIssueData);
            addBank.append('documents[expiry_date]',this.state.commercialCardExpirayData);
            addBank.append('documents[front]',this.state.commecialFrontPicId);
            addBank.append('documents[back]',this.state.commecialBackPicId);

            // const IdentityCard = {
            //   type:"IdentityCard",
            //   number:this.state.identityCardNumber, 
            //   issuing_country:this.state.selectedCountry,
            //    issuing_date:this.state.selectedIdentityIssueDate, 
            //    expiry_date:this.state.selectedIdentityExpirayDate, 
            //    front:this.state.identitFrontPicId, 
            //    back:this.state.identitBackPicId,
            // }
            addBank.append('identification[type]',"CommercialRegistration");
            addBank.append('identification[number]',this.state.identityCardNumber);
            addBank.append('identification[issuing_country]',this.state.selectedCountry);
            addBank.append('identification[issuing_date]',this.state.selectedIdentityIssueDate);
            addBank.append('identification[expiry_date]',this.state.selectedIdentityExpirayDate);
            addBank.append('identification[front]',this.state.identitFrontPicId);
            addBank.append('identification[back]',this.state.identitBackPicId);
          //  addBank.append('identification',IdentityCard)
           addBank.append('doc_front',{
             uri:this.state.commecialFrontPic,
             name:'doc.jpg',
             type:'image/jpeg',
           })
           addBank.append('doc_back',{
            uri:this.state.commercialBack,
            name:'doc.jpg',
            type:'image/jpeg',
          })
          addBank.append('identi_front',{
            uri:this.state.identityFrontPic,
            name:'doc.jpg',
            type:'image/jpeg',
          })
          addBank.append('identi_back',{
            uri:this.state.identityBack,
            name:'doc.jpg',
            type:'image/jpeg',
          })
            console.log('---------------form data', addBank);
            Helper.mainApp.showLoader();
            ApiCall.ApiMethod({
              Url: 'add-bank',
              method: 'POSTUPLOAD',
              data: addBank,
            })
              .then((response) => {
                console.log('add bank response', response);

                if (response?.status) {
                  Helper.showToast(
                    response?.message
                      ? response?.message
                      : AlertMsg?.success?.ADDBANK,
                  );
                  Helper.mainApp.hideLoader();
                  this.goBack();
                  return;
                } else {
                  Helper.mainApp.hideLoader();
                  // Helper.showToast(
                  //   response?.message
                  //     ? response?.message
                  //     : AlertMsg.error.NETWORK,
                  // );
                  return;
                }
              })
              .catch((error) => {
                // Helper.showToast(response?.message ? response?.message : AlertMsg.error.NETWORK)
                Helper.mainApp.hideLoader();
                console.log('edit profile error', error);
                return;
              });
          } else {
            Helper.mainApp.hideLoader();
            Helper.showToast(AlertMsg.error.NETWORK);
            return;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  getImage = () => {
    CameraController.open((response) => {
      if (response.path) {
        console.log('image path', response.path);
      }
    });
  };

  date = (val) => {
    console.log(
      'date return',
      val,
      moment(val).format('YYYY-MM-DD'),
      moment(val).format('DD-MM-YYYY'),
    );
    this.setState({
      selectedIdentityIssueDate: moment(val).format('YYYY-MM-DD'),
    });
  };

  expirydate = (val) => {
    console.log(
      'date return',
      val,
      moment(val).format('YYYY-MM-DD'),
      moment(val).format('DD-MM-YYYY'),
    );
    this.setState({
      selectedIdentityExpirayDate: moment(val).format('YYYY-MM-DD'),
    });
  };

  commercialdate = (val) => {
    console.log(
      'date return',
      val,
      moment(val).format('YYYY-MM-DD'),
      moment(val).format('DD-MM-YYYY'),
    );
    this.setState({
      commercialCardIssueData: moment(val).format('YYYY-MM-DD'),
    });
  };

  commecialexpirydate = (val) => {
    console.log(
      'date return',
      val,
      moment(val).format('YYYY-MM-DD'),
      moment(val).format('DD-MM-YYYY'),
    );
    this.setState({
      commercialCardExpirayData: moment(val).format('YYYY-MM-DD'),
    });
  };

  uploadFrontIdentityCard = async () => {
    CameraController.open(async (response) => {
      console.log('image path', response.path);
      if (await response.path) {
        this.setState({
          identityFrontPic: response.path,
        });

        Network.isNetworkAvailable().then((isConnected) => {
          let formdata = new FormData();
        formdata.append('file', {
            uri: response.path,
            name: "test.jpg",
            type: "	image/jpeg",
        });
        formdata.append('purpose', 'identity_document');
        formdata.append('file_link_create', true);

        var varheaders = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            "Authorization":'Bearer sk_test_NUKx2SJj6m3otYDI7infTXuL'
        }

        console.log(formdata,"formdata")
        Helper.mainApp.showLoader()
        return fetch('https://api.tap.company/v2/files', {
            body: formdata,
            method: 'POST',
            headers: varheaders,
        })
            .then((response) => {
                return response.json();
            })
            .then((responseJson) => {
              Helper.mainApp.hideLoader()
                console.log(responseJson, "responseJson")
                this.setState({
                  identitFrontPicId:responseJson?.id
                })
            })
            .catch((error, a) => {
                Helper.showToast('Error in Data Fetching!', 'error');
                Helper.mainApp.hideLoader()
                console.log("Error---->  ", error);
                return false;
            });

        });
      }
    });
  };

  uploadBackIdentityCard = async () => {
    CameraController.open(async (response) => {
      console.log('image path', response.path);
      if (await response.path) {
        this.setState({
          identityBack: response.path,
        });

        Network.isNetworkAvailable().then((isConnected) => {
          let formdata = new FormData();
        formdata.append('file', {
            uri: response.path,
            name: "test.jpg",
            type: "	image/jpeg",
        });
        formdata.append('purpose', 'identity_document');
        formdata.append('file_link_create', true);

        var varheaders = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            "Authorization":'Bearer sk_test_NUKx2SJj6m3otYDI7infTXuL'
        }

        console.log(formdata,"formdata")
        Helper.mainApp.showLoader()
        return fetch('https://api.tap.company/v2/files', {
            body: formdata,
            method: 'POST',
            headers: varheaders,
        })
            .then((response) => {
                return response.json();
            })
            .then((responseJson) => {
              Helper.mainApp.hideLoader()
                console.log(responseJson, "responseJson")
                this.setState({
                  identitBackPicId:responseJson?.id
                })
            })
            .catch((error, a) => {
                Helper.showToast('Error in Data Fetching!', 'error');
                Helper.mainApp.hideLoader()
                console.log("Error---->  ", error);
                return false;
            });

        });
      }
    });
  };

  uploadFrontCommecialCard = () => {
    CameraController.open(async (response) => {
      console.log('image path', response.path);
      if (await response.path) {
        this.setState({
          commecialFrontPic: response.path,
        });

        Network.isNetworkAvailable().then((isConnected) => {
          let formdata = new FormData();
        formdata.append('file', {
            uri: response.path,
            name: "test.jpg",
            type: "	image/jpeg",
        });
        formdata.append('purpose', 'identity_document');
        formdata.append('file_link_create', true);

        var varheaders = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            "Authorization":'Bearer sk_test_NUKx2SJj6m3otYDI7infTXuL'
        }
        Helper.mainApp.showLoader()
        console.log(formdata,"formdata")
        return fetch('https://api.tap.company/v2/files', {
            body: formdata,
            method: 'POST',
            headers: varheaders,
        })
            .then((response) => {
                return response.json();
            })
            .then((responseJson) => {
              Helper.mainApp.hideLoader()
                console.log(responseJson, "responseJson")
                this.setState({
                  commecialFrontPicId:responseJson?.id
                })
            })
            .catch((error, a) => {
                Helper.showToast('Error in Data Fetching!', 'error');
                Helper.mainApp.hideLoader()
                console.log("Error---->  ", error);
                return false;
            });

        });
      }
    });
  };

  uploadbackCommecialCard = () => {
    CameraController.open(async (response) => {
      console.log('image path', response.path);
      if (await response.path) {
        this.setState({
          commercialBack: response.path,
        });

        Network.isNetworkAvailable().then((isConnected) => {
          let formdata = new FormData();
        formdata.append('file', {
            uri: response.path,
            name: "test.jpg",
            type: "	image/jpeg",
        });
        formdata.append('purpose', 'identity_document');
        formdata.append('file_link_create', true);

        var varheaders = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            "Authorization":'Bearer sk_test_NUKx2SJj6m3otYDI7infTXuL'
        }

        console.log(formdata,"formdata")
        Helper.mainApp.showLoader()
        return fetch('https://api.tap.company/v2/files', {
            body: formdata,
            method: 'POST',
            headers: varheaders,
        })
            .then((response) => {
                return response.json();
            })
            .then((responseJson) => {
              Helper.mainApp.hideLoader()
                console.log(responseJson, "responseJson")
                this.setState({
                  commecialBackPicId:responseJson?.id
                })
            })
            .catch((error, a) => {
                Helper.showToast('Error in Data Fetching!', 'error');
                Helper.mainApp.hideLoader()
                console.log("Error---->  ", error);
                return false;
            });

        });
      }
    });
  };



  render() {
    //console.log('this.props.route.params?.data',JSON.parse(this.props.route.params?.data?.documents))
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          contentContainerStyle={{flexGrow: 1}}>
          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Bank Name
          </Text>
          <View style={styles.viewDropdown}>
            <RNPickerSelect
              items={this.state.bankNameList}
              // useNativeAndroidPickerStyle={true}

              placeholder={{label: 'Select Bank', value: null}}
              onValueChange={(val) => {
                this.setState({selectedId: val});
              }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={images.down_arrow}
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 15,
                      tintColor: 'black',
                      marginRight: 20,
                    }}
                  />
                );
              }}
              value={this.state.selectedId}
              style={pickerSelectStyles}
            />
          </View>

          <BankInput
            label={translate('BranchName')}
            placeholder={translate('BranchName')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'default'}
            returnKeyType={'next'}
            onChangeText={(text) => {
              this.setState({branchName: text});
            }}
            value={this.state.branchName}
            setfocus={(input) => {
              this.branchName = input;
            }}
            getfocus={() => {
              this.accountNumber.focus();
            }}
          />

          <BankInput
            label={translate('AccountNumber')}
            placeholder={translate('AccountNumber')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'number-pad'}
            returnKeyType={'next'}
            onChangeText={(text) => {
              this.setState({accountNumber: text.replace(/[^0-9]/g, '')});
            }}
            value={this.state.accountNumber}
            setfocus={(input) => {
              this.accountNumber = input;
            }}
            getfocus={() => {
              this.accountHolderName.focus();
            }}
          />

          <BankInput
            label={translate('AccountHolderName')}
            placeholder={translate('EnterName')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'default'}
            returnKeyType={'next'}
            onChangeText={(text) => {
              this.setState({accountHolderName: text});
            }}
            value={this.state.accountHolderName}
            setfocus={(input) => {
              this.accountHolderName = input;
            }}
            getfocus={() => {
              this.iFSCCode.focus();
            }}
          />

          <BankInput
            label={translate('IFSCCode')}
            placeholder={translate('EnterIFSCCode')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'default'}
            returnKeyType={'done'}
            // onChangeText={(text) => { this.setState({ iFSCCode: text.replace(/[^0-9]/g, '') }) }}
            onChangeText={(text) => {
              this.setState({iFSCCode: text});
            }}
            value={this.state.iFSCCode}
            setfocus={(input) => {
              this.iFSCCode = input;
            }}
          />
          <BankInput
            label={translate('IdentityCardNumber')}
            placeholder={translate('EnterIdentityCardNumber')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'numeric'}
            returnKeyType={'done'}
            // onChangeText={(text) => { this.setState({ iFSCCode: text.replace(/[^0-9]/g, '') }) }}
            onChangeText={(text) => {
              this.setState({identityCardNumber: text});
            }}
            value={this.state.identityCardNumber}
            setfocus={(input) => {
              this.identityCardNumber = input;
            }}
          />
          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Issuing country
          </Text>
          <View style={styles.viewDropdown}>
            <RNPickerSelect
              items={this.state.setCountryValue}
              // useNativeAndroidPickerStyle={true}

              placeholder={{label: 'Select country', value: null}}
              onValueChange={(val) => {
                this.setState({selectedCountry: val});
              }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={images.down_arrow}
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 15,
                      tintColor: 'black',
                      marginRight: 20,
                    }}
                  />
                );
              }}
              value={this.state.selectedCountry}
              style={pickerSelectStyles}
            />
          </View>
          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Issuing Date
          </Text>
          <View style={{marginHorizontal: 10}}>
            <DatePicker
              width={'87%'}
              backgroundcolor={'white'}
              color={Colors.brownishGrey}
              leftimage={images.calendar_icon}
              marginleft={-10}
              date={(val) => this.date(val)}
              value={this.state.selectedIdentityIssueDate}
            />
          </View>

          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Expiray Date
          </Text>
          <View style={{marginHorizontal: 10}}>
            <DatePicker
              width={'87%'}
              backgroundcolor={'white'}
              color={Colors.brownishGrey}
              leftimage={images.calendar_icon}
              marginleft={-10}
              date={(val) => this.expirydate(val)}
              value={this.state.selectedIdentityExpirayDate}
            />
          </View>

          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Attach Id Photo
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginHorizontal: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.uploadFrontIdentityCard();
              }}>
              {this.state.identityFrontPic ? (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={{uri: this.state.identityFrontPic}}
                  style={{width: 150, height: 150}}
                />
              ) : (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={images.id_card}
                  style={{width: 150, height: 150}}
                />
              )}
            </TouchableOpacity>
            <TouchableOpacity
            onPress={() => {
              this.uploadBackIdentityCard();
            }}>
               {this.state.identityBack ? (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={{uri: this.state.identityBack}}
                  style={{width: 150, height: 150}}
                />
              ) : (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={images.id_card}
                  style={{width: 150, height: 150}}
                />
              )}
            </TouchableOpacity>
          </View>

          {/* commercial regiserstion */}
          <BankInput
            label={translate('CommercialRegistrationCardNumber')}
            placeholder={translate('EnterCommercialRegistrationNumber')}
            placeholderTextColor={Colors.brownishGrey}
            fontFamily={Fonts.segui_semiBold}
            keyboardType={'number-pad'}
            returnKeyType={'done'}
            // onChangeText={(text) => { this.setState({ iFSCCode: text.replace(/[^0-9]/g, '') }) }}
            onChangeText={(text) => {
              this.setState({commercialCardNumber: text});
            }}
            value={this.state.commercialCardNumber}
            setfocus={(input) => {
              this.commercialCardNumber = input;
            }}
          />
          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Issuing country
          </Text>
          <View style={styles.viewDropdown}>
            <RNPickerSelect
              items={this.state.setCountryValue}
              // useNativeAndroidPickerStyle={true}

              placeholder={{label: 'Select country', value: null}}
              onValueChange={(val) => {
                this.setState({commecialselectedCountry: val});
              }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={images.down_arrow}
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 15,
                      tintColor: 'black',
                      marginRight: 20,
                    }}
                  />
                );
              }}
              value={this.state.commecialselectedCountry}
              style={pickerSelectStyles}
            />
          </View>
          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Issuing Date
          </Text>
          <View style={{marginHorizontal: 10}}>
            <DatePicker
              width={'87%'}
              backgroundcolor={'white'}
              color={Colors.brownishGrey}
              leftimage={images.calendar_icon}
              marginleft={-10}
              date={(val) => this.commercialdate(val)}
              value={this.state.commercialCardIssueData}
            />
          </View>

          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Expiray Date
          </Text>
          <View style={{marginHorizontal: 10}}>
            <DatePicker
              width={'87%'}
              backgroundcolor={'white'}
              color={Colors.brownishGrey}
              leftimage={images.calendar_icon}
              marginleft={-10}
              date={(val) => this.commecialexpirydate(val)}
              value={this.state.commercialCardExpirayData}
            />
          </View>

          <Text
            style={[
              styles.labelOfCss,
              {marginHorizontal: 15, top: 5, marginTop: 10},
            ]}>
            Attach Id Photo
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginHorizontal: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.uploadFrontCommecialCard();
              }}>
              {this.state.commecialFrontPic ? (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={{uri: this.state.commecialFrontPic}}
                  style={{width: 150, height: 150}}
                />
              ) : (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={images.id_card}
                  style={{width: 150, height: 150}}
                />
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.uploadbackCommecialCard();
              }}>
              {this.state.commercialBack ? (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={{uri: this.state.commercialBack}}
                  style={{width: 150, height: 150}}
                />
              ) : (
                <ProgressiveImage
                  resizeMode={'contain'}
                  source={images.id_card}
                  style={{width: 150, height: 150}}
                />
              )}
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.bttOfViewCss}>
          <TouchableOpacity
            disabled={this.checkIsFilled()}
            onPress={() => {
              this.addBank();
            }}
            style={{padding: 15, width: '100%'}}>
            {this.state.type == 'ADD' ? (
              <Text
                style={[
                  styles.bttTextOfCss,
                  {
                    color: this.checkIsFilled()
                      ? Colors.pinkishGrey
                      : Colors.kelleyGreen,
                  },
                ]}>
                Submit
              </Text>
            ) : (
              <Text
                style={[
                  styles.bttTextOfCss,
                  {
                    color: this.checkIsFilled()
                      ? Colors.pinkishGrey
                      : Colors.kelleyGreen,
                  },
                ]}>
                Save
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo},
  inptMailViewOfCss: {marginTop: 20, paddingHorizontal: 20},
  labelOfCss: {
    color: Colors.black,
    fontFamily: Fonts.segoeui,
    fontSize: 14,
    marginBottom: 12,
  },
  inputOfCssView: {
    borderWidth: 1,
    borderColor: Colors.whiteEight,
    borderRadius: 5,
    paddingHorizontal: 10,
    height: 43,
    width: '100%',
    justifyContent: 'center',
  },
  inputTextOfCss: {
    color: Colors.black,
    fontFamily: Fonts.segui_semiBold,
    fontSize: 12,
  },
  bttOfViewCss: {
    //position: "absolute",
    bottom: 0,
    backgroundColor: 'green',
    shadowColor: Colors.black,
    width: '100%',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    backgroundColor: Colors.whiteTwo,
    elevation: 5,
  },
  bttTextOfCss: {
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
    textAlign: 'center',
  },
  imgOfRightCss: {height: 20, width: 20, resizeMode: 'contain'},
  viewDropdown: {
    borderWidth: 1,
    borderColor: Colors.pinkishGreyTwo,
    marginHorizontal: 15,
    marginTop: 10,
    borderRadius: 4,
    borderColor: Colors.whiteEight,
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    // backgroundColor: Color.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '100%',
    minWidth: '100%',
    height: 54,
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    // borderColor: Color.black,
    borderWidth: 2,
  },
  inputAndroid: {
    //backgroundColor: Colors.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '100%',
    minWidth: '100%',
    height: 43,
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    borderColor: Colors.pinkishGrey,
    borderWidth: 1,
  },
});
