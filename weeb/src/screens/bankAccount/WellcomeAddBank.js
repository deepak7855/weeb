import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import {handleNavigation} from '../../navigation/routes';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ApiCall,LoaderForList} from '../../Api/index';
import {Validation, AlertMsg, Network} from '../../Lib/index';
import Helper from '../../Lib/Helper';
import { translate } from '../../Language';
export default class WellcomeAddBank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bankList: '',
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Bank Account',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
   this.getBankList();
  }
getBankList = () =>{
  Network.isNetworkAvailable().then((isConnected) => {
    if (isConnected) {
      // Helper.mainApp.showLoader();
      this.setState({isLoading: true});
      ApiCall.ApiMethod({Url: 'get-bank-list', method: 'GET'})
        .then((res) => {
          console.log('bank list api', res);
          Helper.mainApp.hideLoader();
          if (res?.status) {
            this.setState({
              bankList: res?.data,
              isLoading: false,
              emptymessage: false,
            });
          } else {
            this.setState({
              bankList: [],
              isLoading: false,
              emptymessage: true,
            });
          }
        })
        .catch((err) => {
          console.log(err);
          Helper.mainApp.hideLoader();
        });
    }
  });
}
  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  addBank = () => {
    this.props.navigation.navigate('EditAddBank', {type: 'ADD'});
  };
  makePrimaryAccount = (id) =>{
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        let data = {
          account_id:id,
        }
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({Url: 'set-primary-account', method: 'POST',data: data})
          .then((res) => {
           // console.log('bank list api', res);
            Helper.mainApp.hideLoader();
            Helper.showToast(res?.message)
            if(res?.status){
              this.setState({
                bankList:[]
              },()=>{
                this.getBankList()
              })
            }
          })
          .catch((err) => {
            console.log(err);
            Helper.mainApp.hideLoader();
          });
      }
    });
  }
  editAddBank = (item) =>{
    this.props.navigation.navigate('EditAddBank', {type: 'EDIT',data:item});
  }
  renderBankList = (data) => {
      console.log('data',data?.item)
    return (
      <View style={styles.bankContainer}>
        <View style={styles.mainCss}>
          <View style={{justifyContent: 'center'}}>
            <Image
              source={images.bank_img_circle}
              style={styles.bankimgOffCss}
            />
          </View>
          <View style={{marginLeft: 13, flex: 1}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text  style={styles.headingOfCss}>{data?.item?.bank_name}</Text>
              <TouchableOpacity
                onPress={() => {
                  this.editAddBank(data?.item);
                }}>
                <Image
                  source={images.edit_bank_icon}
                  style={styles.imgCssEdit}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                alignItems: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.samTextOFCss}>Branch - {data?.item?.branch_name}</Text>
              <TouchableOpacity
              onPress={() => {this.deleteBank(data?.item?.id)}}
              style={{ justifyContent: 'center', alignItems: 'center',backgroundColor:Colors.cherryRed,width:'30%',height:20,borderRadius:10}}>
                <Text style={{color: Colors.black,
    fontSize: 10,
    fontFamily: Fonts.segoeui,}}>Delete Bank</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
                style={{
                borderRadius:5,
                  paddingHorizontal: 10,
                  paddingVertical: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: data?.item?.primary_account == 1?Colors.darkSeafoamGreen:Colors.cherryRed,
                  marginTop:10
                }}
                onPress={() => { this.makePrimaryAccount(data?.item?.id) }}
              >
                <Text style={[styles.samTextOFCss,{ color: Colors.white}]}>{data?.item?.primary_account == 1?'Primary Account':'Make Primary Account'}</Text>
              </TouchableOpacity>
          </View>
        </View>
        <View style={styles.sencondMainCss}>
          <View style={{flex: 1}}>
            <Text style={styles.accountTextCss}>{translate("AccountNumber")}</Text>
            <Text style={styles.samTextOFCss}>{data?.item?.account_number}</Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.accountTextCss}>{translate("AccountHolder")}</Text>
            <Text style={styles.samTextOFCss}>{data?.item?.account_name}</Text>
          </View>
        </View>
      </View>
    );
  };

  deleteBank = (id) =>{
    Network.isNetworkAvailable().then((isConnected) => {
      if(isConnected){
        Helper.mainApp.showLoader()
        ApiCall.ApiMethod({Url: 'delete-bank-detail', method: 'POST',data:{bank_detail_id:id}}).then((res)=>{
          Helper.mainApp.hideLoader()
          Helper.showToast(res?.message)
            if(res?.status){
              this.setState({
                bankList:[]
              },()=>{
                this.getBankList()
              })
            }
        }).catch((err)=>{
          Helper.mainApp.hideLoader()
          console.log(err)
        })
      }
    })
  }
  render() {
    return (
      <View style={styles.container}>
        { this.state.isLoading? 
       <LoaderForList /> 
       :
       this.state.bankList.length ==  0
       ?
       <View
       style={{
         justifyContent: 'center',
         alignItems: 'center',
         marginTop: 10,
       }}>
       <Image source={images.bank_img} style={styles.imgOffCss} />
       <View style={{marginVertical: 25}}>
         <Text style={styles.subTextOffCss}>
         {translate("YoudonthaveanyBankAccountyet")}. {'\n'}{translate("Pleaseaddyourbankaccounthere")}. {'\n'}
           <Text style={{color: Colors.black}}>{translate("ClickAddBankButton")}</Text>
         </Text>
       </View>
     </View>
       :
          <FlatList
            renderItem={this.renderBankList}
            data={this.state.bankList}
            keyExtractor={(item, index) => item + index}
            ListFooterComponent={() => {
              return this.state.isLoading ? <LoaderForList /> : null;
            }}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false
                    ? null
                    : 'No request property'}
                </Text>
              </View>
            )}
          />
        }

        <TouchableOpacity
          onPress={() => {
            this.addBank();
          }}
          style={styles.bttViewOfCss}>
          <Image source={images.plus} style={styles.imgCssPlus} />
          <Text style={styles.bttTextCss}>{translate("AddBank")}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whiteTwo,
    // alignItems: 'center', justifyContent: 'center'
  },
  imgOffCss: {height: 170, width: 170},
  subTextOffCss: {
    color: Colors.brownishGreyTwo,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    textAlign: 'center',
  },
  bttViewOfCss: {
    justifyContent: 'center',
    backgroundColor: Colors.ice,
    borderRadius: 5,
    borderColor: Colors.darkSeafoamGreen,
    borderStyle: 'dashed',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 6,
    marginHorizontal: '10%',
    marginVertical: 10,
    width: '80%',
  },
  imgCssPlus: {height: 14, width: 14, resizeMode: 'contain', marginRight: 10},
  bttTextCss: {
    color: Colors.darkSeafoamGreen,
    fontFamily: Fonts.segoeui_bold,
    fontSize: 16,
  },

  // Bank CArds
  bankContainer: {
    flex: 1,
    backgroundColor: Colors.whiteTwo,
    paddingHorizontal: 15,
    paddingTop: 25,
  },
  mainCss: {
    flexDirection: 'row',
    backgroundColor: Colors.lightMint,
    paddingLeft: 20,
    padding: 13,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  bankimgOffCss: {height: 40, width: 40, resizeMode: 'contain'},
  headingOfCss: {
    color: Colors.black,
    fontSize: 16,
    fontFamily: Fonts.segui_semiBold,
  },
  samTextOFCss: {
    color: Colors.black,
    fontSize: 13,
    fontFamily: Fonts.segoeui,
    lineHeight: 18,
  },
  imgCssEdit: {height: 25, width: 25, resizeMode: 'contain', top: 4},
  accountTextCss: {
    color: '#909e93',
    fontSize: 11,
    fontFamily: Fonts.segoeui,
    lineHeight: 21,
  },
  sencondMainCss: {
    flexDirection: 'row',
    backgroundColor: Colors.ice,
    paddingLeft: 20,
    padding: 13,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
});
