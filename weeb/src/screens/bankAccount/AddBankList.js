import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import AppHeader from '../../Components/AppHeader';
import { translate } from '../../Language';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';

export default class AddBankList extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Bank Account',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    goBack = () => {
        handleNavigation({ type: 'push', page: 'Settings', navigation: this.props.navigation });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    editAddBank = () => {
        this.props.navigation.navigate("EditAddBank", { type: "EDIT" })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.mainCss}>
                    <View style={{ justifyContent: 'center' }}>
                        <Image source={images.bank_img_circle}
                            style={styles.imgOffCss} />
                    </View>
                    <View style={{ marginLeft: 13, flex: 1 }}>
                        <Text style={styles.headingOfCss}>{translate("DubaiIslamicBank")}</Text>
                        <Text style={styles.samTextOFCss}>{translate("BranchDubai")}</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.editAddBank() }}>
                        <Image source={images.edit_bank_icon}
                            style={styles.imgCssEdit} />
                    </TouchableOpacity>
                </View>
                <View style={styles.sencondMainCss}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.accountTextCss}>{translate("AccountNumber")}</Text>
                        <Text style={styles.samTextOFCss}>0123 4567 7890</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.accountTextCss}>{translate("AccountHolder")}</Text>
                        <Text style={styles.samTextOFCss}>Irshad Khan</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.white, paddingHorizontal: 15, paddingTop: 25 },
    mainCss: { flexDirection: 'row', backgroundColor: Colors.lightMint, paddingLeft: 20, padding: 13, borderTopLeftRadius: 6, borderTopRightRadius: 6 },
    imgOffCss: { height: 40, width: 40, resizeMode: "contain" },
    headingOfCss: { color: Colors.black, fontSize: 16, fontFamily: Fonts.segui_semiBold },
    samTextOFCss: { color: Colors.black, fontSize: 13, fontFamily: Fonts.segoeui, lineHeight: 18 },
    imgCssEdit: { height: 25, width: 25, resizeMode: "contain", top: 4 },
    accountTextCss: { color: "#909e93", fontSize: 11, fontFamily: Fonts.segoeui, lineHeight: 21 },
    sencondMainCss: { flexDirection: 'row', backgroundColor: Colors.ice, paddingLeft: 20, padding: 13, borderBottomLeftRadius: 6, borderBottomRightRadius: 6 },
})