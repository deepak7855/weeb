import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList, Modal, Image, TouchableOpacity, DeviceEventEmitter, RefreshControl } from 'react-native'
import BookingRequestViews from '../Components/BookingRequestViews';
import ModalView from '../Components/ModalView';
import Colors from '../Lib/Colors'
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import OpenDocument from '../Components/OpenDocument';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import {Helper, Network, AlertMsg, CallTenant} from '../Lib/index';
import { ApiCall, LoaderForList } from '../Api/index';
import { Constant } from '../Lib/Constant';
import Fonts from '../Lib/Fonts'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
let i  = 0;
export default class BookingAccepted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accepttListItem: '',
      acceptListItemOwner: [],
      modalVisibleAccept: false,
      modalVisibleDecline: false,
      modalVisibleDocument: false,
      userType: Helper.userType,
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
      front_id: '',
      back_id: '',
      terminatePropertyId:''
    };
  }

  componentDidMount() {
    this.listner = DeviceEventEmitter.addListener('Call-Accept-Api', (data) => {
      this.getTenantBookingRequestData(true);
    });
    this.listner2 = DeviceEventEmitter.addListener(
      'getTenantBookingRequestData',
      (data) => {
       // alert('call')
       this.setState({currentPage: 1, refreshing: false}, () => {
        this.getTenantBookingRequestData(true);
      });
        //this.getTenantBookingRequestData();
        //this.onRefresh();
      },
    );
    this.setState({
      userType: Helper.userType,
    });
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.setState({currentPage: 1, refreshing: false}, () => {
        this.getTenantBookingRequestData(true);
      });
    });
    if(i == 0){
      this.getTenantBookingRequestData();
    }
    
  }

  componentWillUnmount() {
    if (this.lisener) {
      this.lisener.remove();
      this.listner2.remove();
    }
    //this._unsubscribe();
  }

  getTenantBookingRequestData = (already) => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (!this.state.refreshing) {
          this.setState({isLoading: true});
        }
        let url;
        const data = {
          status: 2,
        };
        if (this.state.userType == 'OWNER') {
          url = 'get-bookings-by-owner' + '?page=' + this.state.currentPage;
        } else {
          url = 'get-booking-by-tenant' + '?page=' + this.state.currentPage;
        }
        // Helper.mainApp.showLoader()
        console.log('total render--------in booking accept',i++)
        ApiCall.ApiMethod({Url: url, method: 'POST', data: data})
          .then((res) => {
            Helper.mainApp.hideLoader();
            if (res?.status) {
             // console.log('booking accept data', res?.data, url);
              if (this.state.userType == 'OWNER') {
             //  console.log('booking accept OWNER data', res?.data?.data, url);
                if (res?.data?.data && res?.data?.data.length > 0) {
                 // this.props.updateCountNumber();
                  if (already) {
                    this.setState({
                      acceptListItemOwner: [],
                    });
                  }
                  this.setState(
                    {
                      acceptListItemOwner: this.state.acceptListItemOwner
                        ? [
                            ...this.state.acceptListItemOwner,
                            ...res?.data?.data,
                          ]
                        : res?.data?.data,
                      next_page_url: res?.data?.next_page_url,
                      isLoading: false,
                      refreshing: false,
                      emptymessage: false,
                    },
                    () => {
                      //console.log('after owner data', this.state.acceptListItemOwner);
                    },
                  );
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                    emptymessage: true,
                  });
                }
              } else {
               this.props.updateCountNumber();
                if (res?.data?.data && res?.data?.data.length > 0) {
                  // console.log(
                  //   'booking accept tenant data',
                  //   res?.data?.data,
                  //   url,
                  // );
                  if (already) {
                    this.setState({
                      accepttListItem: '',
                    });
                  }
                  this.setState({
                    accepttListItem: [
                      ...this.state.accepttListItem,
                      ...res?.data?.data,
                    ],
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    emptymessage: false,
                  });
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                    emptymessage: true,
                    accepttListItem: '',
                  });
                }
              }
            } else {
              this.setState({
                ext_page_url: res?.data?.next_page_url,
                isLoading: false,
                refreshing: false,
                emptymessage: true,
              });
             // console.log('tenant fail data', res);
              return;
            }
          })
          .catch((err) => {
            Helper.mainApp.hideLoader();
            //console.log('api fail err', err);
          });
      }
    });
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState({currentPage: 1, refreshing: false}, () => {
        this.getTenantBookingRequestData(true);
      });
    }, 2000);
  };

  onCall = (item) => {
    //  console.log('call item', item?.user?.mobile_number);
    CallTenant(item?.user?.mobile_number);
  };

  onScroll = () => {

    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getTenantBookingRequestData();
      });
    }
  };

  onDocumentModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleDocument}
        onRequestClose={() => {
          this.setState({modalVisibleDocument: false});
        }}>
        <OpenDocument
          maodalHeid={() => {
            this.setState({modalVisibleDocument: false});
          }}
          front_id={this.state.front_id}
          back_id={this.state.back_id}
        />
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
      </Modal>
    );
  };

  onChats = (item) => {
    // console.log(
    //   'tenant bookin request chat',
    //   item?.user?.id,
    //   item?.property?.user?.id,
    // );
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {
        userId:
          this.state.userType == 'OWNER'
            ? item?.user?.id
            : item?.property?.user?.id,
      },
      navigation: this.props.navigation,
    });
  };

  addAgreement = (item) => {
    handleNavigation({
      type: 'push',
      page: 'AddAgreement',
      passProps: {
        propertyData: item,
      },
      navigation: this.props.navigation,
    });
  };

  viewAgreementOwner(item) {
    handleNavigation({
      type: 'push',
      page: 'AddAgreement',
      passProps: {
        propertyData: item,
        view: true,
      },
      navigation: this.props.navigation,
    });
  }

  renderRequestList = ({item, index}) => {
 // console.log('booking accept item', item?.property_id);
    return (
      <BookingRequestViews
        item={item}
        Button
        onClickAgreement={() => {}}
        onClick1={() => {
          this.setState({modalVisibleDecline: true,terminatePropertyId:item?.property_id});
        }}
        onClick2={() => {
          this.onAcceptBooking();
        }}
        title={'Terminate'}
        subTitle={'Accepted'}
        onChat={() => {
          this.onChats(item);
        }}
        onCall={() => {
          this.onCall(item);
        }}
        OwnerTypeButton
        RejectProperty={() => {
          this.setState({
            terminatePropertyId:item?.property_id
          },()=>{
         //  alert(this.state.terminatePropertyId)
             this.openModelDecline();
          })
         
        }}
        viewDocBtn={'View Document'}
        adAgreementBtn={'Add Agreement'}
        ViewClickOwner={() => {
          this.setState(
            {
              //modalVisibleDocument: true,
              front_id: item?.tenantdoc[0]?.front_image_url,
              back_id: item?.tenantdoc[0]?.back_image_url,
            },
            () => {
              // console.log(
              //   'tenant doc',
              //   this.state.front_id,
              //   this.state.back_id,
              //   item?.tenantdoc,
              // );
              this.state.front_id && this.state.back_id
                ? this.setState({
                    modalVisibleDocument: true,
                  })
                : Helper.showToast('Documents not uploaded');
            },
          );
        }}
        addAgreementOwner={() => {
          this.addAgreement(item);
        }}
        viewAgreementTenant={() => {
          this.viewAgreementOwner(item);
        }}
        // onClickAgreement={() => {
        //   this.state.back_id && this.state.front_id
        //     ? this.setState({
        //         modalVisibleDocument: true,
        //         front_id: item?.tenantdoc?.front_image_url,
        //         back_id: item?.tenantdoc?.back_image_url,
        //       })
        //     : Helper.showToast('Documents not uploaded');
        // }}
       // onClickAgreement={() => {}}
        AgreementBttTENANT
      />
    );
  };

  onAcceptBooking = () => {
    this.setState({modalVisibleAccept: true});
    // handleNavigation({ type: 'push', page: 'MyRentProperty', navigation: this.props.navigation });
    this.props.navigation.navigate('MyRentProperty');
  };

  openModelAccept = () => {
    return (
      <Modal
        animationType={'slide'}
        visible={this.state.modalVisibleAccept}
        transparent={true}
        onRequestClose={() => {
          this.setState({modalVisibleAccept: false});
        }}>
        <ModalView
          icon={images.success}
          title={'Accept'}
          color={Colors.kelleyGreen}
          subTitle={'Are you sure? \nYou want to Accept this Property'}
          onClickYess={() => {
            this.setState({modalVisibleAccept: false});
          }}
          onClickNo={() => {}}
          bttNoYess
        />
      </Modal>
    );
  };

TenantDeclineProperty = () =>{
  //console.log('property id',item)
  Network.isNetworkAvailable().then((isConnected) => {
    this.setState({modalVisibleDecline: false});
      Helper.mainApp.showLoader();
    if(isConnected){
      
      ApiCall.ApiMethod({Url: 'terminate-property', method: 'POST', data: {property_id:this.state.terminatePropertyId}})
      .then((res) => {
        Helper.mainApp.hideLoader();
           // console.log('property terminate data', res);
            Helper.showToast(res?.message ? res.message :'Something went wrong')
        if (res?.status) {
       this.setState({currentPage: 1, refreshing: false}, () => {
        this.getTenantBookingRequestData(true);
      });
        } else {
          this.setState({modalVisibleDecline: false});
        }
       
      }).catch((err) => {
        Helper.mainApp.hideLoader();
       // console.log('api fail err', err);
      });
    }
  }).catch(()=>{
    Helper.showToast(AlertMsg.error.NETWORK)
    this.setState({modalVisibleDecline: false});
    Helper.mainApp.hideLoader();
  })
}

  openModelDecline = (item) => {
    return (
      <Modal
        animationType={'slide'}
        visible={this.state.modalVisibleDecline}
        transparent={true}
        onRequestClose={() => {
          this.setState({modalVisibleDecline: false});
        }}>
        <ModalView
          icon={images.failed}
          title={'Decline'}
          color={Colors.cherryRed}
          subTitle={'Are you sure? \nYou want to Terminate this Property'}
          onClickYess={() => {
            this.TenantDeclineProperty()
            
          }}
          onClickNo={() => {
            this.setState({modalVisibleDecline: false});
          }}
          bttNoYess
        />
      </Modal>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          bounces={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          //contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}>
          <FlatList
            style={{marginTop: 20}}
            data={
              this.state.userType == 'OWNER'
                ? this.state.acceptListItemOwner
                : this.state.accepttListItem
            }
            renderItem={this.renderRequestList}
            extraData={this.state}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
              />
            }
           
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false
                    ? null
                    : 'No accepted property.'}
                </Text>
              </View>
            )}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
          />
          {this.openModelAccept()}
          {this.openModelDecline()}
          {this.onDocumentModal()}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
})