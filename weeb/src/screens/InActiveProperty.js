import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TextInput, DeviceEventEmitter, RefreshControl, FlatList, TouchableOpacity, Platform, SafeAreaView } from 'react-native'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import { Helper, Network, AlertMsg } from '../Lib/index';
import { ApiCall, ApiCallForProperty, LoaderForList } from '../Api/index';
import { ProgressiveImage } from '../Components/Common/index';
import { translate } from '../Language';
export default class InActiveProperty extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inActiveProperty: '',
            status: 0,
            address: '',
            lat: '',
            long: '',
            modalVisible: false,
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: false,
            emptymessage: false,
        }

    }

    async componentDidMount() {
        // this._unsubscribe = this.props.navigation.addListener('focus', () => {
        //    // this.getInactiveData(true)
        //     this.setState({ currentPage: 1, refreshing: false }, () => {
        //         this.getInactiveData(true)
        //         //this.getTenantBookingRequestData({ loader: true  });
        //     });
        // });
        this.listner2 = DeviceEventEmitter.addListener('owner-property-location', (data) => {
            console.log('device event', data)
            this.setState({
                lat: data?.lat,
                long: data?.long,
                currentPage: 1
            }, () => {
                this.getInactiveData(true)
            })
        })

        this.listner = DeviceEventEmitter.addListener('Listed-Property-Filter', (data) => {
            console.log('device event', data)
            this.setState({
                currentPage: 1
            }, () => {
                if (data?.type) {
                    this.getInactiveData(true, data?.type)
                } else {
                    this.getInactiveData(true, '')
                }

            })
        })

        this.listner3 = DeviceEventEmitter.addListener('Call-Active-Api', (data) => {
            console.log('device event', data)
            this.setState({
                lat: data?.lat,
                long: data?.long,
                currentPage: 1
            }, () => {
                this.getInactiveData(true, '')
            })
        })
        this.getInactiveData(true)
    }
    componentWillUnmount() {
        // this._unsubscribe();
        if (this.listner) {
            this.listner.remove();
            this.listner2.remove();
            this.listner3.remove();
        }
    }

    getInactiveData = async (already, type) => {
        this.setState({
            isLoading: true
        })
        const requestData = await ApiCallForProperty.getPropertyList({ status: 2, currentPage: this.state.currentPage, lat: this.state.lat, long: this.state.long, type: type })
       // console.log('request data inactive screen', requestData?.data)
        if (already) {
            this.setState({
                inActiveProperty: '',
            })
        }
        if (requestData?.status) {
            if (requestData?.data?.data && requestData?.data?.data.length > 0) {
                this.setState({
                    inActiveProperty: this.state.inActiveProperty ? [...this.state.inActiveProperty, ...requestData?.data?.data] : requestData?.data?.data,
                    isLoading: false,
                    next_page_url: requestData?.data?.next_page_url,
                    refreshing: false,
                    emptymessage: false,
                })
            } else {
                this.setState({
                    isLoading: false,
                    next_page_url: requestData?.data?.next_page_url,
                    currentPage: 1,
                    refreshing: false,
                    emptymessage: true,
                })
            }

        } else {
            this.setState({
                isLoading: false,
                next_page_url: requestData?.data?.next_page_url,
                currentPage: 1,
                refreshing: false,
                emptymessage: true,
            })
        }

    }
    onRefresh = () => {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this.setState({ currentPage: this.state.currentPage, refreshing: false, lat: '', long: '', address: '' }, () => {
                this.getInactiveData(true)
                // this.props.clearAddress()
                //this.getTenantBookingRequestData({ loader: true });
            });
        }, 2000);
    }

    onScroll = () => {
        if (this.state.next_page_url && !this.state.isLoading) {
            this.setState({ currentPage: this.state.currentPage + 1 }, () => {
                this.getInactiveData()
                //  this.getTenantBookingRequestData({ loader: true });
            });
        }
    }

    onProperty = (index) => {
        Helper.propertyData = this.state.inActiveProperty[index]
        handleNavigation({ type: 'pushTo', page: 'SelectPropertyType', passProps: { title: 'Edit Property', }, navigation: this.props.navigation });
    }

    deleteProperty = async (index) => {
        const id = this.state.inActiveProperty[index]?.id;
        let deleteData = await ApiCallForProperty.deleteProperty(id);
        console.log('delete data', deleteData)
        if (deleteData || deleteData?.status) {
            this.setState({ currentPage: 1, refreshing: false }, () => {
                this.getInactiveData(true)
                //this.getTenantBookingRequestData({ loader: true  });
            });
        }

    }

    goDetails = (index) => {
        handleNavigation({ type: 'pushTo', page: 'PropertyDetails', passProps: { propertyID: this.state.inActiveProperty[index]?.id, }, navigation: this.props.navigation });
    }

    ActiveProperty = async (index) => {
        const id = this.state.inActiveProperty[index]?.id;
        let activeData = await ApiCallForProperty.ActiveAndDeactiveProperty(id, 1);
        console.log('delete data', activeData?.message)
         if(activeData?.message == 'Please add bank details.' ){
            this.props.navigation.navigate('EditAddBank', {type: 'ADD'});
             return true;
         }
        if (activeData || activeData?.status) {
            this.setState({ currentPage: 1, refreshing: false }, () => {
                this.getInactiveData(true)
                DeviceEventEmitter.emit('Call-Active-Api', { type: 'Call-Active-Api' });
                //this.getTenantBookingRequestData({ loader: true  });
            });
        }
    }
    renderOfProperty = ({ item, index }) => {
        // console.log('in active flat list data', item?.propertyphotos ? item?.propertyphotos[0] :index)
        return (
            <TouchableOpacity onPress={() => { this.goDetails(index) }}>
                <View style={styles.view1}>
                    <ProgressiveImage
                        source={{ uri: item?.propertyphotos ? item?.propertyphotos[0]?.imgurl : '' }}
                        style={styles.iconHome}
                        resizeMode="cover"
                    />
                    {/* <Image style={styles.iconHome} source={{ uri: item?.propertyphotos[0]?.imgurl}} /> */}
                    <View style={{ marginLeft: 13 }}>
                        <Text style={styles.textTitle}>{item?.title}</Text>
                        <View style={styles.view3}>
                            <Image style={styles.iconLocation}
                                source={images.location} />
                            <Text numberOfLines={2} style={styles.textAddress}>{item?.location}</Text>
                        </View>
                        <View style={styles.view4}>
                            <Text style={styles.textRate}>{item?.rent}</Text>
                            <Text style={[styles.textRate, { fontFamily: Fonts.segoeui }]}>{item.rateType}</Text>
                            <View style={styles.dot} />
                            <Text style={styles.textPropType}>{item?.type}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 12, marginTop: 15 }}>
                    <TouchableOpacity onPress={() => { this.onProperty(index) }}>
                        <Image style={styles.editProp} source={images.property_edit} />
                    </TouchableOpacity>
                    <View style={styles.midTouch}>
                        <TouchableOpacity style={{ width: '50%' }}
                            onPress={() => { this.deleteProperty(index) }}>
                            <Image style={styles.editProp} source={images.property_delete} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.viewActive} onPress={() => this.ActiveProperty(index)}>
                        <Text style={styles.textActivate}>Activate</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }

    onAddProperty = () => {
        Helper.propertyData = {};
        handleNavigation({ type: 'pushTo', page: 'SelectPropertyType', navigation: this.props.navigation });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.viewTextTop}>
                    <Text style={styles.textTop}>{translate("AllProperty")}</Text>
                    <View style={styles.dotTop} />
                    <Text style={styles.text150}>{this.state.inActiveProperty.length}</Text>
                    <TouchableOpacity onPress={() => { this.onAddProperty() }}>
                        <Image style={styles.iconAddProp} source={images.addproperty} />
                    </TouchableOpacity>
                </View>
                <View style={styles.line1} />
                <View>

                    <FlatList
                        data={this.state.inActiveProperty}
                        renderItem={this.renderOfProperty}
                        extraData={this.state}
                        ItemSeparatorComponent={() => <View style={styles.seperator}></View>}
                        ListFooterComponent={() => {
                            return (
                                this.state.isLoading ? <LoaderForList /> : null
                            )
                        }}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this.onRefresh()}
                            />
                        }

                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        keyboardShouldPersistTaps={'handled'}
                        refreshing={this.state.refreshing}
                        ListEmptyComponent={() => (
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.cherryRed, textAlign: 'center' }}>{this.state.emptymessage == false ? null : 'No inactive property.'}</Text>
                            </View>
                        )}
                    />



                </View>
                <SafeAreaView />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 60 },
    textIncomp: { fontSize: 12, fontFamily: Fonts.segui_semiBold, color: Colors.cherryRed },
    textIncomp1: { fontSize: 12, fontFamily: Fonts.segui_semiBold, color: Colors.brownishGreyTwo },
    viewTextTop: { paddingLeft: 12, paddingRight: 9, paddingTop: 15, flexDirection: 'row', alignItems: 'center' },
    searchIcon: { width: 11, height: 11, resizeMode: 'contain' },
    textInputStyl: { fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40 },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold },
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { width: '80%', fontSize: 12, lineHeight: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 },
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, fontFamily: Fonts.segoeui_bold, color: Colors.darkSeafoamGreen },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, color: Colors.marigold, fontFamily: Fonts.segui_semiBold },
    filterTouch: { position: 'absolute', bottom: 20, right: 12 },
    iconFilter: { width: 56, height: 56, resizeMode: 'contain' },
    view1: { flexDirection: 'row', marginTop: 15, paddingLeft: 12 },
    textReq: { fontSize: 12, color: Colors.dustyOrange, fontFamily: Fonts.segui_semiBold, flex: 0.65, marginBottom: 10 },
    viewTextInput: { flexDirection: 'row', backgroundColor: Colors.whiteTwo, alignItems: 'center', paddingLeft: 13 },
    textTop: { fontSize: 16, lineHeight: 16, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    line1: { marginHorizontal: 12, borderRadius: 0.5, backgroundColor: Colors.warmGrey, height: 1, opacity: 0.18, marginTop: 15 },
    seperator: { borderRadius: 0.5, backgroundColor: Colors.warmGrey, height: 1, opacity: 0.18, marginHorizontal: 12, marginTop: 15 },
    midTouch: { marginLeft: 10, flex: 0.93, },
    editProp: { width: 68, height: 22, resizeMode: 'contain' },
    filteView: { position: 'absolute', bottom: 90, right: 12, backgroundColor: Colors.whiteTwo, elevation: 5, width: 200, borderRadius: 10 },
    viewTopTextOfFilter: { paddingVertical: 10, paddingHorizontal: 20, backgroundColor: Colors.whiteNine, borderBottomColor: Colors.whiteFive, borderBottomWidth: 1 },
    textPropTypeOfFilter: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    filterSeperator: { borderWidth: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.05 },
    viewActive: { marginLeft: 10, borderWidth: 0.5, backgroundColor: Colors.darkSeafoamGreen, borderRadius: 2, borderColor: Colors.darkSeafoamGreen, paddingHorizontal: 25, alignItems: 'center' },
    textActivate: { top: Helper.hasNotch ? 1 : Platform.OS === 'ios' ? 0 : -2, fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.whiteTwo, textAlign: 'center' },
    dotTop: { width: 4, height: 4, backgroundColor: Colors.pinkishGreyTwo, borderRadius: 2, marginHorizontal: 7 },
    text150: { fontSize: 16, lineHeight: 16, color: Colors.pinkishGrey, fontFamily: Fonts.segui_semiBold, flex: 1 },
    iconAddProp: { width: 100, height: 20, resizeMode: 'contain' },
})