import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, FlatList, TouchableOpacity, ScrollView } from 'react-native'
import AppHeader from '../Components/AppHeader';
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import {
    ProgressiveImage,
  } from '../Components/Common/index';
  import {Constant} from '../Lib/Constant';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import moment from 'moment';
import { translate } from '../Language';
export default class Maintenance_16_2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOfMaintenence_16_2: [],
            propertyData:this.props.route.params?.propertyData,
        }
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Maintenance',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    componentDidMount(){
        this.getMaintenanceRequest()
    }

   getMaintenanceRequest = () =>{
       Network.isNetworkAvailable().then((isConnected) => {
           if(isConnected){
            const data = {
                property_id: this.props.route.params?.propertyData?.property_id,
              };
            ApiCall.ApiMethod({
                Url: 'get-property-by-id',
                method: 'POST',
                data: data,
              }).then((res) => {
               // Helper.mainApp.hideLoader();
              //  console.log('res',res?.data?.maintenance_request);
                if (res?.status) {
                 
                  this.setState({
                    dataOfMaintenence_16_2: res?.data?.maintenance_request,
                  });
                }
              });
           }
       })
    
   }

    goBack=()=>{
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    addPost=()=>{
        handleNavigation({ type: 'push', page: 'Maintenance_16_3', passProps: {
            propertyData:this.state.propertyData,
          },navigation: this.props.navigation });
    }

    renderOfCard = ({ item }) => {
        console.log('maintenance_request',item);
        return (
            <View style={{}}>
                <View style={styles.view1Fl}>
                    <Text style={[styles.textIssue, { flex: 1 }]}>{translate("IssueID")}: {item.id}</Text>
                    <Image style={styles.iconTime} source={images.clock} />
                    <Text style={styles.textIssue}>{moment(item?.created_at).format('YYYY-MM-DD h:mm a')}</Text>
                </View>
                <View style={styles.card}>
                    <View style={styles.view1Card}>
                        <Text style={styles.textTypeOfProp}>{item.type}</Text>
                    </View>
                    <Text style={styles.textDetails}>{item.description}</Text>
                    <View style={styles.line} />
                    <View style={styles.viewStatus}>
                        <Text style={[styles.textStatus, { color:item.status == 0?Colors.cherryRed: item?.status == 1 ?Colors.marigold:Colors.darkSeafoamGreen }]}>{'Status'}</Text>
                        <Text style={[styles.textStatus, { color:item.status == 0?Colors.cherryRed: item?.status == 1 ?Colors.marigold:Colors.darkSeafoamGreen }]}>{item.status == 0?'Incomplete': item?.status == 1 ?'Inprocess':'Complete'}</Text>
                    </View>
                </View>
            </View>
        )
    }
    topBar = () => {
        return (
            <View style={{backgroundColor: Colors.lightMint, height:100 }}>
                <View style={[styles.viewTop, {marginTop:19}]}>
                <ProgressiveImage
          source={{
            uri: this.state.propertyData?.property?.propertyphotos
              ? this.state.propertyData?.property?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.iconHome}
          resizeMode="cover"
        />
                <View style={{ marginLeft: 13 }}>
                    <Text style={styles.textTitle}>{this.props.route.params?.propertyData?.property?.title}</Text>
                    <View style={styles.view3}>
                        <Image style={styles.iconLocation} source={images.location} />
                        <Text numberOfLines={1} style={styles.textAddress}>{this.props.route.params?.propertyData?.property?.location}</Text>
                    </View>
                    <View style={styles.view4}>
                        <Text style={styles.textRate}>{this.props.route.params?.propertyData?.property?.rent}</Text>
                        <Text style={[styles.textRate, { fontFamily: Fonts.segoeui }]}>/m</Text>
                        <View style={styles.dot} />
                        <Text style={styles.textPropType}>{this.props.route.params?.propertyData?.property?.type}</Text>
                    </View>
                </View>
            </View>
            </View>
        )
    }
    render() {
       // console.log('propertyData',this.state.propertyData)
        return (
            <View style={styles.container}>
                {this.topBar()}
                <View style={styles.viewMaintenance}>
                    <Text style={styles.textMaint}>{translate("MaintenanceRequests")}</Text>
                    <Text style={styles.text150}>{this.state.dataOfMaintenence_16_2?.length}</Text>
                </View>
                <FlatList
                    data={this.state.dataOfMaintenence_16_2}
                    renderItem={this.renderOfCard}
                    extraData={this.state} />
                <TouchableOpacity
                    onPress={() => this.addPost()}
                    style={styles.filterTouch}>
                    <Image style={styles.iconFilter} source={images.floting_plus} />
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 10 },
    viewMaintenance: { alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: 12, paddingTop: 10 },
    textMaint: { fontSize: 16, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, },
    text150: { fontSize: 16, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, },
    view1Fl: { marginTop: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 12 },
    textIssue: { fontFamily: Fonts.segoeui, fontSize: 12, color: Colors.brownishGreyTwo },
    iconTime: { width: 9, height: 9, resizeMode: 'contain' },
    textTitle: { fontFamily: Fonts.segoeui, fontSize: 12, color: Colors.brownishGreyTwo, marginLeft: 4 },
    card: { borderWidth: 1, marginHorizontal: 12, borderRadius: 4, borderColor: Colors.whiteFive, marginTop: 11 },
    view1Card: { backgroundColor: Colors.whiteNine, paddingLeft: 18, paddingTop: 7, paddingBottom: 12 },
    textTypeOfProp: { fontSize: 16, color: Colors.blackTwo, fontFamily: Fonts.segui_semiBold },
    textDetails: { marginLeft: 18, marginTop: 10, marginRight: 15, fontSize: 12, lineHeight: 22, fontFamily: Fonts.segoeui },
    line: { borderWidth: 0.5, borderWidth: 1, opacity: 0.18, borderColor: Colors.warmGrey, marginTop: 14, marginBottom: 10, marginHorizontal: 15 },
    viewStatus: { flexDirection: 'row', justifyContent: 'space-between', marginRight: 15, marginLeft: 18, marginBottom: 15 },
    textStatus: { fontSize: 12, fontFamily: Fonts.segui_semiBold, },
    viewTop: { flexDirection: 'row', paddingLeft: 14, },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold },
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { fontSize: 12, lineHeight: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 ,width:'85%'},
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, fontFamily: Fonts.segoeui_bold, color: Colors.darkSeafoamGreen },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, color: Colors.marigold, fontFamily: Fonts.segui_semiBold },
    filterTouch: { position: 'absolute', bottom: 20, right: 12 },
    iconFilter: { width: 56, height: 56, resizeMode: 'contain' },

})