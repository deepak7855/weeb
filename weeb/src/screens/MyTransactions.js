import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
    FlatList,
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import { translate } from '../Language';
import moment from 'moment';
export default class MyTransactions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TransactionData: [],
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: false,
            emptymessage: false,

        };
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'My Transactions',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    componentDidMount(){
        Network.isNetworkAvailable().then((isConnected)=>{
            if(isConnected){
                this.setState({isLoading: true});
                ApiCall.ApiMethod({Url: 'get-transactions', method: 'GET'}).then((res)=>{
                    console.log('payment trasction list',res);
                    if(res?.status){
                        this.setState({
                            TransactionData: res?.data,
                            isLoading: false,
                            emptymessage: false,
                        })
                    }else{
                        this.setState({
                            isLoading: false,
                            emptymessage: true,
                        })
                    }
                }).catch((err)=>{
                    this.setState({
                        isLoading: false,
                        emptymessage: true,
                    })
                        console.log('err',err)
                })
            }
        })
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    renderTransaction = ({ item }) => {
        return (
            <View style={[styles.view_TransactionMain, { backgroundColor:item?.status  === 'CAPTURED' ?Colors.ice : Colors.lightCherryRed}]}>
                <View style={{ flexDirection: 'row', paddingHorizontal: 22, alignItems: 'center' }}>
                    <Image
                        source={item?.status  === 'CAPTURED' ?images.trans_check:images.trabs_failed} style={styles.icon_Transaction}
                    />
                    <Text style={[styles.txt_Transaction, { color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed }]}>{item?.status  === 'CAPTURED' ?'Success':'Failed'}</Text>
                    <Image source={images.clock} style={styles.icon_Clock} />
                    <Text style={{ fontSize: 10, fontFamily: Fonts.segoeui, color: Colors.brownishGrey }}>  { moment(item?.created_at).format('MM/DD/YYYY')} </Text>
                </View>
                <View
                    style={{
                        backgroundColor: Colors.darkSeafoamGreen,
                        marginTop: 15,
                        height: 0.5,
                        opacity: 0.5
                    }} />
                <View style={{ paddingHorizontal: 22, }}>
                    <View style={{flexDirection: 'row',justifyContent: 'space-between'}}>
                        <Text style={styles.txt_TxIdNumber}>Rent</Text>
                        <Text style={styles.txt_TxIdNumber}>Service Charge</Text>
                        <Text style={styles.txt_TxIdNumber}>Total</Text>
                    </View>
                    <View style={{flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',}}>
                    <Text style={[styles.txt_TransactionPrice, { color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed }]}>KWD {Math.round(item.rent)}</Text>
                    <Text style={[styles.txt_TransactionPrice, { color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed }]}>KWD {Math.round(item.service_charge)}</Text>
                    <Text style={[styles.txt_TransactionPrice, { color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed }]}>KWD {Math.round(item?.rent)+Math.round(item.service_charge)}</Text>
                    </View>
                    <Text style={[styles.txt_property,{marginTop:10}]}>{item?.property_type + ',' +item?.property }</Text>
                    <View style={{ flexDirection: 'row', marginTop: 18,justifyContent:"space-between" }}>
                        <Text style={{  color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed, fontFamily: Fonts.segui_semiBold, fontSize: 10 }}>{translate("TX_ID")}</Text>
                        <Text style={{  color: item?.status  === 'CAPTURED' ?Colors.darkSeafoamGreen:Colors.cherryRed, fontFamily: Fonts.segui_semiBold, fontSize: 10 }}>{translate("PAID_BY")}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flex: 1, paddingBottom: 18,justifyContent: "space-between"}}>
                        <Text style={styles.txt_TxIdNumber}>{item?.txn_id}</Text>
                        <Text style={styles.txt_DebitCard}>{item?.card_type}</Text>
                    </View>
                </View>
            </View>
        );
    };
    render() {
        return (
            <View style={{ flex:1 }}>
                <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
                <View style={{flex:1}}>
                    <FlatList
                        data={this.state.TransactionData}
                        keyExtractor={(item) => item.key}
                        renderItem={this.renderTransaction}
                        showsVerticalScrollIndicator={false}
                        ListFooterComponent={() => {
                            return this.state.isLoading ? <LoaderForList /> : null;
                          }}
                          ListEmptyComponent={() => (
                            <View
                              style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  fontSize: 14,
                                  fontFamily: Fonts.segoeui,
                                  color: Colors.cherryRed,
                                  textAlign: 'center',
                                }}>
                                {this.state.emptymessage == false
                                  ? null
                                  : 'No Transactions'}
                              </Text>
                            </View>
                          )}
                    />
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    view_TransactionMain: { flex: 1, marginHorizontal: 12, paddingTop: 12, marginBottom: 16 },
    icon_Transaction: { height: 14, width: 14, resizeMode: 'contain' },
    txt_Transaction: { marginLeft: 4, fontFamily: Fonts.segui_semiBold, flex: 1, fontSize: 12, },
    icon_Clock: { height: 10, width: 10, resizeMode: 'contain', tintColor: Colors.brownishGrey },
    txt_TransactionPrice: { fontSize: 16, fontFamily: Fonts.segoeui_bold, marginTop: 15, },
    txt_property: { marginTop: 2, fontSize: 12, fontFamily: Fonts.segui_semiBold, color: Colors.black },
    txt_TxIdNumber: { fontSize: 12, color: Colors.black, fontFamily: Fonts.segui_semiBold, },
    txt_DebitCard: { fontSize: 12, color: Colors.black, fontFamily: Fonts.segui_semiBold, },
});