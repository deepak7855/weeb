import React, { Component } from 'react'
import { Text, RefreshControl, ScrollView, View, DeviceEventEmitter, StyleSheet, Image, TextInput, TouchableOpacity, FlatList, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import InActiveProperty from '../screens/InActiveProperty'
import ActiveProperty from '../screens/ActiveProperty'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images'
import AppHeader from '../Components/AppHeader';
import { handleNavigation } from '../navigation/routes';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import { Helper, Network, AlertMsg } from '../Lib/index';
import { ApiCall } from '../Api/index';
import { GoogleApiAddressList } from '../Components/Common/index';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { translate } from '../Language';


const Tab = createMaterialTopTabNavigator();
function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="inactive"
            tabBarOptions={{
                activeTintColor: Colors.darkSeafoamGreen,
                inactiveTintColor: Colors.greyishBrown,
                indicatorStyle: { backgroundColor: Colors.darkSeafoamGreen },
                labelStyle: { fontSize: 16, fontFamily: Fonts.segoeui_bold },
                style: { backgroundColor: Colors.lightMint },
            }}
        >
            <Tab.Screen
                name="inactive"
                // component={InActiveProperty}
                options={{ tabBarLabel: translate("INACTIVE") }}
            // children={() => <InActiveProperty  />}
            >
                {props => <InActiveProperty {...props} />}
            </Tab.Screen>
            <Tab.Screen
                name="active"
                //  component={ActiveProperty}
                options={{ tabBarLabel: translate("ACTIVE") }}
            >
                {props => <ActiveProperty {...props} />}
            </Tab.Screen>
        </Tab.Navigator>
    );
}


export default class ListedPropertyTabs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openId: '',
            opener: false,
            dataOfFilter: [
                { iconChecked: images.filter_check, text: 'Full House', selected: false },
                { iconChecked: images.filter_check, text: 'Apartment in House ', selected: false },
                { iconChecked: images.filter_check, text: 'Chalet', selected: false },
                { iconChecked: images.filter_check, text: 'Apartment in Building', selected: false },
            ],
            selectedArr: [],
            ActivePropertyDataState: '',
            InActivePropertyDataState: '',
            address: '',
            lat: '',
            long: '',
            modalVisible: false,
            refreshing: false,
            checkInd: -1
        }
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Listed Property',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }






    onAddProperty = () => {
        handleNavigation({ type: 'pushTo', page: 'SelectPropertyType', navigation: this.props.navigation });
    }


    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    goBack = () => {
        handleNavigation({ type: 'push', page: 'DrawerStack', navigation: this.props.navigation, });
    }

    openFilter = () => {
        this.setState({ opener: !this.state.opener })
    }

    selectItemGroup(value) {
        if (this.state.checkInd == value) {
            this.setState({ checkInd: -1 })
            DeviceEventEmitter.emit('Listed-Property-Filter', { type: '' });
            this.setState({ opener: false })
        }
        else {
            this.setState({ checkInd: value })
            DeviceEventEmitter.emit('Listed-Property-Filter', { type: this.state.dataOfFilter[value]?.text });
            this.setState({ opener: false })
        }
    }

    renderOfFilter = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.selectItemGroup(index)}
                    style={styles.ViewRenderFilter}>
                    <Image style={this.state.checkInd == index ? styles.checkIcon : styles.uncheckIcon} source={item.iconChecked} />
                    <Text style={this.state.checkInd == index ? styles.selectedText : styles.unselectText}>{item.text}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    handleAddress = (data) => {
        console.log('property location', data)
        this.setState({ address: data?.addressname, lat: data?.lat, long: data?.long })
        DeviceEventEmitter.emit('owner-property-location', { lat: data?.lat, long: data?.long });
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBarCustom backgroundColor={Colors.lightMint10} translucent={false} />
                <KeyboardAwareScrollView

                    keyboardShouldPersistTaps={'handled'} bounces={false} contentContainerStyle={{ flex: 1 }}>


                    <TouchableOpacity style={styles.viewTop} onPress={() => this.setState({ modalVisible: true })}>
                        <View style={styles.viewTextInput}>
                            <Image style={styles.searchIcon} source={images.search_icon} />
                            <TextInput editable={false} placeholder={'Search by Location or Address'} placeholderTextColor={Colors.pinkishGrey} style={styles.textInputStyl} value={this.state.address} />
                        </View>
                    </TouchableOpacity>
                    <GoogleApiAddressList
                        modalVisible={this.state.modalVisible}
                        hideModal={() => { this.setState({ modalVisible: false }) }}
                        onSelectAddress={this.handleAddress}

                    />
                    <MyTabs props={this.props} />
                    <View>
                        <TouchableOpacity
                            onPress={() => this.openFilter()}
                            style={styles.filterTouch}>
                            <Image style={styles.iconFilter} source={images.property_filter} />
                        </TouchableOpacity>
                        {this.state.opener ? <View style={styles.filteView}>
                            <View style={[styles.viewTopTextOfFilter, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
                                <Text style={styles.textPropTypeOfFilter}>{translate("PropertyType")}</Text>
                                <TouchableOpacity onPress={() => { this.openFilter() }}>
                                    <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={images.close} />
                                </TouchableOpacity>
                            </View>
                            <FlatList
                                data={this.state.dataOfFilter}
                                renderItem={this.renderOfFilter}
                                ItemSeparatorComponent={() => <View style={styles.filterSeperator} />} />
                        </View> : null}
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    viewTextInput: { flexDirection: 'row', backgroundColor: Colors.whiteTwo, alignItems: 'center', paddingLeft: 13, borderRadius: 4, marginTop: 0 },
    viewTop: { paddingLeft: 13, paddingRight: 10, paddingTop: 18, backgroundColor: Colors.lightMint, height: 60 },
    searchIcon: { width: 11, height: 11, resizeMode: 'contain' },
    textInputStyl: { fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40, width: 300 },
    filterTouch: { position: 'absolute', bottom: 20, right: 12 },
    iconFilter: { width: 56, height: 56, resizeMode: 'contain' },
    filteView: { position: 'absolute', bottom: 90, right: 12, backgroundColor: Colors.whiteTwo, elevation: 5, width: 200, borderRadius: 10 },
    viewTopTextOfFilter: { paddingVertical: 10, paddingHorizontal: 20, backgroundColor: Colors.whiteNine, borderBottomColor: Colors.whiteFive, borderBottomWidth: 1 },
    textPropTypeOfFilter: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    filterSeperator: { borderWidth: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.05 },
    ViewRenderFilter: { flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 23, backgroundColor: Colors.whiteTwo, alignItems: 'center' },
    checkIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.darkSeafoamGreen },
    uncheckIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.pinkishGrey },
    selectedText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.black },
    unselectText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.pinkishGrey },
})

