import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, TouchableOpacity, TextInput, Platform, SafeAreaView } from 'react-native'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import RNPickerSelect from 'react-native-picker-select';
import { handleNavigation } from '../navigation/routes';
import AppHeader from '../Components/AppHeader';
import {Constant} from '../Lib/Constant';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import {
    ProgressiveImage,
    GoogleApiAddressList,
    FlootingButton,
  } from '../Components/Common/index';
import { translate } from '../Language';
export default class Maintenance_16_3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            setSelectedValue: [
                { label: 'Plumber', value: 'plumber', },
                { label: 'Water', value: 'water', },
                { label: 'Electricity', value: 'electricity', },
                { label: 'Others', value: 'others', }
            ],
            selectedId: null,
            propertyData:this.props.route.params?.propertyData,
            discraption:null,
        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Add Request',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    addRequest = () =>{
        Network.isNetworkAvailable().then((isConnected) => {
            if(isConnected){
                Helper.mainApp.showLoader();
                ApiCall.ApiMethod({
                    Url:  'add-maintenance-request',
                    method: 'POST',
                    data: {
                      type: this.state.selectedId,
                      property_id: this.state.propertyData?.property_id,
                      description: this.state.discraption,
                    },
                  }).then((res)=>{
                        console.log('request res',res)
                        Helper.mainApp.hideLoader()
                        if(res?.status){
                            this.setState({
                                selectedId: null,
                                description:null
                            },()=>{
                                handleNavigation({ type: 'push', page: 'MaintenanceTab', navigation: this.props.navigation })
                            })
                            Helper.showToast(translate('Yourrequestissubmitted'));
                        }else{
                            Helper.showToast(translate("Somethingwentwrong,pleasetryagainlater"));
                        }
                  }).catch((err)=>{
                    Helper.mainApp.hideLoader()
                        console.log('err',err)
                  })
            }
        }).catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
    }

    onTabSumit = () => {
        if(!this.state.selectedId){
            Helper.showToast(translate('PleaseSelectIssue'))
            return false;
        }
        if(!this.state.discraption){
            Helper.showToast(translate('PleaseEnterDescription'))
            return false;
        }

        this.addRequest();
        //handleNavigation({ type: 'push', page: 'MaintenanceTab', navigation: this.props.navigation });
    }

    dropDowns = () => {
        return (
            <View style={styles.viewDropdown}>
                <RNPickerSelect
                    items={this.state.setSelectedValue}
                    useNativeAndroidPickerStyle={false}
                    placeholder={{label:"Select Issue Type",value:null,}}
                    onValueChange={(val) => {
                        this.setState({ selectedId: val });
                    }}
                    Icon={() => {
                        return (
                            <Image
                                resizeMode="contain"
                                source={images.down_arrow}
                                style={{
                                    width: 12,
                                    height: 12,
                                    marginTop: 15,
                                    tintColor: 'black',
                                    marginRight: 20,
                                }}
                            />
                        );
                    }}
                    value={this.state.selectedId}
                    style={pickerSelectStyles}
                />
            </View>
        )
    }
    TopBar = () => {
        return (
            <View style={styles.viewTop}>
                  <ProgressiveImage
          source={{
            uri: this.state.propertyData?.property?.propertyphotos
              ? this.state.propertyData?.property?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.iconHome}
          resizeMode="cover"
        />
                {/* <Image style={styles.iconHome} source={images.propert_requested} /> */}
                <View style={{ marginLeft: 13 }}>
                    <Text style={styles.textTitle}>{this.state.propertyData?.property?.title}</Text>
                    <View style={styles.view3}>
                        <Image style={styles.iconLocation} source={images.location} />
                        <Text style={styles.textAddress}>{this.state.propertyData?.property?.location}</Text>
                    </View>
                    <View style={styles.view4}>
                        <Text style={styles.textRate}>${this.state.propertyData?.property?.rent}</Text>
                        <Text style={[styles.textRate, { fontFamily: Fonts.segoeui }]}>/m</Text>
                        <View style={styles.dot} />
                        <Text style={styles.textPropType}>{this.state.propertyData?.property?.type}</Text>
                    </View>
                </View>
            </View>
        )
    }
    render() {
        console.log('this.state.propertyData?.property?.title',this.state.propertyData)
        return (
            <View style={styles.container}>
                <ScrollView>
                    {this.TopBar()}
                    <Text style={styles.textIssue}>{translate("IssueType")}</Text>
                    {this.dropDowns()}
                    <Text style={styles.textDescription}>{translate("Description")}</Text>
                    <View style={styles.viewTextinput}>
                        <Image style={styles.iconWrite} source={images.customer_support} />
                        <TextInput 
                        style={styles.textinputWrite}
                         placeholder={'Write here'} 
                         multiline={true}
                         placeholderTextColor={Colors.pinkishGrey} 
                         onChangeText={(text)=>this.setState({discraption: text })}
                         />
                    </View>
                    <View >
                        <Text style={styles.textRed}>{translate("MaxWords")}</Text>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    onPress={() => { this.onTabSumit() }}
                    style={styles.touchBtn}>
                    <Text style={styles.textSubmit}>{translate("Submit")}</Text>
                </TouchableOpacity>
                <SafeAreaView/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, },
    viewTop: { flexDirection: 'row', paddingLeft: 14, paddingVertical: 25, backgroundColor: Colors.whiteTwo, elevation: 1 },
    textRed: { fontSize: 10, marginLeft: 11, marginTop: 10, color: Colors.cherryRed },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold ,width:"75%"},
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { fontSize: 12, lineHeight: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 },
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, fontFamily: Fonts.segoeui_bold, color: Colors.darkSeafoamGreen },
    viewTextinput: {marginHorizontal: 11, marginTop: 10, borderWidth: 0.5, borderColor: Colors.pinkishGreyTwo, borderRadius: 4, flexDirection: 'row', paddingHorizontal: 20, height: 250, },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, color: Colors.marigold, fontFamily: Fonts.segui_semiBold },
    textSubmit: { fontSize: 18, color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui_bold },
    touchBtn: {
        alignItems: 'center', paddingVertical: 10, width: '100%', backgroundColor: Colors.whiteTwo, shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        //shadowOpacity: 0.1,
        //shadowRadius: 5.46,

        
    },
    iconWrite: { width: 15, height: 15, marginTop: 12, resizeMode: 'contain', },
    textinputWrite: { top:Platform.OS === 'ios'? 7:0,left:Platform.OS === 'ios'? 5:0,textAlignVertical: 'top', fontSize: 14,width:'100%' },
    textDescription: { marginLeft: 13, marginTop: 10, fontSize: 16, color: Colors.blackThree, fontFamily: Fonts.segoeui_bold },
    viewDropdown: { marginRight: 10, borderWidth: 0.4, borderColor: Colors.pinkishGreyTwo, marginHorizontal: 11, marginTop: 10 },
    textIssue: { marginLeft: 13, marginTop: 10, fontSize: 16, color: Colors.blackThree, fontFamily: Fonts.segoeui_bold },
})


const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        // backgroundColor: Color.white,
        paddingHorizontal: 20,
        color: Colors.pinkishGrey,
        maxWidth: '100%',
        minWidth: '100%',
        height: 54,
        fontSize: 14,
        fontFamily: Fonts.segoeui,
        // borderColor: Color.black,
        //borderWidth: 2,
    },
    inputAndroid: {
        //backgroundColor: Colors.white,
        paddingHorizontal: 20,
        color: Colors.black,
        maxWidth: '100%',
        minWidth: '100%',
        height: 43,
        fontSize: 14,
        fontFamily: Fonts.segoeui,
        borderColor: Colors.pinkishGrey,
        borderWidth: 1
    },
});
