import React, { Component } from 'react';
import { View, Image, StyleSheet, TextInput, TouchableOpacity, SafeAreaView,FlatList, Text, ActivityIndicator,ScrollView, Modal,Platform } from 'react-native';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../Components/AppHeader';
import { handleNavigation } from '../navigation/routes';
import ModalView from '../Components/ModalView';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import RNGoSell from '@tap-payments/gosell-sdk-react-native';
import { translate } from '../Language';
 const {
    Languages,
    PaymentTypes,
    AllowedCadTypes,
    TrxMode,
    SDKMode
} = RNGoSell.goSellSDKModels;

export default class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isButton: 0,
            SelectMonthData: '',
            SelectMonthConfirm: '',
            SelectPayment: [
                { img_SelectPaymnet: images.master_card, id: 1 },
                { img_SelectPaymnet: images.visa, id: 2 },
            ],
            active: '',
            modalVisibleAccept: false,
            unPaidMonth:this.props.route.params?.unPaidMonths,
            propertyData:this.props.route.params?.propertyData,
            propertyId:this.props.route.params?.propertyData?.property_id,
            year:this.props.route.params?.year,
            tempUnPaidMonth:'',
            totalPayAmount:'',
            paymentDetails:'',
            next_page_url: '',
            currentPage: 1,
            refreshing: false,
            isLoading: false,
            emptymessage: false,
        };
    this.changeState = this.changeState.bind(this);
    this.startSDK = this.startSDK.bind(this);
    this.handleResult = this.handleResult.bind(this);
    this.handleSDKResult = this.handleSDKResult.bind(this);
    this.printSDKResult = this.printSDKResult.bind(this);

    if (!this.sdkModule && RNGoSell && RNGoSell.goSellSDK) {
      this.sdkModule = RNGoSell.goSellSDK
    }
    if (!this.sdkModule && RNGoSell && RNGoSell.goSellSDKModels) {
      this.sdkModels = RNGoSell.goSellSDKModels
    }
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Payment',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });

        
    }
   async startSDK() {
        let ownerShare = await this.state.paymentDetails?.ownerShare/100*this.state.totalPayAmount;
       console.log('a----',this.state.paymentDetails?.ownerShare,'b-----',this.state.totalPayAmount,'c----------',ownerShare,'d-----------',this.state.totalPayAmount-ownerShare)
      const  customer = {
            isdNumber:await Helper.userData?.country_code,
            number: await Helper.userData?.mobile_number,
            customerId: Helper.userData?.customer_id ?Helper.userData?.customer_id : null,
            first_name:await Helper.userData?.name,
            middle_name: await Helper.userData?.name,
            last_name: await Helper.userData?.name,
            email:await Helper.userData?.email,
        };

        console.log('info done',Helper.userData?.customer_id)
      const  paymentReference = {
            track: 'track',
            payment: 'payment',
            gateway: 'gateway',
            acquirer: 'acquirer',
            transaction: null,
            order: null,
            gosellID: null,
        };
        
        
       const  appCredentials = {
            production_secrete_key: (Platform.OS == 'ios') ? 'iOS-Live-KEY' : 'Android-Live-KEY',
            language: Languages.EN,
            sandbox_secrete_key: (Platform.OS == 'ios') ? 'sk_test_b6RyxF7Pm3qi4oXBjOKAprct' : 'sk_test_lAiWOcSI7zBojU2N31HhqywE',
            bundleID: (Platform.OS == 'ios') ? 'com.weeb.ios' : 'com.weeb',
        }
        
      const  allConfigurations = {
            appCredentials: appCredentials,
            sessionParameters: {
                paymentStatementDescriptor: 'paymentStatementDescriptor',
                instantiatePaymentDataSource:'',
                transactionCurrency: 'kwd',
                isUserAllowedToSaveCard: true,
                paymentType: PaymentTypes.ALL,
                amount:this.state.totalPayAmount.toString(),
                //shipping: shipping,
                allowedCadTypes: AllowedCadTypes.ALL,
               // paymentitems: paymentitems,
                paymenMetaData: { a: 'a meta', b: 'b meta' },
                applePayMerchantID: 'applePayMerchantID',
                authorizeAction: { timeInHours: 10, time: 10, type: 'CAPTURE' },
                cardHolderName: '',
                editCardHolderName: true,
                postURL: 'https://tap.company',
                paymentDescription: 'paymentDescription',
                destinations: {
                    "destination": [
                      {
                        "id":this.state.paymentDetails?.ownerDestinationId ,
                        "amount":ownerShare,
                        "currency": "KWD",
                        "description":"testing",
                      },
                    ]
                  },
                trxMode: TrxMode.PURCHASE,
               // taxes: taxes,
                merchantID: '',
                SDKMode: SDKMode.Sandbox,
                customer: customer,
                isRequires3DSecure: true,
                receiptSettings: { id: null, email: true, sms: true },
                allowsToSaveSameCardMoreThanOnce: false,
                paymentReference: paymentReference,
            },
        };
        // console.log(typeof(Helper.ownerPaymentDatails?.totalGrandTotal),Helper.ownerPaymentDatails?.totalGrandTotal);
         console.log(ownerShare,'-----',this.state.paymentDetails?.ownerDestinationId,this.state.tempUnPaidMonth);
        this.sdkModule && this.sdkModule.startPayment(allConfigurations, this.handleResult)
        this.setState({isLoading:true})
       }

       

       getMonthNumber = (monthName) =>{
        let month1 = monthName;
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        month1 = months.indexOf(month1)+1;
        console.log('month number',month1,monthName);
        return month1;
       }
     
      async handleResult(error, status) {
         var myString = status;
         console.log('status is ' + status,error);
         console.log(myString);
        //  Helper.showToast(myString)
         var resultStr = String(status.sdk_result);
         switch (resultStr) {
           case 'SUCCESS':
            // Helper.showToast('Payment Succesfull');
             break
           case 'FAILED':
            Helper.showToast(translate('PaymentFailed'));
             break
           case "SDK_ERROR":
             console.log('sdk error............');
             console.log(status['sdk_error_code']);
             console.log(status['sdk_error_message']);
             console.log(status['sdk_error_description']);
             console.log('sdk error............');
             break
           case "NOT_IMPLEMENTED":
             break
         }
         this.changeState(resultStr, myString, () => {
           console.log('done',resultStr,myString);
           if(resultStr == 'CANCELLED'){
               Helper.showToast(translate('paymentiscancel'));
               this.setState({
                   isLoading:false
               })
               return false;
           }
          else if(resultStr == 'FAILED'){
            Helper.showToast(translate('PaymentFailed'));
            this.setState({
                isLoading:false
            })
            this.sendPaymentDetails(myString);
            this.props.navigation.navigate('PayRenttab')
            return false;
        }
        else if(resultStr == 'SUCCESS'){
            this.sendPaymentDetails(myString);
            this.setState({
                modalVisibleAccept: true,
                isLoading: false,
            })
            return false;
        } else{
            this.setState({
                isLoading:false
            })
            Helper.showToast(translate('PaymentFailed'));
            return false;
        }
        
         });
       }

       sendPaymentDetails = (myString) =>{
              Network.isNetworkAvailable().then((isConnected) => {
               if(isConnected){
                let paidmonths=[]
              this.state.tempUnPaidMonth.map((item)=>{
                    paidmonths.push({
                        month: this.getMonthNumber(item?.month),
                        year:this.state.year,
                        amount:item?.rent
                    })
                   
                })
                const data = {
                    property_id:this.state.propertyId,
                    paidmonths:paidmonths,
                    payment:myString

                }
                console.log('confirm payment data------------',data)
                Helper.mainApp.showLoader();
                   ApiCall.ApiMethod({Url: 'confirm-payment',method: 'POST', data: data}).then((res)=>{
                    Helper.mainApp.hideLoader();
                        console.log('confirm payment api',res)
                        if(res?.status){
                                return true;   
                        }
                   }).catch((err)=>{
                    Helper.mainApp.hideLoader();
                       console.log('api err',err)
                   })
               }
           })
       }
     
       handleSDKResult(result) {
         console.log('trx_mode::::');
         console.log(result['trx_mode'])
        //  switch (result['trx_mode']) {
        //    case "CHARGE":
        //      console.log('Charge');
        //      console.log(result);
        //      this.printSDKResult(result);
        //      break;
     
        //    case "AUTHORIZE":
        //      this.printSDKResult(result);
        //      break;
     
        //    case "SAVE_CARD":
        //      this.printSDKResult(result);
        //      break;
     
        //    case "TOKENIZE":
        //      Object.keys(result).map((key) => {
        //        console.log(`TOKENIZE \t${key}:\t\t\t${result[key]}`);
        //      })
     
        //      // responseID = tapSDKResult['token'];
        //      break;
        //  }
       }
     
       printSDKResult(result) {
        //  if (!result) return
        //  Object.keys(result).map((key) => {
        //    console.log(`${result['trx_mode']}\t${key}:\t\t\t${result[key]}`);
        //  })
       }
     
     
       changeState(newName, resultValue, callback) {
         console.log('the new value is' + newName);
         this.setState(
           {
             statusNow: newName,
             result: resultValue,
           },
           callback,
         );
       }

    componentDidMount() {
        let tempMonth=[];
        let temp = []
        this.state.unPaidMonth.map((item,index)=>{
            tempMonth.push({
                SelectMonth:item?.month,
                isSelect:true,
                id:item?.month,
            })
            temp.push({
                month:item?.month,
                rent:item?.rent,
                serviceCharge:item?.serviceCharge,
                GrandTotal:item?.rent + item?.serviceCharge
            })
        })

        this.setState({
            SelectMonthData:tempMonth, 
            tempUnPaidMonth:temp,
            SelectMonthConfirm:tempMonth
        });
        ApiCall.ApiMethod({Url:'get-settings',method: 'POST',data:{property_id:this.state.propertyId}}).then((res)=>{
            console.log('perchnatege res',res,res?.data?.settings)
                    if(res?.status){
                        let payment = {
                                ownerName:res?.data?.bank_detail?.account_name,
                                ownerAccountNumber:res?.data?.bank_detail?.account_number,
                                ownerBussinessId:res?.data?.bank_detail?.business_id,
                                ownerDestinationId:res?.data?.bank_detail?.destination_id,
                                ownerShare:res?.data?.settings[1]?.percentage
                        }

                        this.setState({
                            paymentDetails:payment
                        })
                      //  Helper.ownerPaymentDatails = payment;
                    }
        }).catch((err)=>{
            console.log('err',err)
        })  
        ApiCall.ApiMethod({Url:'get-cards',method: 'GET'}).then((res)=>{
            console.log('save card list',res);
            if(res?.status){
                this.setState({
                    SelectPayment:res?.data,
                    emptymessage:false,
                })
            }else{
                this.setState({
                    SelectPayment:[],
                    emptymessage:true,
                })
            }
        })
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    onSelect = (item, index) => {
        let SelectMonthData = this.state.SelectMonthData
        SelectMonthData[index] = { ...item, isSelect: !item.isSelect }
       
        if(SelectMonthData[index]?.isSelect){
            let addmonth = [...this.state.unPaidMonth];
           
            for(let i = 0;i<addmonth.length;i++){
                console.log('add month call--------->-------',addmonth[i]);
                if(addmonth[i]?.month == SelectMonthData[index]?.id){
                    console.log('-----m-----',addmonth[i], SelectMonthData[index]?.id);
                    this.state.tempUnPaidMonth.push({month:addmonth[i]?.month,rent:addmonth[i]?.rent,GrandTotal:addmonth[i]?.rent + addmonth[i]?.serviceCharge,serviceCharge:addmonth[i]?.serviceCharge });
                    this.state.SelectMonthConfirm.push({ SelectMonth:addmonth[i]?.month,isSelect:true,id:addmonth[i]?.month})
                    break;
                }
            }
            this.setState({ SelectMonthData})
           
        }else{
            let removemonth= [...this.state.tempUnPaidMonth];
            let removeConfirmMonth =[...this.state.SelectMonthData];
           // console.log('remove month call------------------',removemonth);
            for(let i = 0;i<removemonth.length;i++){
                if(removemonth[i]?.month == SelectMonthData[index]?.id){
                    console.log('-----r-----',removemonth[i], SelectMonthData[index]?.id);
                    if (i > -1) {
                        removemonth.splice(i, 1);
                        removeConfirmMonth.splice(i,1)
                      }
                    break;
                }
            }
           
            this.setState({ SelectMonthData,tempUnPaidMonth:removemonth ,SelectMonthConfirm:removeConfirmMonth})
        }
       // console.log('selectmonthdata------->',SelectMonthData,this.state.tempUnPaidMonth[index])
       // console.log('selectmonthdata2--------',this.state.tempUnPaidMonth,'unpaindmoth-------',this.state.unPaidMonth)
        
    }

    openModelAccept = () => {
        return (
            <Modal
                animationType={"slide"}
                visible={this.state.modalVisibleAccept}
                transparent={true}
                onRequestClose={() => { this.setState({ modalVisibleAccept: false }) }}>
                <ModalView
                    icon={images.success}
                    title={"Success!"}
                    color={Colors.kelleyGreen}
                    subTitle={`Your payment of KWD${this.state.totalPayAmount} has been processed successfully.`}
                    buttonTitle={'Done'}
                    onClick={()=>{this.onPayDone()}}
                />
            </Modal>
        )
    }

    onPayDone=()=>{
        this.setState({modalVisibleAccept: false})
        this.props.navigation.navigate('PayRenttab')
    }

    SelectMonthList = ({ item, index }) => {
        //console.log('confirm',item)
        return (
            <TouchableOpacity onPress={() => this.onSelect(item, index)}
                style={item.isSelect ? styles.btn_ViewActive : styles.btn_ViewUnActive}>
                <Text style={item.isSelect ? styles.txt_MonthActive : styles.txt_MonthUnActive}>{item.SelectMonth}</Text>
            </TouchableOpacity>
        )
    }

    SelectConfirmMonthList = ({ item, index }) => {
        //console.log('confirm',item)
        return (
            <TouchableOpacity
            // onPress={() => this.onSelect(item, index)}
                style={ styles.btn_ViewActive }>
                <Text style={styles.txt_MonthActive }>{item.month}</Text>
            </TouchableOpacity>
        )
    }


    PaymentList = ({ item }) => {
        return (
            <View style={{ marginHorizontal: 12 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.txt_month}>{item.month}</Text>
                </View>
                <View style={[styles.boderBlack, { marginHorizontal: 0 }]}></View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.txt_Label}>{translate("MonthlyRent")}</Text>
                    <Text style={styles.txt_LabelPrice}>KWD {item?.rent}</Text>
                </View>
                <View style={styles.boderGrey}></View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.txt_Label}>{translate("ServiceFee")}</Text>
                    <Text style={styles.txt_LabelPrice}>KWD {item?.serviceCharge}</Text>
                </View>
                <View style={styles.boderGrey}></View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.txt_Total}>{translate("Total")}</Text>
                    <Text style={styles.txt_TotalPrice}>KWD {item?.GrandTotal}</Text>
                </View>
                {item.GrandTotal ?
                    <View >
                        <View style={styles.boderGrey}></View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.txt_GrandTotal}>{translate("GrandTotal")}</Text>
                            <Text style={styles.txt_GrandTotalPrice}>KWD {item?.GrandTotal}</Text>
                        </View>
                    </View>
                    :
                    null
                }
                <View style={[styles.boderBlack, { marginHorizontal: 0, }]}></View>
            </View>
        );
    };

    viewRentDetail() {
        return (
            <ScrollView style={{}}
                showsVerticalScrollIndicator={false}>
                <View style={{ paddingBottom: 70 }}>
                    <Text style={styles.txt_SelectProperty}>{translate("SelectedProperty")}</Text>
                    <View style={styles.view_PropertyDetails}>
                        <Image style={styles.img_Property} source={{uri:this.state.propertyData?.property?.propertyphotos[0]?.imgurl}} />
                        <View style={{ flex: 1, marginLeft: 13 }}>
                            <Text style={styles.txt_Lighhouse}>{this.props.route.params?.propertyData?.property?.title}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Image
                                    style={styles.icon_Location}
                                    source={images.profile_location} />
                                <Text style={styles.txt_PropertyLocation}>{this.props.route.params?.propertyData?.property?.location}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={styles.txtPrice}>KWD {this.props.route.params?.propertyData?.property?.rent} </Text>
                                <Text style={styles.txt_Property}>{this.props.route.params?.propertyData?.property?.type}</Text>
                            </View>
                        </View>
                    </View>

                    <Text style={[styles.txt_SelectProperty, { marginTop: 18 }]}>{translate("SelectMonth")}</Text>

                    {this.state.isButton == 0 &&
                        <View style={{ marginTop: 13 }}>
                            <FlatList
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                data={this.state.SelectMonthData}
                                renderItem={this.SelectMonthList}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()} />
                        </View>
                    }

                    {this.state.isButton == 1 &&
                        <View style={{ marginTop: 13 }}>
                            <FlatList
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                data={this.state.tempUnPaidMonth}
                                renderItem={this.SelectConfirmMonthList}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()} />
                        </View>
                    }

                    <Text style={[styles.txt_SelectProperty, { marginTop: 18, marginBottom: 10 }]}>{translate("AmountDetails")}</Text>
                    <View style={styles.boderBlack}></View>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.tempUnPaidMonth}
                        renderItem={this.PaymentList}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

            </ScrollView>

        )
    }

    onSelectPayment(type) {
        this.setState({ active: type })
    }

    SelectPaymentList = ({ item }) => {
        console.log('card details',item)
        return (
            <TouchableOpacity onPress={() => this.onSelectPayment(item.id)}
                style={(this.state.active == item.id) ? styles.view_SelectCard : styles.view_UnSelectCard}>
                <Image style={styles.img_SelectPaymnet} source={item?.brand == 'MASTERCARD' ?images.master_card:images.visa} />
                <View style={{ flex: 1, marginHorizontal: 12 }}>
                    <Text style={styles.txt_Payment}>XXXX XXXX XXXX {item?.last_four}</Text>
                    <Text style={styles.txt_ExpDate}>Exp. Date : {item?.exp_month}/{item?.exp_year}</Text>
                </View>
                <Image style={styles.icon_Select}
                    source={(this.state.active == item.id) ? images.payment_green_check : images.payment_gray_check} />
            </TouchableOpacity>
        )
    }

    SelectListFooter = () => {
        return (
            <View >
                <TouchableOpacity onPress={() => this.onSelectPayment('ADD')}
                    style={(this.state.active == 'ADD') ? styles.view_SelectCard : styles.view_UnSelectCard}>
                    <Image style={styles.img_SelectPaymnet} source={images.debit_card} />
                    <View style={{ flex: 1, marginHorizontal: 12 }}>
                        <Text style={styles.txt_Payment}>New Debit or Credit Card</Text>
                    </View>
                    <Image style={styles.icon_Select}
                        source={(this.state.active == 'ADD') ? images.payment_green_check : images.payment_gray_check} />
                </TouchableOpacity>
                {this.state.active == 'ADD' &&
                    <View style={styles.view_AddCard}>
                        <Text style={styles.txt_CardLabel}>Card Number</Text>
                        <View style={styles.view_CardBoder}>
                            <TextInput style={styles.input_CardDetail}
                                placeholder={'Enter Card Number'}
                                placeholderTextColor={Colors.brownishGrey} />
                        </View>
                        <Text style={styles.txt_CardLabel}>Card Holder Name</Text>
                        <View style={styles.view_CardBoder}>
                            <TextInput style={styles.input_CardDetail}
                                placeholder={'Enter Card Holder Name'}
                                placeholderTextColor={Colors.brownishGrey} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0.6, marginRight: 8 }}>
                                <Text style={styles.txt_CardLabel}>Expiry Date</Text>
                                <View style={styles.view_CardBoder}>
                                    <TextInput style={styles.input_CardDetail}
                                        placeholder={'MM / YYYY'}
                                        placeholderTextColor={Colors.brownishGrey} />
                                </View>
                            </View>
                            <View style={{ flex: 0.4, marginLeft: 8 }}>
                                <Text style={styles.txt_CardLabel}>CVV</Text>
                                <View style={styles.view_CardBoder}>
                                    <TextInput style={styles.input_CardDetail}
                                        placeholder={'Enter CVV'}
                                        placeholderTextColor={Colors.brownishGrey} />
                                </View>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 12, alignItems: 'center', marginBottom: 20 }}>
                            <Image style={styles.icon_SaveCard} source={images.uncheck_charges} />
                            <Text style={styles.txt_SaveCard}>Save card for future payment.</Text>
                        </View>
                    </View>
                }

                <TouchableOpacity onPress={() => this.onSelectPayment('KNET')}
                    style={(this.state.active == 'KNET') ? styles.view_SelectCard : styles.view_UnSelectCard}>
                    <Image style={styles.img_SelectPaymnet} source={images.knet} />

                    <View style={{ flex: 1, marginHorizontal: 12 }}>
                        <Text style={styles.txt_Payment}>KNET</Text>
                    </View>
                    <Image style={styles.icon_Select}
                        source={(this.state.active == 'KNET') ? images.payment_green_check : images.payment_gray_check} />
                </TouchableOpacity>
            </View>
        )
    }

    viewPayment() {
        return (
            <KeyboardAwareScrollView >
                <Text style={styles.txt_Pay}>{translate("Pay_with")}</Text>
                <View style={{ paddingBottom: 60 }}>
                    <FlatList
                        data={this.state.SelectPayment}
                        renderItem={this.SelectPaymentList}
                        extraData={this.state}
                        contentContainerStyle={{ flexGrow: 1 }}
                        ListFooterComponentStyle={{ flex: 1, justifyContent: 'flex-end' }}
                       // ListFooterComponent={this.SelectListFooter()}
                        ListEmptyComponent={() => (
                            <View
                              style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <Text
                                style={{
                                  fontSize: 14,
                                  fontFamily: Fonts.segoeui,
                                  color: Colors.cherryRed,
                                  textAlign: 'center',
                                }}>
                                {this.state.emptymessage == false
                                  ? null
                                  : 'No Saved Card'}
                              </Text>
                            </View>
                          )}
                        />
                        
                </View>
            </KeyboardAwareScrollView>
        )
    }
paymentConfirm = () =>{
    let totalAmountArray = [...this.state.tempUnPaidMonth];
    let totalAmount = 0;
    totalAmountArray.map((item,index)=>{
            totalAmount = totalAmount+item?.GrandTotal

    })
    this.setState({
        isButton:1,
        totalPayAmount:totalAmount
    })
}
    render() {
        console.log('unPaidMonth',this.state.tempUnPaidMonth,this.state.totalPayAmount)
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ backgroundColor: Colors.lightMint, height: 90 }}>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 30, marginTop: 20 }}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <Text style={styles.txt_Active}>{translate("RENT_DETAILS")}</Text>
                            <Image style={styles.icon_Dot} source={images.payment_green_dot} />
                           <View style={{backgroundColor:Colors.whiteEight,height:2,width:'90%',left:'50%',marginTop:-6}} />
                        </View>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <Text style={styles.txt_Unactive}>{translate("CONFIRM")}</Text>
                            {this.state.isButton > 0 ?
                                <Image style={styles.icon_Dot} source={images.payment_green_dot} />
                                :
                                <Image style={styles.icon_Dot} source={images.payment_red_dot} />
                            }
                            <View style={{backgroundColor:Colors.whiteEight,height:2,width:'90%',left:'50%',marginTop:-6}} />
                        </View>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <Text style={styles.txt_Unactive}>{translate("PAYMENT")}</Text>
                            {this.state.isButton > 1 ?
                                <Image style={styles.icon_Dot} source={images.payment_green_dot} />
                                :
                                <Image style={styles.icon_Dot} source={images.payment_red_dot} />
                            }
                        </View>
                    </View>
                </View>
                {this.state.isButton == 0 && this.viewRentDetail()}
                {this.state.isButton == 1 && this.viewRentDetail()}
                {this.state.isButton == 2 && this.viewPayment()}
                {this.state.isButton == 0 && this.state.tempUnPaidMonth.length >0 ?
                    <TouchableOpacity onPress={() => { this.paymentConfirm() }}
                        style={styles.commanClick}>
                        <Text style={styles.btn_Continue}>{translate("Continue")}</Text>
                    </TouchableOpacity>: null
                }

                {this.state.isButton == 1 &&
                    <TouchableOpacity onPress={() => { this.setState({ isButton: 2 }) }}
                        style={styles.commanClick}>
                        <Text style={styles.btn_Continue}>{translate("Confirm")}</Text>
                    </TouchableOpacity>
                }

                {this.state.isButton == 2 &&
                <View>
                    {this.state.isLoading
                    ?
                    <ActivityIndicator animating={this.state.isLoading} color={Colors.darkSeafoamGreen}/>
                    :
                    <TouchableOpacity 
                    onPress={this.startSDK}
                   // onPress={()=>{this.setState({modalVisibleAccept: true})}}
                     style={styles.commanClick}>
                        <Text style={styles.btn_Continue}>{translate("Pay")} KWD{this.state.totalPayAmount}</Text>
                    </TouchableOpacity>
                    }
                   </View>
                }
                {this.openModelAccept()}

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, },
    txt_Active: { fontFamily: Fonts.segoeui_bold, fontSize: 12, color: Colors.blackTwo },
    icon_Dot: { width: 10, height: 10, resizeMode: 'contain', marginTop: 7 },
    txt_Unactive: { fontFamily: Fonts.segoeui_bold, fontSize: 12, color: Colors.pinkishGrey },
    txt_SelectProperty: { fontFamily: Fonts.segui_semiBold, fontSize: 16, color: Colors.black, marginLeft: 12, marginTop: 20 },
    img_Property: { width: 67, height: 57, resizeMode: 'contain' },
    txt_Lighhouse: { fontFamily: Fonts.segoeui_bold, fontSize: 13, color: Colors.blackTwo },
    txt_PropertyLocation: { color: Colors.brownishGreyTwo, fontSize: 11, marginLeft: 6, fontFamily: Fonts.segoeui, },
    icon_Location: { height: 10, width: 8, resizeMode: 'contain', tintColor: Colors.brownishGreyTwo },
    txtPrice: { fontFamily: Fonts.segoeui_bold, fontSize: 12, color: Colors.darkSeafoamGreen },
    txt_Property: { fontFamily: Fonts.segui_semiBold, fontSize: 12, color: Colors.marigold, marginLeft: 16 },
    view_PropertyDetails: { flexDirection: 'row', borderColor: Colors.whiteFive, borderWidth: 0.5, paddingHorizontal: 15, paddingVertical: 13, borderRadius: 3, alignItems: 'center', marginHorizontal: 12, marginTop: 10 },
    btn_ViewUnActive: { paddingHorizontal: 25, borderWidth: 1, paddingVertical: 4, borderColor: Colors.darkSeafoamGreen, color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui, textAlign: 'center', fontSize: 14, marginHorizontal: 11 },
    btn_ViewActive: { paddingHorizontal: 25, borderWidth: 1, paddingVertical: 4, borderColor: Colors.darkSeafoamGreen, backgroundColor: Colors.darkSeafoamGreen, color: Colors.whiteTwo, fontFamily: Fonts.segoeui, textAlign: 'center', fontSize: 14, marginHorizontal: 11 },
    txt_MonthActive: { fontFamily: Fonts.segoeui, fontSize: 16, color: Colors.white },
    txt_MonthUnActive: { fontFamily: Fonts.segoeui, fontSize: 16, color: Colors.darkSeafoamGreen },
    boderBlack: { backgroundColor: Colors.blackThree, height: 0.5, marginHorizontal: 12, opacity: 0.6 },
    txt_month: { flex: 1, fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.blackTwo, paddingVertical: 12 },
    txt_pay: { fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.whiteTwo, paddingHorizontal: 11, borderRadius: 2 },
    txt_Label: { flex: 1, fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.blackTwo, paddingVertical: 11 },
    txt_LabelPrice: { fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.blackTwo, },
    boderGrey: { backgroundColor: Colors.warmGrey, height: 0.5, opacity: 0.5 },
    txt_Total: { flex: 1, fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, paddingVertical: 11 },
    txt_TotalPrice: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, paddingVertical: 11 },
    txt_GrandTotal: { flex: 1, fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.cherryRed, paddingVertical: 11 },
    txt_GrandTotalPrice: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.cherryRed, paddingVertical: 11 },
    btn_Continue: { flex: 1, textAlign: 'center', fontSize: 18, fontFamily: Fonts.segui_semiBold, color: Colors.darkSeafoamGreen, paddingTop: 10, paddingBottom: 15, elevation: 10 },
    txt_Pay: { fontSize: 16, fontFamily: Fonts.segoeui_bold, color: Colors.black, marginLeft: 15, marginTop: 18 },
    view_SelectCard: { flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.lightMint, paddingHorizontal: 18, paddingVertical: 14, borderRadius: 4, marginHorizontal: 16, marginTop: 15 },
    view_UnSelectCard: { flexDirection: 'row', alignItems: 'center', borderColor: Colors.whiteFive, borderWidth: 1, paddingHorizontal: 18, paddingVertical: 14, borderRadius: 4, marginHorizontal: 16, marginTop: 15 },
    img_SelectPaymnet: { width: 44, height: 30, resizeMode: 'contain' },
    txt_Payment: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo, },
    txt_ExpDate: { fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.brownishGreyTwo, },
    icon_Select: { width: 30, height: 30, resizeMode: 'contain' },
    view_AddCard: { borderColor: Colors.whiteFive, borderWidth: 1, paddingHorizontal: 18, marginHorizontal: 16, marginTop: -1 },
    txt_CardLabel: { fontFamily: Fonts.segoeui, fontSize: 12, color: Colors.black, marginTop: 12 },
    view_CardBoder: { flexDirection: 'row', borderWidth: 1, borderColor: Colors.whiteEight, borderRadius: 4, marginTop: 5 },
    input_CardDetail: { flex: 1, fontFamily: Fonts.segoeui, color: Colors.black, paddingHorizontal: 15, fontSize: 12, paddingVertical: 8 },
    icon_SaveCard: { height: 18, width: 18, resizeMode: 'contain', tintColor: Colors.pinkishGreyTwo },
    txt_SaveCard: { fontFamily: Fonts.segoeui, color: Colors.black, fontSize: 11, marginLeft: 12 },
    commanClick:  {width: "100%", position: 'absolute', bottom: 0, backgroundColor: Colors.whiteTwo, elevation: 10 }


});
