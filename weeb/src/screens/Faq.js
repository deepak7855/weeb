import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ScrollView,
  RefreshControl
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import { handleNavigation } from '../navigation/routes';

export default class Faq extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      id: '',
      FeedDataCollection: [],
      currentPage: 1,
      emptymessage: false,
      isLoading: false,
      refreshing: false,
      next_page_url: '',
      faq:'',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'FAQs',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    this.getFaqList();
  }

  getFaqList() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //Helper.mainApp.showLoader();
          let data = {
            search:this.state.faq
          };
          ApiCall.ApiMethod({
            Url: 'faq' + '?page=' + this.state.currentPage,
            method: 'POST',
            data:data,
          })
            .then((res) => {
              // console.log(
              //   'get faq list',
              //   res?.status
              // );
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res?.status) {
                // this.setState({
                //   FeedDataCollection: res?.data?.data,
                //   isLoading: false,
                //   emptymessage: false,
                //   next_page_url: res?.next_page_url,
                // });
                let arr = [];
                res?.data?.map((item, index) => {
                  console.log('faq data', item);
                  arr.push({
                    title: item?.question,
                    answer:item?.answer,
                    isOpen: false,
                    id:item?.id
                  })
                })

                this.setState({
                  FeedDataCollection: arr,
                  isLoading: false,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
              });
              Helper.mainApp.hideLoader();
              console.log('noti api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getFaqList();
      });
    }
  };
  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: this.state.currentPage, refreshing: false, name: ''},
        () => {
          this.getFaqList();
        },
      );
    }, 2000);
  };

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  collapse(selectid, index) {
    var newAr = this.state.FeedDataCollection;
    newAr[index].isOpen = !newAr[index].isOpen;

    this.setState({FeedDataCollection: newAr});
  }

  RenderFeedCard = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.collapse(item.id, index);
        }}
        style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            marginHorizontal: '7%',
            marginTop: '8%',
            paddingVertical: '4%',
            paddingLeft: 11,
            borderRadius: 4,
            borderColor: Colors.whiteFour,
          }}>
          <Text style={{flex: 1, fontSize: 12, fontWeight: 'bold'}}>
            {item.title}
          </Text>
          <View style={{}}>
            <Image
              source={images.arrow}
              style={{
                width: 15,
                height: 15,
                resizeMode: 'contain',
                marginRight: 14,
                transform: [{ rotate: item.isOpen?'180deg':'360deg' }]
              }}
            />
          </View>
        </View>
        {item.isOpen && (
          <View
            style={{
              flexDirection: 'row',
              borderWidth: 1,
              marginHorizontal: '7%',
              borderBottomRightRadius: 4,
              marginTop: -1,
              paddingVertical: '4%',
              paddingLeft: 11,
              borderBottomLeftRadius: 4,
              borderColor: Colors.whiteFour,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontFamily: Fonts.segoeui,
                color: Colors.black,
              }}>
              {item?.answer}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  onSearch(val) {
    this.setState({
      faq: val
    }, () => {
      this.getFaqList();
    })
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            marginHorizontal: 35,
            marginTop: 15,
            borderColor: Colors.whiteFive,
            borderRadius: 4,
          }}>
          <Image source={images.search_icon} style={styles.icon_search} />
          <TextInput
            style={{
              
              flex: 1,
              fontSize: 12,
              fontFamily: Fonts.segoeui,
              marginLeft: 5,
              color: Colors.black,
              paddingVertical: 8,
            }}
            placeholder="Search Question.."
            placeholderTextColor={Colors.pinkishGrey}
            onChangeText={(text) =>
              this.onSearch(text)
            }
          />
        </View>

        <ScrollView>
          <FlatList
            style={{}}
            data={this.state.FeedDataCollection}
            renderItem={this.RenderFeedCard}
            keyExtractor={(item) => item.id}
            extraData={this.state}
            ItemSeparatorComponent={() => (
              <View style={styles.seperator}></View>
            )}
            ListFooterComponent={() => {
              return this.state.isLoading ? <LoaderForList /> : null;
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
              />
            }
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false ? null : 'No FAQ.'}
                </Text>
              </View>
            )}
          />
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  icon_search: { width: 11, height: 11, resizeMode: 'contain', alignSelf: 'center', marginLeft: 10 }

});