import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
    FlatList,
} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';
import {Helper, Network, AlertMsg} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {ProgressiveImage} from '../../Components/Common/index';
export default class OpenNotification extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passProps: this.props.route.params,
            notificationDetails:[]
        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Notifications',
            rightHide: true
        });
    }


    componentDidMount() {
        // alert('lll')
        this.getNotificationDetails()
    }

    getNotificationDetails() {
        Helper.mainApp.showLoader();
        let data = {
              notification_id: this.state.passProps?.id,
            };
        ApiCall.ApiMethod({
          Url: 'notification-detail',
          method: 'POST',
          data: data,
        })
          .then((res) => {
            Helper.mainApp.hideLoader();
              console.log('open notification details', res);
              if (res?.status) {
                  this.setState({
                      notificationDetails:res?.data
                  })
                  return;
              }
              this.setState({
                    notificationDetails:[],
              })
          })
          .catch(() => {
              Helper.mainApp.hideLoader();
              this.setState({
                notificationDetails: [],
              });
          });
        // Network.isNetworkAvailable((isConnected) => {
        //   if (isConnected) {
        //     alert('l');
        //     let data = {
        //       notification_id: this.state.passProps?.id,
        //     };
        //     Helper.mainApp.showLoader();
        //     ApiCall.ApiMethod({
        //       Url: 'notification-detail',
        //       method: 'POST',
        //       data: data,
        //     })
        //       .then((res) => {
        //         Helper.mainApp.hideLoader();
        //         console.log('open notification details', res);
        //       })
        //       .catch(() => {
        //         Helper.mainApp.hideLoader();
        //       });
        //   }
        // }).catch((err) => {
        //   console.log('network err', err);
        // });
    }

    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    render() {
        // console.log('notification',this.state.passProps?.id)
        return (
          <View style={{marginHorizontal: '4.5%'}}>
            {/* <View style={{flexDirection: 'row', marginTop: '9.5%'}}>
              <Text style={{flex: 1, fontSize: 12, color: '#bbb6b6'}}>
                11 jan 2021
              </Text>
              <Text style={{fontSize: 12, color: '#bbb6b6'}}>10:10 pm</Text>
            </View> */}
            <View
              style={{
                borderWidth: 0.5,
                marginTop: 15,
                paddingBottom: '10%',
                borderColor: '#bbb6b6',
                borderRadius: 4,
              }}>
              <View style={{marginTop: 13, flexDirection: 'row'}}>
                {/* <Image
                            source={this.state.passProps.icon}
                            style={{
                                width: 41,
                                height: 41,
                                marginLeft: 10,
                                alignSelf: 'center',
                                borderRadius: 50
                            }}
                        /> */}
                <ProgressiveImage
                            source={{
                                uri: this.state.notificationDetails
                                    ?
                                    this.state.notificationDetails?.sender?.profile_picture
                                    :'' }}
                  style={{
                                width: 41,
                                height: 41,
                                marginLeft: 10,
                                alignSelf: 'center',
                                borderRadius: 50
                            }}
                  resizeMode="cover"
                />
                <View style={{marginLeft: 11, marginTop: 4}}>
                  <Text style={{fontWeight: 'bold', fontSize: 12}}>
                    {this.state.notificationDetails?.title}
                  </Text>
                  {/* <Text style={{ fontSize: 12 }}>Subheading Here</Text> */}
                </View>
              </View>
              <View
                style={{
                  borderBottomWidth: 0.5,
                  marginTop: 16.5,
                  borderBottomColor: '#bbb6b6',
                }}
              />
              <View style={{marginHorizontal: 13.5, marginVertical: 12.5}}>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#000',
                    lineHeight: 21,
                    opacity: 0.5,
                  }}>
                  {this.state.notificationDetails?.message}
                </Text>
              </View>
            </View>
          </View>
        );
    }
}
const styles = StyleSheet.create({});