import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    FlatList,
    ScrollView,
    TouchableOpacity,
  SafeAreaView,
    RefreshControl
} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images'
import { handleNavigation } from '../../navigation/routes';
import {Helper, Network, AlertMsg} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {ProgressiveImage} from '../../Components/Common/index';
import { translate } from '../../Language';
export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FeedDataCollection: [],
      currentPage: 1,
      emptymessage: false,
      isLoading: false,
      refreshing: false,
      next_page_url: '',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Notifications',
      rightHide: true,
    });
  }

  componentDidMount() {
    this.getNotificationList();
  }

  getNotificationList() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
         // Helper.mainApp.showLoader();
          let data = {};
          ApiCall.ApiMethod({
            Url: 'get-notifications' + '?page=' + this.state.currentPage,
            method: 'GET',
          })
            .then((res) => {
              // console.log(
              //   'get notification list',
              //   res?.data
              // );
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res?.status) {
                this.setState({
                  FeedDataCollection: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
              });
              Helper.mainApp.hideLoader();
              console.log('noti api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  openNotification = (item) => {
    handleNavigation({
      type: 'push',
      page: 'OpenNotification',
      navigation: this.props.navigation,
      passProps: item,
    });
  };

  RenderFeedCard = ({item, index}) => {
    console.log('item', item?.sender);
    return (
      <View>
        {index == 0 ||
        this.state.FeedDataCollection[index - 1].date != item.date ? (
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              marginTop: '4%',
              marginHorizontal: 13,
            }}>
            {item.date}
          </Text>
        ) : null}
        <TouchableOpacity
          onPress={() => {
            this.openNotification(item);
          }}
          style={{marginTop: 16, backgroundColor: Colors.whiteTwo}}>
          <View
            style={{
              borderWidth: 0.5,
              marginHorizontal: 15,
              paddingVertical: 14,
              flexDirection: 'row',
              borderRadius: 4,
              borderColor: '#bbb6b6',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ProgressiveImage
              source={{
                uri: item?.sender ? item?.sender?.profile_picture : '',
              }}
              style={{
                height: 41,
                width: 41,
                marginLeft: 5,
                marginRight: 20,
                borderRadius: 50,
              }}
              resizeMode="cover"
            />
            {/* <Image source={item.icon}
                style={{
                  height: 2, width: 2,
                  marginLeft: 3,
                  marginRight: 3,
                  borderRadius: 50
                }} /> */}
            <View style={{flex: 1}}>
              <Text style={{fontFamily: Fonts.segoeui_bold, fontSize: 12}}>
                {item?.title}
              </Text>
              <Text style={{fontSize: 12, marginTop: 7, lineHeight: 21}}>
                {item?.message}
              </Text>
              {/* <Text style={{ fontSize: 12, color: '#bbb6b6', marginTop: 7, fontFamily: Fonts.segoeui_bold }}>10:10 am</Text> */}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getNotificationList()
      });
    }
  };
  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: this.state.currentPage, refreshing: false, name: ''},
        () => {
          this.getNotificationList();
        },
      );
    }, 2000);
  };

  render() {
    return (
      <ScrollView style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
        <StatusBarCustom
          backgroundColor={Colors.whiteTwo}
          translucent={false}
        />
        <View style={{marginHorizontal: 13, marginTop: '10%'}}>
          <Text style={{fontSize: 15}}>
            {translate("Youhave")}
            <Text style={{color: Colors.cherryRed}}>
              {' '}
              {this.state.FeedDataCollection
                ? this.state.FeedDataCollection.length
                : 0}{' '}
              {translate("Notifications")}
            </Text>{' '}
          </Text>
        </View>
        <FlatList
          style={{}}
          data={this.state.FeedDataCollection}
          renderItem={this.RenderFeedCard}
          extraData={this.state}
          ItemSeparatorComponent={() => <View style={styles.seperator}></View>}
          ListFooterComponent={() => {
            return this.state.isLoading ? <LoaderForList /> : null;
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          keyboardShouldPersistTaps={'handled'}
          refreshing={this.state.refreshing}
          ListEmptyComponent={() => (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: Fonts.segoeui,
                  color: Colors.cherryRed,
                  textAlign: 'center',
                }}>
                {this.state.emptymessage == false ? null : 'No Notification.'}
              </Text>
            </View>
          )}
        />
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({});