import React from 'react';
import { Text, View, Image, StyleSheet,DeviceEventEmitter } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import BookingRequest from './BookingRequest';
import BookingAccepted from './BookingAccepted';
import BookingDeclined from './BookingDeclined';
import { handleNavigation } from '../navigation/routes';
import AppHeader from '../Components/AppHeader';
import { Helper, Network, AlertMsg } from '../Lib/index';
import { ApiCall } from '../Api/index';
import { translate } from '../Language';
let i  = 0;
const RenderTabIcons = (props) => {
  const { icon, lable, isFocused, subIcon } = props;
  return (
    <View style={[styles.topTabMainView]}>
      <Image source={icon} style={styles.tabIconOfCss} />
      <View style={{ width: 100 }}>
        <Text
          style={[
            styles.lableText,
            { color: isFocused ? Colors.darkSeafoamGreen : Colors.greyishBrown },
          ]}>
          {lable}
        </Text>
      </View>
      {subIcon
        ?
        <View style={{ position: 'absolute', right: -10, top: 0, height: 15, width: 15, borderRadius: 15, backgroundColor: Colors.cherryRed, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 10, fontFamily: Fonts.segoeui }}>{subIcon}</Text>
          {/* <Image
            source={subIcon}
            style={{ height: 15, width: 15, resizeMode: 'contain' }}
          /> */}
        </View>
        :
        null
      }

    </View>

  );
};

const Tab = createMaterialTopTabNavigator();

export default class BookingRequestTopTab extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tenantRequestData: '',
      tenantAcceptedData: '',
      tenantDeclinedData: '',
      tenantAcceptCount: '',
      ownerAcceptCount: ''
    }
    AppHeader({
      ...this,
      leftHeide: false,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.goBack() },
      title: 'Booking Requests',
      searchClick: () => { this.notification() },
      searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
      search: true
    });
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
    //  alert('call')
       DeviceEventEmitter.emit('getTenantBookingRequestData', '');
     // this.getTenantBookingRequestData();
    });
    this.getTenantBookingRequestData(2)
    DeviceEventEmitter.emit('getTenantBookingRequestData', '');
    this.listner = DeviceEventEmitter.addListener(
      'notificaionCount',
      (data) => {
       // alert(data);
        // console.log('noti count in booking request',data)
        AppHeader({
          ...this,
          leftHeide: false,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          title: 'Booking Requests',
          searchClick: () => {
            this.notification();
          },
          searchIcon:
            data > 0 ? images.notification_goup : images.notification_icon,
          search: true,
        });
      },
    );
  }

  componentWillUnmount() {
    this.listner.remove()
  }

  getTenantBookingRequestData = (status) => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {

        const data = {
          "status": status
        }
        console.log('total render -------in booknig tabs',i++)
        if(Helper.userType == 'OWNER'){
          ApiCall.ApiMethod({ Url: 'get-bookings-by-owner', method: 'POST', data: data }).then((res) => {

            if (res?.status) {
              this.setState({
                ownerAcceptCount: res?.data ? res?.data?.data.length : 0
              })
  
            } else {
              console.log('tenant fail data', res)
            }
          }).catch((err) => {
            //console.log('api fail err', err)
          })
        }else{
          ApiCall.ApiMethod({ Url: 'get-booking-by-tenant', method: 'POST', data: data }).then((res) => {
            //console.log('accepted length',res?.data);
            if (res?.status) {
              this.setState({
                tenantAcceptCount: res?.data ? res?.data?.data.length : 0
              })
  
            } else {
              console.log('tenant fail data', res)
            }
          }).catch((err) => {
            //console.log('api fail err', err)
          })
        }
        

        
      }
    })
  }

  updateCountNumber = () => {
    this.getTenantBookingRequestData(2)
  }
  goBack = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }


   MyTabs = ({ propertyData, propertyDetails, propertyCancel }) => {
    return (
      Helper.userType == 'TENANT' ?
        <View style={{ flex: 1, backgroundColor: Colors.lightMint, top: -25 }}>
          <Tab.Navigator
            initialRouteName="BookingRequest"
            tabBarOptions={{
              showIcon: true,
              style: { backgroundColor: Colors.lightMint, hieght: 151, marginTop: 50, elevation: 0 },
            }}>
            <Tab.Screen
              name="Request"
              //component={() => <BookingRequest type={'Request'} navigation={this.props.navigation}/>}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.requested_active : images.requested}
                      lable={translate("REQUESTED")}
                      isFocused={focused}
                    />
                  );
                },
              }}
            >
              {props => <BookingRequest {...props} type={'Request'} navigation={this.props.navigation} propertyData={propertyData} propertyCancel={propertyCancel} />}
            </Tab.Screen>
            <Tab.Screen
              name="Accepted"
              // component={() => <BookingAccepted type={'Accepted'} navigation={this.props.navigation} />}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.accepted_active : images.acceped}
                      lable={translate("ACCEPTED")}
                      isFocused={focused}
                      subIcon={this.state.tenantAcceptCount}
                    />
                  );
                },
              }}
            >
              {props => <BookingAccepted {...props} type={'Accepted'} navigation={this.props.navigation} updateCountNumber={() => this.updateCountNumber} />}
            </Tab.Screen>
            <Tab.Screen
              name="Declined"
              //   component={() => <BookingDeclined type={'Declined'} navigation={this.props.navigation}/>}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.declined_active : images.declined}
                      lable={translate("DECLINED")}
                      isFocused={focused}
                    />
                  );
                },
              }}
            >
              {props => <BookingDeclined {...props} type={'Declined'} navigation={this.props.navigation} />}
            </Tab.Screen>
          </Tab.Navigator>
        </View>
        :
        <View style={{ flex: 1, backgroundColor: Colors.lightMint, top: -25 }}>
          <Tab.Navigator
            initialRouteName="BookingRequest"
            tabBarOptions={{
              showIcon: true,
              style: { backgroundColor: Colors.lightMint, hieght: 151, marginTop: 50, elevation: 0 },
            }}>
            <Tab.Screen
              name="Request"
              // component={BookingRequest}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.actived : images.deactived}
                      lable={translate("REQUESTS")}
                      isFocused={focused}
                    />
                  );
                },
              }}
            >
              {props => <BookingRequest {...props} type={'Request'} navigation={this.props.navigation} />}
            </Tab.Screen>
            <Tab.Screen
              name="Accepted"
              //  component={BookingAccepted}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.accepted_active : images.acceped}
                      lable={translate("ACCEPTED")}
                      isFocused={focused}
                      subIcon={this.state.ownerAcceptCount}
                    />
                  );
                },
              }}
            >
              {props => <BookingAccepted {...props} updateCountNumber={this.updateCountNumber} />}
            </Tab.Screen>
            <Tab.Screen
              name="DeclinedOwner"
              //  component={BookingDeclined}
              options={{
                tabBarLabel: '',
                tabBarIcon: ({ focused }) => {
                  return (
                    <RenderTabIcons
                      icon={focused ? images.declined_active : images.declined}
                      lable={translate("DECLINED")}
                      isFocused={focused}
                    />
                  );
                },
              }}
            >
              {props => <BookingDeclined {...props} />}
            </Tab.Screen>
          </Tab.Navigator>
        </View>
    );
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.MyTabs({
          propertyData:
            !this.props?.route?.state?.index ?
              this.state.tenantRequestData :
              this.props?.route?.state?.index == 1 ?
                this.state.tenantAcceptedData : this.state.tenantDeclinedData
        })
        }
      </View>
    )
  }
}


const styles = StyleSheet.create({
  topTabMainView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    paddingTop: 10,

  },
  tabIconOfCss: { height: 27, width: 27, resizeMode: 'contain' },
  lableText: {
    fontSize: 13,
    textAlign: 'center',
    fontFamily: Fonts.segui_semiBold,
  },
});
