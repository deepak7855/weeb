import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,TouchableOpacity, Platform,DeviceEventEmitter } from 'react-native';
import RenderUserProfile from './renderUserProfile';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Colors from '../../Lib/Colors'
import Fonts from '../../Lib/Fonts';
import Images from '../../Lib/Images';
import { Inputs, GeoLocation, CountryCodePickerModal } from '../../Components/Common/index';
import CameraController from '../../Lib/CameraController';
import AppHeader from '../../Components/AppHeader';
import { handleNavigation } from '../../navigation/routes';
import images from '../../Lib/Images';
import Helper from '../../Lib/Helper';
import { ApiCall } from '../../Api/index';
import { Validation, AlertMsg, Network } from '../../Lib/index';
import { Constant } from '../../Lib/Constant';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import FastImage from 'react-native-fast-image';
import { translate } from '../../Language';
export default class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedAvtar: Helper.userData?.profile_picture ? Helper.userData?.profile_picture : '',
            name: Helper.userData?.name ? Helper.userData?.name : '',
            email: Helper.userData?.email ? Helper.userData?.email : '',
            noumber: Helper.userData?.mobile_number ? Helper.userData?.mobile_number : '',
            city: Helper.userData?.city ? Helper.userData?.city : '',
            countryCode: Helper.userData?.country_code ? '+'+Helper.userData?.country_code : '+965',
            modalVisibleCode: false,
            countryCodeWithoutPlus: '965',
            profilePicture:''
        };
        AppHeader({
            ...this,
            leftHeide: false,
            backgroundColor: Colors.lightMint10,
            leftIcon: Helper.userData?.customer_id?images.black_arrow_btn:null,
            leftClick: Helper.userData?.customer_id?() => { this.goBack() }:null,
            title: '',
            searchClick: Helper.userData?.customer_id?() => { this.notification() }:null,
            searchIcon:Helper.userData?.customer_id? images.notification_goup:null,
            search: true
        });
    }

    // componentDidMount() {
    //     console.log('user data',Helper.userData)
    // }
    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }
    callCounteryCodeApi = (dial_code) => {
        this.setState({
            countryCode: dial_code,
            modalVisibleCode: false,
            countryCodeWithoutPlus: dial_code.substring(1),
        })
    }

    NameAndEmailAndMobile = () => {
        return (
            <View>
                <Inputs
                    labelcolor={Colors.brownishGrey}
                    labelsize={12}
                    labelfontfamily={Fonts.segoeui}
                    labels={translate("FullName")}
                    width={'80%'}
                    backgroundcolor={'white'}
                    color={Colors.brownishGrey}
                    rightimage={Images.edit_pofile}
                    placeholder={translate("FullName")}
                    marginleft={-10}
                    keyboard={'default'}
                    maxlength={20}
                    onChangeText={(text) => { this.setState({ name: text }) }}
                    value={this.state.name}
                    setfocus={(input) => {
                        this.name = input;
                    }}
                    getfocus={() => {
                        this.email.focus();
                    }}
                    returnKeyType={'next'}
                    bluronsubmit={false}
                    
                />
                <Inputs
                    labelcolor={Colors.brownishGrey}
                    labelsize={12}
                    labelfontfamily={Fonts.segoeui}
                    labels={translate("EmailAddress")}
                    width={'80%'}
                    backgroundcolor={'white'}
                    color={Colors.brownishGrey}
                    rightimage={Images.email}
                    placeholder={translate("EmailAddress")}
                    marginleft={-10}
                    keyboard={'email-address'}
                   // maxlength={15}
                    onChangeText={(text) => { this.setState({ email: text }) }}
                    value={this.state.email}
                    setfocus={(input) => {
                        this.email = input;
                    }}
                    getfocus={() => {
                        this.noumber.focus();
                    }}
                    returnKeyType={'next'}
                    bluronsubmit={false}
                    editable={Helper.userData?.email?false:true}
                />
                <Text style={{ top: 30, marginHorizontal: 10, fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.brownishGrey }}>{translate("MobileNumber")}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.4 }}>
                        <TouchableOpacity onPress={()=>this.setState({modalVisibleCode:true})} style={[styles.countryView, { backgroundColor: Colors.whiteTwo }]} disabled={Helper.userData?.mobile_number?true:false}>
                            <Image style={{ height: 12, width: 12, resizeMode: 'contain' }} source={images.mobile} />
                            <Text>{this.state.countryCode}</Text>
                            <Image style={{ height: 10, width: 10, resizeMode: 'contain' }} source={images.drop_arrow} />
                        </TouchableOpacity>
                    </View>
                    <CountryCodePickerModal
                        visible={this.state.modalVisibleCode}
                        onRequestClose={() => { this.setState({ modalVisibleCode: false }) }}
                        onPress={() => { this.setState({ modalVisibleCode: false }) }}
                        callCounteryCodeApi={(dial_code) => { this.callCounteryCodeApi(dial_code) }}
                    />
                    <View style={{ flex: 0.6 ,top:Platform.OS == 'ios'?7:0}}>
                        <Inputs
                            labelcolor={Colors.brownishGrey}
                            labelsize={12}
                            labelfontfamily={Fonts.segoeui}
                            labels={true}
                            maxlength={12}
                            width={'80%'}
                            backgroundcolor={'white'}
                            color={Colors.brownishGrey}
                            placeholder={'968-926-0227'}
                            keyboard={'number-pad'}
                            onChangeText={(text) => this.setState({ noumber: text })}
                            value={this.state.noumber}
                            setfocus={(input) => {
                                this.mobile = input;
                            }}
                            setfocus={(input) => {
                                this.noumber = input;
                            }}
                            returnKeyType={'done'}
                            bluronsubmit={false}
                            editable={Helper.userData?.mobile_number ?false:true}
                        />
                    </View>
                </View>
            </View>
        );
    };

    City = () => {
        return (
            <Inputs
                labels={translate("City")}
                width={'87%'}
                value={this.state.city}
                backgroundcolor={Colors.whiteTwo}
                color={Colors.brownishGrey}
                rightimage={Images.location}
                placeholder={this.state.city}
               // marginleft={-10}
                keyboard={'email-address'}
                //maxlength={15}
                onChangeText={(text) => { this.setState({ city: text }) }}
               // leftimage={Images.arrow}
                modalVisible={this.state.modalVisible}
                showModal={() => {
                    this.setState({ modalVisible: true });
                }}
                hideModal={() => {
                    this.setState({ modalVisible: false });
                }}
                modalBackIcon={Images.black_arrow_btn}
                bluronsubmit={true}
            />
        );
    };

    changeImage = (type) => {
        CameraController.open((response) => {
           // console.log(response)
            if (response.path) {
                this.setState({
                    selectedAvtar: response.path,
                    profilePicture: response.path
                });
            }
        });
    }

    updateProfile = () => { 
        if (Validation.checkAlphabat('Full Name', 3, 25, this.state.name) &&
            Validation.checkAlphabat('City', 3, 25, this.state.city)
        ) {
            Network.isNetworkAvailable().then((isConnected) => {
                if (isConnected) {
                    let updateProfile = new FormData();

                    updateProfile.append('name', this.state.name)
                    updateProfile.append('email', this.state.email);
                    updateProfile.append('mobile_number', this.state.noumber);
                    updateProfile.append('country_code', this.state.countryCodeWithoutPlus);
                    updateProfile.append('city', this.state.city);


                    



                    if (this.state.profilePicture) {
                        updateProfile.append('profile_picture', {
                            uri: this.state.profilePicture,
                            name: 'test.jpg',
                            type: 'image/jpeg'
                        });
                    }
                    console.log('---------------form data',updateProfile)
                    Helper.mainApp.showLoader()
                    ApiCall.ApiMethod({ Url: 'update-profile', method: 'POSTUPLOAD', data: updateProfile }).then((response) => {
                        console.log('update profile response', response)

                        if (response?.status) {
                            Helper.showToast(response?.message ? response?.message : AlertMsg?.success?.PROFILEUPDATE)
                            Helper.mainApp.hideLoader()
                            Helper.setData(Constant.USER_DATA, response?.data);
                            Helper.userData = response?.data;
                            this.goBack()
                            return;
                        } else {
                            Helper.mainApp.hideLoader()

                            Helper.showToast(response?.message ? response?.message : AlertMsg.error.NETWORK)
                            return
                        }
                    }).catch((error) => {
                        // Helper.showToast(response?.message ? response?.message : AlertMsg.error.NETWORK)
                        Helper.mainApp.hideLoader()
                        console.log('edit profile error', error)
                        return
                    })    
                } else {
                    Helper.mainApp.hideLoader()
                    Helper.showToast(AlertMsg.error.NETWORK);
                return;
                }
                
            }).catch((err) => {
                console.log(err)
            })
            
            }
     
    }

    componentDidMount() {
        console.log('edit ptofile data',Helper.userData)
        // this.listner2 = DeviceEventEmitter.addListener(Constant.NOTIFICATION_COUNT, (data) => {
        //     alert(data)
        //     console.log('device event', data)
          
        // })
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBarCustom backgroundColor={Colors.lightMint10} translucent={false}/>
                <KeyboardAwareScrollView  keyboardShouldPersistTaps={'handled'}>
                    <RenderUserProfile
                        onPress={() => { this.changeImage() }}
                        selectedAvtar={this.state.selectedAvtar}
                        rightimage={Images.camera}
                        leftimage={Images.profile_check}
                        onSetting={() => { this.updateProfile()}}
                        name={this.state.name}
                        email={this.state.email}
                    />
                    <View style={{ margin: 20 }}>
                        <Text style={{ marginLeft: 10, marginBottom: 20, fontSize: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segui_semiBold }}>{translate("EDITPROFILE")}</Text>
                        {this.NameAndEmailAndMobile()}
                        {this.City()}

                    </View>

                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    countryView: {
        marginHorizontal: 8, marginTop: 43, paddingVertical: 8, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderWidth: 0.5, borderRadius: 4, borderBottomColor: Colors.pinkishGrey,
        borderTopColor: Colors.pinkishGrey,
        borderEndColor: Colors.pinkishGrey,
        borderLeftColor: Colors.pinkishGrey,
        borderRightColor: Colors.pinkishGrey,
        borderColor: Colors.pinkishGrey
    }
})
