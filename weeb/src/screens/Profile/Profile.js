import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, SafeAreaView,Keyboard,DeviceEventEmitter } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AppHeader from '../../Components/AppHeader';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import CameraController from '../../Lib/CameraController';
import Colors from '../../Lib/Colors'
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import Images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';
import RenderUserProfile from './renderUserProfile';
import { ApiCall } from '../../Api/index';
import Helper from '../../Lib/Helper';
import {Constant} from '../../Lib/Constant'
import { translate } from '../../Language';
export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedAvtar: Helper.userData?.profile_picture ? Helper.userData?.profile_picture :'' ,
            name: Helper.userData?.name ? Helper.userData?.name : '',
            email: Helper.userData?.email ? Helper.userData?.email : '',
            phoneNumber: Helper.userData?.mobile_number ? Helper.userData?.mobile_number : '',
            city: Helper.userData?.city ? Helper.userData?.city : '',
            countryCode: Helper.userData?.country_code ? '+' + Helper.userData?.country_code : '',
        };
        AppHeader({
            ...this,
            leftHeide: true,
            backgroundColor: Colors.lightMint10,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            searchClick: () => { this.goNotification() },
            searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
            search: true,
            location: true,
            addBtn: Helper.userType == 'OWNER' ? true : null,
            onAdd: () => { this.addPropartyScreen() },
            addItem: images.addproperty,
            menuClick: () => { this.menuClick() },
            city: this.state.city
        });
    }

    componentDidMount() {
        console.log('user data in profile', Helper.userData)
       this.listner =  DeviceEventEmitter.addListener(Constant.USER_TYPE, (data) => {
            AppHeader({
                ...this,
                leftHeide: true,
                backgroundColor: Colors.lightMint10,
                leftIcon: images.black_arrow_btn,
                leftClick: () => { this.goBack() },
                searchClick: () => { this.goNotification() },
                searchIcon: images.notification_goup,
                search: true,
                location: true,
                addBtn: data?.type == 'OWNER' ? true : null,
                onAdd: () => { this.addPropartyScreen() },
                addItem: images.addproperty,
                menuClick: () => { this.menuClick() },
                city: Helper.userData?.city
            });
        })
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.setState({
                selectedAvtar: Helper.userData?.profile_picture ? Helper.userData?.profile_picture : '',
                name: Helper.userData?.name ? Helper.userData?.name : '',
                email: Helper.userData?.email ? Helper.userData?.email : '',
                phoneNumber: Helper.userData?.mobile_number ? Helper.userData?.mobile_number : '',
                city: Helper.userData?.city ? Helper.userData?.city : '',
                countryCode: Helper.userData?.country_code ? '+' + Helper.userData?.country_code : '',
            })
          
        });
        
    }

    componentWillUnmount() {
        if (this.listner) {
            this.listner.remove();
        }
        this._unsubscribe();
    }
    addPropartyScreen = () => {
        Helper.propertyData = {};
        handleNavigation({ type: 'push', page: 'SelectPropertyType', navigation: this.props.navigation });
    }

    goNotification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    openDrawer = () => {
        Keyboard.dismiss()
        handleNavigation({ type: 'drawer', navigation: this.props.navigation });
    }
    menuClick = () => {
        this.props.navigation.openDrawer();
    }


    changeImage = (type) => {
        CameraController.open((response) => {
            if (response.path) {
                this.setState({ selectedAvtar: response.path });
            }
        });
    }

    onTabSettings = () => {
        handleNavigation({ type: 'push', page: 'Settings', navigation: this.props.navigation });
    }


    renderUserProfie = (icon, label, info) => {
        return (
            <View style={{ marginTop: 25 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={icon} style={{ height: 12, width: 12, resizeMode: 'contain' }} />
                    <Text style={{ marginLeft: 20, fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.brownishGrey }}>{label}</Text>
                </View>
                <Text style={{ fontFamily: Fonts.segoeui, fontSize: 18, color: Colors.black, marginLeft: 28 }}>{info}</Text>
                <View style={{ marginTop: 20, opacity: 0.1, borderWidth: 0.2, backgroundColor: Colors.warmGrey, justifyContent: 'center', marginLeft: 27, marginRight: 27 }} />
            </View>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBarCustom backgroundColor={Colors.lightMint10} translucent={false} />
                <KeyboardAwareScrollView>
                    <RenderUserProfile
                        onPress={() => {  }}
                        selectedAvtar={this.state.selectedAvtar}
                      // rightimage={Images.camera}
                        onSetting={()=>{this.onTabSettings()}}
                        leftimage={Images.settings}
                        name={this.state.name}
                        email={this.state.email}
                    />
                    <View style={{ margin: 20 }}>
                        <Text style={{ fontSize: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segui_semiBold }}>{translate("PERSONALINFO")}</Text>
                        {this.renderUserProfie(Images.full_name,translate("FullName"), this.state.name)}
                        {this.renderUserProfie(Images.profile_email, translate("Email"), this.state.email)}
                        {this.renderUserProfie(Images.profile_phone, translate("MobileNumber"),this.state.countryCode+' '+this.state.phoneNumber)}
                        {this.renderUserProfie(Images.profile_location, translate("City"), this.state.city)}


                        </View>
                        <View style={{ height: 50 }} />
                    </KeyboardAwareScrollView>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteTwo
    },
    headerView: {
        width: '100%',
        height: 150,
        backgroundColor: Colors.lightMint,
        overflow: 'hidden'
    },
    profileView: {
        width: '90%',
        borderRadius: 15,
        alignSelf: 'center',
        paddingHorizontal: 14,
        //paddingBottom: 30,
        marginTop: -100,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        // borderWidth: 1,
        zIndex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: "row"
    }
})