import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image,ActivityIndicator } from 'react-native';
import Colors from '../../Lib/Colors'
import Fonts from '../../Lib/Fonts';
import Images from '../../Lib/Images';
import { ProgressiveImage } from '../../Components/Common/index';
import FastImage from 'react-native-fast-image'
export default class renderUserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        };
    }

    loadStart = (e) => {
        console.log('fast imag load start', e)
        this.setState({ loading: true })
    }


    loadEnd = (e) => {
        console.log('fast imag load end', e)
        this.setState({ loading: false })
    }

    

    render() {
        const {onPress, selectedAvtar, onSetting} = this.props
        return (
            <View>
                <View style={styles.headerView} />
                <View style={styles.profileView}>
                    <TouchableOpacity onPress={()=>{onPress()}}>
                        <Image style={{ marginLeft: 30, height: 30, width: 30, resizeMode: 'contain', top:-10  }} source={this.props.rightimage} />
                    </TouchableOpacity>
                    <ProgressiveImage
                        source={{ uri: selectedAvtar }}
                        style={{ width: 150, height: 150, borderRadius: 100 }}
                        resizeMode="cover"
                    />
                    <TouchableOpacity onPress={() => { onSetting() }}>
                        <Image style={{ marginRight: 30, height: 30, width: 30, resizeMode: 'contain', top:-10 }} source={this.props.leftimage} />
                    </TouchableOpacity>
                </View>
                <View style={{ margin: 15, justifyContent: "center", alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, fontFamily: Fonts.segui_semiBold, color: Colors.blackThree }}>{this.props.name}</Text>
                    <Text style={{ fontSize: 13, fontFamily: Fonts.segoeui, color: Colors.brownishGrey }}>{this.props.email}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        width: '100%',
        height: 110,
        backgroundColor: Colors.lightMint,
        overflow: 'hidden'
    },
    profileView: {
        width: '90%',
        borderRadius: 15,
        alignSelf: 'center',
        paddingHorizontal: 14,
        //paddingBottom: 30,
        marginTop: -100,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        // borderWidth: 1,
        zIndex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: "row",
    },
    imageOverlay: {
       // position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },
    activityIndicator: {
        position: 'absolute',
        zIndex: 1,
        backgroundColor: "transparent",
        left: 0,
        right: 0,
        bottom: 0,
    }
})
