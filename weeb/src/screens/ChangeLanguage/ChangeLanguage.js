import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image
} from 'react-native';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import AppHeader from '../../Components/AppHeader';
import {handleNavigation} from '../../navigation/routes';
import {Helper} from '../../Lib';
import * as RNLocalize from "react-native-localize";
import {translate,setI18nConfig} from '../../Language'
export default class ChangeLanguage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: Helper.currentLanguage == 'en' ?'en':'ar',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: translate('ChangeLanguage'),
      searchClick: () => {
        this.notification();
      },
      searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
      search: true,
    });
  }

  goBack = () => {
    //handleNavigation({type: 'pop', navigation: this.props.navigation});
    this.props.navigation.reset({
        index: 0,
        routes: [
          { name: 'DrawerStack' },
        ],
      })
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  changeLanguages = () => {
    setI18nConfig(this.state.checked);
    this.forceUpdate();
    Helper.setData('currentLanguage', this.state.checked);
    Helper.currentLanguage = this.state.checked;
    Helper.showToast(`Language is changed to ${this.state.checked == 'en' ?'English':'Arabic'}`)
     this.goBack()
  }
  componentDidMount() {
    RNLocalize.addEventListener("change", this.handleLocalizationChange(this.state.checked));
}

componentWillUnmount() {
    RNLocalize.removeEventListener("change", this.handleLocalizationChange);
}

handleLocalizationChange = (lang) => {
    this.setState({
        checked: lang
    })
};
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{marginVertical: '10%'}}>
            
          <TouchableOpacity
          onPress={() => 
            //this.setState({checked: 'en'})
            this.handleLocalizationChange('en')
        }
          style={styles.buttonStyle}
            >
            {this.state.checked == 'en' ?
            <Image source={images.terms_check} 
            style={[styles.termImage,{tintColor:Colors.darkSeafoamGreen}]} />
            :
            <View
            style={styles.unCheckedBox}/>
                
            }
           
            <Text style={{fontSize:16, fontFamily: Fonts.segoeui}}>English</Text>
          </TouchableOpacity>
          
          <TouchableOpacity
          onPress={() =>
            // this.setState({checked: 'ar'})
            this.handleLocalizationChange('ar')
            }
            style={[styles.buttonStyle,{marginTop:'5%'}]}>
               {this.state.checked == 'ar' ?
 <Image source={images.terms_check} style={[styles.termImage,{tintColor:Colors.darkSeafoamGreen}]} />
            :
            <View
            style={styles.unCheckedBox}/>
                
            }
            <Text style={{fontSize:16, fontFamily: Fonts.segoeui}}>Arabic</Text>
          </TouchableOpacity>
     
        </View>
               
        <TouchableOpacity
        onPress={()=>this.changeLanguages()}
        style={{borderWidth:1,width:'90%',height:50,borderColor:Colors.darkSeafoamGreen,justifyContent: 'center',alignItems: 'center',borderRadius:10,marginHorizontal:'5%'}}>
            <Text style={{fontSize:16, fontFamily: Fonts.segoeui}}>{translate('ChangeLanguage')}</Text>
        </TouchableOpacity>
     
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whiteTwo,
    justifyContent: 'space-between',
  },
  termImage: {
    height: 18,
    width: 18,
    padding: 5,
    marginRight: 5,
    resizeMode: 'contain',
  },
  buttonStyle:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
    height:50,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    marginHorizontal:'5%',
    backgroundColor:Colors.whiteTwo,
    paddingHorizontal:'3%'
  },
  unCheckedBox:{
    height: 18,
    width: 18,
    borderRadius: 2,
    borderWidth: 1,
    marginRight: 10,
    borderColor: Colors.darkSeafoamGreen
  }
});
