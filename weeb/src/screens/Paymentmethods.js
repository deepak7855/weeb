import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import { translate } from '../Language';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import { handleNavigation } from '../navigation/routes';

export default class Paymentmethods extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Payment Methods',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    goBack=()=>{
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    addBankCard = () => {
        handleNavigation({ type: 'push', page: 'SelectCardType', navigation: this.props.navigation });
    }


    render() {
        return (
            <View style={styles.container}>
                <StatusBarCustom backgroundColor={Colors.white} translucent={false}/>
                <View>
                    <Image source={images.payment_method}
                        style={styles.imgOffCss} />
                </View>
                <View style={{ marginVertical: 25 }}>
                    <Text style={styles.subTextOffCss}>{translate("YoudonthaveanyBankAccountyet")}. {"\n"}{translate("Pleaseaddyourbankaccounthere")}. {"\n"}<Text style={{ color: Colors.black }}> {translate("ClickAddBankButton")}</Text></Text>
                </View>
                <TouchableOpacity onPress={() => { this.addBankCard() }}
                    style={styles.bttViewOfCss}>
                    <Image source={images.plus} 
                        style={styles.imgCssPlus} />
                    <Text style={styles.bttTextCss}>{translate("AddPaymentMethod")}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo, alignItems: 'center', justifyContent: 'center' },
    imgOffCss: { height: 201, width: 201 },
    subTextOffCss: { color: Colors.brownishGreyTwo, fontSize: 14, fontFamily: Fonts.segoeui, textAlign: 'center' },
    bttViewOfCss: { backgroundColor: Colors.ice, borderRadius: 5, borderColor: Colors.darkSeafoamGreen, borderStyle: 'dashed', borderWidth: 1, flexDirection: 'row', alignItems: 'center', padding: 6, paddingHorizontal: 20 },
    imgCssPlus: { height: 14, width: 14, resizeMode: "contain", marginRight: 10 },
    bttTextCss: { color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui_bold, fontSize: 16, },
})


