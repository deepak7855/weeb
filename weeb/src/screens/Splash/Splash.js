import React, { Component } from 'react';
import { View, Image, StyleSheet, DeviceEventEmitter, ImageBackground } from 'react-native';
import SplashImage from '../../assets/images/splash.png';
import SplashScreen from 'react-native-splash-screen';
import Helper from '../../Lib/Helper';
import { Constant } from '../../Lib/Constant'
import {updateDeviceToken} from '../../Lib/FirebaseHelper'
import {translate,setI18nConfig} from '../../Language';
import moment from 'moment';
export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  async componentDidMount() {
    updateDeviceToken()
    const lang = await Helper.getData('currentLanguage');

    if (lang) {
      if (lang == 'en') {
          moment.locale('en')
      } else {
          moment.locale('ar')
      }
    }
    setI18nConfig(lang);
                    Helper.currentLanguage = lang;
                    
    setTimeout(() => {
      Helper.getData(Constant.USER_DATA).then((response) => {
        if (response) {
          Helper.getData(Constant.TOKEN).then((responseData) => {
            
            Helper.token = responseData
            Helper.userData = response
            Helper.getData(Constant.USER_TYPE).then(async (responseType) => {
               DeviceEventEmitter.emit(Constant.USER_TYPE, { type: responseType });
              Helper.userType = await responseType;
              Helper.userData['user_type'] = await responseType
             // alert(responseType)
            })
           
           
            SplashScreen.hide();
            this.props.navigation.reset({
              index: 0,
              routes: [
                { name: 'DrawerStack' },
              ],
            })
          });
        }
        else {
          SplashScreen.hide();
          this.props.navigation.reset({
            index: 0,
            routes: [
              { name: 'Signin' },
            ],
          })

        }
      }).catch((error) => {
        SplashScreen.hide();
        this.props.navigation.reset({
          index: 0,
          routes: [
            { name: 'Signin' },
          ],
        })
      })
    }, 100);

    Helper.navigationRef = this.props.navigation
  }

  render() {
    return (

      // <View style={styles.container}>
      <ImageBackground source={SplashImage} style={styles.splashImage} />
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashImage: {
    flex: 1,
    // width: '100%',
    // alignSelf: 'center',
  },
});
