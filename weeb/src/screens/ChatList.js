import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  Text,
  DeviceEventEmitter,
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import {handleNavigation} from '../navigation/routes';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Helper from '../Lib/Helper';
import {Network, Validation, AlertMsg} from '../Lib/index'
import ChatController from '../Lib/SocketManager';
import {ProgressiveImage} from '../Components/Common/index';
import { translate } from '../Language';
export default class ChatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatList: [],
      emptyList: [],
      allChats: [],
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Chat',
     // searchIcon: images.more_icon,
      searchClick: () => {},
     // search: true,
    });
  }

  componentDidMount() {
    Network.isNetworkAvailable().then((res)=>{
if(!res){
Helper.showToast(AlertMsg.error.NETWORK)
}
    })
    this.getUserList();
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getUserList();
    });
    this.inboxlistener = DeviceEventEmitter.addListener(
      ChatController.events.get_message_history_response,
      (data) => {
        //console.log('history', data.responseData);
        if (data.responseData.length > 0) {
          this.setState({
            allChats: data.responseData,
            emptyList: {title: '', subTitle: ''},
          });
        } else {
          this.setState({
            allChats: [],
            emptyList: {title: 'Oops!', subTitle: 'No Record Found!'},
          });
        }
      },
    );

    this.searchlistener = DeviceEventEmitter.addListener(
      ChatController.events.search_response,
      (data) => {
      //  console.log('search user', data?.responseDatasir);
        if (data.responseData.length > 0) {
          this.setState({
            allChats: data.responseData,
            emptyList: {title: '', subTitle: ''},
          });
        } else {
          this.setState({
            allChats: [],
            emptyList: {title: 'Oops!', subTitle: 'No Record Found!'},
          });
        }
      },
    );
    this.receive_messagelistener = DeviceEventEmitter.addListener(
      ChatController.events.recevied_message_by_others,
      (data) => {
        console.log('Received message in chat list', data);
        DeviceEventEmitter.emit('user_count', 'data');
        let filterData;
        if (this.state.allChats.length > 0) {
          var index = this.state.allChats.findIndex(
            (p) => p?.conversation_id == data?.responseData?.conversation_id,
          );
          const datas = {
            user_id: data?.requestData?.other_user_id,
            other_user_id: data?.responseData?.userid,
            // user_info: {
            //   picture: this.state.allChats[index]?.other_user_info?.picture,
            //   name: data?.responseData?.other_user_name,
            // },
            other_user_info: {
              picture: this.state.allChats[index]?.other_user_info?.picture,
              name: data?.responseData?.name,
            },
            msg: data?.requestData?.msg,
            conversation_id: this.state.allChats[index]?.conversation_id,
          };
           //console.log('conversion id index', index,datas);
          if (index > -1) {
            let temp = this.state.allChats.splice(index, 1);
            //  console.log('all chats', this.state.allChats)
          }
          this.state.allChats.unshift(datas);
          //console.log('all chats hotory',this.state.allChats)
          this.setState({
            chatList: [],
          });
        }
      },
    );
  }

  componentWillUnmount() {
    this._unsubscribe()
  }

  getUserList = () => {
    //this.setState({showLoader: true});
    let data = {
      user_id: Helper.userData.id,
      limit: 20,
    };
    ChatController.callbackSocket(
      ChatController.events.get_message_history,
      data,
    );
  };

  searchUser(val) {
    if (val == '') {
      this.getUserList();
    }
    let filterData = [];
    let lowercasedFilter = val.toLowerCase();
    if (this.state.allChats.length > 0) {
      filterData = this.state.allChats.filter((item) => {
        return Object.keys(item).some(
          (key) =>
            item[key] &&
            item[key].toString().toLowerCase().startsWith(lowercasedFilter),
        );
      });
    }

   // console.log('filter data', filterData);
    this.setState({
      allChats: filterData,
    });
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  onChatPage = (item) => {
    console.log('item', item?.user_id);
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {
        userId:
          Helper.userData.id == item?.user_id
            ? item?.other_user_id
            : item?.user_id,
      },
      navigation: this.props.navigation,
    });
  };

  renderItem = ({item, index}) => {
   console.log('chat list', item);
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.onChatPage(item);
          }}
          style={styles.viewMain}>
          <View style={styles.img_ChatUser}>
            <ProgressiveImage
              source={{
                uri:item?.other_user_info?.picture}}
              style={styles.img_ChatUser}
              resizeMode="cover"
            />
            {/* <Image style={styles.img_ChatUser} source={item.img_ChatUser} /> */}
            {/* <Image
              style={styles.icon_Online}
              source={
                item.active ? images.online_status : images.offline_status
              }
            /> */}
          </View>
          <View style={{flex: 1, marginLeft: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.txt_ChatUser}>
                {  item?.other_user_info?.name }
              </Text>
              {item?.unread_message ? (
                <View style={styles.txt_ReceiveChat}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: Fonts.segoeui,
                      fontSize: 10,
                      color: Colors.whiteTwo,
                    }}>
                    {item?.unread_message}
                  </Text>
                </View>
              ) : null}
            </View>
            <Text style={styles.txt_ChatMessage}>{item?.msg}</Text>
            {/* <Text style={styles.txt_ChatTime}>10:15 pm</Text> */}
          </View>
        </TouchableOpacity>
        <View style={styles.boder_grey}></View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        <View style={styles.viewSearch}>
          <Image style={styles.icon_Search} source={images.search_icon} />
          <TextInput
            style={styles.txt_SearchName}
            placeholder={translate("SearchbyName")}
            placeholderTextColor={Colors.pinkishGrey}
            onChangeText={(val) => {
              this.searchUser(val);
            }}
          />
        </View>

        <FlatList
          style={{marginTop: 30, paddingTop: 12}}
          data={this.state.allChats}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo},
  viewSearch: {
    backgroundColor: Colors.whiteSix,
    flexDirection: 'row',
    marginHorizontal: 12,
    paddingVertical: 7,
    paddingHorizontal: 21,
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 3,
  },
  icon_Search: {width: 12, height: 12, resizeMode: 'contain'},
  txt_SearchName: {
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.black,
    marginLeft: 9,
    paddingVertical: 0,
  },
  viewMain: {flexDirection: 'row', marginHorizontal: 12, alignItems: 'center'},
  img_ChatUser: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    borderRadius: 60,
  },
  txt_ChatUser: {
    flex: 1,
    fontFamily: Fonts.segui_semiBold,
    fontSize: 16,
    color: Colors.black,
  },
  txt_ChatMessage: {
    fontFamily: Fonts.segoeui,
    fontSize: 14,
    color: Colors.brownishGrey,
  },
  txt_ChatTime: {
    fontFamily: Fonts.segoeui,
    fontSize: 12,
    color: Colors.pinkishGrey,
  },
  txt_ReceiveChat: {
    justifyContent: 'center',
    height: 20,
    width: 20,
    backgroundColor: Colors.cherryRed,
    borderRadius: 20,
  },
  boder_grey: {
    backgroundColor: Colors.warmGrey,
    height: 0.5,
    marginTop: 20,
    marginBottom: 22,
    marginHorizontal: 12,
    opacity: 0.5,
  },
  icon_Online: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    position: 'absolute',
    right: 5,
  },
});
