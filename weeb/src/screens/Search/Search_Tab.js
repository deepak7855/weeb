import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  RefreshControl,
  TextInput,
  TouchableOpacity,
  Dimensions,
  DeviceEventEmitter,
  Keyboard,
  FlatList,
  Modal,
  ScrollView,
  SafeAreaView,
  Platform,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AppHeader from '../../Components/AppHeader';
import WillasList from '../../Components/WillasList';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import PropertyType from '../../Components/PropertyType';
import { handleNavigation } from '../../navigation/routes';
import AvailablefromRooms from '../HomeScreen/Components/AvailablefromRooms';
import Amenities from '../HomeScreen/Components/Amenities';
import AmenitiesList from '../../Components/AmenitiesList';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import { Helper, Network, AlertMsg } from '../../Lib/index';
import { Constant } from '../../Lib/Constant';
import { geoCurrentLocation } from '../../Lib/LocationsPermission';
import { ApiCall, LoaderForList } from '../../Api/index';
import { GoogleApiAddressList } from '../../Components/Common/index';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { translate } from '../../Language';
let selectedFurniture = [];
let selectedAmenties = [];
// let userSelectedFurniture = [];
// let userSelectedAmenties = [];
const { width, height } = Dimensions.get('window');
export default class Search_Tab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: Helper.userData?.city,
      search: '',
      amenitiesSingle: 3,
      selectfurnishing: 0,
      value: 25,
      modalVisible: false,
      PropirtSelect: 0,
      selectBaddRoom: 0,
      PropirtList: [
        {
          propertyTypeIcon: images.full_house_green,
          routh: images.chalet_active,
          sarviceName: 'Full House',
          id: 1,
          type: 'Full House',
        },
        {
          propertyTypeIcon: images.apartment_green,
          routh: images.apartmentin_active,
          sarviceName: 'Apartment \nin House',
          id: 2,
          type: 'Apartment in House',
        },
        {
          propertyTypeIcon: images.chalet_green,
          routh: images.chalet_active,
          sarviceName: 'Chalet',
          id: 3,
          type: 'Chalet',
        },
        {
          propertyTypeIcon: images.apartmentbuilding_green,
          routh: images.apartment_active,
          sarviceName: 'Apartment \nin Building',
          id: 4,
          type: 'Apartment in Building',
        },
      ],
      barRoomList: [
        { badCount: 1, id: 1 },
        { badCount: 2, id: 2 },
        { badCount: 3, id: 3 },
        { badCount: 4, id: 4 },
        { badCount: 5, id: 5 },
        { badCount: '5+', id: '5+' },
      ],
      bathRoomList: [
        { badCount: 1, id: 1 },
        { badCount: 2, id: 2 },
        { badCount: 3, id: 3 },
        { badCount: 4, id: 4 },
        { badCount: 5, id: 5 },
        { badCount: 'Any', id: '5+' },
      ],
      furnishingList: '',
      amenitiesList: '',
      propertyList: '',
      address: '',
      lat: '',
      long: '',
      addressFilter: '',
      latFilter: '',
      longFilter: '',
      modalVisibleForLocation: false,
      modalVisibleForLocationFilter: false,
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      showDataButton: false,
      monthlyPrice: '',
      selectedTotalBathroom: 0,
      selectTotalBadRoom: 0,
      propertyName: '',
      emptymessage: false,
      minSliderVal: 0,
      filterDate: '',
      immedtily: '',
      userSelectedFurniture: [],
      userSelectedAmenties: [],
      selectedType: '',
    };
    AppHeader({
      ...this,
      leftHeide: true,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      searchClick: () => {
        this.goNotification();
      },
      searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
      search: true,
      location: true,
      addBtn: Helper.userType == 'OWNER' ? true : null,
      onAdd: () => {
        this.addPropartyScreen();
      },
      addItem: images.addproperty,
      menuClick: () => {
        this.menuClick();
      },
      city: this.state.city,
    });
  }

  componentDidMount() {
    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        AppHeader({
          ...this,
          leftHeide: true,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          searchClick: () => {
            this.goNotification();
          },
          searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
          search: true,
          location: true,
          addBtn: data?.type == 'OWNER' ? true : null,
          onAdd: () => {
            this.addPropartyScreen();
          },
          addItem: images.addproperty,
          menuClick: () => {
            this.menuClick();
          },
          city: Helper?.userData?.city,
        });
      },
    );
    // this.callPropertyList();
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
    this.getFurnitureAndAmenties();
    geoCurrentLocation(1, (data) => {
      if (data.latitude && data.longitude) {
        Helper.setData('currentLocation', data);
        this.setState(
          { lat: data.latitude, long: data.longitude },
          () => {
            this.callPropertyList(true);
          },
        );
      }
    });
  }
  getFurnitureAndAmenties = () => {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          //  Helper.mainApp.showLoader();
          ApiCall.ApiMethod({ Url: 'get-amenities', method: 'GET' })
            .then((res) => {
              let arr = [];
              if (res?.status) {
                res?.data.forEach((element) => {
                  arr.push({
                    id: element?.id,
                    isSelect: false,
                    image: element?.image,
                    title: element?.title,
                  });
                });

                // console.log('furniture data', arr);
                this.setState({ amenitiesList: arr });
              }
            })
            .catch((err) => {
              console.log('amenties error', err);
              Helper.mainApp.hideLoader();
            });

          ApiCall.ApiMethod({ Url: 'get-furnishings', method: 'GET' })
            .then((res) => {
              let arr = [];
              if (res?.status) {
                res?.data.forEach((element) => {
                  arr.push({
                    id: element?.id,
                    isSelect: false,
                    image: element?.image,
                    title: element?.title,
                  });
                });
                this.setState({ furnishingList: arr });
              }
            })
            .catch((err) => {
              console.log('furniture api err', err);
              Helper.mainApp.hideLoader();
            });
        }
      })
      .catch((err) => {
        console.log('network err', err);
        Helper.mainApp.hideLoader();
      });
  };

  onFurnitureSelect(index) {
    let selected = [...this.state.furnishingList];
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({ furniture: selected });
    if (selected[index].isSelect) {
      selectedFurniture.push(selected[index].id);
      this.state.userSelectedFurniture.push(selected[index].id);
    } else {
      console.log(
        'selected[index].id',
        selected[index].id,
        selectedFurniture.indexOf(selected[index].id),
      );
      if (selectedFurniture.indexOf(selected[index].id) > -1) {
        selectedFurniture.splice(
          selectedFurniture.indexOf(selected[index].id),
          1,
        );
        this.state.userSelectedFurniture.splice(
          this.state.userSelectedFurniture.indexOf(selected[index].id),
          1,
        );
      }
    }
  }

  onAmentiesSelect(index) {
    let selected = [...this.state.amenitiesList];
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({ amenities: selected });

    if (selected[index].isSelect) {
      selectedAmenties.push(selected[index].id);
      this.state.userSelectedAmenties.push(selected[index].id);
    } else {
      console.log(
        'selected[index].id',
        selected[index].id,
        selectedAmenties.indexOf(selected[index].id),
      );
      if (selectedAmenties.indexOf(selected[index].id) > -1) {
        selectedAmenties.splice(
          selectedAmenties.indexOf(selected[index].id),
          1,
        );
        this.state.userSelectedAmenties.splice(
          this.state.userSelectedAmenties.indexOf(selected[index].id),
          1,
        );
      }
    }
  }

  renderFuritures = (data) => {
    //ConsoleLog('flatlist', data.item);
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 13, marginTop: 10 }}
        onPress={() => this.onFurnitureSelect(data.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{ uri: data.item?.image }}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.darkSeafoamGreen,
              borderWidth: 1.5,
            }}
          />
        ) : (
            <Image
              source={{ uri: data.item?.image }}
              style={{
                height: 44,
                width: 44,
                resizeMode: 'contain',
                borderColor: Colors.white,
                borderWidth: 1.5,
              }}
            />
          )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  renderAmenities = (data) => {
    // console.log('amenities data in flatlist',data?.item)
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 13, marginTop: 10 }}
        onPress={() => this.onAmentiesSelect(data?.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{ uri: data?.item?.image }}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.darkSeafoamGreen,
              borderWidth: 1.5,
            }}
          />
        ) : (
            <Image
              source={{ uri: data?.item?.image }}
              style={{
                height: 44,
                width: 44,
                resizeMode: 'contain',
                borderColor: Colors.white,
                borderWidth: 1.5,
              }}
            />
          )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data?.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  callPropertyList = (loader) => {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          if (!this.state.refreshing) {
            this.setState({ isLoading: loader });
          }
          let data = {
            latitude: this.state.lat,
            longitude: this.state.long,
            status: 1,
            type: this.state.propertyName,
            rent: this.state.monthlyPrice,
            rooms: this.state.selectBaddRoom,
            bathrooms:
              this.state.selectedTotalBathroom == 'Any'
                ? ''
                : this.state.selectedTotalBathroom,
            available_types:
              this.state.selectedType == 'immediately'
                ? this.state.selectedType
                : this.state.selectedType == 'date' && this.state.filterDate
                  ? this.state.selectedType
                  : '',
            available_date: this.state.filterDate ? this.state.filterDate : '',
            furnishing_id: this.state.userSelectedFurniture.join(','),
            amenity_id: this.state.userSelectedAmenties.join(','),
          };
          console.log(data, 'datadatadata');
          // Helper.mainApp.showLoader()
          ApiCall.ApiMethod({
            Url: 'get-property-list-others' + '?page=' + this.state.currentPage,
            method: 'POST',
            data: data,
          })
            .then((res) => {
              Helper.mainApp.hideLoader();
              console.log('property list in home screen for ', res);
              if (res?.status) {
                if (res?.data?.data && res?.data?.data.length > 0) {
                  this.setState({
                    propertyList: this.state.propertyList
                      ? [...this.state.propertyList, ...res?.data?.data]
                      : res?.data?.data,
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    emptymessage: false,
                  });
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                    emptymessage: true,
                    // propertyList: '',
                  });
                }
                // this.setState({
                //     propertyList: res?.data?.data
                // })
              }
            })
            .catch((err) => {
              console.log('api error', err);
              this.setState({
                next_page_url: res?.data?.next_page_url,
                isLoading: false,
                refreshing: false,
                currentPage: 1,
              });
            });
        }
      })
      .catch((err) => {
        console.log('err', err);
      });
  };

  onRefresh = () => {
    this.setState({ refreshing: true, propertyList: [] });
    setTimeout(() => {
      this.setState(
        { currentPage: 1, refreshing: false, emptymessage: false },
        () => {
          this.callPropertyList(false);
        },
      );
    }, 2000);
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState(
        { currentPage: this.state.currentPage + 1, emptymessage: false },
        () => {
          this.callPropertyList(true);
        },
      );
    }
  };
  componentWillUnmount() {
    // this._unsubscribe();
    if (this.listner) {
      this.listner.remove();
    }
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  addPropartyScreen = () => {
    Helper.propertyData = {};
    handleNavigation({
      type: 'push',
      page: 'SelectPropertyType',
      navigation: this.props.navigation,
    });
  };

  goNotification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  openDrawer = () => {
    Keyboard.dismiss;
    //  Keyboard.dismiss()
    handleNavigation({ type: 'drawer', navigation: this.props.navigation });
  };
  menuClick = () => {
    this.props.navigation.openDrawer();
  };
  onAmenitiesSingle = (item) => {
    this.setState({ amenitiesSingle: item.id });
  };

  amenitiesSingleSelect = ({ item }) => {
    return (
      <Amenities
        item={item}
        onAmenitiesSingle={() => this.onAmenitiesSingle(item)}
        amenitiesSingle={this.state.amenitiesSingle}
      />
    );
  };

  onSelectProperty = (item) => {
    this.setState({ PropirtSelect: item.id, propertyName: item?.type });
  };

  modalPropirtList = ({ item }) => {
    return (
      <PropertyType
        item={item}
        onSelectProperty={() => this.onSelectProperty(item)}
        routh={this.props.routh}
        PropirtSelect={this.state.PropirtSelect}
      />
    );
  };

  selectBadCounting = (item) => {
    this.setState({ selectBaddRoom: item.id, selectTotalBadRoom: item?.id });
  };

  badModalList = ({ item }) => {
    return (
      <AmenitiesList
        item={item}
        badCount={this.props.badCount}
        Select={() => this.selectBadCounting(item)}
        selectBaddRoom={this.state.selectBaddRoom}
      />
    );
  };

  furnishing = (item) => {
    this.setState({
      selectfurnishing: item.id,
      selectedTotalBathroom: item?.badCount,
    });
  };

  furnishingList = ({ item }) => {
    return (
      <AmenitiesList
        item={item}
        badCount={this.props.badCount}
        Select={() => this.furnishing(item)}
        selectBaddRoom={this.state.selectfurnishing}
      />
    );
  };

  onSelectAmenities = (index) => {
    let selected = [...this.state.amenities];
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({ amenities: selected });
  };

  amenitiesList = ({ item, index }) => {
    return (
      <AmenitiesList
        socialIcon={this.props.socialIcon}
        socialText={this.props.socialText}
        onMultiSelect={this.onSelectAmenities}
        routh={this.props.routh}
        item={item}
        index={index}
        height={53}
        width={53}
      />
    );
  };

  filterModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({ modalVisible: false });
        }}>
        <SafeAreaView style={{ backgroundColor: Colors.white, flex: 1 }}>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps={'handled'}
            bounces={false}
            showsVerticalScrollIndicator={false}
            style={{ flex: 1, backgroundColor: Colors.whiteTwo }}>
            <StatusBarCustom
              backgroundColor={Colors.white}
              translucent={false}
            />
            <View style={styles.filteView}>
              <Text style={styles.filterText}>Filter</Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ modalVisible: false });
                }}>
                <Image style={styles.closeIcon} source={images.close} />
              </TouchableOpacity>
            </View>
            <Text style={styles.propertyText}>PROPERTY TYPE</Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.PropirtList}
                renderItem={this.modalPropirtList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={{ marginHorizontal: 12, marginTop: 13 }}>
              <Text style={styles.locationModalText}>SELECT LOCATION</Text>

              <TouchableOpacity
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                style={styles.searchViewFilter}
                onPress={() =>
                  this.setState({ modalVisibleForLocationFilter: true })
                }>
                <Image style={styles.searchIcon} source={images.search_icon} />
                <View
                  style={styles.inputStyle}
                  placeholderTextColor={Colors.pinkishGrey}
                  onChangeText={(text) => {
                    this.setState({ search: text });
                  }}
                  value={this.state.address}
                  placeholder="Search by Location or Address"
                  editable={false}
                // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: Fonts.segoeui,
                      color: Colors.pinkishGrey,
                    }}>
                    {' '}
                    {this.state.address
                      ? this.state.address
                      : 'Search by Location or Address'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <GoogleApiAddressList
              modalVisible={this.state.modalVisibleForLocationFilter}
              hideModal={() => {
                this.setState({ modalVisibleForLocationFilter: false });
              }}
              onSelectAddress={this.handleAddressForFilter}
            />
            <View>
              <Text
                style={[
                  styles.locationModalText,
                  { marginHorizontal: 17, marginTop: 30, marginBottom: 5 },
                ]}>
                PRICE MONTHLY
              </Text>
              {/* <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 12, backgroundColor: Colors.darkSeafoamGreen, borderRadius: 10, width: '13%' }}>
                                <Text style={{ fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.black }}>{this.state.monthlyPrice + '$'}</Text>
                            </View> */}
              <View style={{ width: '95%', marginHorizontal: 20 }}>
                <MultiSlider
                  selectedStyle={{ backgroundColor: Colors.darkSeafoamGreen }}
                  unselectedStyle={{ backgroundColor: 'silver' }}
                  markerStyle={{
                    backgroundColor: Colors.darkSeafoamGreen,
                    height: 15,
                    width: 15,
                  }}
                  trackStyle={{ borderRadius: 7, height: 3 }}
                  // values={[this.state.filteredData.distance]}
                  sliderLength={width - width / 9.3}
                  touchDimensions={{ height: 100, width: 100, borderRadius: 15 }}
                  min={0}
                  max={500000}
                  onValuesChangeFinish={(value) =>
                    this.setState({ monthlyPrice: Math.round(value) })
                  }
                  step={1}
                  enableLabel
                  enabledOne
                  value={this.state.monthlyPrice}
                // labels={['00', '01', '02', '03', '04']}
                // customLabel={(e) => {
                //     return (this.customLabelUi(e))
                // }}
                />
              </View>
            </View>
            <Text
              style={[
                styles.locationModalText,
                { marginHorizontal: 10, marginTop: 40 },
              ]}>
              BEDROOMS
            </Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.barRoomList}
                renderItem={this.badModalList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <Text
              style={[
                styles.locationModalText,
                { marginHorizontal: 10, marginTop: 31 },
              ]}>
              BATHROOMS
            </Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.bathRoomList}
                renderItem={this.furnishingList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View>
              <Text
                style={[
                  styles.locationModalText,
                  { marginHorizontal: 12, marginTop: 31 },
                ]}>
                FURNISHING
              </Text>
              <View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.furnishingList}
                  renderItem={this.renderFuritures}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>
            <View style={{ marginHorizontal: 6, marginTop: 25 }}>
              <Text style={[styles.availbleText, { marginHorizontal: 6 }]}>
                AMENITIES
              </Text>
              <FlatList
                numColumns={5}
                data={this.state.amenitiesList}
                renderItem={this.renderAmenities}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={{ marginTop: 25 }}>
              <Text style={[styles.availbleText, { marginHorizontal: 12 }]}>
                AVAILABLE FROM
              </Text>
              <AvailablefromRooms
                onApplayFilter={() => this.onApplayFilter()}
                resetFilter={() => this.resetFilter()}
                filterDate={(val) => this.setState({ filterDate: val })}
                //  immedtilyButton={(val) => this.setState({ immedtily: val })}
                onSelect={(type) => this.setState({ selectedType: type })}
              // onHideModal={() => { this.setState({ modalVisible: false }) }}
              />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </Modal>
    );
  };

  onApplayFilter = () => {
    this.setState(
      {
        currentPage: 1,
        modalVisible: false,
        propertyList: [],
      },
      () => {
        this.callPropertyList(true);
      },
    );
  };

  resetFilter = () => {
    this.state.userSelectedFurniture = [];
    this.state.userSelectedAmenties = [];
    selectedAmenties = [];
    selectedFurniture = [];
    this.setState({
      address: '',
      lat: '',
      long: '',
      monthlyPrice: '',
      selectBadCounting: 0,
      selectBaddRoom: 0,
      selectTotalBadRoom: 0,
      PropirtSelect: 0,
      propertyName: '',
      selectedTotalBathroom: 0,
      selectfurnishing: 0,
      resetButton: true,
      immedtily: '',
      selectedType: '',
      currentlat: '',
      currentlong: '',
      filterDate: '',
    });
  };

  resetFilter = () => {
    this.state.userSelectedFurniture = [];
    this.state.userSelectedAmenties = [];
    selectedAmenties = [];
    selectedFurniture = [];
    this.setState({
      address: '',
      lat: '',
      long: '',
      monthlyPrice: '',
      selectBadCounting: 0,
      selectBaddRoom: 0,
      selectTotalBadRoom: 0,
      selectfurnishing: 0,
      selectedTotalBathroom: 0,
      propertyName: '',
      minSliderVal: 0,
      filterDate: '',
      immedtily: '',
      PropirtSelect: 0,
      selectedType: '',
      currentlat: '',
      currentlong: '',
      filterDate: '',
    });
  };
  propertyDetails = (index) => {
    // console.log('selected property', index, this.state.propertyList[index])
    if (this.state.propertyList[index]) {
      handleNavigation({
        type: 'pushTo',
        page: 'PropertyDetails',
        passProps: { propertyID: this.state.propertyList[index]?.id },
        navigation: this.props.navigation,
      });
    }
  };

  pageListed = ({ item, index }) => {
    return (
      <WillasList
        item={item}
        iconWillas={this.props.iconWillas}
        onPress={() => this.propertyDetails(index)}
      />
    );
  };

  handleAddress = (data) => {
    console.log('property location', data);

    this.setState({
      address: data?.addressname,
      lat: data?.lat,
      long: data?.long,
      propertyList: [],
    });
    this.callPropertyList();
  };
  handleAddressForFilter = (data) => {
    console.log('property location', data);

    //this.setState({ addressFilter: data?.addressname, latFilter: data?.lat, longFilter: data?.long })
    this.setState({
      address: data?.addressname,
      lat: data?.lat,
      long: data?.long,
    });
    // this.callPropertyList({ lat: data?.lat, long: data?.long, already: true })
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.whiteTwo }}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          bounces={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          // contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          style={{ backgroundColor: Colors.whiteTwo }}>
          <StatusBarCustom
            backgroundColor={Colors.lightMint10}
            translucent={false}
          />
          <View style={{ height: 90, backgroundColor: Colors.lightMint }}>
            <TouchableOpacity
              hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
              style={styles.searchView}
              onPress={() => this.setState({ modalVisibleForLocation: true })}>
              <Image style={styles.searchIcon} source={images.search_icon} />
              <View
                style={styles.inputStyle}
                placeholderTextColor={Colors.pinkishGrey}
                onChangeText={(text) => {
                  this.setState({ search: text });
                }}
                value={this.state.address}
                placeholder="Search by Location or Address"
                editable={false}
              // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: Fonts.segoeui,
                    color: Colors.pinkishGrey,
                  }}>
                  {' '}
                  {this.state.address
                    ? this.state.address
                    : translate("SearchbyLocationorAddress")}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <GoogleApiAddressList
            modalVisible={this.state.modalVisibleForLocation}
            hideModal={() => {
              this.setState({ modalVisibleForLocation: false });
            }}
            onSelectAddress={this.handleAddress}
          />
          <View style={styles.filterMainView}>
            <Text numberOfLines={1} style={[styles.rentText, { width: '75%' }]}>
              {translate("Exclusiverent")}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.setState({ modalVisible: true, selectedType: '' });
              }}>
              <Image style={styles.filterBtn} source={images.filter} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 20, flex: 1 }}>
            <FlatList
              style={{ margin: 4 }}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              data={this.state.propertyList}
              renderItem={this.pageListed}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
              ListFooterComponent={() => {
                return this.state.isLoading ? <LoaderForList /> : null;
              }}
              ListEmptyComponent={() =>
                !this.state.isLoading && !this.state.refreshing ? (
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: Fonts.segoeui,
                        color: Colors.cherryRed,
                        textAlign: 'center',
                      }}>
                      {this.state.emptymessage == false
                        ? null
                        : 'Property not found.'}
                    </Text>
                  </View>
                ) : null
              }
              onEndReached={this.onScroll}
              onEndReachedThreshold={0.5}
              keyboardShouldPersistTaps={'handled'}
              //    onRefresh={}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.onRefresh()}
                />
              }
              refreshing={this.state.refreshing}
            />
          </View>
          {this.filterModal()}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 12,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
  },
  searchViewFilter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 3,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
    borderWidth: 0.5,
  },
  searchIcon: { height: 12, width: 12, resizeMode: 'contain', marginLeft: 14 },
  inputStyle: {
    justifyContent: 'center',
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
    width: '85%',
    height: Platform.OS === 'ios' ? 30 : 50,
  },
  filterMainView: {
    marginHorizontal: 14,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  xt: { fontSize: 16, fontFamily: Fonts.segui_semiBold },
  filterBtn: { height: 24, width: 66, resizeMode: 'contain' },
  rentText: { fontSize: 14, fontFamily: Fonts.segui_semiBold },
  filteView: {
    marginTop: 11,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 11,
  },
  filterText: { fontSize: 21, fontFamily: Fonts.segoeui_bold },
  closeIcon: { height: 18, width: 18, resizeMode: 'contain' },
  propertyText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
    marginHorizontal: 13,
    marginTop: 23,
  },
  locationModalText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
    marginHorizontal: 3,
  },
  locationView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 12,
    borderColor: Colors.pinkishGreyTwo,
    borderRadius: 4,
    marginTop: 13,
  },
  locationInrView: { flexDirection: 'row', marginLeft: 16, alignItems: 'center' },
  locationIcon: { height: 10, width: 10, resizeMode: 'contain' },
  locationText: { fontSize: 12, fontFamily: Fonts.segui_semiBold, marginLeft: 9 },
  availbleText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
  },
});
