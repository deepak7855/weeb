import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  DeviceEventEmitter
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SplashScreen from 'react-native-splash-screen';
import Images from '../../Lib/Images';
import {StatusBar, Inputs, CircleButton} from '../../Components/Common/index';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import {handleNavigation} from '../../navigation/routes';
import {ApiCall} from '../../Api/index';
import {Validation, Network, AlertMsg, RegexValid} from '../../Lib/index';
import Helper from '../../Lib/Helper';
import {Constant} from '../../Lib/Constant';
import {
  configureGoogleLogin,
  googleLogin,
  FacebookLogin,
} from '../../Components/Common/index'; 
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import {translate} from '../../../src/Language';
import {AppleButton,appleAuth} from '@invertase/react-native-apple-authentication';
 
export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      rememberMe: false, 
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      Helper.getData(Constant.REMEMBER_ME).then((response) => {
        //console.log('rember me signin', response);
        if (response) {
          this.setState({
            rememberMe: true,
            email: response?.email,
            password: response?.password,
          });
        }
      });
    });

    configureGoogleLogin();
  }
 
  googleLogin = () => {
    googleLogin((result) => {
      if (result) {
        //hit the social login api
        Helper.mainApp.showLoader();
        ApiCall.socialLogin(result)
          .then((res) => {
            if (res.status) {
              Helper.mainApp.hideLoader();
              Helper.setData(Constant.USER_DATA, res?.data);
              Helper.setData(Constant.TOKEN, res?.token);
              Helper.setData(Constant.USER_TYPE, res?.data?.user_type);
              DeviceEventEmitter.emit(Constant.USER_TYPE, {type: res?.data?.user_type});
              Helper.userData = res?.data;
              Helper.userType = res?.data?.user_type;
              Helper.token = res?.token;
              handleNavigation({
                type: 'setRoot',
                page: 'DrawerStack',
                navigation: this.props.navigation,
              });
            } else {
              Helper.mainApp.hideLoader();
            }
          })
          .catch((err) => {
            console.log('google err', err);
          });
      } else {
        Helper.mainApp.hideLoader();
        console.log('user google data not found signin');
      }
    });
  };

  faceBookLogin = () => {
    FacebookLogin((result) => {
      if (result) {
        Helper.mainApp.showLoader();
        ApiCall.socialLogin(result).then((res) => {
          if (res?.status) {
            Helper.mainApp.hideLoader();
            Helper.setData(Constant.USER_DATA, res?.data);
            Helper.setData(Constant.TOKEN, res?.token);
            Helper.token = res?.token;
            Helper.setData(Constant.USER_TYPE, res?.data?.user_type);
            DeviceEventEmitter.emit(Constant.USER_TYPE, {type: res?.data?.user_type});
             Helper.userData = res?.data;
             Helper.userType = res?.data?.user_type;
            handleNavigation({
              type: 'setRoot',
              page: 'DrawerStack',
              navigation: this.props.navigation,
            });
          } else {
            Helper.mainApp.hideLoader();
          }
        });
      } else {
        Helper.mainApp.hideLoader();
        console.log('fb  data not found signin');
      }
    });
  };

  appleLogin = async () => {
    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      let {
        user,
        email,
        fullName,
        nonce,
        identityToken,
        realUserStatus /* etc */,
      } = appleAuthRequestResponse;

      if (user) {
        console.log(
          'apple login data',
          user,
          email,
          fullName,
          nonce,
          identityToken,
          realUserStatus,
        );
         Helper.mainApp.showLoader();
         let formdata = new FormData();
         formdata.append("social_type", 'APPLE');
         formdata.append("social_id", user);
         formdata.append("device_type", Helper.device_type);
         formdata.append("device_id", this.state.fcmToken);

         if (email) {
             formdata.append("email", email);
      }
         if (fullName) {
             formdata.append("name", fullName);
         }
        // if (json.picture.data.url) {
        //     formdata.append("profile_picture", json.picture.data.url);
        // }
         ApiCall.socialLogin(formdata).then((res) => {
           if (res?.status) {
             Helper.mainApp.hideLoader();
             Helper.setData(Constant.USER_DATA, res?.data);
             Helper.setData(Constant.TOKEN, res?.token);
             Helper.setData(Constant.USER_TYPE, res?.data?.user_type);
             DeviceEventEmitter.emit(Constant.USER_TYPE, {type: res?.data?.user_type});
             Helper.userData = res?.data;
             Helper.userType = res?.data?.user_type;
             Helper.token = res?.token;
             handleNavigation({
               type: 'setRoot',
               page: 'DrawerStack',
               navigation: this.props.navigation,
             });
           } else {
             Helper.mainApp.hideLoader();
           }
         });
      }
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  };

  forgetPassword() {
    handleNavigation({
      type: 'push',
      page: 'ForgetPassword',
      navigation: this.props.navigation,
    });
  }

  Signup() {
    handleNavigation({
      type: 'push',
      page: 'Signup',
      navigation: this.props.navigation,
    });
  }

  userSignIn() {
    Network.isNetworkAvailable()
      .then((isConnected) => { 
        if (isConnected) {
          if (Validation.checkEmail('Email', this.state.email)) {
            Helper.mainApp.showLoader();
            let signInData = {
              user_name: this.state.email,
              password: this.state.password,
              device_type: Helper.device_type,
              device_id: Helper.device_id,
            };
            Helper.token = '';
            ApiCall.ApiMethod({Url: 'login', method: 'POST', data: signInData})
              .then((res) => {
                console.log('sign in data', res);
                if (res.status) {
                  // Helper.showToast(res?.message ? res?.message : AlertMsg?.success?.LOGIN)
                  Helper.mainApp.hideLoader();
                  Helper.setData(Constant.USER_DATA, res?.data);
                  Helper.setData(Constant.TOKEN, res?.token);
                 Helper.setData(Constant.USER_TYPE, res?.data?.user_type);
                 DeviceEventEmitter.emit(Constant.USER_TYPE, {type: res?.data?.user_type});
                  Helper.userData = res?.data;
                  Helper.userType = res?.data?.user_type;
                  Helper.token = res?.token;
                  if (this.state.rememberMe) {
                    const rem = {
                      email: this.state.email,
                      password: this.state.password,
                    };
                    Helper.setData(Constant.REMEMBER_ME, rem);
                    Helper.rememberMe['email'] = this.state.email;
                    Helper.rememberMe['password'] = this.state.password;
                  } else {
                    Helper.removeItemValue(Constant.REMEMBER_ME);
                    Helper.rememberMe = {};
                  }
                  this.props.navigation.reset({
                    index: 0,
                    routes: [{name: 'DrawerStack'}],
                  });
                  return;
                  //  handleNavigation({ type: 'push', page: 'DrawerStack', navigation: this.props.navigation });
                } else {
                  Helper.showToast(
                    res?.message ? res?.message : AlertMsg.error.NETWORK,
                  );
                  Helper.mainApp.hideLoader();
                  return;
                }
              })
              .catch((err) => {
                Helper.mainApp.hideLoader();
                // Helper.showToast(err?.data.message ? err?.data.message : AlertMsg.error.NETWORK);
                return;
              });
          }
        } else {
          Helper.mainApp.hideLoader();
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch((err) => {
        Helper.mainApp.hideLoader();
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBarCustom backgroundColor={Colors.lightMint10} />
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
          alwaysBounceVertical={false}
          bounces={false}>
          <Image source={Images.sign_in_bg} style={styles.signUpBg} />
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image source={Images.sign_up_logo} style={styles.logo} />
            <Text style={styles.title}>Welcome back!</Text>
          </View>

          <View style={[styles.authBox, {backgroundColor: Colors.whiteFive}]}>
            <Inputs
              labels={translate("PhonenumberEmailaddress")}
              labelsize={12}
              labelcolor={Colors.black}
              width={'80%'}
              backgroundcolor={Colors.whiteTwo}
              rightimage={Images.edit_pofile}
              placeholder={translate("EmailAddress")}
              marginleft={-5}
              keyboard={'default'}
              leftimage={
                RegexValid.EmailRegex.test(this.state.email)
                  ? Images.tick
                  : null
              }
              onChangeText={(text) => {
                this.setState({email: text});
              }}
              // onChangeText={(text) => { this.setState({ email: text }) }}
              value={this.state.email}
              returnKeyType={'next'}
              inputSize={12}
              setfocus={(input) => {
                this.email = input;
              }}
              getfocus={() => {
                this.password.focus();
              }}
              bluronsubmit={false}
            />
            <Inputs
              labels={translate("Password")}
              labelsize={12}
              labelcolor={Colors.black}
              width={'80%'}
              color={Colors.brownishGrey}
              backgroundcolor={Colors.whiteTwo}
              rightimage={Images.lock}
              placeholder={'**********'}
              marginleft={-5}
              keyboard={'default'}
              onChangeText={(text) => {
                this.setState({password: text});
              }}
              value={this.state.password}
              password={true}
              returnKeyType={'done'}
              setfocus={(input) => {
                this.password = input;
              }}
              bluronsubmit={true}
            />

            <View
              style={{
                flexDirection: 'row',
                marginTop: 5,
                marginLeft: 12,
                alignItems: 'center',
              }}>
              <View
                style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({rememberMe: !this.state.rememberMe})
                  }>
                  {this.state.rememberMe ? (
                    <Image
                      source={Images.terms_check}
                      style={styles.termImage}
                    />
                  ) : (
                    <View
                      style={{
                        height: 14,
                        width: 14,
                        borderRadius: 2,
                        borderWidth: 1,
                        marginRight: 10,
                      }}></View>
                  )}
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: Fonts.segoeui,
                    color: Colors.black,
                  }}>
                  {' '}
                  {translate("RememberMe")}
                </Text>
              </View>

              <TouchableOpacity
                onPress={() => this.forgetPassword()}
                style={{marginRight: 9}}>
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: Fonts.segoeui,
                    color: Colors.darkSeafoamGreen,
                  }}>
                  {translate("ForgotPassword")}?
                </Text>
              </TouchableOpacity>
            </View>
            <CircleButton
              navigate={() => {
                this.userSignIn();
              }}
              fontsize={18}
              labelfonts={Fonts.segoeui_bold}
              colors={Colors.lightMint}
              arrowimage={Images.arrow_green}
              label={'Sign in'}
            />
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={{marginTop: 30}}>
              <Text style={{fontSize: 14, fontFamily: Fonts.segoeui}}>
                {translate("ConnectwithSocialAccount")}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              {Platform.OS === 'android' ? null : (
                // <TouchableOpacity
                // onPress={()=>this.appleLogin()}
                // >
                //   <Image style={styles.socialIcon} source={Images.apple_icon} />
                // </TouchableOpacity>
                <AppleButton
                  buttonStyle={AppleButton.Style.BLACK}
                 buttonType={AppleButton.Type.SIGN_IN}
                  cornerRadius={20}
                  style={styles.appleButton}
                  onPress={() => this.appleLogin()}
                />
              )}

              <TouchableOpacity onPress={() => this.faceBookLogin()}>
                <Image source={Images.facebook} style={styles.socialIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.googleLogin()}>
                <Image
                  source={Images.google_hangouts}
                  style={styles.socialIcon}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 40,
            }}>
            <Text style={{fontSize: 14, fontFamily: Fonts.segoeui}}>
              {translate("Donthaveanaccount")}
            </Text>
            <Text>{'  '}</Text>
            <TouchableOpacity
              onPress={() => {
                this.Signup();
              }}>
              <Text
                style={{
                  textDecorationLine: 'underline',
                  fontSize: 14,
                  fontFamily: Fonts.segoeui,
                  color: Colors.darkSeafoamGreen,
                }}>
                {translate("SignUp")}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{margin: 5}} />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whiteTwo,
  },
  signUpBg: {
    height: 450,
    width: '100%',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    overflow: 'hidden',
    position: 'absolute',
  },
  logo: {
    resizeMode: 'contain',
    width: 70,
    height: 50,
    marginTop: 50,
  },
  title: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 18,
    color: Colors.black,
    marginTop: 15,
  },
  authBox: {
    padding: 10,
    width: '90%',
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 14,
    marginTop: 25,
    paddingTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  termImage: {
    height: 14,
    width: 14,
    padding: 5,
    marginRight: 10,
    resizeMode: 'contain',
  },
  signUpCircle: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: Colors.lightMint,
    justifyContent: 'center',
  },
  verifyImageIcon: {
    height: 13,
    width: 19,
    resizeMode: 'contain',
    transform: [{rotate: '180deg'}],
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    margin: 10,
    marginTop: 15,
  },
  appleButton: {
   height: 35,
    width: 35,
    marginLeft: 8,
    resizeMode: 'contain',
    marginTop:7
  },
});
