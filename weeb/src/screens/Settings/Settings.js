import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Switch, TouchableOpacity,DeviceEventEmitter } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AppHeader from '../../Components/AppHeader';
import Helper from '../../Lib/Helper';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import Colors from '../../Lib/Colors'
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import { Network, Validation, AlertMsg } from '../../Lib/index';
import { ApiCall } from '../../Api/index';
import { Constant } from '../../Lib/Constant';
import { handleNavigation } from '../../navigation/routes';
import { translate } from '../../Language';
import { EventRegister } from 'react-native-event-listeners'
export default class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notificationSwitch: true,
            appnotificationSwitch: true,
            isSocialType: Helper.userData ? Helper.userData?.social_type : ''
        };
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Settings',
            searchClick: () => { this.notification() },
            searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
            search: true
        });
    }
        
    componentDidMount() {

            // this.listner2 = DeviceEventEmitter.addListener(Constant.NOTIFICATION_COUNT, (data) => {
            //     alert(data)
            //     console.log('device event', data)
              
            // })
            this.stop_call_response = EventRegister.addEventListener(Constant.NOTIFICATION_COUNT, (data) => {
              alert(data)
            })
            // this.listner2 = DeviceEventEmitter.addListener(
            //     'DigitalAgreementPdfUrl',
            //     (data) => {
            //       console.log('pdf url in settings', data);
            //      alert(data)
            //       return true
            //     },
            //   );
           // console.log('user daa in setings', this.state.userData?.isSocialType)
    }

    componentWillUnmount(){
        // this.listner2.remove();
        EventRegister.removeEventListener(this.stop_call_response)
    }

    onOffNotification() {
        Network.isNetworkAvailable()
          .then((isConnected) => {
            if (isConnected) {
              this.setState({
                isLoading: true,
              });
              //Helper.mainApp.showLoader();
              ApiCall.ApiMethod({
                Url:'notification-permissions',
                method: 'POST',
                data: {
                  push_notify_status: this.state.notificationSwitch ? 1 :0,
                  app_notify_status: this.state.appnotificationSwitch ? 1 : 0,
                },
              })
                .then((res) => {
                  //   console.log(
                  //     'get tenant property list',
                  //     res?.data?.data
                  //   );
                  //   Helper.mainApp.hideLoader();
                    Helper.showToast(res?.message)
                    if (!res?.status) {
                        this.setState({
                          notificationSwitch: !this.state.notificationSwitch,
                          appnotificationSwitch:!this.state.appnotificationSwitch
                        });
                    }
                })
                .catch((err) => {
               this.setState({
                 notificationSwitch: !this.state.notificationSwitch,
                 appnotificationSwitch: !this.state.appnotificationSwitch,
               });
                  Helper.mainApp.hideLoader();
                  console.log('add tenant api err', err);
                });
            } else {
              Helper.showToast(AlertMsg.error.NETWORK);
            }
          })
          .catch(() => {
            Helper.showToast(AlertMsg.error.NETWORK);
          });

    }
    goBack = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }

    appnotificationSwitchToggle = () => {
        this.setState({
            appnotificationSwitch: !this.state.appnotificationSwitch
        }, () => {
            this.onOffNotification();
        })
    }
    notificationSwitchToggle = () => {
        this.setState({
            notificationSwitch: !this.state.notificationSwitch
        }, () => {
            this.onOffNotification();
        })
    }

    profile = (value) => {
        handleNavigation({ type: 'push', page: value, navigation: this.props.navigation });
    }

    SignOut = () => {
        Helper.confirmPopUp(
            'Are you sure you want to logout?',
            (status) => {
                if (status) {
                    Helper.mainApp.showLoader();
                    let logOutData = {
                        'device_type': Helper.device_type,
                        'device_id': Helper.device_id,
                    }
                    console.log('logout data------',logOutData)
                    Network.isNetworkAvailable().then((isConnected) => {
                        if (isConnected) {
                            ApiCall.ApiMethod({ Url: 'logout', method: 'POST', data: logOutData }).then((res) => {
                                if (res.status) {
                                    Helper.mainApp.hideLoader()
                                    Helper.showToast('Logout successful')
                                    Helper.LogOutMethod();
                                } else {
                                    Helper.mainApp.hideLoader()
                                    Helper.showToast(res?.message)
                                }
                                Helper.mainApp.hideLoader()
                            }).catch((err) => {
                                Helper.mainApp.hideLoader()

                            })
                        } else {
                            Helper.mainApp.hideLoader()
                            Helper.showToast(AlertMsg.error.NETWORK);
                        }
                    }).catch((err) => {
                        console.log('err', err)
                    })
                }
            },
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
                    <View style={{ marginLeft: 12 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12 }}>
                            <Image style={styles.icon_Main} source={images.account} />
                            <Text style={styles.txt_Main}>{translate("Account")}</Text>
                        </View>
                        <View style={[styles.border_Grey, { marginHorizontal: 12 }]}></View>
                        <TouchableOpacity onPress={() => { this.profile('EditProfile') }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.edit_pofile} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <Text style={styles.txt_Label}>{translate("EditProfile")}</Text>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.profile('Paymentmethods') }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.payment} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <Text style={styles.txt_Label}>{translate("PaymentMethods")}</Text>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.profile('WellcomeAddBank') }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.add_bank} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <Text style={styles.txt_Label}>{translate("AddBankAccount")}</Text>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity 
                       onPress={() => { this.profile('ChangeLanguage') }} 
                        style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.add_bank} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <Text style={styles.txt_Label}>{translate("ChangeLanguage")}</Text>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </TouchableOpacity>
                        {this.state.isSocialType ?
                            null
                            :
                            <TouchableOpacity onPress={() => { this.profile('ChangePassword') }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                                <Image style={styles.icon_Label} source={images.change_password} />
                                <View style={{ flex: 1, marginLeft: 14 }}>
                                    <Text style={styles.txt_Label}>{translate("ChangePassword")}</Text>
                                    <View style={styles.border_Grey}></View>
                                </View>
                            </TouchableOpacity>
                        }

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, marginTop: 14 }}>
                            <Image style={styles.icon_Main} source={images.notification_icon} />
                            <Text style={styles.txt_Main}>{translate("Notification")}</Text>
                        </View>
                        <View style={[styles.border_Grey, { marginHorizontal: 12 }]}></View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.app_notification} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.txt_Label}>{translate("Notifications")}</Text>
                                    <TouchableOpacity onPress={() => this.notificationSwitchToggle()}>
                                        <Image style={styles.icon_notification}
                                            source={this.state.notificationSwitch ? images.on : images.off} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, }}>
                            <Image style={styles.icon_Label} source={images.app_notification} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={styles.txt_Label}>{translate("AppNotifications")}</Text>
                                    <TouchableOpacity onPress={() => this.appnotificationSwitchToggle()}>
                                        <Image style={styles.icon_notification}
                                            source={this.state.appnotificationSwitch ? images.on : images.off} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.border_Grey}></View>
                            </View>
                        </View>
                        {/* <TouchableOpacity onPress={() => { this.profile('Signin') }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 12, marginTop: 40, marginBottom: 20 }}>
                            <Image style={styles.icon_Label} source={images.change_password} />
                            <View style={{ flex: 1, marginLeft: 14 }}>
                                <Text style={[styles.txt_Label, { color: Colors.cherryRed }]}>Sign out</Text>
                            </View>
                        </TouchableOpacity> */}
                    </View>
                    <TouchableOpacity onPress={() => { this.SignOut() }} style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 14, marginTop: 40, marginBottom: 20, marginLeft: 30 }}>
                        <Image style={styles.icon_Label} source={images.logout} />
                        <View style={{ flex: 1, marginLeft: 14 }}>
                            <Text style={[styles.txt_Label, { color: Colors.cherryRed }]}>{translate("Signout")}</Text>
                        </View>
                    </TouchableOpacity>
                </KeyboardAwareScrollView >
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
    icon_Main: { width: 17, height: 20, resizeMode: 'contain' },
    txt_Main: { fontSize: 16, fontFamily: Fonts.segoeui_bold, color: Colors.black, paddingVertical: 16, marginLeft: 12 },
    border_Grey: { height: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.5 },
    icon_Label: { width: 12, height: 13, resizeMode: 'contain' },
    txt_Label: { flex: 1, fontSize: 16, fontFamily: Fonts.segoeui, color: Colors.blackThree, paddingVertical: 17, },
    icon_notification: { width: 35, height: 30, resizeMode: 'contain' },

})