import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Inputs } from '../../Components/Common';
import Colors from '../../Lib/Colors';
import images from '../../Lib/Images';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import AppHeader from '../../Components/AppHeader';
import { handleNavigation } from '../../navigation/routes';
import { translate } from '../../Language';

import {Helper, Network, AlertMsg} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';

export default class AddNewCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cardNo: '',
      holdername: '',
      cvv: '',
      date: '',
      isDatePickerVisible: false,
      startdate: '',
    }
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.goBack() },
      title: 'Add Payment Method',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }

  goBack = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  summitCardDetails = () => {
   // handleNavigation({ type: 'push', page: 'PaymentMethods', navigation: this.props.navigation });

   var varheaders = {
    Accept: "application/json",
    'Content-Type': 'application/json; charset=utf-8',
    "Authorization":'Bearer sk_test_NUKx2SJj6m3otYDI7infTXuL'
}

Helper.mainApp.showLoader()
const carddata= {
  "card": {
    "number": 5123450000000008,
    "exp_month": 12,
    "exp_year": 21,
    "cvc": 124,
    "name": "test user"
  }
}
return fetch('https://api.tap.company/v2/tokens', {
    body: {
      "card": {
        "number": 5123450000000008,
        "exp_month": 12,
        "exp_year": 21,
        "cvc": 124,
        "name": "test user"
      }
    },
    method: 'POST',
    headers: varheaders,
})
    .then((response) => {
        return response.json();
    })
    .then((responseJson) => {
      Helper.mainApp.hideLoader()
        console.log(responseJson, "responseJson")
    })
    .catch((error, a) => {
        Helper.showToast('Error in Data Fetching!', 'error');
        Helper.mainApp.hideLoader()
        console.log("Error---->  ", error);
        return false;
    });
  }

  handleConfirmStart = (date) => {
    this.state.startdate = moment(date).format('MM/YYYY');
    this.setState({
      startdate: moment(date).format('MM/YYYY'),
      startText: moment(date).format('h:mm a'),
      isDatePickerVisible: false,
    });
  };

  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false });
  };

  render() {
    return (
      <View style={{ height: '100%', backgroundColor: Colors.whiteTwo }}>
        <View style={{ marginTop: "10%", paddingHorizontal: 16 }}>
          <View style={{}}>
            <Text style={{ fontWeight: 'bold', fontSize: 16, marginHorizontal: 12 }}>{translate("DebitorCreditCardDetails")}</Text>
          </View>
          <View style={{}}>
            <Inputs
              onTbShow={()=>{}}
              labels={translate('CardNumber')}
              width={'80%'}
              color={Colors.black}
              labelsize={12}
              inputSize={12}
              widthleft={30}
              heightleft={30}
              InputPaddingLeft={1}
              backgroundcolor={'white'}
              rightheight={30}
              rightwidth={30}
              placeholdercolor={Colors.brownishGrey}
              color={Colors.brownishGrey}
              leftimage={images.master_card}
              placeholder={translate("EnterCardNumber")}
              keyboard={'default'}
              onChangeText={(text) => { this.setState({ cardNo: text }) }}
              value={this.state.cardNo}
              returnKeyType={'numeric'}
              returnKeyType={'next'}
              setfocus={(input) => {
                this.cardNo = input;
              }}
              getfocus={() => {
                this.holdername.focus();
              }}

              bluronsubmit={false}
            />
          </View>
          <Inputs
            labels={translate("CardHolderName")}
            width={'87%'}
            color={Colors.black}
            labelsize={12}
            inputSize={12}
            InputPaddingLeft={1}
            backgroundcolor={'white'}
            rightheight={30}
            rightwidth={30}
            placeholdercolor={Colors.brownishGrey}
            color={Colors.brownishGrey}
            placeholder={translate("EnterCardHolderName")}
            keyboard={'default'}
            returnKeyType={'next'}
            onChangeText={(text) => { this.setState({ holdername: text }) }}
            value={this.state.holdername}
            setfocus={(input) => {
              this.holdername = input;
            }}
            getfocus={() => {
              this.cvv.focus();
            }}
            bluronsubmit={false}
          />
          <View style={{ flexDirection: 'row', }}>

            <View style={{ flex: 0.6 }}>
              <Inputs
                onTbShow={() => { this.setState({ isDatePickerVisible: true }) }}
                labels={translate("ExpiryDate")}
                width={'87%'}
                color={Colors.black}
                labelsize={12}
                inputSize={12}
                backgroundcolor={'white'}
                rightheight={30}
                rightwidth={30}
                leftimage={images.expiry_calendar}
                placeholdercolor={Colors.brownishGrey}
                color={Colors.brownishGrey}
                value={this.state.startdate}
                onChangeText={(text) => { this.setState({ startdate: text }) }}
                placeholder={'MM/YYYY'}
                marginright={10}
                keyboard={'default'}
                returnKeyType={'next'}
                editable={false}
                bluronsubmit={true}
              />
              <DateTimePickerModal
                isVisible={this.state.isDatePickerVisible}
                mode="date"
                maximumDate={new Date()}
                onConfirm={(date) => {
                  this.handleConfirmStart(date);
                }}
                onCancel={() => this.hideDatePicker()}
              />
            </View>

            <View style={{ flex: 0.4 }}>
              <Inputs
              icon={images.red_info}
                labels={'CVV'}
                width={'87%'}
                color={Colors.black}
                labelsize={12}
                inputSize={12}
                backgroundcolor={'white'}
                rightheight={30}
                rightwidth={30}
                placeholdercolor={Colors.brownishGrey}
                color={Colors.brownishGrey}
                placeholder={'Enter CVV'}
                keyboard={'default'}
                returnKeyType={'done'}
                onChangeText={(text) => { this.setState({ cvv: text }) }}
                value={this.state.cvv}
                setfocus={(input) => {
                  this.cvv = input;
                }}
              />
            </View>

          </View>

        </View>
        <TouchableOpacity onPress={() => { this.summitCardDetails() }} style={styles.btnView}>
          <Text style={styles.btnText}>{translate("Submit")}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
// PaymentMethods
const styles = StyleSheet.create({
  btnView: { borderWidth: 0.5, width: "100%", borderColor: '#bbb6b6', bottom: 0, position: 'absolute' },
  btnText: { textAlign: 'center', paddingVertical: 12, color: '#4bb17b', fontWeight: 'bold', fontSize: 18 }
});
