import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    TextInput,
    FlatList,
    ScrollView
} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import { translate } from '../../Language';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';


export default class PaymentMethods extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FeedDataCollection: [
                { icon: images.master_card, id: 1 },
                { icon: images.visa, id: 2 },
                { icon: images.knet, id: 3 },
            ],
        }
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.goBack() },
            title: 'Payment Methods',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
          });
        }
      
        goBack = () => {
            handleNavigation({ type: 'push', page: 'Settings', navigation: this.props.navigation });
        }
      
        notification = () => {
          handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
        }
    
    RenderFeedCard = ({ item }) => {
        return (
            <View style={{}}>

                <View style={styles.view_AddCard}>

                    <Image
                        source={item.icon}
                        style={{ height: 30, width: 44, resizeMode: 'contain', alignSelf: 'center' }}
                    />

                    <View style={{ flex: 1, marginLeft: 15 }}>
                        <Text style={{ fontFamily: Fonts.segui_semiBold, color: Colors.black, fontSize: 14 }}>XXXX XXXX XXXX 1254</Text>

                        <Text style={{ fontSize: 12, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui }}>Exp. Date : 08/24</Text>
                    </View>

                    <Image
                        source={images.card_delete}
                        style={{ height: 30, width: 30, alignSelf: 'center', resizeMode: 'contain' }}
                    />
                </View>

            </View>
        )
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.whiteTwo }}>
                <TouchableOpacity
                    style={{ marginHorizontal: 13, marginTop: '10%' }}>
                    <Text style={styles.txt_AddPayment}>+ {translate("AddPaymentMethod")}</Text>
                </TouchableOpacity>
                <FlatList
                    style={{}}
                    data={this.state.FeedDataCollection}
                    renderItem={this.RenderFeedCard}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    txt_AddPayment: { fontSize: 16, fontFamily: Fonts.segoeui_bold, textAlign: 'center', borderColor: Colors.darkSeafoamGreen, borderStyle: 'dashed', paddingVertical: "5%", backgroundColor: Colors.ice, color: Colors.darkSeafoamGreen, borderWidth: 1, borderRadius: 4 },
    view_AddCard: {
        borderWidth: 1, marginHorizontal: 15, paddingVertical: 15, flexDirection: 'row'
        , borderRadius: 4, borderColor: Colors.whiteFive, paddingHorizontal: 18, marginTop: '6%'
    },
});