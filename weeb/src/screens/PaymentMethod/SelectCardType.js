import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import AppHeader from '../../Components/AppHeader';
import { translate } from '../../Language';


import images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';

export default class SelectCardType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FeedDataCollection: [
        { icon: images.credit_card, id: 1, text: 'Debit or Credit Card' }, { icon: images.credit_card, id: 2, text: 'KNET' },
      ],
      toggle:1
    }
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.goBack() },
      title: 'Add Payment Method',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }

  goBack = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  toChangeTheIcon = (id) => {
    this.setState({ toggle: id })
  }

  RenderFeedCard = ({ item }) => {
    return (
      <View style={{ marginTop: "6%", flex: 1 }}>
        <TouchableOpacity onPress={() => this.toChangeTheIcon(item.id)} style={{}}>
          <View style={{ flexDirection: 'row', marginLeft: "5%", }}>
            <Image
              source={this.state.toggle == item.id ? images.radio_active : images.radio_unactive}
              style={{ height: 21, width: 21, }} />
            <Image
              source={item.icon}
              style={{ height: 20, width: 30, marginLeft: "7%", marginRight: "5%" }} />
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{item.text}</Text>
          </View>
        </TouchableOpacity>
        <View
          style={{
            borderBottomWidth: 0.5,
            marginHorizontal: 11.5,
            marginTop: "6%",

            borderBottomColor: '#bbb6b6',
          }}
        />
      </View>
    )
  }

  ContinueCard = () => {
    handleNavigation({ type: 'push', page: 'AddNewCard', navigation: this.props.navigation });
}

  render() {
    return (
      <View style={{ height: "100%" }}>

        <FlatList
          style={{ marginTop: "10%" }}
          data={this.state.FeedDataCollection}
          renderItem={this.RenderFeedCard}
        />

        <TouchableOpacity
          onPress={() => {this.ContinueCard()}}
          style={styles.clickLisnr}>
          <Text style={styles.btnText}>{translate("Continue")}</Text>
        </TouchableOpacity>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  clickLisnr: { bottom: 0, borderWidth: 0.5, width: "100%", borderColor: '#bbb6b6' },
  btnText: { textAlign: 'center', paddingVertical: 12, color: '#4bb17b', fontWeight: 'bold', fontSize: 18 }
});
