import React, { Component } from 'react'
import { Text, Image, View, StyleSheet, FlatList, TouchableOpacity, TouchableHighlight, ScrollView, TextInput } from 'react-native'
import { translate } from '../Language';
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
export default class Agreement15_4_9 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataOfAreement: [
                { id: 1, icon: images.propert_requested, Title: 'Lighthouse Excursions London ...', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Full House' },
                { id: 2, icon: images.propert_requested_t, Title: 'Royal Treasure', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Full House' },
                { id: 3, icon: images.propert_requested3, Title: 'Star Green Zenith', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Appartment' },
                { id: 4, icon: images.propert_requested, Title: 'Lighthouse Excursions London ...', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Full House' },
                { id: 5, icon: images.propert_requested, Title: 'Star Green Zenith', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Appartment' },
                { id: 6, icon: images.propert_requested_t, Title: 'Star Green Zenith', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Full House' },
                { id: 7, icon: images.propert_requested, Title: 'Star Green Zenith', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Appartment' },
                { id: 8, icon: images.propert_requested, Title: 'Star Green Zenith', address: '16545 Et. Solve Ave, Suite 54564, London...', rate: '$250',rateType:'/m', propertyType: 'Full House' },
                ],
            selectorOfAppartment: '',
            openerOfDetails: false,
            opener: false,
            dataOfFilter: [
                { iconChecked: images.filter_check, text: 'Full House', selected: true },
                { iconChecked: images.filter_check, text: 'Apartment in House ', selected: false },
                { iconChecked: images.filter_check, text: 'Chalet', selected: true },
                { iconChecked: images.filter_check, text: 'Apartment in Building', selected: false },
            ],
            dataOfDownloadAgreement: [
                { icon: images.property_pdf, text1: 'Agreement part -1', text2: '15 Jan 2021 | 10:15 pm', iconDownload: images.download_arrow },
                { icon: images.jpg_doc, text1: 'Agreement part -2', text2: '15 Jan 2021 | 10:15 pm', iconDownload: images.download_arrow },
            ]
        }
    }
    openAppartment = (item) => {
        this.setState({ selectorOfAppartment: item.id })
    }
    openDetails = () => {
        this.setState({ openerOfDetails: !this.state.openerOfDetails })
    }
    openFilter = () => {
        this.setState({ opener: !this.state.opener })
    }
    toSelectSpecializatio = (index) => {
        let NewVar = [...this.state.dataOfFilter]
        NewVar[index].selected = !NewVar[index].selected
        this.setState({ dataOfFilter: NewVar })
    }
    renderOfPropertyDetails = ({ item }) => {
        return (
            <View style={styles.viewProDetail}>
                <Image style={styles.iconPDF} source={item.icon} />
                <View style={styles.view6}>
                    <Text style={styles.textpart1}>{item.text1}</Text>
                    <Text style={styles.textpart2}>{item.text2}</Text>
                </View>
                <TouchableOpacity>
                    <Image style={styles.iconDownloads} source={item.iconDownload} />
                </TouchableOpacity>
                

            </View>
        )
    }
    renderOfFilter = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.toSelectSpecializatio(index)}
                    style={styles.ViewRenderFilter}>
                    <Image style={item.selected ? styles.checkIcon : styles.uncheckIcon} source={item.iconChecked} />
                    <Text style={item.selected ? styles.selectedText : styles.unselectText}>{item.text}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    renderOfProperty = ({ item }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => { this.openAppartment(item); this.openDetails(item) }}
                    style={(this.state.selectorOfAppartment == item.id) ? styles.view11 : styles.view1}>
                    <Image style={styles.iconHome} source={item.icon} />
                    <View style={{ marginLeft: 13 }}>
                        <Text style={styles.textTitle}>{item.Title}</Text>
                        <View style={styles.view3}>
                            <Image style={styles.iconLocation}
                                source={images.location} />
                            <Text style={styles.textAddress}>{item.address}</Text>
                        </View>
                        <View style={styles.view4}>

                            <Text style={styles.textRate}>{item.rate}</Text>
                            <Text style={[styles.textRate,{fontFamily:Fonts.segoeui}]}>{item.rateType}</Text>
                            <View style={styles.dot} />
                            <Text style={styles.textPropType}>{item.propertyType}</Text>
                        </View>
                    </View>

                </TouchableOpacity>
                {this.state.selectorOfAppartment == item.id && this.state.openerOfDetails ? <View style={styles.view5}>
                <Image style={{width:30,height:20,resizeMode:'contain',right:40,top:-15,position:'absolute'}} source={images.home_tab} />
                    <FlatList data={this.state.dataOfDownloadAgreement} renderItem={this.renderOfPropertyDetails}
                        ItemSeparatorComponent={() => <View style={styles.seperatorOfFilter} />} />
                </View> : null}
            </View>
        )
    }
    render() {
        return (

            <View style={styles.container}>
                <View style={styles.viewTop}>
                    <Image style={styles.searchIcon} source={images.search_icon} />
                    <TextInput placeholder={'Search Property'} placeholderTextColor={Colors.pinkishGrey} style={styles.textInputStyl} />
                </View>
                <View style={styles.view2}>
                    <Text style={styles.textYouHave}>{translate("Youhave")} <Text style={styles.textRed}>8 {translate("rentproperty")}</Text></Text>
                </View>
                <View>
                    <FlatList
                        data={this.state.dataOfAreement}
                        renderItem={this.renderOfProperty}
                        extraData={this.state} />
                </View>
                <TouchableOpacity
                    onPress={() => this.openFilter()}
                    style={styles.filterTouch}>
                    <Image style={styles.iconFilter} source={images.property_filter} />
                </TouchableOpacity>
                {this.state.opener ? <View style={styles.filteView}>
                    <View style={styles.viewTopTextOfFilter}>
                        <Text style={styles.textPropTypeOfFilter}>{translate("PropertyType")}</Text>
                    </View>
                    <FlatList
                        data={this.state.dataOfFilter}
                        renderItem={this.renderOfFilter}
                        ItemSeparatorComponent={() => <View style={styles.filterSeperator} />} />
                </View> : null}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo,paddingBottom:10 },
    viewTop: { flexDirection: 'row', marginTop: 17, borderWidth: 1,borderRadius:4, marginHorizontal: 11, alignItems: 'center', paddingHorizontal: 13, borderColor: Colors.whiteFive },
    searchIcon: { width: 11, height: 11, resizeMode: 'contain' },
    textInputStyl: { fontSize: 12, lineHeight: 16, marginLeft: 10 },
    view2: { marginLeft: 11, paddingTop: 12, paddingBottom: 5 },
    textYouHave: { fontSize: 14, lineHeight: 25, color: Colors.blackTwo, fontFamily: Fonts.segoeui },
    textRed: { fontSize: 14, lineHeight: 25, color: Colors.cherryRed, fontFamily: Fonts.segoeui },
    iconHome: { width: 67, height: 57, resizeMode: 'contain' },
    textTitle: { fontSize: 14, lineHeight: 16, color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold },
    view3: { flexDirection: "row", alignItems: 'center', marginTop: 5, marginBottom: 6 },
    iconLocation: { width: 7, height: 9, resizeMode: 'contain' },
    textAddress: { fontSize: 12, lineHeight: 16, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginLeft: 5 },
    view4: { flexDirection: "row", alignItems: 'center' },
    textRate: { fontSize: 12, lineHeight: 16, color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui_bold },
    dot: { width: 2, height: 2, borderRadius: 1, backgroundColor: Colors.pinkishGrey, marginHorizontal: 7 },
    textPropType: { fontSize: 12, lineHeight: 16, color: Colors.marigold, fontFamily: Fonts.segoeui_bold },
    view5: { marginHorizontal: 11, paddingHorizontal: 15, borderWidth: 1, borderColor: Colors.whiteFive, borderRadius: 4, top: -5 },
    seperatorOfFilter: {  borderWidth: 0.4, backgroundColor: Colors.warmGrey, opacity: 0.18 },
    filterTouch:{ position: 'absolute', bottom: 21, right: 13 },
    iconFilter:{ width: 56, height: 56, resizeMode: 'contain' },
    filteView:{ position: 'absolute', bottom: 90, right: 13, backgroundColor: Colors.whiteTwo, elevation: 5,width:200,borderRadius:10 },
    viewTopTextOfFilter:{ paddingVertical:10, paddingHorizontal: 20, backgroundColor: Colors.whiteNine, borderBottomColor: Colors.whiteFive, borderBottomWidth: 1 },
    textPropTypeOfFilter:{ fontSize: 14,  fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    filterSeperator:{  borderWidth: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.05 },
    ViewRenderFilter:{ flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 23, backgroundColor: Colors.whiteTwo, alignItems: 'center' },
    viewProDetail:{ flexDirection: 'row', paddingVertical: 15, alignItems: 'center' },
    iconPDF:{ width: 30, height: 30, resizeMode: 'contain' },
    view6:{ marginLeft: 12, flex: 0.95 },
    textpart1:{ fontSize: 12, lineHeight: 15, fontFamily: Fonts.segoeui },
    textpart2:{ fontSize: 8, lineHeight: 25, color: Colors.pinkishGrey, fontFamily: Fonts.segoeui },
    iconDownloads:{ width: 17, height: 17, resizeMode: 'contain' },
    checkIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.darkSeafoamGreen },
    uncheckIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.pinkishGrey },
    unselectText: { fontSize: 14, paddingVertical:10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.pinkishGrey },
    selectedText: { fontSize: 14, paddingVertical:10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.black },
    view1: { flexDirection: 'row', marginHorizontal: 11, marginVertical: 7, borderWidth: 1, borderRadius: 4, borderColor: Colors.whiteFive, paddingLeft: 15, paddingVertical: 15,},
    view11: { flexDirection: 'row', marginHorizontal: 11, marginVertical: 7, borderWidth: 1, borderRadius: 4, borderColor: Colors.whiteFive, paddingLeft: 15, paddingVertical: 15, backgroundColor: Colors.lightMint }
})