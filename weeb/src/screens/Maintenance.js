import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Keyboard,
  DeviceEventEmitter,
  RefreshControl,
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import {Constant} from '../Lib/Constant';
import {handleNavigation} from '../navigation/routes';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import {
  ProgressiveImage,
  GoogleApiAddressList,
  FlootingButton,
} from '../Components/Common/index';
import { translate } from '../Language';
export default class Maintenance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: Helper.userData?.city,
      dataOfAreement: [],
      dataOfFilter: [
        {iconChecked: images.filter_check, text: 'Full House', selected: false},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in House ',
          selected: false,
        },
        {iconChecked: images.filter_check, text: 'Chalet', selected: false},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in Building',
          selected: false,
        },
      ],
      checkInd: -1,
      openId: '',
      opener: false,
      currentPage: 1,
      emptymessage: false,
      propertyType: '',
      propertyName: '',
      isLoading: false,
      refreshing: false,
      next_page_url: '',
      address: '',
      lat: '',
      long: '',
      modalVisibleForLocation: false,
      userType:Helper.userType,
    };
    AppHeader({
      ...this,
      leftHeide: true,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      searchClick: () => {
        this.goNotification();
      },
      searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
      search: true,
      location: true,
      addBtn: Helper.userType == 'OWNER' ? true : null,
      onAdd: () => {
        this.addPropartyScreen();
      },
      addItem: images.addproperty,
      menuClick: () => {
        this.menuClick();
      },
      city: this.state.city,
    });
  }
  componentDidMount() {
    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        AppHeader({
          ...this,
          leftHeide: true,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          searchClick: () => {
            this.goNotification();
          },
          searchIcon: images.notification_goup,
          search: true,
          location: true,
          addBtn: data?.type == 'OWNER' ? true : null,
          onAdd: () => {
            this.addPropartyScreen();
          },
          addItem: images.addproperty,
          menuClick: () => {
            this.menuClick();
          },
          city: Helper?.userData?.city,
        });
        this.setState({
          userType: data?.type,
        })
        this.getRentedProperty(data?.type)
      },
     
    );
    this.getRentedProperty(Helper.userType)
  }

  getRentedProperty(userType) {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let Url = userType == 'OWNER' ?'owner-rented-properties':'tenant-rented-properties'
          ApiCall.ApiMethod({
            Url: Url + '?page=' + this.state.currentPage,
            method: 'POST',
            data: {
              type: this.state.propertyType,
              lat: this.state.lat,
              lng: this.state.long,
            },
          })
            .then((res) => {
              console.log('res',res)
              if (res?.status) {
                this.setState({
                  dataOfAreement: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
                next_page_url: res?.next_page_url,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
    }
    //   this._unsubscribe();
  }
  addPropartyScreen = () => {
    Helper.propertyData = {};
    handleNavigation({
      type: 'push',
      page: 'SelectPropertyType',
      navigation: this.props.navigation,
    });
  };

  goNotification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  openDrawer = () => {
    Keyboard.dismiss();
    handleNavigation({type: 'drawer', navigation: this.props.navigation});
  };
  menuClick = () => {
    this.props.navigation.openDrawer();
  };

  renderOfProperty = ({item}) => {
    console.log('item',item)
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
           this.state.userType == 'OWNER' ? this.props.navigation.navigate('Maintenance_16_4',{propertyData:item}):this.props.navigation.navigate('Maintenance_16_2',{propertyData:item})
          }}
          >
          <View style={styles.view1}>
            {this.state.userType == 'OWNER' ?
              <ProgressiveImage
            source={{
              uri: item?.propertyphotos
                ? item?.propertyphotos[0]?.imgurl
                : '',
            }}
            style={styles.iconHome}
            resizeMode="cover"
          />
          :
          <ProgressiveImage
          source={{
            uri: item?.property?.propertyphotos
              ? item?.property?.propertyphotos[0]?.imgurl
              : '',
          }}
          style={styles.iconHome}
          resizeMode="cover"
        />

          }
            <View style={{marginLeft: 13}}>
              <Text style={styles.textTitle}>{this.state.userType == 'OWNER' ?item.title:item?.property?.title}</Text>
              <View style={styles.view3}>
                <Image style={styles.iconLocation} source={images.location} />
                <Text style={styles.textAddress}>{this.state.userType == 'OWNER' ?item.location:item?.property?.location}</Text>
              </View>
              <View style={styles.view4}>
                <Text style={styles.textRate}>{this.state.userType == 'OWNER'?'$'+ item.rent+'/m':'$'+ item?.property?.rent+'/m'}</Text>
                <Text style={[styles.textRate, {fontFamily: Fonts.segoeui}]}>
                  {item.rateType}
                </Text>
                <View style={styles.dot} />
                <Text style={styles.textPropType}>{this.state.userType == 'OWNER' ?item.type:item?.property?.type}</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingLeft: 13}}>
            <Text style={styles.textReq}>{`Requests:${this.state.userType == 'OWNER' ?item.requests:item?.property?.requests}`}</Text>
            <Text
              style={
                this.state.userType == 'OWNER' ?!item.incomplete:!item?.property?.incomplete  == 0 ?  styles.textIncomp:styles.textIncomp1
              }>
              {translate("Incomplete")}: {this.state.userType == 'OWNER' ?item.incomplete:item?.property?.incomplete}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  renderOfFilter = ({item, index}) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.selectItemGroup(index)}
          style={styles.ViewRenderFilter}>
          <Image
            style={
              this.state.checkInd == index
                ? styles.checkIcon
                : styles.uncheckIcon
            }
            source={item.iconChecked}
          />
          <Text
            style={
              this.state.checkInd == index
                ? styles.selectedText
                : styles.unselectText
            }>
            {item.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  openFilter = () => {
    this.setState({opener: !this.state.opener});
  };
  selectItemGroup(value) {
    if (this.state.checkInd == value) {
      this.setState({checkInd: -1});
      this.setState(
        {
          opener: false,
          propertyType: '',
        },
        () => {
          this.getRentedProperty();
        },
      );
      this.setState({opener: false});
    } else {
      this.setState({checkInd: value});
      this.setState(
        {
          opener: false,
          propertyType: this.state.dataOfFilter[value]?.text,
        },
        () => {
          this.getRentedProperty();
        },
      );
      this.setState({opener: false});
    }
  }

  handleAddress = (data) => {
    this.setState(
      {
        address: data?.addressname,
        lat: data?.lat,
        long: data?.long,
      },
      () => {
        this.getRentedProperty();
      },
    );
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {
          currentPage: 1,
          refreshing: false,
          propertyType: '',
          address: '',
          lat: '',
          long: '',
        },
        () => {
          this.getRentedProperty(this.state.userType);
        },
      );
    }, 2000);
  };
  render() {
    return (
      // <SafeAreaView style={{backgroundColor:Colors.lightMint10, flex:1}}>
      <View style={styles.container}>
        <View style={{height: 90, backgroundColor: Colors.lightMint}}>

          <TouchableOpacity
          hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
          style={styles.searchView}
          onPress={() => this.setState({modalVisibleForLocation: true})}>
          <Image style={styles.searchIcon} source={images.search_icon} />
          <View
            style={styles.inputStyle}
            placeholderTextColor={Colors.pinkishGrey}
            onChangeText={(text) => {
              this.setState({search: text});
            }}
            value={this.state.address}
            placeholder="Search by Location or Address"
            editable={false}
            // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: Fonts.segoeui,
                color: Colors.pinkishGrey,
              }}>
              {' '}
              {this.state.address
                ? this.state.address
                : translate("SearchbyLocationorAddress")}
            </Text>
          </View>
        </TouchableOpacity>
        <GoogleApiAddressList
          modalVisible={this.state.modalVisibleForLocation}
          hideModal={() => {
            this.setState({modalVisibleForLocation: false});
          }}
          onSelectAddress={this.handleAddress}
        />
        </View>
        <View style={styles.viewTextTop}>
          <Text style={styles.textTop}>
            {translate("SelectPropertyRequesttoMaintenance")}
          </Text>
          <View style={styles.line1} />
        </View>
        <View style={{flex: 1}}>
          <FlatList
            data={this.state.dataOfAreement}
            renderItem={this.renderOfProperty}
            extraData={this.state}
            ItemSeparatorComponent={() => (
              <View style={styles.seperator}></View>
            )}
               refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.onRefresh()}
                />
              }
              extraData={this.state}
              ItemSeparatorComponent={() => <View style={styles.seperator}></View>}
              ListFooterComponent={() => {
                return this.state.isLoading ? <LoaderForList /> : null;
              }}
            //  onEndReached={this.onScroll}
              onEndReachedThreshold={0.5}
              keyboardShouldPersistTaps={'handled'}
              refreshing={this.state.refreshing}
              ListEmptyComponent={() => (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: Fonts.segoeui,
                      color: Colors.cherryRed,
                      textAlign: 'center',
                    }}>
                    {this.state.emptymessage == false ? null : 'No Property.'}
                  </Text>
                </View>
              )}
          />
        </View>
        <TouchableOpacity
          onPress={() => this.openFilter()}
          style={styles.filterTouch}>
          <Image style={styles.iconFilter} source={images.property_filter} />
        </TouchableOpacity>
        {this.state.opener ? (
          <View style={styles.filteView}>
            <View
              style={[
                styles.viewTopTextOfFilter,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.textPropTypeOfFilter}>{translate("PropertyType")}</Text>
              <TouchableOpacity
                onPress={() => {
                  this.openFilter();
                }}>
                <Image
                  style={{height: 10, width: 10, resizeMode: 'contain'}}
                  source={images.close}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              data={this.state.dataOfFilter}
              renderItem={this.renderOfFilter}
              ItemSeparatorComponent={() => (
                <View style={styles.filterSeperator} />
              )}
            />
          </View>
        ) : null}
      </View>
      // </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 10},
  textIncomp: {
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.cherryRed,
  },
  textIncomp1: {
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.brownishGreyTwo,
  },
  viewTextTop: {paddingLeft: 13, paddingRight: 11, paddingTop: 10},
  viewTop: {
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 13,
    paddingRight: 10,
    paddingVertical: 18,
    borderColor: Colors.whiteTwo,
    backgroundColor: Colors.lightMint,
  },
  searchIcon: {width: 11, height: 11, resizeMode: 'contain'},
  textInputStyl: {fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40},
  iconHome: {width: 67, height: 57, resizeMode: 'contain'},
  textTitle: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.blackTwo,
    fontFamily: Fonts.segoeui_bold,
  },
  view3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 6,
  },
  iconLocation: {width: 7, height: 9, resizeMode: 'contain'},
  textAddress: {
    fontSize: 12,
    lineHeight: 16,
    color: Colors.brownishGreyTwo,
    fontFamily: Fonts.segoeui,
    marginLeft: 5,
    width:'86%'
  },
  view4: {flexDirection: 'row', alignItems: 'center'},
  textRate: {fontSize: 12, fontFamily: Fonts.segoeui_bold},
  dot: {
    width: 2,
    height: 2,
    borderRadius: 1,
    backgroundColor: Colors.pinkishGrey,
    marginHorizontal: 7,
  },
  textPropType: {
    fontSize: 12,
    color: Colors.brownishGrey,
    fontFamily: Fonts.segui_semiBold,
  },
  filterTouch: {position: 'absolute', bottom: 20, right: 12},
  iconFilter: {width: 56, height: 56, resizeMode: 'contain'},
  view1: {flexDirection: 'row', marginVertical: 7, paddingLeft: 13},
  textReq: {
    fontSize: 12,
    color: Colors.dustyOrange,
    fontFamily: Fonts.segui_semiBold,
    flex: 0.65,
    marginBottom: 10,
  },
  viewTextInput: {
    flexDirection: 'row',
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
    paddingLeft: 13,
  },
  textTop: {
    fontSize: 16,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  line1: {
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginTop: 10,
  },
  seperator: {
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginLeft: 13,
    marginRight: 11,
    marginBottom: 5,
  },
  searchIcon: {height: 12, width: 12, resizeMode: 'contain', marginLeft: 14},
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 12,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
  },
  inputStyle: {
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
    width: '85%',
    height: Platform.OS === 'ios' ? 30 : 50,
  },
  filterTouch: {position: 'absolute', bottom: 20, right: 12},
  iconFilter: {width: 56, height: 56, resizeMode: 'contain'},
  filteView: {
    position: 'absolute',
    bottom: 90,
    right: 12,
    backgroundColor: Colors.whiteTwo,
    elevation: 5,
    width: 200,
    borderRadius: 10,
  },
  viewTopTextOfFilter: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: Colors.whiteNine,
    borderBottomColor: Colors.whiteFive,
    borderBottomWidth: 1,
  },
  textPropTypeOfFilter: {
    fontSize: 14,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  filterSeperator: {
    borderWidth: 0.5,
    backgroundColor: Colors.warmGrey,
    opacity: 0.05,
  },
  ViewRenderFilter: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 23,
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
  },
  checkIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.darkSeafoamGreen,
  },
  uncheckIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.pinkishGrey,
  },
  selectedText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.black,
  },
  unselectText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.pinkishGrey,
  },
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 3,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
    borderWidth: 0.5,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  searchIcon: {height: 12, width: 12, resizeMode: 'contain', marginLeft: 14},
  inputStyle: {
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
    width: '80%',
    height: Platform.OS === 'ios' ? 30 : 50,
    justifyContent: 'center',
  },
});
