import React, { Component } from 'react';
import { View, Image, StyleSheet, TextInput, TouchableOpacity, FlatList, Text,RefreshControl } from 'react-native';
import AppHeader from '../../Components/AppHeader';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';
import {Helper, Network, AlertMsg} from '../../Lib/index';
import {ApiCall, LoaderForList} from '../../Api/index';
import {
  ProgressiveImage,
  GoogleApiAddressList,
  FlootingButton,
} from '../../Components/Common/index';
import { translate } from '../../Language';
export default class MyRentel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FeedDataCollection: [],
      opener: false,
      checkInd: -1,
      currentPage: 1,
      emptymessage: false,
      propertyType: '',
      propertyName: '',
      isLoading: false,
      refreshing: false,
      next_page_url: '',
      address: '',
      lat: '',
      long: '',
      modalVisibleForLocation: false,
      dataOfFilter: [
        {iconChecked: images.filter_check, text: 'Full House', selected: false},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in House ',
          selected: false,
        },
        {iconChecked: images.filter_check, text: 'Chalet', selected: false},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in Building',
          selected: false,
        },
      ],
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'My Rentals',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    this.getOwnerRentedProperty();
  }
  openFilter = () => {
    this.setState({opener: !this.state.opener});
  };
  selectItemGroup(value) {
    if (this.state.checkInd == value) {
      this.setState({checkInd: -1});
      this.setState({opener: false});
    } else {
      this.setState({checkInd: value});
      this.setState(
        {
          opener: false,
          propertyType: this.state.dataOfFilter[value]?.text,
        },
        () => {
          this.getOwnerRentedProperty();
        },
      );
    }
  }
  getOwnerRentedProperty() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let data = {};
          ApiCall.ApiMethod({
            Url: 'owner-rented-properties' + '?page=' + this.state.currentPage,
            method: 'POST',
            data: {
              type: this.state.propertyType,
              lat: this.state.lat,
              lng: this.state.long,
            },
          })
            .then((res) => {
              // console.log('get owner list rented property', res?.data?.data[0]);
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res?.status) {
                this.setState({
                  FeedDataCollection: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.data?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
                next_page_url: res?.data?.next_page_url,
                currentPage: 1,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                FeedDataCollection: [],
                currentPage: 1,
              });
              Helper.mainApp.hideLoader();
              console.log('add tenant api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {
          currentPage: 1,
          refreshing: false,
          propertyType: '',
          lat: '',
          long: '',
          address: '',
        },
        () => {
          this.getOwnerRentedProperty();
        },
      );
    }, 2000);
  };

  onListClicking = (index) => {
    if (this.state.FeedDataCollection[index]?.id) {
      handleNavigation({
        type: 'push',
        page: 'PaymentDetails',
        navigation: this.props.navigation,
        passProps: {propertyData: this.state.FeedDataCollection[index]},
      });
    }
    //this.props.navigation.navigate('PaymentDetails');
  };

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  detailsClick = (index) => {
    console.log('id', this.state.FeedDataCollection[index]?.id);
    if (this.state.FeedDataCollection[index]?.id) {
      handleNavigation({
        type: 'push',
        page: 'PropertyDetails',
        navigation: this.props.navigation,
        passProps: {propertyID: this.state.FeedDataCollection[index]?.id},
      });
    }
  };

  RenderFeedCard = ({item, index}) => {
    console.log('property list', item?.rent);
    return (
      <View
        style={{
          borderWidth: 1,
          borderColor: Colors.whiteFive,
          marginHorizontal: 12,
          borderRadius: 4,
          marginBottom: 21,
        }}>
        <View>
          <ProgressiveImage
            source={{
              uri: item?.propertyphotos ? item?.propertyphotos[0]?.imgurl : '',
            }}
            style={styles.imgProperty}
            resizeMode="cover"
          />
          {/* <Image source={item.imgProperty} style={styles.imgProperty} /> */}
          <View style={styles.view_PropertyPrice}>
            <Text style={styles.txt_PropertyPrice}>{item?.rent}</Text>
            <Text style={styles.txt_PropertyName}>{item?.type}</Text>
          </View>
        </View>
        <Text style={styles.txt_Lighhouse}>{item?.title}</Text>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 20,
            marginTop: 4,
            alignItems: 'center',
          }}>
          <Image
            source={images.profile_location}
            style={styles.icon_Location}
          />

          <Text style={styles.txt_PropertyLocation}>{item?.location}</Text>
        </View>
        <View style={styles.detailsClick}>
          <TouchableOpacity
            onPress={() => {
              this.detailsClick(index);
            }}
            style={styles.clickDetails}>
            <Text style={styles.btn_ViewDetails}>{translate("ViewDetails")}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.onListClicking(index);
            }}
            style={styles.details}>
            <Text style={styles.btn_PaymentDetails}>{'PaymentDetails'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  handleAddress = (data) => {
    this.setState(
      {
        address: data?.addressname,
        lat: data?.lat,
        long: data?.long,
      },
      () => {
        this.getOwnerRentedProperty();
      },
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
        {/* <View style={styles.viewSearch}>
          <Image source={images.search_icon} style={styles.icon_search} />
          <TextInput
            style={styles.input_SearchLocation}
            placeholder={'Search by Address or Location'}
            placeholderTextColor={Colors.pinkishGrey}
          />
        </View> */}
        <TouchableOpacity
          hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
          style={styles.searchView}
          onPress={() => this.setState({modalVisibleForLocation: true})}>
          <Image style={styles.searchIcon} source={images.search_icon} />
          <View
            style={styles.inputStyle}
            placeholderTextColor={Colors.pinkishGrey}
            onChangeText={(text) => {
              this.setState({search: text});
            }}
            value={this.state.address}
            placeholder="Search by Location or Address"
            editable={false}
            // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: Fonts.segoeui,
                color: Colors.pinkishGrey,
              }}>
              {' '}
              {this.state.address
                ? this.state.address
                : 'Search by Location or Address'}
            </Text>
          </View>
        </TouchableOpacity>
        <GoogleApiAddressList
          modalVisible={this.state.modalVisibleForLocation}
          hideModal={() => {
            this.setState({modalVisibleForLocation: false});
          }}
          onSelectAddress={this.handleAddress}
        />
        <Text style={[styles.txt_Property, {color: Colors.blackTwo}]}>
          {translate("Youhave")}
          <Text style={[styles.txt_Property, {color: Colors.cherryRed}]}>
            {this.state.FeedDataCollection
              ? this.state.FeedDataCollection.length
              : ' 0'}{' '}
            {translate("rentproperty")}
          </Text>
        </Text>
        <FlatList
          style={{marginTop: 21}}
          data={this.state.FeedDataCollection}
          keyExtractor={(item) => item.key}
          renderItem={this.RenderFeedCard}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          extraData={this.state}
          ItemSeparatorComponent={() => <View style={styles.seperator}></View>}
          ListFooterComponent={() => {
            return this.state.isLoading ? <LoaderForList /> : null;
          }}
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          keyboardShouldPersistTaps={'handled'}
          refreshing={this.state.refreshing}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: Fonts.segoeui,
                  color: Colors.cherryRed,
                  textAlign: 'center',
                }}>
                {this.state.emptymessage == false ? null : 'No Property.'}
              </Text>
            </View>
          )}
        />
        <FlootingButton
          toSelectSpecializatio={(index) => this.selectItemGroup(index)}
          opener={this.state.opener}
          openFilter={() => this.openFilter()}
          closeFilter={() => {
            this.openFilter();
          }}
          filterlist={this.state.dataOfFilter}
          checkInd={this.state.checkInd}
        />
        {/* <Image style={styles.filter} source={images.property_filter} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
    input_SearchLocation: { fontFamily: Fonts.segoeui, fontSize: 12, color: Colors.black, marginLeft: 8, paddingVertical: 7, },
    icon_search: { height: 12, width: 12, alignSelf: 'center', },
    viewSearch: { flexDirection: 'row', borderWidth: 1, borderColor: Colors.whiteFive, marginVertical: 20, marginHorizontal: 12, paddingHorizontal: 21, borderRadius: 4 },
    txt_Property: { fontSize: 14, fontFamily: Fonts.segoeui, marginLeft: 12, marginTop: 25 },
    imgProperty: { height: 165, width: "100%", alignSelf: 'center', borderTopLeftRadius: 4, borderTopRightRadius: 4 },
    view_PropertyPrice: { flexDirection: 'row', position: 'absolute', bottom: 11, paddingHorizontal: 20, },
    txt_PropertyPrice: { flex: 1, color: Colors.whiteTwo, fontSize: 14, fontFamily: Fonts.segoeui_bold },
    txt_PropertyName: { color: Colors.marigold, fontSize: 12, fontFamily: Fonts.segui_semiBold },
    txt_Lighhouse: { marginTop: 13, paddingLeft: 20, fontFamily: Fonts.segoeui_bold, fontSize: 14, color: Colors.blackTwo },
    txt_PropertyLocation: { color: Colors.brownishGreyTwo, fontSize: 12, marginLeft: 6, fontFamily: Fonts.segoeui, },
    icon_Location: { height: 11, width: 11, resizeMode: 'contain', tintColor: Colors.brownishGreyTwo },
    btn_ViewDetails: { color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui, textAlign: 'center', fontSize: 14, marginRight: 12 },
    btn_PaymentDetails: { color: Colors.whiteTwo, fontFamily: Fonts.segoeui, textAlign: 'center', fontSize: 14, marginLeft: 12 },
    filter: { width: 56, height: 56, resizeMode: 'contain', position: 'absolute', right: 10, bottom: 40 },
    detailsClick: { flexDirection: 'row', marginTop: 15, marginHorizontal: 15, marginBottom: 16, },
    clickDetails: { flex: 1, borderWidth: 1, paddingVertical: 3, borderColor: Colors.darkSeafoamGreen, margin: 5 },
    details: { flex: 1, borderWidth: 1, paddingVertical: 3, borderColor: Colors.darkSeafoamGreen, backgroundColor: Colors.darkSeafoamGreen, margin: 5 },
 searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 3,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
    borderWidth: 0.5,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  searchIcon: {height: 12, width: 12, resizeMode: 'contain', marginLeft: 14},
  inputStyle: {
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
    width: '80%',
    height: Platform.OS === 'ios' ? 30 : 50,
    justifyContent: 'center',
  },
  checkIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.darkSeafoamGreen,
  },
  uncheckIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.pinkishGrey,
  },
  selectedText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.black,
  },
  unselectText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.pinkishGrey,
  },
});
