import React from 'react';
import { SafeAreaView, View, Text, Image, Dimensions, StyleSheet } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './HomeScreen/Home'
import Search_Tab from './Search/Search_Tab'
import PayRent from './PayRent'
import Profile from '../screens/Profile/Profile'
import Maintenance from './Maintenance'
import images from '../Lib/Images';
import Fonts from '../Lib/Fonts'
import Colors from '../Lib/Colors'
import { Helper } from '../Lib';
import { translate } from '../Language';

const Tabs = createBottomTabNavigator();
const DeviceW = Dimensions.get('screen').width

const RenderTabIcons = (props) => {
    const { icon, lable, isFocused, backgroundColor } = props;
    return (
        <View style={{ alignItems: "center", justifyContent: "center", width: DeviceW / 5, height: 70, marginTop: 13, backgroundColor: backgroundColor }}>
            <Image
                source={icon}
                style={{ height: 40, width: 40, resizeMode: 'contain', }}
            />
            <Text style={[styles.lableText, { color: isFocused ? Colors.greyishBrown : Colors.greyishBrown }]}>{lable}</Text>
        </View>
    );
}

const ReturnNavigator = createStackNavigator();
function stackNavigatorHome() {
    return (
        <ReturnNavigator.Navigator >
            <ReturnNavigator.Screen name="Home" component={Home} />
        </ReturnNavigator.Navigator>
    )
}
const searchNavigator = createStackNavigator();
function searchStackNavigator() {
    return (
        <searchNavigator.Navigator >
            <ReturnNavigator.Screen name="Search_Tab" component={Search_Tab} />
        </searchNavigator.Navigator>
    )
}

const MaintenanceNavigator = createStackNavigator();
function MaintenanceStackNavigator() {
    return (
        <MaintenanceNavigator.Navigator >
            <ReturnNavigator.Screen name="Maintenance" component={Maintenance} />
        </MaintenanceNavigator.Navigator>
    )
}


const PayRentNavigator = createStackNavigator();
function PayRentStackNavigator() {
    return (
        <PayRentNavigator.Navigator >
            <ReturnNavigator.Screen name="PayRent" component={PayRent} />
        </PayRentNavigator.Navigator>
    )
}

const ProfileNavigator = createStackNavigator();
function ProfileStackNavigator() {
    return (
        <ProfileNavigator.Navigator >
            <ReturnNavigator.Screen name="Profile" component={Profile} />
        </ProfileNavigator.Navigator>
    )
}

export default class HomeTabs extends React.Component {
    render() {
        return (
            <Tabs.Navigator
                tabBarOptions={{
                    style: {
                        height: Helper.hasNotch ? 100 : 70,
                    },

                }}
            >
                <Tabs.Screen
                    name="HomeScreen"
                    component={stackNavigatorHome}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={focused ? images.home_tab_active : images.home_tab}
                                    lable={translate("HOME")}
                                    isFocused={focused}
                                />
                            );
                        },
                    }}
                />
                <Tabs.Screen
                    name="Search"
                    component={searchStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    icon={focused ? images.search_tab_active : images.search_tab}
                                    lable={translate("SEARCH")}
                                    isFocused={focused}
                                />
                            );
                        },

                    }}
                />
                <Tabs.Screen
                    name="MaintenanceTab"
                    component={MaintenanceStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    lable={translate("MAINTENANCE")}
                                    icon={focused ? images.maintenance_tab_active : images.maintenance_tab}
                                    isFocused={focused}
                                />
                            );
                        },


                    }}
                />
                <Tabs.Screen
                    name="PayRenttab"
                    component={PayRentStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    lable={translate("PAYRENT")}
                                    icon={focused ? images.payrent_tab_active : images.pay_rent_tab}
                                    isFocused={focused}
                                />
                            );
                        },

                    }}
                />

                <Tabs.Screen
                    name="Profile"
                    component={ProfileStackNavigator}
                    options={{
                        tabBarLabel: "",
                        tabBarIcon: ({ focused }) => {
                            return (
                                <RenderTabIcons
                                    lable={translate("PROFILE")}
                                    icon={focused ? images.profile_tab_active : images.profile_tab}
                                    isFocused={focused}
                                />
                            );
                        },

                    }}
                />


            </Tabs.Navigator>

        )
    }
}

const styles = StyleSheet.create({
    lableText: { fontSize: 10, fontFamily: Fonts.segoeui }
})