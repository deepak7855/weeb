import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';
import Images from '../../Lib/Images';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { CircleButton } from '../../Components/Common/index'
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { handleNavigation } from '../../navigation/routes';
import { getWidth } from '../../Lib/Constant';
import { Network, Validation, AlertMsg } from '../../Lib/index';
import Helper from '../../Lib/Helper'
import { ApiCall } from '../../Api/index';
import { Constant } from '../../Lib/Constant';
import OTPTextView from 'react-native-otp-textinput';
import { translate } from '../../Language';
const { height, width } = Dimensions.get('window');
export default class VerifyOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resendOtp: false,
      minutes: 2,
      seconds: 0,
      screenName: props.route.params.screenName,
      countryCode: props?.route?.params?.countrycode,
      phoneNumber: props?.route?.params?.phonenumber,
      otp: '123456',
    };
  }

  componentDidMount() {
    // Helper.getData(Constant.TOKEN).then((res) => {
    //   console.log('token res', res)
    // })
    // console.log('token 1', Helper.getData(Constant.TOKEN))
    this.myinterval = setInterval(() => {
      const { minutes, seconds } = this.state;
      if (seconds > 0) {
        this.setState(({ seconds }) => ({
          seconds: seconds - 1,
        }));
      }

      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(this.myinterval);
        } else {
          this.setState(({ minutes }) => ({
            minutes: minutes - 1,
            seconds: 59,
          }));
        }
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.myinterval);
  }

  handleBackButtonClick = () => {
    handleNavigation({ type: 'push', page: 'Signup', navigation: this.props.navigation });
    return true;
  };

  VerifyOtp = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        let otp = Helper.tempValue;

        if (Validation.checkOtp('Otp', this.state.otp)) {
          if (otp == this.state.otp) {
            handleNavigation({ type: 'push', page: 'Resetpassword', navigation: this.props.navigation });
          } else {
            Helper.showToast('OTP is invalid');
          }

        }
      } else {
        Helper.showToast(AlertMsg.error.NETWORK);
      }
    }).catch((err) => {
      Helper.showToast(AlertMsg.error.NETWORK);
    })


  }


  VerifyOtpWithSignUp = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (Validation.checkOtp('Otp', this.state.otp)) {
          let otpData = {
            'country_code': this.state.countryCode,
            'mobile_number': this.state.phoneNumber,
            'otp': this.state.otp,
          }
          // let otpData = new FormData();
          // otpData.append('country_code', this.state.countryCode);
          // otpData.append('mobile_number', this.state.phoneNumber);
          // otpData.append('otp', this.state.otp);
          Helper.mainApp.showLoader()
          ApiCall.ApiMethod({ Url: 'otp-verify', method: 'POST', data: otpData }).then((res) => {
            console.log('verify',res)
            if (res.status) {
              Helper.showToast(res?.message ? res?.message : AlertMsg?.success?.SIGNUP)
              Helper.mainApp.hideLoader()
              Helper.setData(Constant.USER_DATA, res?.data);
              Helper.setData(Constant.TOKEN, res?.token);
              Helper.userData = res?.data;
              Helper.token = res?.token
              this.props.navigation.reset({
                index: 0,
                routes: [
                  { name: 'DrawerStack' },
                ],
              })
              //this.props.navigation.navigate('VerifyOtp', { screenName: '', countrycode: this.state.countryCodeWithoutPlus, phonenumber: this.state.mobile })
              return true;
            } else {
              Helper.showToast(res?.message ? res?.message : AlertMsg.error.NETWORK);
              Helper.mainApp.hideLoader()
              return false
            }

          }).catch((err) => {
            Helper.showToast(AlertMsg.error.NETWORK);
            Helper.mainApp.hideLoader()
            return false
          })
        }
      } else {
        Helper.showToast(AlertMsg.error.NETWORK);
      }
    }).catch((err) => {
      Helper.showToast(AlertMsg.error.NETWORK);
    })

  }


  reSendOtp = () => {
    if (this.state.screenName === 'forget') {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          let email = Helper.resetEmail;
          Helper.mainApp.showLoader()
          let forgetEmail = {
            email: email
          }
          ApiCall.ApiMethod({ Url: 'forgot', method: 'POST', data: forgetEmail }).then((res) => {

            if (res.status) {
              Helper.tempValue = res?.data?.otp
              Helper.showToast(res?.message ? res?.message : AlertMsg?.success?.LOGIN)
              Helper.mainApp.hideLoader()
              return
            } else {
              Helper.showToast(res?.message ? res?.message : AlertMsg.error.NETWORK);
              Helper.mainApp.hideLoader()
              return
            }
          }).catch((err) => {
            Helper.mainApp.hideLoader()
            // Helper.showToast(err?.data.message ? err?.data.message : AlertMsg.error.NETWORK);
            return
          });

        } else {
          Helper.mainApp.hideLoader()
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      }).catch((err) => {
        Helper.mainApp.hideLoader()
        Helper.showToast(AlertMsg.error.NETWORK);
      })
    } else {
      Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
          Helper.mainApp.showLoader()
          let resendNumber = {
            'country_code': this.state.countryCode,
            'mobile_number': this.state.phoneNumber,
          }
          ApiCall.ApiMethod({ Url: 'resend_otp', method: 'POST', data: resendNumber }).then((res) => {

            if (res.status) {
              Helper.tempValue = res?.data?.otp
              Helper.showToast(res?.message ? res?.message : AlertMsg?.success?.LOGIN)
              Helper.mainApp.hideLoader()
              return
            } else {
              Helper.showToast(res?.message ? res?.message : AlertMsg.error.NETWORK);
              Helper.mainApp.hideLoader()
              return
            }
          }).catch((err) => {
            Helper.mainApp.hideLoader()
            // Helper.showToast(err?.data.message ? err?.data.message : AlertMsg.error.NETWORK);
            return
          });

        } else {
          Helper.mainApp.hideLoader()
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      }).catch((err) => {
        Helper.mainApp.hideLoader()
        Helper.showToast(AlertMsg.error.NETWORK);
      })
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'} >
          <Image source={Images.signup_bg} style={styles.signUpBg} />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={Images.sign_up_logo} style={styles.logo} />
            <Text style={styles.title}>{translate("Verification")}</Text>
          </View>
          <View style={[styles.authBox, { backgroundColor: Colors.whiteFive }]}>
            <View style={{ margin: 15 }}>
              {this.state.screenName === 'forget' ? (
                <View>
                  <Text style={{ fontSize: 16, fontFamily: Fonts.segoeui_bold, textAlign: 'center' }}>{translate("Nice")}!</Text>
                  <Text style={styles.emailTitleText}>
                    {' '}
                    {translate("Wejustsent")}!
                    {translate("Lookslike")}!
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      color: Colors.brownishGreyTwo,
                      marginTop: 15,
                      fontFamily: Fonts.segoeui,
                      textAlign: 'center'
                    }}>
                    {translate("Enterthiscode")}
                  </Text>
                </View>
              ) : (
                  <View>
                    <Text style={styles.numberTitleText}>
                      {translate("Enteronetimepassword")}{' '}
                      <Text style={{ color: Colors.dustyOrange }}>
                        +{this.state.countryCode + ' ' + this.state.phoneNumber}
                      </Text>
                    </Text>
                  </View>
                )}
            </View>

            <View style={{ width: getWidth(340) }}>
              <OTPTextView
                ref={(e) => (this.input1 = e)}
                containerStyle={{ width: getWidth(44), height: getWidth(52), marginHorizontal: getWidth(8) }}
                handleTextChange={(text) => this.setState({ otp: text })}
                inputCount={6}
                textInputStyle={{
                  width: getWidth(44),
                  height: getWidth(52),
                  borderWidth: getWidth(1.5),
                  borderBottomWidth: 1,
                  color: Colors.black,
                  borderRadius: 12,
                  margin: getWidth(5),
                }}
                keyboardType="numeric"
                tintColor={Colors.black}
                offTintColor={Colors.pinkishGrey}
                defaultValue={this.state.screenName === 'forget' ? '' : '123456'}
              />
            </View>

            <View style={{ justifyContent: 'center', alignSelf: 'center', }}>
              {this.state.minutes === 0 && this.state.seconds === 0 ? null : (
                <Text
                  style={{
                    color: Colors.cherryRed,
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    marginTop: 20
                  }}>
                  {this.state.minutes}:
                  {this.state.seconds < 10
                    ? `0${this.state.seconds}`
                    : this.state.seconds}
                </Text>
              )}
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  textAlign: 'center',
                  paddingTop: 10,
                  color:
                    this.state.screenName === 'forget'
                      ? Colors.brownishGrey
                      : Colors.black,
                }}>
                {translate("Didntgetacode")}?{' '}
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.state.minutes === 0 && this.state.seconds === 0
                    ? this.reSendOtp()
                    : null
                }>
                <Text
                  style={{
                    fontSize: 14,
                    color: Colors.darkSeafoamGreen,
                    marginTop: 7,
                    textAlign: 'center',
                  }}>
                  {translate("Sendagain")}
                </Text>
              </TouchableOpacity>
            </View>
            {this.state.screenName === 'forget'
              ?
              <CircleButton
                navigate={() => this.VerifyOtp()}
                fontsize={18} labelfonts={Fonts.segoeui_bold} colors={Colors.lightMint} arrowimage={Images.arrow_green} label={translate("Verify")} />
              :
              <CircleButton
                navigate={() => this.VerifyOtpWithSignUp()}
                fontsize={16} labelfonts={Fonts.segui_semiBold} colors={Colors.lightMint} arrowimage={Images.arrow_green} label={translate("Verify")} />
            }

          </View>
          {this.state.screenName === 'forget' ? (
            <View
              style={[styles.doNotText, { width: '100%', marginTop: 50, }]}>
              <Text style={{ fontSize: 14, fontFamily: Fonts.segoeui }}>
                {translate("Donthaveanaccount")}
              </Text>
              <Text>{'  '}</Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Signup')}>
                <Text
                  style={styles.signupText}>
                  {translate("SignUp")}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 10,
                  height: 100,
                }}
                onPress={() => this.props.navigation.goBack()}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.darkSeafoamGreen,
                  }}>
                  {translate("ChangeMobileNumber")}
              </Text>
              </TouchableOpacity>
            )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.whiteTwo
  },
  signUpBg: {
    height: 500,
    width: '100%',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    overflow: 'hidden',
    position: 'absolute',
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  authBox: {
    width: getWidth(350),
    borderRadius: 15,
    alignSelf: 'center',
    padding: 5,
    paddingBottom: 30,
    marginTop: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  logo: {
    resizeMode: 'contain',
    width: 90,
    height: 100,
    marginTop: 50,
    borderColor: 'black',
  },
  title: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 18,
  },
  numberTitleText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.black,
    textAlign: 'center',
  },
  emailTitleText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui,
    color: Colors.black,
    textAlign: 'center',
  },
  borderStyleBase: {
    width: 44,
    height: 52,
  },

  borderStyleHighLighted: {
    borderColor: Colors.black,
  },

  underlineStyleBase: {
    width: getWidth(44),
    height: getWidth(52),
    borderWidth: getWidth(1.5),
    //borderBottomWidth: 1,
    color: Colors.black,
    borderRadius: 12,
    margin: getWidth(5),
  },

  underlineStyleHighLighted: {
    borderColor: Colors.pinkishGrey,
  },
  signUpCircle: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: Colors.lightMint,
    justifyContent: 'center',
  },
  verifyImageIcon: {
    height: 13,
    width: 19,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signupText: {
    textDecorationLine: 'underline',
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
  },
  doNotText: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
