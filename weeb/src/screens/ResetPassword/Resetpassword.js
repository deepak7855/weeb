import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Images from '../../Lib/Images';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
const { height, width } = Dimensions.get('window');
import { Inputs, CircleButton } from '../../Components/Common/index';
import images from '../../Lib/Images';
import { Network, Validation, AlertMsg } from '../../Lib/index';
import Helper from '../../Lib/Helper'
import { ApiCall } from '../../Api/index';
import { Constant } from '../../Lib/Constant';
import { translate } from '../../Language';
export default class Resetpassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      reNewPassword: '',
      newPasswordShow: true,
      reNewPasswordShow: true
    };
  }

  userResetPassword = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (Validation.checkNotNull('Password', 3, 25, this.state.newPassword) && Validation.checkMatch('Password', this.state.newPassword, this.state.reNewPassword)) {
          
          let resetPassword = {
            'email': Helper.resetEmail,
            'password':this.state.newPassword,
          }
          //let resetPassword = new FormData();
          // resetPassword.append('email', Helper.resetEmail);
          // resetPassword.append('password', this.state.newPassword);
          Helper.mainApp.showLoader()
          Helper.token = ''
          ApiCall.ApiMethod({ Url:'reset-password', method:'POST',data: resetPassword }).then((res) => {
            console.log('res password res', res);
            if (res.status) {
              Helper.mainApp.hideLoader()
              Helper.showToast(res?.message)
              this.props.navigation.reset({
                index: 0,
                routes: [
                  { name: 'Signin' },
                ],
              })
            } else {
              Helper.mainApp.hideLoader()
              Helper.showToast(res?.message)
            }
            Helper.mainApp.hideLoader()
          }).catch((err) => {

            Helper.mainApp.hideLoader()
            console.log('reset api fail', err);
          })
        }
      } else {
        Helper.mainApp.hideLoader()
        Helper.showToast(AlertMsg.error.NETWORK);
      }
    }).catch((err) => {
      console.log('err',err)
      Helper.showToast(AlertMsg.error.NETWORK);
    })
  }



  render() {
    const { newPasswordShow, reNewPasswordShow } = this.state
    return (
      <View style={styles.container}>
        <ScrollView keyboardShouldPersistTaps={'handled'}>
          <Image source={Images.signup_bg} style={styles.signUpBg} />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={Images.sign_up_logo} style={styles.logo} />
            <Text style={styles.title}>{translate("ResetPassword")}</Text>
          </View>
          <View style={[styles.authBox, { backgroundColor: Colors.white }]}>
            <View
              style={{
                margin: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.authTitleText}>{translate("Great")}! </Text>
            </View>

            <View
              style={{
                margin: 13,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.authMessageText}>
                {translate("Youhavejustresetyour")}.{' '}
              </Text>
            </View>

            <View
              style={{
                margin: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Inputs
                onTbShow={() => { this.setState({ newPasswordShow: !newPasswordShow }) }}
                labels={translate("NewPassword")}
                width={'83%'}
                backgroundcolor={Colors.whiteEight}
                color={Colors.brownishGrey}
                rightimage={Images.lock}
                placeholder={'**********'}
                marginleft={10}
                keyboard={'default'}
                maxlength={15}
                onChangeText={(text) => { this.setState({ newPassword: text }) }}
                value={this.state.newPassword}
                password={newPasswordShow}
                leftimage={newPasswordShow ? images.eye_off : Images.eye}
                marginright={5}
                setfocus={(input) => {
                  this.newPassword = input;
                }}
                getfocus={() => {
                  this.reNewPassword.focus();
                }}
                returnKeyType={'next'}
                bluronsubmit={false}
              />
              <Inputs
                onTbShow={() => { this.setState({ reNewPasswordShow: !reNewPasswordShow }) }}
                labels={translate("ReEnterPassword")}
                width={'83%'}
                backgroundcolor={Colors.whiteEight}
                color={Colors.brownishGrey}
                rightimage={Images.lock}
                placeholder={'**********'}
                marginleft={10}
                keyboard={'default'}
                maxlength={15}
                onChangeText={(text) => { this.setState({ reNewPassword: text }) }}
                value={this.state.reNewPassword}
                password={reNewPasswordShow}
                leftimage={reNewPasswordShow ? images.eye_off : Images.eye}
                marginright={5}
                setfocus={(input) => {
                  this.reNewPassword = input;
                }}
                returnKeyType={'done'}
                bluronsubmit={true}
              />
            </View>
            <CircleButton navigate={() => this.userResetPassword()
            } fontsize={18} labelfonts={Fonts.segoeui_bold} colors={Colors.lightMint} arrowimage={Images.arrow_green} label={'Continue'} />


          </View>

          <View
            style={[styles.signupView, { width: '100%', marginTop: 50 },]}>
            <Text style={{ fontSize: 14, fontFamily: Fonts.segoeui }}>
              {translate("Donthaveanaccount")}
            </Text>
            <Text>{'  '}</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Signup')}>
              <Text
                style={styles.sinupText}>
                Sign up
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  signUpBg: {
    height: 500,
    width: '100%',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    overflow: 'hidden',
    position: 'absolute',
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  authBox: {
    width: '90%',
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 14,
    paddingBottom: 30,
    marginTop: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    //elevation: 5,
    //borderWidth: 1,
  },
  logo: {
    resizeMode: 'contain',
    width: 90,
    height: 100,
    marginTop: 50,
    borderColor: 'black',
  },
  title: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 18,
  },
  authTitleText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.black,
  },
  authMessageText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui,
    color: Colors.black,
    textAlign: 'center',
  },
  authSendText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui,
    color: Colors.brownishGreyTwo,
    textAlign: 'center',
  },
  borderStyleBase: {
    width: 44,
    height: 52,
  },

  borderStyleHighLighted: {
    borderColor: Colors.pinkishGrey,
  },

  underlineStyleBase: {
    width: 44,
    height: 52,
    borderWidth: 1,
    //borderBottomWidth: 1,
    color: Colors.black,
    borderRadius: 12,
    margin: 5,
  },

  underlineStyleHighLighted: {
    borderColor: Colors.pinkishGrey,
  },
  signUpCircle: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: Colors.lightMint,
    justifyContent: 'center',
  },
  verifyImageIcon: {
    height: 13,
    width: 19,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sinupText: {
    textDecorationLine: 'underline',
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
  },
  signupView: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
