import React, { Component } from 'react'
import { Text, Image, View, StyleSheet, TouchableOpacity, ScrollView, Modal, FlatList,Alert } from 'react-native'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {Helper, Network, AlertMsg} from '../Lib/index';
import { ApiCall, LoaderForList } from '../Api/index';
import {ProgressiveImage} from '../Components/Common/index';
import {translate} from '../Language'

export default class MyDocuments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idCardData: [{text: 'Front ID'}, {text: 'Back ID'}],
      modalVisible: false,
      showDocument: false,
      documentData: [],
      tenatModalPic:'',
    };
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () => {
        this.setState({ showDocument: Helper.showDocument });
         this.getTenantid();
    });
    this.getTenantid();
  }

  getTenantid() {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        ApiCall.ApiMethod({Url: 'get-tenant-document', method: 'GET'}).then(
          (res) => {
            console.log('get teant id', res?.data)
            if (res?.status) {
              this.setState({
                documentData: res?.data,
              });
              return;
            }

            this.setState({
              documentData: [],
            });
          },
        );
      }
    });
  }

  setModalVisible = (visible,url) => {
    this.setState({modalVisible: visible,tenatModalPic:url});
  };
  deleteId(index) {
      Alert.alert(
        'Weeb',
        translate('AreyousureyouwanttodeletetheID?'),
        [
          {
            text: 'No',
            //  onPress: () => this.openDocumentPicker(),
          },
          {
            text: 'Yes',
            onPress: () => this.deletePic(index),
          },
        ],
        {cancelable: true},
      );
  }
  
  deletePic(index) {
  let id = this.state.documentData[index]?.id;
  Network.isNetworkAvailable().then((isConnected) => {
    if (isConnected) {
      Helper.mainApp.showLoader();
      ApiCall.ApiMethod({
        Url: 'remove-tenant-document',
        method: 'POST',
        data: {document_id: id},
      })
        .then((res) => {
          Helper.mainApp.hideLoader();
          Helper.showToast(res?.message);
          if (res) {
            this.getTenantid();
          }
        })
        .catch((err) => {
          console.log('err', err);
        });
    }
  });
}
  renderOfID = ({item,index}) => {
  // console.log('doc', item);
    return (
      <View style={[styles.viewF2, {margin: 5}]}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.viewFl}>
            {item?.status == 1 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_checked} />
              </View>
            ) : null}
            {item?.status == 2 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_error} />
              </View>
            ) : null}
            {item?.status == 0 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_check} />
              </View>
            ) : null}

            <TouchableOpacity
              onPress={() => this.setModalVisible(true, item?.front_image_url)}
              style={{alignItems: 'center'}}>
              <ProgressiveImage
                source={{
                  uri: item?.front_image_url ? item?.front_image_url : '',
                }}
                style={styles.cardImg}
                resizeMode="cover"
              />
              {/* <Image style={styles.cardImg} source={images.id_card} /> */}
              <Text style={styles.textFace}>{item.text}</Text>
            </TouchableOpacity>
            <View style={styles.line} />
            <TouchableOpacity
              onPress={() => {
                this.deleteId(index);
              }}>
              <Image style={styles.delIcon} source={images.delete_id} />
            </TouchableOpacity>
          </View>
          <View style={styles.viewFl}>
            {item?.status == 1 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_checked} />
              </View>
            ) : null}
            {item?.status == 2 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_error} />
              </View>
            ) : null}
            {item?.status == 0 ? (
              <View style={styles.touch2}>
                <Image style={styles.checkId} source={images.id_check} />
              </View>
            ) : null}
            <TouchableOpacity
              onPress={() => this.setModalVisible(true, item?.back_image_url)}
              style={{alignItems: 'center'}}>
              <ProgressiveImage
                source={{
                  uri: item?.back_image_url ? item?.back_image_url : '',
                }}
                style={styles.cardImg}
                resizeMode="cover"
              />
              {/* <Image style={styles.cardImg} source={images.id_card} /> */}
              <Text style={styles.textFace}>{item.text}</Text>
            </TouchableOpacity>
            <View style={styles.line} />
            <TouchableOpacity
              onPress={() => {
                this.deleteId(index);
              }}>
              <Image style={styles.delIcon} source={images.delete_id} />
            </TouchableOpacity>
          </View>
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            this.deleteId(index);
          }}
          >
          <Image style={styles.delIcon} source={images.delete_id} />
        </TouchableOpacity> */}
      </View>
    );
  };

  ImageModal = () => {
    const {modalVisible} = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          // Alert.alert('Modal has been closed.');
          this.setModalVisible(!modalVisible);
        }}>
        <View style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
          <TouchableOpacity
          hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
            onPress={() => this.setModalVisible(!modalVisible)}
            style={styles.modalTouch}>
            <Image style={styles.closeModal} source={images.close} />
          </TouchableOpacity>
          <View style={styles.viewModal}>
            <ProgressiveImage
              source={{
                uri: this.state.tenatModalPic
                  ? this.state.tenatModalPic
                  : images.id_card_view,
              }}
              style={styles.imgModal}
              resizeMode="cover"
            />
            {/* <Image style={styles.imgModal} source={images.id_card_view} /> */}
          </View>
        </View>
      </Modal>
    );
  };
    render() {
      //console.log(this.state.documentData)
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          {this.ImageModal()}
          <View style={{paddingTop: 15}}>
            <Text style={styles.textDocu}>{translate("Documents")}</Text>
          </View>

          {this.state.documentData && this.state.documentData.length > 0 ? (
            <View style={styles.view4}>
              <View>
                <FlatList
                  data={this.state.documentData}
                  renderItem={this.renderOfID}
                  numColumns={2}
                />
              </View>
              {this.state.documentData &&
              this.state.documentData.length > 0 &&
              this.state.documentData[0]?.status == 0 ? (
                <View style={styles.view2Fl}>
                  <View style={styles.dot} />
                  <Text style={styles.textFl}>
                    {translate("YourIdInApproveProcessing")}. {translate("pleaseWaitSomeTimeFor")}
                    {'\n'}{translate("approveOfID")}.
                  </Text>
                </View>
              ) : null}
              {this.state.documentData &&
              this.state.documentData.length > 0 &&
              this.state.documentData[0]?.status == 1 ? (
                <View style={styles.view2Fl}>
                  <View style={styles.dot} />
                  <Text style={styles.textFl}>{translate("YourIdIsApproved")}</Text>
                </View>
              ) : null}
              {this.state.documentData &&
              this.state.documentData.length > 0 &&
              this.state.documentData[0]?.status == 2 ? (
                <View style={styles.view2Fl}>
                  <View style={styles.dot} />
                  <Text style={styles.textFl}>
                    {translate("YourIdIsNotApproved")}. {translate("PleaseUploadAgainRightId")}.
                  </Text>
                </View>
              ) : null}
            </View>
          ) : (
            <View style={styles.view3}>
              <View>
                <Image style={styles.docIcon} source={images.no_document} />
                <Text style={styles.textDetails}>
                  {translate("YouDontHaveAnyDocumentYet")}.{'\n'}{translate("PleaseUploadYourGovernmentID")}.{'\n'}
                  <Text style={styles.textClick}>{translate("ClickAddButton")}</Text>
                </Text>
              </View>
              <TouchableOpacity
                style={styles.touchPlus}
                onPress={() =>
                  this.props.navigation.navigate('UploadDocuments')
                }>
                <Image style={styles.plusIcon} source={images.floting_plus} />
              </TouchableOpacity>
            </View>
          )}
          {/* {!this.state.documentData && (
            <View style={styles.view3}>
              <View>
                <Image style={styles.docIcon} source={images.no_document} />
                <Text style={styles.textDetails}>
                  You don't have any document yet.{'\n'}Please upload your
                  government ID.{'\n'}
                  <Text style={styles.textClick}>Click Add Button</Text>
                </Text>
              </View>
              <TouchableOpacity
                style={styles.touchPlus}
                onPress={() =>
                  this.props.navigation.navigate('UploadDocuments')
                }>
                <Image style={styles.plusIcon} source={images.floting_plus} />
              </TouchableOpacity>
            </View>
          )} */}
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
    line: { width: '100%', height: 2, marginTop: 17, backgroundColor: Colors.darkSeafoamGreen },
    textDocu: { fontSize: 16, lineHeight: 16, color: Colors.blackTwo, marginLeft: 13, fontFamily: Fonts.segui_semiBold },
    view3: { alignItems: 'center', justifyContent: 'center', flex: 1 },
    docIcon: { width: 200, height: 200, resizeMode: 'contain' },
    textDetails: { textAlign: 'center', fontSize: 14, lineHeight: 22, color: Colors.brownishGreyTwo, fontFamily: Fonts.segoeui, marginTop: 23 },
    textClick: { fontSize: 14, lineHeight: 22, color: Colors.black, fontFamily: Fonts.segoeui },
    touchPlus: { position: 'absolute', bottom: 40, right: 20 },
    plusIcon: { width: 56, height: 56, resizeMode: 'contain' },
  viewFl: { borderWidth: 1, flex: 1, borderColor: Colors.whiteFive, borderRadius: 4, alignItems: 'center', paddingTop: 30, marginHorizontal: 8 },
    viewF2: {  flex: 1, borderColor: Colors.whiteFive, borderRadius: 4, alignItems: 'center', paddingTop: 30, marginHorizontal: 8 },
    checkId: { width: 20, height: 20, resizeMode: 'contain' },
    cardImg: { width: 107, height: 72, resizeMode: 'contain' },
    textFace: { fontSize: 14, lineHeight: 22, fontFamily: Fonts.segui_semiBold },
    line: { width: '100%', height: 1, backgroundColor: Colors.warmGrey, opacity: 0.18, marginTop: 5 },
    delIcon: { width: 12, height: 12, resizeMode: 'contain', marginVertical: 7 },
    textFl: { marginLeft: 8, fontSize: 12, lineHeight: 12, color: Colors.black, fontFamily: Fonts.segoeui, marginBottom: 20 },
    dot: { width: 4, height: 4, borderRadius: 2, marginTop: 2, backgroundColor: Colors.pinkishGrey },
    modalTouch: {  top: 30, right: 23,height:50,width:'100%' ,justifyContent:"flex-end",alignContent:'flex-end',flexDirection:"row"},
    closeModal: { width: 16, height: 16, resizeMode: 'contain' },
    viewModal: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    imgModal: { width: 328, height: 212, resizeMode: 'contain' },
    view2Fl: { flexDirection: 'row', marginTop: 30, marginHorizontal: 13 },
    touch2: { position: 'absolute', top: 5, right: 10 },
    view4: { marginHorizontal: 5, marginTop: 15 }
});