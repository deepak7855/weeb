import React, { Component } from 'react'
import {Text, View, DeviceEventEmitter} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MyDocuments from '../screens/MyDocuments'
import Agreement from '../screens/Agreement'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts';
import { handleNavigation } from '../navigation/routes';
import AppHeader from '../Components/AppHeader';
import images from '../Lib/Images';
import { translate } from '../Language';

const Tab = createMaterialTopTabNavigator();

export default class DocumentTabs extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      type: this.props.route.params.type
    }
    AppHeader({
      ...this,
      leftHeide: false,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.goBack() },
      title: 'Documents',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }
  componentDidMount() {
  this.listner = DeviceEventEmitter.addListener('notificaionCount', (data) => {
    AppHeader({
      ...this,
      leftHeide: false,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Documents',
      searchClick: () => {
        this.notification();
      },
       searchIcon:  data > 0
              ? images.notification_goup
              : images.notification_icon,
      search: true,
    });
  });
}
  goBack = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  MyTabs = () => {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.lightMint, top:-25 }}>
        <Tab.Navigator
          initialRouteName="Feed"
          tabBarOptions={{
            activeTintColor: Colors.darkSeafoamGreen,
            inactiveTintColor: Colors.greyishBrown,
            indicatorStyle: { backgroundColor: Colors.darkSeafoamGreen },
            labelStyle: { fontSize: 16, fontFamily: Fonts.segoeui_bold },
            style: { backgroundColor: Colors.lightMint, hieght: 151, marginTop: 50, elevation: 0 },
          }}
        >
          <Tab.Screen
            name="my documents"
            component={MyDocuments }
            options={{ tabBarLabel: translate("MYDOCUMENT") }}
          />
          <Tab.Screen
            name="agreement"
            component={Agreement}
            options={{ tabBarLabel: translate("AGREEMENTS") }}
          />
        </Tab.Navigator>
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.MyTabs()}
      </View>

    );
  }
}
