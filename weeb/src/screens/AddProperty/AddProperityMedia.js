import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    Modal
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Images from '../../Lib/Images';
import { TwoCircleButton } from '../../Components/Common/index'
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import ModalView from '../../Components/ModalView';
import CameraController from '../../Lib/CameraController';
import { Helper, Network, AlertMsg } from '../../Lib/index'
import { ApiCall } from '../../Api/index';
import { translate } from "../../Language";
let lodash = require("lodash");  
import { CommonActions } from '@react-navigation/native';
let propertyImageFixArray = ['', '', '', '', '', '', '', '', ''];
let sendPropertyImage = [];
export default class AddProperityMedia extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisibleAccept: false,
            renderComponent: true,
            propertyImage: '',
            edit: props.route.params?.title

        };
        { propertyImageFixArray }
        { sendPropertyImage}
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.leftNavigation() },
            title: props.route.params?.title ? props.route.params?.title : 'Add Property',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
        // this.propertyImageFixArray = this.propertyImageFixArray
    }

    componentDidMount() {
       // console.log('property media', Helper.propertyData?.propertyphotos);
        if (this.props.route.params?.title) {

            Helper.propertyData?.propertyphotos.forEach((element, index) => {
                sendPropertyImage.push(element?.id);
                propertyImageFixArray[index] = element?.imgurl;
            });
            this.setState({
                propertyImage: propertyImageFixArray
            })
        }
    }

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }


    step = () => {
        return (
            <View style={{ width: '90%', alignSelf: 'center' }}>
                <Text
                    style={{
                        fontSize: 12,
                        fontFamily: Fonts.segoeui,
                        color: Colors.blackThree,
                        marginVertical: 7,
                    }}>
                    {'Step 6 / 6'}
                </Text>

                <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                </View>
            </View>
        );
    };

    leftNavigation = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    goToNextScreen = () => {
        this.setState({ modalVisibleAccept: false })
        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    { name: 'DrawerStack' },
                    {
                        name: 'ListedPropertyTabs',
                    },
                ],
            })
        );
       // handleNavigation({ type: 'push', page: 'ListedPropertyTabs', navigation: this.props.navigation });
    }

    rightnavihation = () => {
        console.log('semdasdsadsa', sendPropertyImage, sendPropertyImage.join(','), lodash.compact(sendPropertyImage).join(','))
        Helper.propertyData['property_photos_ids'] = lodash.compact(sendPropertyImage).join(',')

        if (this.props.route.params?.title) {
            Helper.propertyData['property_id'] = Helper.propertyData?.id

            delete Helper.propertyData?.propertyfurnishings;
            delete Helper.propertyData?.propertyamenities;
            delete Helper.propertyData?.propertyphotos;
        }
        // console.log('formdata',propertyMedia,Helper.propertyData)
        // this.props.navigation.popToTop

        if (sendPropertyImage.length == 0) {
            Helper.showToast(translate('Pleaseattachatleastoneimage'));
            return false
        }
        Network.isNetworkAvailable().then((isConnected) => {
            if (isConnected) {
                if (this.props.route.params?.title) {
                    Helper.mainApp.showLoader();
                    ApiCall.ApiMethod({ Url: 'edit-property', Method: 'POST', data: Helper.propertyData }).then((res) => {
                        console.log('add property response', res);
                        Helper.mainApp.hideLoader();
                        if (res?.status) {
                            delete Helper.propertyData?.id
                           // Helper.showToast(res?.message)
                            Helper.propertyData = {};
                            propertyImageFixArray = ['', '', '', '', '', '', '', '', ''];
                            sendPropertyImage = [];
                            this.setState({ modalVisibleAccept: true })
                        } else {
                            Helper.mainApp.hideLoader();
                            Helper.showToast(res?.message)
                        }

                    }).catch((err) => {
                        Helper.mainApp.hideLoader();
                        console.log('add property err', err)
                    })
                } else {
                    Helper.mainApp.showLoader();
                    ApiCall.ApiMethod({ Url: 'save-property', Method: 'POST', data: Helper.propertyData }).then((res) => {
                        console.log('add property response', res);
                        Helper.mainApp.hideLoader();
                        if (res?.status) {
                            //Helper.showToast(res?.message )
                            Helper.propertyData = {};
                            propertyImageFixArray = ['', '', '', '', '', '', '', '', ''];
                            sendPropertyImage = [];
                            handleNavigation({ type: 'push', page: 'ListedPropertyTabs', navigation: this.props.navigation });
                            this.setState({ modalVisibleAccept: true })
                        } else {
                            Helper.mainApp.hideLoader();
                            Helper.showToast(res?.message)
                        }

                    }).catch((err) => {
                        Helper.mainApp.hideLoader();
                        console.log('add property err', err)
                    })
                }
            }
        })
    }

    uploadPropertyImage = (data) => {
        console.log('index', data)

        CameraController.open(async response => {
            console.log('image path', response.path)
            if (await response.path) {
            
                
                propertyImageFixArray.unshift(response.path)
                propertyImageFixArray.splice(-1, 1)
        
                
                Network.isNetworkAvailable().then((isConnected) => {
                   
                    if (isConnected) {
                        let propertyMedia = new FormData();

                        propertyMedia.append("image", {
                            uri: response.path,
                            name: "property.jpg",
                            type: "image/jpeg",
                        })
                        console.log('qqqqqqq-------', propertyMedia)
                        Helper.mainApp.showLoader();
                        ApiCall.ApiMethod({ Url: 'save-property-photo', data: propertyMedia, method: 'POSTUPLOAD' }).then((res) => {
                            Helper.mainApp.hideLoader();
                            console.log('image res', res,propertyMedia)
                            if (res?.status) {
                             
                                sendPropertyImage.unshift(res?.data?.id)
                              //  sendPropertyImage.splice(-1, 1)
                                // sendPropertyImage.splice(sendPropertyImage.length, 1)
                               // sendPropertyImage[data] = res?.data?.id;
                                this.setState({
                                    propertyImage: propertyImageFixArray
                                })
                            }
                        }).catch((err) => {
                            Helper.mainApp.hideLoader();
                            console.log('image err err', err)
                        })
                    }
                })

            }

        });

        this.setState({
            renderComponent: !this.state.renderComponent
        })
    }

    deleteImage = (index) => {
        console.log('delete selected image', propertyImageFixArray[index],sendPropertyImage)
        Network.isNetworkAvailable().then((isConnected) => {
            if (isConnected) {
                const data = {
                    property_photo_id: sendPropertyImage[index]
                }
                Helper.mainApp.showLoader()
                ApiCall.ApiMethod({ Url: 'delete-property-photo', method: 'POST', data: data }).then((res) => {
                    console.log('image delete res', res)
                    Helper.mainApp.hideLoader()
                    if (res?.status) {
                        propertyImageFixArray[index] = '';
                        sendPropertyImage[index] = undefined;
                        this.setState({
                            propertyImage: propertyImageFixArray
                        })
                    }
                }).catch((err) => {
                    Helper.mainApp.hideLoader()
                    console.log('err delete image',err)
                })
            }
        })
      
    }
    imageConatiner = (data) => {
        // console.log('picture data', data)
        return (
            // <View>
            //     <Image source={Images.close} style={{ height: 20, width: 20, borderRadius: 20 }} /> 
            <TouchableOpacity
                onPress={() => this.uploadPropertyImage(data.index)}
                style={[styles.imageConatiner, { borderColor: Colors.lightMint }]}>
                {data?.item ?
                    <View>
                        <TouchableOpacity hitSlop={{ right: 30, left: 30, top: 30, bottom: 30 }} style={{ justifyContent: 'center',alignItems: 'center',right: -83, top: 10, zIndex: 5 ,height:15,width:15,borderRadius:15,backgroundColor:Colors.cherryRed}} onPress={() => this.deleteImage(data?.index)}>
                            <Image  source={Images.close} style={{ tintColor:Colors.white,height: 10, width: 10, borderRadius: 10 }} />
                        </TouchableOpacity>
                        <Image source={{ uri: data?.item }} style={{ height: 126, width: 105, resizeMode: 'contain' }} />
                    </View>
                    :
                    <View>



                        <Image source={Images.add_property} style={{ height: 126, width: 105, resizeMode: 'contain' }} />
                    </View>
                }

            </TouchableOpacity>
            // </View>

        )
    }

    openModelAccept = () => {
        return (
            <Modal
                animationType={"slide"}
                visible={this.state.modalVisibleAccept}
                transparent={true}
                onRequestClose={() => { this.setState({ modalVisibleAccept: false }) }}>
                <ModalView
                    icon={images.congratuations}
                    width={150}
                    height={150}
                    marginVertical={0}
                    color={Colors.kelleyGreen}
                    subTitle={`You have Successfully ${this.props.route.params?.title ? 'updated' : 'added'} \n your property. `}
                    onClick={() => { this.goToNextScreen() }}
                    buttonTitle={'Done'}
                />
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.step()}
                <KeyboardAwareScrollView alwaysBounceVertical={false} bounces={false}>
                    <Text style={{ fontSize: 16, fontFamily: Fonts.segoeui_bold, color: Colors.blackThree, marginTop: 25, marginHorizontal: 15 }}>{translate("AddPropertyPhoto")}</Text>
                    <View style={{ marginHorizontal: 10 }}>
                        <FlatList
                            data={this.state.propertyImage ? this.state.propertyImage : propertyImageFixArray}
                            numColumns={3}
                            renderItem={this.imageConatiner}
                            // refreshControl={
                            //     <RefreshControl onRefresh={this.onRefresh} refreshing={this.state.refresh} tintColor={Colors.deepSkyBlue} />
                            // }
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </KeyboardAwareScrollView>
                <TwoCircleButton
                    leftnavigation={() => this.leftNavigation()}
                    rightimage={Images.arrow_left_green}
                    leftimage={Images.signup_arrow}
                    rightnavigation={() => this.rightnavihation()}
                />
                {this.openModelAccept()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteTwo
    },
    stepGreenLine: {
        width: '16%',
        height: 2,
        backgroundColor: Colors.darkSeafoamGreen,
        margin: 1,
    },
    stepRedLine: {
        width: '16%',
        height: 2,
        backgroundColor: Colors.cherryRed,
        margin: 1,
    },
    imageConatiner: {
        flex: 1,
        height: 126,
        justifyContent: 'center',
        alignItems: 'center',
        //  borderWidth: 1,
        //  borderColor: Colors.lightMint,
        marginHorizontal: 20,
        marginVertical: 10
    }
});

