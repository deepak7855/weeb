import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  SectionList,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TwoCircleButton } from '../../Components/Common/index';
import Images from '../../Lib/Images';
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import {Helper} from '../../Lib/index'
import { translate } from '../../Language';
export default class SelectPropertyType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      openId: props.route.params?.title ? Helper?.propertyData?.type == 'Full House' ? 0 : Helper?.propertyData?.type == 'Apartment in House' ? 1 : Helper?.propertyData?.type == 'Chalet' ? 2 : Helper?.propertyData?.type == 'Apartment in Building'?3:0:'',
      edit: props.route.params?.title,
      propertyType: props.route.params?.title ? Helper?.propertyData?.type == 'Full House' ? 'Full House' : Helper?.propertyData?.type == 'Apartment in House' ? 'Apartment in House' : Helper?.propertyData?.type == 'Chalet' ? 'Chalet' : Helper?.propertyData?.type == 'Apartment in Building' ? 'Apartment in Building' : 'Full House' : 'Full House',
      data: [
        {
          typename: 'Full House',
          typeActiveIcon: Images.full_house_white,
          typeInActiveIcon: Images.full_house_green
        },
        {
          typename: 'Apartment in House',
          typeActiveIcon: Images.apartment_white,
          typeInActiveIcon: Images.apartment_green
        },
        {
          typename: 'Chalet',
          typeActiveIcon: Images.chalet_white,
          typeInActiveIcon: Images.chalet_green
        },
        {
          typename: 'Apartment in Building',
          typeActiveIcon: Images.apartmentbuilding_white,
          typeInActiveIcon: Images.apartmentbuilding_green
        },
      ]
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.goBack() },
      title: this.props.route.params?.title ? this.props.route.params?.title : translate('AddProperty'),
     // title: props.route.params?.title ? props.route.params?.title: 'Add Property',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }

  // componentDidMount() {
  //   if (this.props.route.params?.title) {
  //     console.log('edit data', Helper.propertyData)
  //   }
  // }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  goBack = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }


  step = () => {
    return (
      <View style={{marginHorizontal:14}}>
        <Text
          style={{
            fontSize: 12,
            fontFamily: Fonts.segoeui,
            color: Colors.blackThree,
            margin: 7,
          }}>
          {'Step 1 / 6'}
        </Text>

        <View style={{ flexDirection: 'row', margin: 5 }}>
          <View style={styles.stepGreenLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
        </View>
      </View>
    );
  };

  getdata = (index) => {
    console.log('data get', index,index?.item?.typename);
    if (this.state.openId === index.index) {
      this.setState({
        openId: '',
      });
    } else {
      this.setState({ openId: index.index, propertyType: index?.item?.typename });
    }
  };
  renderFlatListItem = (data) => {
    return (
      <TouchableOpacity
        style={
          this.state.openId === data.index
            ? styles.flatListCheckedView
            : styles.flatListView
        }
        onPress={() => {
          this.getdata(data);
        }}>
        <Image
          source={
            this.state.openId === data.index
              ? data.item.typeActiveIcon
              : data.item.typeInActiveIcon
          }
          style={styles.flatImage}
        />
        <View style={styles.verticleLine}></View>

        <View style={{ width: '50%', margin: 10 }}>
          <Text
            style={{
              fontFamily: Fonts.segui_semiBold,
              color:
                this.state.openId === data.index
                  ? Colors.whiteTwo
                  : Colors.black,
            }}>
            {data.item.typename}
          </Text>
        </View>
        <Image
          source={
            this.state.openId === data.index
              ? Images.property_type_check
              : Images.peropert_type_check
          }
          style={styles.flatImage}
        />
      </TouchableOpacity >
    );
  };

  activeProparty = () => {
    console.log('openid', this.state.openId)
    if (this.state.openId === '') {
      Helper.showToast(translate('Pleaseselectpropertytype'))
      return false;
    }
    Helper.propertyData["type"] = this.state.propertyType 
    
    handleNavigation({ type: 'push', page: 'ProperityInformation', passProps: { title: this.state.edit?'Edit Property':null},navigation: this.props.navigation });
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBarCustom backgroundColor={Colors.whiteTwo} translucent={false}/>
        {this.step()}
        <KeyboardAwareScrollView>
          <Text style={styles.propartyText}>{translate("SelectPropertyType")}</Text>
          <View style={{}}>
          <FlatList
            data={this.state.data}
            renderItem={this.renderFlatListItem}
            keyExtractor={(_, index) => index.toString()}
          />
          </View>
          <TwoCircleButton
            disabled={true}
            rightimage={Images.arrow_gray}
            rightnavigation={() => this.activeProparty()}
            leftimage={Images.signup_arrow}
          />
        </KeyboardAwareScrollView>

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  stepGreenLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.darkSeafoamGreen,
    margin: 1,
  },
  stepRedLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.cherryRed,
    margin: 1,
  },
  flatListView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    marginHorizontal: 16,
    backgroundColor: '#f8fffa',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
    marginTop:15
  },
  flatListCheckedView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    marginHorizontal: 16,
    backgroundColor: Colors.darkSeafoamGreen,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
    marginTop:15
  },
  flatImage: {
    width: 53,
    height: 48,
    margin: 5,
  },
  verticleLine: {
    height: '80%',
    margin: 5,
    width: 1,
    backgroundColor: Colors.whiteFive,
  },
  propartyText: {fontSize:16, fontFamily: Fonts.segoeui_bold, color: Colors.blackThree, marginHorizontal:16, marginVertical:10}
});
