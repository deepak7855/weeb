import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    Dimensions
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Images from '../../Lib/Images';
import { DatePicker, Inputs, CustomSquareButton, CheckBox, LargeTextInput, TwoCircleButton } from '../../Components/Common/index'
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import moment from 'moment';
import { Helper,Validation, AlertMsg} from '../../Lib/index';
import { translate } from "../../Language";

const { height, width } = Dimensions.get('window')
let othercharge = [];
export default class ProperityCharges extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showDataButton: props.route.params?.title ? Helper?.propertyData?.available_date ? true:false:false,
            showImmediatelyButon: false,
            firstCheck:  false,
            secondCheck: false,
            depositAmount: props.route.params?.title ?String( Helper?.propertyData?.deposite_amount):'',
            selectedDate: props.route.params?.title ? Helper?.propertyData?.available_date? moment(Helper?.propertyData?.available_date).format('DD/MM/YYYY'):'' :'' ,
            houseRule: props.route.params?.title ? Helper?.propertyData?.home_rules : '',
            edit: props.route.params?.title,
            sendSelectedDate: props.route.params?.title ? moment(Helper?.propertyData?.available_date).format('YYYY-MM-DD') : '',
        };
        AppHeader({
            ...this,
            leftHeide: false,
            leftIcon: images.black_arrow_btn,
            leftClick: () => { this.leftNavigation() },
            title: props.route.params?.title ? props.route.params?.title :'Add Property',
            searchClick: () => { this.notification() },
            searchIcon: images.notification_goup,
            search: true
        });
    }

    componentDidMount() {
        console.log('helper data in harges',Helper.propertyData)
        if (Helper?.propertyData?.other_charges) {
            let check = Helper?.propertyData?.other_charges.split(',')
            console.log(check[0], check[1]);
            if (check[0]) {
                othercharge[0] = 'maintenance_charge'
                this.setState({
                    firstCheck: true
                })
            }

            if (check[1]) {
                othercharge[1] = 'electricity_charge'
                this.setState({
                    secondCheck: true
                })
            }
        }
}

    notification = () => {
        handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
    }


    step = () => {
        return (
            <View style={{ width: '90%', alignSelf: 'center' }}>
                <Text
                    style={{
                        fontSize: 12,
                        fontFamily: Fonts.segoeui,
                        color: Colors.blackThree,
                        marginVertical: 15,
                    }}>
                    {'Step 5 / 6'}
                </Text>

                <View style={{ flexDirection: 'row', marginVertical: 15 }}>
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepGreenLine} />
                    <View style={styles.stepRedLine} />
                </View>
            </View>
        );
    };

    showDataButtonFunction = () => {
        this.setState({
            showDataButton: true,
            showImmediatelyButon: false,
        })
    }

    leftNavigation = () => {
        handleNavigation({ type: 'pop', navigation: this.props.navigation, });
    }

    rightnavihation = () => {
        if (this.state.showDataButton) {
            if (!this.state.selectedDate) {
                Helper.showToast(translate('PleaseSelectDate'))
                return
            }
            Helper.propertyData['available_types'] = 'date'
            Helper.propertyData['available_date'] = this.state.sendSelectedDate
        } else {
            Helper.propertyData['available_types'] = 'Immediately'
        }
        Helper.propertyData['deposite_amount'] = this.state.depositAmount;
        
  
        if (this.state.firstCheck) {
            othercharge[0] = 'maintenance_charge'
        }
         else {
            if (othercharge.indexOf('maintenance_charge') > -1) {
                othercharge.splice(othercharge.indexOf('maintenance_charge'), 1);

            }
        }
        if (this.state.secondCheck) {
            if (othercharge.length == 0) {
                othercharge[0] = 'electricity_charge'
            } else {
                othercharge[1] = 'electricity_charge'
            }
        }
        else {
            console.log('in else', othercharge.indexOf('electricity_charge'), this.state.secondCheck)
            if (othercharge.indexOf('electricity_charge') > -1) {
                othercharge.splice(othercharge.indexOf('electricity_charge'), 1);
                
            }
        }

        if (!this.state.firstCheck && !this.state.secondCheck) {
            Helper.propertyData['other_charges'] = ''
        }
        if (this.state.firstCheck || this.state.secondCheck) Helper.propertyData['other_charges'] = othercharge.join(',');
        Helper.propertyData['Other_charges'] = 'electricity_charge'
        Helper.propertyData['home_rules'] = this.state.houseRule;
        if (Validation.checkPicker('Deposit amount', 1, this.state.depositAmount) && Validation.checkAddress('House rule', 1, this.state.houseRule)) { 
          handleNavigation({ type: 'push', page: 'AddProperityMedia', passProps: { title: this.state.edit ? 'Edit Property' : null }, navigation: this.props.navigation });
        console.log('helper data',Helper.propertyData)
        }
      
    }

    checkMaintenanceCharge = () => { 
        if (this.props.route.params?.title)
        if (this.state.firstCheck) {
            othercharge[0] = 'maintenance_charge'
            console.log(othercharge.indexOf('maintenance_charge'), othercharge)
            this.setState({ firstCheck: !this.state.firstCheck })
        } else {
            this.setState({ firstCheck: !this.state.firstCheck })
            if (othercharge.indexOf('maintenance_charge') > -1) {
                othercharge.splice(othercharge.indexOf('maintenance_charge'), 1);
                console.log(othercharge.indexOf('maintenance_charge'),othercharge,this.state.firstCheck)
            }
        }
    }

    onElectricityCharge = () => {
            
        if (this.state.secondCheck) {
            this.setState({ secondCheck: !this.state.secondCheck })
            othercharge[1] = 'electricity_charge'
            console.log(othercharge.indexOf('electricity_charge'),othercharge)
        } else {
            // this.setState({ secondCheck: !this.state.secondCheck })
                if (othercharge.indexOf('electricity_charge') > -1) {
                    othercharge.splice(othercharge.indexOf('electricity_charge'), 1);
                    console.log(othercharge.indexOf('electricity_charge', othercharge, this.state.secondCheck))
                }
            }
    }

    showImmediatelyButonFunction = () => {
        if (this.props.route.params?.title) {
            this.setState({
                showDataButton: false,
                showImmediatelyButon: true,
            })
        } else {
            this.setState({
                showDataButton: false,
                showImmediatelyButon: true,
                depositAmount: '',
                houseRule: '',
                firstCheck: false,
                secondCheck: false,
                selectedDate:'',
                sendSelectedDate:'',

            })
        }
    }

    date = (val) => { 
        console.log('date return', val, moment(val).format('YYYY-MM-DD'), moment(val).format('DD-MM-YYYY'))  
        this.setState({ 
            selectedDate: moment(val).format('DD-MM-YYYY'),
            sendSelectedDate: moment(val).format('YYYY-MM-DD'),
        })
    }
    render() {
        return (
            <View style={styles.container}>
                {this.step()}
                <KeyboardAwareScrollView >
                    <View style={{ marginHorizontal: 17 }}>
                        <Text style={{ fontSize: 16, fontFamily: Fonts.segoeui_bold, color: Colors.blackThree }}>{translate("SelectAvailableFrom")}</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                            <CustomSquareButton color2={Colors.whiteTwo} state={this.state.showDataButton} showButton={() => this.showDataButtonFunction()} width={'45%'} bordercolor={Colors.lightMint} label={'Date'} backgroundcolor={Colors.darkSeafoamGreen} color={Colors.darkSeafoamGreen} />
                            <CustomSquareButton color2={Colors.whiteTwo} state={this.state.showImmediatelyButon} showButton={() => this.showImmediatelyButonFunction()} width={'45%'} bordercolor={Colors.lightMint} label={'Immediately'} backgroundcolor={Colors.darkSeafoamGreen} color={Colors.darkSeafoamGreen} />
                        </View>
                    </View>

                    {this.state.showDataButton ?
                        <View style={{ marginHorizontal: 10 }}>
                            <DatePicker 
                                width={'87%'}
                                backgroundcolor={'white'}
                                color={Colors.brownishGrey}
                                leftimage={Images.calendar_icon}
                                marginleft={-10}
                                date={(val) => this.date(val)}
                                value={this.state.selectedDate}
                            />
                        </View>
                        :
                        null
                    }
                    <View style={{ marginHorizontal: 10 }}>
                        <Inputs
                            labels={'Deposit Amount'}
                            labelfontfamily={Fonts.segoeui_bold}
                            labelsize={16}
                            width={'87%'}
                            backgroundcolor={'white'}
                            color={Colors.black}
                            leftimage={Images.dallar}
                            placeholder={'Enter Deposit Amount'}
                            marginleft={-10}
                            keyboard={'numeric'}
                            maxlength={20}
                            placeholdercolor={Colors.pinkishGrey}
                            onChangeText={(text) => { this.setState({ depositAmount: text }) }}
                            value={this.state.depositAmount}
                            returnKeyType={'done'}
                            setfocus={(input) => {
                                this.depositAmount = input;
                            }}
                        />
                    </View>

                    <View style={{marginHorizontal:10}}>
                        <CheckBox state={this.state.firstCheck} onpress={() => this.setState({firstCheck:!this.state.firstCheck})} color={Colors.black} fontfamily={Fonts.segui_semiBold} fontsize={14} label={'Maintenance Charge'} />
                        <CheckBox state={this.state.secondCheck} onpress={() => this.setState({secondCheck:!this.state.secondCheck})} color={Colors.black} fontfamily={Fonts.segui_semiBold} fontsize={14} label={'Electricity Charge'} />
                    </View>


                    <View style={{marginHorizontal:14}}>
                    <LargeTextInput value={this.state.houseRule} changetext={(val)=>this.setState({houseRule:val})} height={107} maxlength={500} discription={'House Rules'} placeholder={'Enter House Rules'} />
                    </View>
                    <TwoCircleButton
                        leftnavigation={() => this.leftNavigation()}
                        rightimage={Images.arrow_left_green}
                        leftimage={Images.signup_arrow}
                        rightnavigation={() => this.rightnavihation()}
                    />
                </KeyboardAwareScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    stepGreenLine: {
        width: '16%',
        height: 2,
        backgroundColor: Colors.darkSeafoamGreen,
        margin: 1,
    },
    stepRedLine: {
        width: '16%',
        height: 2,
        backgroundColor: Colors.cherryRed,
        margin: 1,
    },
    DataButton: {
        width: '45%',
        borderWidth: 1,
        height: 40,
        justifyContent: "center",
        alignItems: 'center',
        borderColor: Colors.lightMint,
        //backgroundColor: Colors.darkSeafoamGreen
    }
});

