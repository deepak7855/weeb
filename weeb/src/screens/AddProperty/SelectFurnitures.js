import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Images from '../../Lib/Images';
import { ConsoleLog } from '../../Components/Utils';
import { TwoCircleButton } from '../../Components/Common/index'
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { Helper, Network, AlertMsg } from '../../Lib/index';
import { ApiCall } from '../../Api/index';
import { translate } from '../../Language';
let selectedFurniture = [];
let selectedAmenties = [];
let userSelectedFurniture = [];
let userSelectedAmenties = [];
export default class SelectFurnitures extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openFurnitureId: 0,
      openAmenitiesId: 0,
      edit: props.route.params?.title,
      amenities: '',
      furniture:'',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.leftNavigation() },
      title: props.route.params?.title ? props.route.params?.title :'Add Property',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    console.log('room helper', Helper.propertyData)
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({ Url: 'get-amenities', method: 'GET' }).then((res) => {
          let arr = [];
          if (res?.status) {
            Helper.mainApp.hideLoader();
            if (this.props.route.params?.title) { 
              res?.data.forEach(element => {
                arr.push({
                  id: element?.id,
                  isSelect: Helper.propertyData?.propertyamenities.filter(function (val) {
                    // console.log(val?.amenity?.id === 3)  
                    return val?.amenity?.id === element?.id;
                  }).length > 0
                  ,
                  image: element?.image, title: element?.title
                })
            
              });
             // userSelectedAmenties = arr;
              Helper.propertyData?.propertyamenities.forEach(element => {
                console.log('ele', element?.amenity?.id)
                userSelectedAmenties.push(element?.amenity?.id)
                selectedAmenties.push(element?.amenity?.id)
               // return element?.amenity?.id
              })
              // userSelectedAmenties.push(Helper.propertyData?.propertyamenities.forEach(element => {
              //   console.log('ele', element?.amenity?.id)
              //   return element?.amenity?.id
              // })
              // )
              // selectedAmenties.push(Helper.propertyData?.propertyamenities.forEach(element => {
              //   return element?.amenity?.id
              // }))
              console.log('alredy data',userSelectedAmenties);
            } else {
              res?.data.forEach(element => {
                arr.push({ id: element?.id, isSelect: false, image: element?.image, title: element?.title })
              });
            }
          console.log('furniture data', arr);
            this.setState({ amenities: arr })
          
          }
        }).catch((err) => {
          console.log('amenties error', err)
          Helper.mainApp.hideLoader();
        })

        ApiCall.ApiMethod({ Url:'get-furnishings', method:'GET' }).then((res) => {
          let arr = [];
          if (res?.status) {
            Helper.mainApp.hideLoader();
            if (this.props.route.params?.title) {
              console.log('Helper.propertyData?.propertyfurnishings', Helper.propertyData?.propertyfurnishings)
              res?.data.forEach(element => {

                arr.push({
                  id: element?.id,
                  isSelect: Helper.propertyData?.propertyfurnishings.filter(function (val) { 
                    return val?.furnishing?.id === element?.id;
                  }).length > 0
                  ,
                  image: element?.image, title: element?.title
                })
              });
              Helper.propertyData?.propertyfurnishings.forEach(element => {
                userSelectedFurniture.push(element?.furnishing?.id);
                selectedFurniture.push(element?.furnishing?.id)
              })
               console.log('amenties data', arr);
             
            } else {
              res?.data.forEach(element => {

                arr.push({ id: element?.id, isSelect: false, image: element?.image, title: element?.title })
              });
            }
            this.setState({ furniture: arr })
            
          }
        }).catch((err) => {
          console.log('furniture api err', err);
          Helper.mainApp.hideLoader();
        })
      }
    }).catch((err) => { 
      console.log('network err', err);
      Helper.mainApp.hideLoader();
    })
 
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  step = () => {
    return (
      <View style={{width:'90%', alignSelf:'center'}}>
        <Text
          style={{
            fontSize: 12,
            fontFamily: Fonts.segoeui,
            color: Colors.blackThree,
            marginVertical: 15,
          }}>
          {'Step 4 / 6'}
        </Text>

        <View style={{ flexDirection: 'row', marginVertical: 15 }}>
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
        </View>
      </View>
    );
  };

  leftNavigation = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }


  rightnavihation = () => {
    console.log('send data furniture',userSelectedFurniture.join(','),'send data amenties',userSelectedAmenties.join(','))
    if (userSelectedFurniture.length == 0) {
      Helper.showToast(translate('Pleaseselectatleastonefurnishing'));
      return false
    }
    if (userSelectedAmenties.length == 0) {
      Helper.showToast(translate('Pleaseselectatleastoneamenities'));
      return false;
    }
    Helper.propertyData['furnishing_ids'] = userSelectedFurniture.join(',');
    Helper.propertyData['amenities_ids'] = userSelectedAmenties.join(',');
    handleNavigation({ type: 'push', page: 'ProperityCharges', passProps: { title: this.state.edit ? 'Edit Property' : null }, navigation: this.props.navigation });
  }

  onFurnitureSelect(index) {
    let selected = [...this.state.furniture];
    console.log('index', index);
    console.log(
      'selected aray',
      selected[index].isSelect,
      !selected[index].isSelect,
      userSelectedFurniture
    );
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({ furniture: selected });
    if (selected[index].isSelect) {
      selectedFurniture.push(selected[index].id)
      userSelectedFurniture.push(selected[index].id)
    } else {
      console.log('selected[index].id', selected[index].id, selectedFurniture.indexOf(selected[index].id))
      if (selectedFurniture.indexOf(selected[index].id) > -1) {
        selectedFurniture.splice(selectedFurniture.indexOf(selected[index].id), 1);
        userSelectedFurniture.splice(userSelectedFurniture.indexOf(selected[index].id), 1);
       }
    }
    console.log('furniture result',userSelectedFurniture)
  }

  onAmentiesSelect(index) {
    let selected = [...this.state.amenities];
    console.log('index', index);
    console.log(
      'selected aray',
      selected[index].isSelect,
      !selected[index].isSelect,
    );
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({ amenities: selected });

    if (selected[index].isSelect) {
      selectedAmenties.push(selected[index].id)
      userSelectedAmenties.push(selected[index].id)
    } else {
      console.log('selected[index].id', selected[index].id, selectedAmenties.indexOf(selected[index].id))
      if (selectedAmenties.indexOf(selected[index].id) > -1) {
        selectedAmenties.splice(selectedAmenties.indexOf(selected[index].id), 1);
        userSelectedAmenties.splice(userSelectedAmenties.indexOf(selected[index].id), 1);
      }
    }
    console.log('amenties result',userSelectedAmenties)
  }
  renderFuritures = (data) => {
    //ConsoleLog('flatlist', data.item);
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 15, marginTop:10 }}
        onPress={() => this.onFurnitureSelect(data.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{uri: data.item?.image }}
            style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.darkSeafoamGreen,borderWidth:1.5 }}
          />
        ) : (
            <Image
              source={{ uri: data.item?.image }}
              style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.white, borderWidth: 1.5 }}
            />
          )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  renderAmenities = (data) => {
   // console.log('amenities data in flatlist',data?.item)
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 15, marginTop:10 }}
        onPress={() => this.onAmentiesSelect(data?.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{ uri: data?.item?.image }}
            style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.darkSeafoamGreen, borderWidth: 1.5  }}
          />
        ) : (
            <Image
              source={{ uri: data?.item?.image }}
              style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.white, borderWidth: 1.5  }}
            />
          )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data?.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        {this.step()}
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <Text
            style={{
              fontSize: 16,
              color: Colors.blackThree,
              fontFamily: Fonts.segoeui_bold,
              marginHorizontal: 16, marginTop: 10
            }}>
            {translate("SelectFurnishing")}
            </Text>
          <View>
            <FlatList
              
              data={this.state.furniture}
              renderItem={this.renderFuritures}
              numColumns={5}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <Text
            style={{
              fontSize: 16,
              color: Colors.blackThree,
              fontFamily: Fonts.segoeui_bold,
              marginHorizontal: 16, marginTop: 10
            }}>
           {translate("SelectAmenities")}
            </Text>
          <View>
            <FlatList
              data={this.state.amenities}
              renderItem={this.renderAmenities}
              numColumns={5}
              keyExtractor={(item, index) => item + index}
            />
          </View>

        </KeyboardAwareScrollView>
        <TwoCircleButton
          leftnavigation={() => this.leftNavigation()}
          rightimage={Images.arrow_left_green}
          leftimage={Images.signup_arrow}
          rightnavigation={() => this.rightnavihation()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Colors.whiteTwo,
  },
  stepGreenLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.darkSeafoamGreen,
    margin: 1,
  },
  stepRedLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.cherryRed,
    margin: 1,
  },
  flatListView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.lightMint10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatListCheckedView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.darkSeafoamGreen,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatImage: {
    width: 53,
    height: 48,
    margin: 5,
  },
  verticleLine: {
    height: '80%',
    margin: 5,
    width: 1,
    backgroundColor: Colors.whiteFive,
  },
});
