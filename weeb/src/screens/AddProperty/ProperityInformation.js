import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import StepLine from '../../Components/Common/StepLine';
import Images from '../../Lib/Images';
import { Inputs, MobileInputs, GeoLocation, LargeTextInput, TwoCircleButton, GoogleApiAddressList } from '../../Components/Common/index';
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { Validation, Network, AlertMsg, Helper } from '../../Lib/index'
import { translate } from "../../Language";

export default class ProperityInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: Helper.propertyData?.title ? Helper.propertyData?.title :'',
      discretion: props.route.params?.title ? Helper.propertyData?.description : '',
      modalVisible: false,
      address: props.route.params?.title ? Helper.propertyData?.location : '',
      lat: props.route.params?.title ? Helper.propertyData?.latitude : '',
      long: props.route.params?.title ? Helper.propertyData?.longitude : '',
      edit: props.route.params?.title ? props.route.params?.title : '',
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.leftNavigation() },
      title: this.props.route.params?.title ? this.props.route.params?.title  : 'Add Property',
      //title: props.route.params?.title ? props.route.params?.title : 'Add Property',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }

  componentDidMount() {
   
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      console.log('title', this.state.title, 'edit', this.state.edit, 'props',this.props.route.params?.title)
      this.setState({
        title: this.state.title ? this.state.title : Helper.propertyData?.title,
        discretion: this.state.discretion ? this.state.discretion: Helper.propertyData?.description ,
        address: this.state.address ? this.state.address:  Helper.propertyData?.location ,
        lat: this.state.lat ? this.state.lat:  Helper.propertyData?.latitude,
        long: this.state.long ? this.state.long : Helper.propertyData?.longitude,
        edit: this.props.route.params?.title ? this.props.route.params?.title : this.state.edit
      })

    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }
  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }

  step = () => {
    return (
      <View style={{ marginHorizontal: 14 }}>
        <Text
          style={{
            fontSize: 12,
            fontFamily: Fonts.segoeui,
            color: Colors.blackThree,
            margin: 7,
          }}>
          {'Step 2 / 6'}
        </Text>

        <View style={{ flexDirection: 'row', margin: 5 }}>
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
        </View>
      </View>
    );
  };

  leftNavigation = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  rightnavihation = () => {

    if (Validation.checkAlphaNumeric('Property title', 3, 150, this.state.title ? this.state.title.replace(/^\s\s*/, '').replace(/\s\s*$/, ''): '' )
      && Validation.checkAddress('Address', 3, this.state.address)
      && Validation.checkNotNull('Description', 3, 500, this.state.discretion)) {
      Helper.propertyData['title'] = this.state.title ? this.state.title : Helper.propertyData?.title;
      Helper.propertyData['location'] = this.state.address ? this.state.address : Helper.propertyData?.location;
      Helper.propertyData['latitude'] = this.state.lat ? this.state.lat : Helper.propertyData?.latitude;
      Helper.propertyData['longitude'] = this.state.long ? this.state.long : Helper.propertyData?.longitude;
      Helper.propertyData['description'] = this.state.discretion ? this.state.discretion : Helper.propertyData?.description;
      this.setState({
        title: '',
        address: '',
        discretion: '',
      })
      handleNavigation({ type: 'push', page: 'ProperityRoomInfomation', passProps: { title: this.state.edit ? 'Edit Property' : null }, navigation: this.props.navigation });
    }

  }

  handleAddress = (data) => {
    console.log('property location', data)
    this.setState({ address: data?.addressname, lat: data?.lat, long: data?.long })
  }

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'} bounces={false}>
          {this.step()}
          <View style={{ marginHorizontal: 14 }}>
            <Inputs
              labelcolor={Colors.blackThree}
              labelsize={16}
              labelfontfamily={Fonts.segoeui_bold}
              labels={'Property Title'}
              width={'95%'}
              backgroundcolor={'white'}
              color={Colors.black}
              placeholder={'Enter Title'}
              marginleft={-10}
              keyboard={'default'}
              maxlength={150}
              placeholdercolor={Colors.pinkishGrey}
              onChangeText={(text) => { this.setState({ title: text }) }}
              value={this.state.title}
              maxlengthtext={150}
            />

            <GeoLocation
              labelcolor={Colors.blackThree}
              labelsize={16}
              labelfontfamily={Fonts.segoeui_bold}
              labels={'Location'}
              labelsize={16}
              width={'87%'}
              backgroundcolor={Colors.whiteTwo}
              color={Colors.black}
              placeholder={'Enter Address'}
              marginleft={-10}
              keyboard={'email-address'}
              maxlength={15}
              leftimage={Images.dirction}
              showModal={() => {
                this.setState({ modalVisible: true });
              }}
              value={this.state.address }
            />

            <GoogleApiAddressList
              modalVisible={this.state.modalVisible}
              hideModal={() => { this.setState({ modalVisible: false }) }}
              onSelectAddress={this.handleAddress}

            />

            <LargeTextInput backgroundcolor={'white'}  value={this.state.discretion} changetext={(val) => this.setState({ discretion: val })} height={250} maxlength={500} discription={'Property Description'} placeholder={'Enter Description'} />
            <TwoCircleButton
              leftnavigation={() => this.leftNavigation()}
              rightimage={Images.arrow_left_green}
              rightnavigation={() => { this.rightnavihation() }}
              leftimage={Images.signup_arrow}
            />
          </View>
        </KeyboardAwareScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  stepGreenLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.darkSeafoamGreen,
    margin: 1,
  },
  stepRedLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.cherryRed,
    margin: 1,
  },
  flatListView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.lightMint10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatListCheckedView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.darkSeafoamGreen,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatImage: {
    width: 53,
    height: 48,
    margin: 5,
  },
  verticleLine: {
    height: '80%',
    margin: 5,
    width: 1,
    backgroundColor: Colors.whiteFive,
  },
});
