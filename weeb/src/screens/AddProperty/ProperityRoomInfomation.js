import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import StepLine from '../../Components/Common/StepLine';
import Images from '../../Lib/Images';
import {
  Inputs,
  Picker,
  TwoPicker,
  TwoCircleButton
} from '../../Components/Common/index';
import { handleNavigation } from '../../navigation/routes';
import AppHeader from '../../Components/AppHeader';
import images from '../../Lib/Images';
import { Validation, Network, AlertMsg,Helper } from '../../Lib/index'
import { translate } from "../../Language";

export default class ProperityRoomInfomation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: props.route.params?.title ?String(Helper.propertyData?.rent):'', 
      rooms: props.route.params?.title ? Helper.propertyData?.rooms : '',
      bathroom: props.route.params?.title ? Helper.propertyData?.bathrooms : '',
      balcony: props.route.params?.title ? Helper.propertyData?.balcony : '',
      selectFloor: props.route.params?.title ? Helper.propertyData?.floor : '',
      totalFloor: props.route.params?.title ?Helper.propertyData?.total_floor : '',
      edit: props.route.params?.title ,
    };
    AppHeader({
      ...this,
      leftHeide: false,
      leftIcon: images.black_arrow_btn,
      leftClick: () => { this.leftNavigation() },
      title: props.route.params?.title ? props.route.params?.title :'Add Property',
      searchClick: () => { this.notification() },
      searchIcon: images.notification_goup,
      search: true
    });
  }

  componentDidMount() {
    console.log('room information', Helper.propertyData?.rooms, this.props.route.params?.title)
  }

  notification = () => {
    handleNavigation({ type: 'push', page: 'Notification', navigation: this.props.navigation });
  }
  step = () => {
    return (
      <View style={{ marginHorizontal: 14 }}>
        <Text
          style={{
            fontSize: 12,
            fontFamily: Fonts.segoeui,
            color: Colors.blackThree,
            margin: 7,
          }}>
          {'Step 3 / 6'}
        </Text>

        <View style={{ flexDirection: 'row', margin: 5 }}>
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepGreenLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
        </View>
      </View>
    );
  };

  leftNavigation = () => {
    handleNavigation({ type: 'pop', navigation: this.props.navigation, });
  }

  rightnavihation = () => {
    if (this.state.amount == 0) {
      Helper.showToast( translate('RentAmountisinvalid'))
      return false;
    }
    if (
      Validation.checkAmount('Rent amount', 1, this.state.amount)
      &&
      Validation.checkPicker('Rooms', 1, this.state.rooms) && Validation.checkPicker('Bathrooms', 1, this.state.bathroom) && Validation.checkPicker('Balcony', 1, this.state.balcony) && Validation.checkPicker('Floor', 1, this.state.selectFloor) && Validation.checkPicker('Total floor', 1, this.state.totalFloor)) {
      Helper.propertyData['rent'] = this.state.amount;
      Helper.propertyData['rooms'] = this.state.rooms;
      Helper.propertyData['bathrooms'] = this.state.bathroom;
      Helper.propertyData['balcony'] = this.state.balcony;
      Helper.propertyData['floor'] = this.state.selectFloor;
      Helper.propertyData['total_floor'] = this.state.totalFloor;
      handleNavigation({ type: 'push', page: 'SelectFurnitures', passProps: { title: this.state.edit ? 'Edit Property' : null }, navigation: this.props.navigation });
    }
   
  }

  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView  bounces={false}>
          {this.step()}
          <View style={{ marginHorizontal: 14 }}>
            <Inputs
              labels={'Rent Amount'}
              labeltwo={'(per month)'}
              labelTwoStyle={true}
              labelfontfamily={Fonts.segoeui_bold}
              labelsize={16}
              width={'87%'}
              backgroundcolor={'white'}
              color={Colors.black}
              placeholder={' Enter Rent Amount'}
              marginleft={-10}
              keyboard={'numeric'}
              maxlength={20}
              placeholdercolor={Colors.pinkishGrey}
              onChangeText={(text) => { this.setState({ amount: text }) }}
              value={this.state.amount}
              leftimage={Images.dallar}
            />

            <Picker
              labels={'Number of Rooms'}
              // width={'30%'}
              labelfontfamily={Fonts.segoeui_bold}
              labelsize={16}
              backgroundcolor={'white'}
              color={Colors.black}
              leftviewWidth={'100%'}
              leftimage={Images.drop_arrow}
              placeholder={'Select Number of Rooms'}
              selectedPickerValue={(val) => this.setState({ rooms: val })}
              value={String( this.state.rooms)}
            />

            <Picker
              labels={'Number of Bathrooms'}
              // width={'30%'}
              labelfontfamily={Fonts.segoeui_bold}
              labelsize={16}
              backgroundcolor={'white'}
              color={Colors.black}
              leftviewWidth={'100%'}
              leftimage={Images.drop_arrow}
              placeholder={'Select Number of Bathrooms'}
              selectedPickerValue={(val) => this.setState({ bathroom: val })}
              value={String(this.state.bathroom)}
            />
              <Picker
                labels={'Number of Balcony'}
                labelfontfamily={Fonts.segoeui_bold}
                labelsize={16}
                backgroundcolor={'white'}
                color={Colors.black}
                leftviewWidth={'100%'}
                leftimage={Images.drop_arrow}
              placeholder={'Select Number of Balcony'}
              selectedPickerValue={(val) => this.setState({ balcony: val })}
              value={String(this.state.balcony)}
              />
              <TwoPicker
                labels={'Floor'}
                fontSize={16}
                backgroundcolor={'white'}
                color={Colors.blackThree}
                fontfamily={Fonts.segoeui_bold}
                leftviewWidth={'40%'}
                rightviwewidth={'40%'}
                leftimage={Images.mobile}
                rightimage={Images.arrow}
              placeholderselectfloor={'Select Floor'}
              selectedFloorValue={(val) => this.setState({ selectFloor: val })}
              selectedTotalFloorValue={(val) => this.setState({ totalFloor: val })}
              selectfloor={String(this.state.selectFloor)}
              totalfloor={String(this.state.totalFloor)}
              />
            <TwoCircleButton
              leftnavigation={() => this.leftNavigation()}
              rightimage={Images.arrow_left_green}
              leftimage={Images.signup_arrow}
              rightnavigation={() => this.rightnavihation()}
            />
          </View>
        </KeyboardAwareScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  stepGreenLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.darkSeafoamGreen,
    margin: 1,
  },
  stepRedLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.cherryRed,
    margin: 1,
  },
  flatListView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.lightMint10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatListCheckedView: {
    width: '90%',
    borderColor: Colors.lightMint,
    borderWidth: 1,
    margin: 10,
    backgroundColor: Colors.darkSeafoamGreen,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 92,
  },
  flatImage: {
    width: 53,
    height: 48,
    margin: 5,
  },
  verticleLine: {
    height: '80%',
    margin: 5,
    width: 1,
    backgroundColor: Colors.whiteFive,
  },
});
