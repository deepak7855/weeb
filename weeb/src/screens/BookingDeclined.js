import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList ,DeviceEventEmitter,RefreshControl,Modal} from 'react-native'
import BookingRequestViews from '../Components/BookingRequestViews';
import Colors from '../Lib/Colors'
import images from '../Lib/Images';
import Fonts from '../Lib/Fonts'
import { handleNavigation } from '../navigation/routes';
import {Helper, Network, AlertMsg, CallTenant} from '../Lib/index';
import { ApiCall, LoaderForList } from '../Api/index';
import { Constant } from '../Lib/Constant';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import ModalView from '../Components/ModalView';
import OpenDocument from '../Components/OpenDocument';
let i  = 0;
export default class BookingDeclined extends Component {
  constructor(props) {
    super(props);
    this.state = {
      declinedListItem: '',
      declinedListItemOwner: '',
      userType: Helper.userType,
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      front_id: '',
      back_id: '',
      modalVisibleDocument:false,
    };
  }
  componentDidMount() {
    this.listner = DeviceEventEmitter.addListener('Call-Reject-Api', (data) => {
      this.getTenantBookingRequestData();
    });
    // this.lisener = DeviceEventEmitter.addListener(Constant.USER_TYPE, (data) => {
    //     console.log('device event in booking request', data?.type)
    //     this.setState({
    //         userType: data?.type ? data?.type : Helper.userType
    //     })

    //     // this.setState({ userType: data?.type }, () => {
    //     //   this.getTenantBookingRequestData();
    //     // })
    // })
    this.setState({
      userType: Helper.userType,
    },()=>{
      if(i == 0){
        this.getTenantBookingRequestData();
      }
      
    });
    
  }

  componentWillUnmount() {
    if (this.lisener) {
      this.lisener.remove();
    }
  }

  getTenantBookingRequestData = (already) => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (!this.state.refreshing) {
          this.setState({isLoading: true});
        }
        let url;
        const data = {
          status: 3,
        };
        if (this.state.userType == 'OWNER') {
          url = 'get-bookings-by-owner' + '?page=' + this.state.currentPage;
        } else {
          url = 'get-booking-by-tenant' + '?page=' + this.state.currentPage;
        }
        // Helper.mainApp.showLoader()
        console.log('total render--------in booking declined',i++)
        ApiCall.ApiMethod({Url: url, method: 'POST', data: data})
          .then((res) => {
            Helper.mainApp.hideLoader();
            if (res?.status) {
              // console.log('booking request data rejected', res);
              if (this.state.userType == 'OWNER') {
                if (res?.data?.data && res?.data?.data.length > 0) {
                 // console.log('booking request  data  owner rejected', res);
                  if (already) {
                    this.setState({
                      declinedListItemOwner: '',
                    });
                  }
                  this.setState({
                    declinedListItemOwner: [
                      ...this.state.declinedListItemOwner,
                      ...res?.data?.data,
                    ],
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                  });
                  return;
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                  });
                  return;
                }
              } else {
                if (res?.data?.data && res?.data?.data.length > 0) {
                  // console.log(
                  //   'booking request  data  tenat rejected',
                  //   res?.data,
                  // );
                  if (already) {
                    this.setState({
                      declinedListItem: '',
                    });
                  }
                  this.setState({
                    declinedListItem: [
                      ...this.state.declinedListItem,
                      ...res?.data?.data,
                    ],
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                  });
                  return;
                } else {
                  this.setState({
                    next_page_url: res?.data?.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    currentPage: 1,
                  });
                  return;
                }
              }
            } else {
              this.setState({
                ext_page_url: res?.data?.next_page_url,
                isLoading: false,
                refreshing: false,
              });
              //console.log('tenant fail data', res);
              return;
            }
          })
          .catch((err) => {
            Helper.mainApp.hideLoader();
            //console.log('api fail err', err);
          });
      }
    });
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: this.state.currentPage, refreshing: false},
        () => {
          this.getTenantBookingRequestData(true);
        },
      );
    }, 2000);
  };

  onScroll = () => {
    // console.log(
    //   'next page url',
    //   this.state.next_page_url,
    //   this.state.isLoading,
    // );
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getTenantBookingRequestData({loader: true});
      });
    }
  };
  onChats = (item) => {
    //console.log(item?.property?.user?.id, 'item');
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {
        userId:
          this.state.userType == 'OWNER'
            ? item?.user?.id
            : item?.property?.user?.id,
      },
      navigation: this.props.navigation,
    });
    // handleNavigation({ type: 'push', page: 'Chat', navigation: this.props.navigation });
  };

  onCall = (item) => {
    //  console.log('call item', item?.user?.mobile_number);
    CallTenant(item?.user?.mobile_number);
  };
 onDetails = (index) => {
    if (this.state.userType == 'OWNER') {
      //console.log('-------owner data',this.state.declinedListItem[index])
      handleNavigation({
        type: 'push',
        page: 'PropertyDetails',
        passProps: {bookingID: this.state.declinedListItemOwner[index]?.id},
        navigation: this.props.navigation,
      });
    } else {
     // console.log('-------data',this.state.declinedListItem[index])
      handleNavigation({
        type: 'push',
        page: 'PropertyDetails',
        passProps: {propertyID: this.state.declinedListItem[index]?.property?.id},
        navigation: this.props.navigation,
      });
    }
  };
  renderRequestList = ({item, index}) => {
    return (
      <BookingRequestViews
        item={item}
        onChat={() => {
          this.onChats(item);
        }}
        title={'View Details'}
        //titleOwner={'View Document'}
        onCall={() => {
          this.onCall(item);
        }}
        // titleOwner={'Decline'}
       // AgreementBtt
        OwnerTypeButton
        onClickOwner={() => {}}
        subTitle={'Declined'}
        onClick1={() => {
          this.onDetails(index);
        }}
        RejectProperty={() => {
          // this.rejectProperty(index);
        }}
        //subTitleOwner={'Add Agreement'}
        onClickOwner1={() => {}}
        onClickAgreement={() => {
          this.setState(
            {
              //modalVisibleDocument: true,
              front_id: item?.tenantdoc[0]?.front_image_url,
              back_id: item?.tenantdoc[0]?.back_image_url,
            },
            () => {
              // console.log(
              //   'tenant doc',
              //   this.state.front_id,
              //   this.state.back_id,
              //   item?.tenantdoc,
              // );
              this.state.front_id && this.state.back_id
                ? this.setState({
                    modalVisibleDocument: true,
                  })
                : Helper.showToast('Documents not uploaded');
            },
          );
        }}
      />
    );
  };

  onDocumentModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleDocument}
        onRequestClose={() => {
          this.setState({modalVisibleDocument: false});
        }}>
        <OpenDocument
          maodalHeid={() => {
            this.setState({modalVisibleDocument: false});
          }}
          front_id={this.state.front_id}
          back_id={this.state.back_id}
        />
        <StatusBarCustom backgroundColor={Colors.white} translucent={false} />
      </Modal>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          bounces={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          //contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}>
          <FlatList
            style={{marginTop: 20}}
            data={
              this.state.userType == 'OWNER'
                ? this.state.declinedListItemOwner
                : this.state.declinedListItem
            }
            renderItem={this.renderRequestList}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false
                    ? null
                    : 'No declined property.'}
                </Text>
              </View>
            )}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
          />
          {this.onDocumentModal()}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: Colors.whiteTwo },
})