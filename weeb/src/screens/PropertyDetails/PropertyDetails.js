import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, Platform } from 'react-native'
import Colors from '../../Lib/Colors'
import Swiper from 'react-native-swiper'
import images from '../../Lib/Images'
import Fonts from '../../Lib/Fonts'
import Properydescription from './Components/Properydescription'
import AmenitiesList from '../../Components/AmenitiesList'
import HouseRoules from './Components/HouseRoules'
import { handleNavigation } from '../../navigation/routes'
import StatusBarCustom from '../../Components/Common/StatusBarCustom'
import {Helper, Network, AlertMsg, CallTenant} from '../../Lib/index';
import { ApiCall } from '../../Api/index';
import moment from 'moment';
let amenitiesArr = [];
let furnitureArr = [];
import { ProgressiveImage } from '../../Components/Common/index';
import { translate } from '../../Language'
export default class PropertyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amenitiesData: '',
      furnishingData: '',
      propertyId: props.route.params?.propertyID,
      propertyDetailsData: '',
      bookingID: props.route.params?.bookingID,
      isFavorite:0
    };
  }
  componentDidMount() {
    //  console.log('property id', Helper.userData);
    this.getPropertyDetails()
  }

  getPropertyDetails = () =>{
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          if (this.state.bookingID || this.props.route.params?.bookingID) {
            Helper.mainApp.showLoader();
            const data = {
              booking_id: this.props.route.params?.bookingID,
            };

            ApiCall.ApiMethod({
              Url: 'get-booking-by-id',
              method: 'POST',
              data: data,
            }).then((res) => {
              Helper.mainApp.hideLoader();
               //console.log('property details for reqesut owner', res?.data)
              if (res?.status) {
                if (res?.data?.propertyamenities) {
                  res?.data?.propertyamenities.forEach((ele) => {
                    amenitiesArr.push({
                      socialIcon: ele?.amenity?.imgurl,
                      socialText: ele?.amenity?.title,
                    });
                  });
                }
                if (res?.data?.propertyfurnishings) {
                  res?.data?.propertyfurnishings.forEach((ele) => {
                    furnitureArr.push({
                      socialIcon: ele?.furnishing?.imgurl,
                      socialText: ele?.furnishing?.title,
                    });
                  });
                }
                this.setState({
                  propertyDetailsData: res?.data,
                  // amenitiesData: amenitiesArr,
                  // furnishingData: furnitureArr
                });
              }
            });
          } else {
            // let propertyId = new FormData();
            // propertyId.append('property_id', this.state.propertyId);
            Helper.mainApp.showLoader();
            ApiCall.ApiMethod({
              Url: 'get-property-by-id',
              method: 'POST',
              data:{'property_id':this.state.propertyId},
            }).then((res) => {
              Helper.mainApp.hideLoader();
              
               // console.log('property details for teant', res?.data?.is_favorite);
              amenitiesArr = [];
              furnitureArr = [];
              if (res?.status) {
                if (res?.data?.propertyamenities) {
                  res?.data?.propertyamenities.forEach((ele) => {
                    amenitiesArr.push({
                      socialIcon: ele?.amenity?.imgurl,
                      socialText: ele?.amenity?.title,
                    });
                  });
                }
                if (res?.data?.propertyfurnishings) {
                  res?.data?.propertyfurnishings.forEach((ele) => {
                    furnitureArr.push({
                      socialIcon: ele?.furnishing?.imgurl,
                      socialText: ele?.furnishing?.title,
                    });
                  });
                }

                this.setState({
                  propertyDetailsData: res?.data,
                  amenitiesData: amenitiesArr,
                  furnishingData: furnitureArr,
                  isFavorite:res?.data?.is_favorite,
                });
              }
            });
          }
        }
      })
      .catch((err) => {
        console.log('err', err);
      });
  }
  onChateClick = () => {
    handleNavigation({
      type: 'push',
      page: 'Chat',
      passProps: {userId: this.state.propertyDetailsData?.user?.id},
      navigation: this.props.navigation,
    });
  };

  AmenitiesListing = ({item, index}) => {
    // console.log('item',item);
    return (
      <AmenitiesList
        socialIcon={this.props.socialIcon}
        socialText={this.props.socialText}
        items={item}
        index={index}
        disabled={true}
      />
    );
  };

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  Furnishing = ({item}) => {
    return (
      <AmenitiesList
        socialIcon={this.props.socialIcon}
        socialText={this.props.socialText}
        items={item}
        disabled={true}
      />
    );
  };

  requestToApplay = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        const data = {
          property_id: String(this.state.propertyId),
          dated: moment(new Date()).format('YYYY-MM-DD'),
          description: this.state.propertyDetailsData?.description,
          rent: String(this.state.propertyDetailsData?.rent),
          deposite_amount: String(
            this.state.propertyDetailsData?.deposite_amount,
          ),
        };
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({Url: 'save-booking', method: 'POST', data: data})
          .then((res) => {
            Helper.mainApp.hideLoader();
            console.log('request to applay', res);

            if (res?.status) {
              Helper.showToast('Requested Successfully');
              this.goBack();
            } else {
              Helper.showToast(res?.message);
            }
          })
          .catch(() => {
            Helper.mainApp.hideLoader();
          });
      }
    });
  };

  onCall = () => {
     // console.log('call item', this.state.propertyDetailsData?.user?.mobile_number);
    CallTenant(this.state.propertyDetailsData?.user?.mobile_number);
  };

  likeProperty = (status) =>{
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        const data = {
          property_id: this.state.propertyId,
          is_favorite:status,
        };
       Helper.mainApp.showLoader();
       console.log('data',data)
        ApiCall.ApiMethod({Url: 'update-property-favorite', method: 'POST', data: data})
          .then((res) => {
            Helper.mainApp.hideLoader();
            console.log('request to applay', res);
            if (res?.status) {
             this.getPropertyDetails();
             return true
            } else {
              Helper.showToast(res?.message);
            }
          })
          .catch(() => {
            Helper.mainApp.hideLoader();
          });
      }
    });
  }
  render() {
    return (
      <View style={styles.containView}>
        <StatusBarCustom backgroundColor={'transparent'} translucent={true} />
        <View style={{height: '40%'}}>
          {this.state.propertyDetailsData?.propertyphotos ? (
            <Swiper
              activeDotColor={Colors.whiteTwo}
              dotColor={Colors.pinkishGreyTwo}
              dotStyle={{height: 5, width: 5}}
              activeDotStyle={{height: 5, width: 5}}>
              {this.state.propertyDetailsData?.propertyphotos.map((item) => {
                console.log('property photos', item?.imgurl);
                return (
                  <ProgressiveImage
                    source={{uri: item?.imgurl}}
                    style={{
                      height: 400,
                      width: '100%',
                      resizeMode: 'cover',
                    }}
                    resizeMode="cover"
                  />
                  // <Image style={{ height: 400, width: '100%', resizeMode: 'cover', }} source={{ uri: item?.imgurl}} />
                );
              })}
            </Swiper>
          ) : null}

          <TouchableOpacity
            hitSlop={{height: 15, width: 15, left: 10, right: 10}}
            onPress={() => {
              this.goBack();
            }}
            style={styles.btnViewback}>
            <Image
              style={[styles.backBtn, {marginTop: Helper.hasNotch ? 10 : 10}]}
              source={images.black_arrow_btn}
            />
          </TouchableOpacity>
          {Helper.userType == 'OWNER' ? null : (
            <TouchableOpacity
             hitSlop={{height: 300, width: 300, left: 300, right: 300}}
            onPress={() => {
              this.state.isFavorite?
              this.likeProperty(0)
              :
              this.likeProperty(1)
              
            }}
            style={styles.view_LikeBtn}>
              <Image style={this.state.isFavorite?{width:30,height: 30,bottom:20,right:15,tintColor:Colors.cherryRed}:{width:30,height: 30,bottom:20,right:15,tintColor:Colors.cherryRed}} source={this.state.isFavorite? images.favorite:images.likeblank} resizeMode={'contain'} />
            </TouchableOpacity>
          )}
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 17,
              marginTop: 10,
            }}>
            <View style={styles.countFlex}>
              <View style={[styles.copyView, {marginBottom: 5}]}>
                <Image
                  style={styles.bathroomImg}
                  source={images.bedrooms_img}
                />
                <Text style={styles.countText}>
                  {this.props.route.params?.bookingID
                    ? this.state.propertyDetailsData?.property?.rooms
                    : this.state.propertyDetailsData?.rooms}
                </Text>
              </View>
              <Text style={styles.flexText}>{translate("Bedrooms")}</Text>
            </View>
            <View style={styles.countFlex}>
              <View style={[styles.copyView, {marginBottom: 5}]}>
                <Image
                  style={styles.bathroomImg}
                  source={images.bathrooms_img}
                />
                <Text style={styles.countText}>
                  {this.props.route.params?.bookingID
                    ? this.state.propertyDetailsData?.property?.bathrooms
                    : this.state.propertyDetailsData?.bathrooms}
                </Text>
              </View>
              <Text style={styles.flexText}>{translate("Bathrooms")}</Text>
            </View>
            <View style={styles.countFlex}>
              <View style={[styles.copyView, {marginBottom: 5}]}>
                <Image style={styles.bathroomImg} source={images.balcony} />
                <Text style={styles.countText}>
                  {this.props.route.params?.bookingID
                    ? this.state.propertyDetailsData?.property?.balcony
                    : this.state.propertyDetailsData?.floor}
                </Text>
              </View>
              <Text style={styles.flexText}>{translate("Balcony")}</Text>
            </View>
          </View>
          <View style={styles.borderView} />
          {Helper.userType == 'OWNER' ? null : (
            <View style={styles.contactView}>
              <View style={styles.copyView}>
                <ProgressiveImage
                  source={{
                    uri: this.state.propertyDetailsData?.user?.profile_picture,
                  }}
                  style={styles.ownerImg}
                  resizeMode="cover"
                />
                {/* <Image style={styles.ownerImg} source={images.property_owner2} /> */}
                <View style={{marginLeft: 10}}>
                  <Text numberOfLines={1} style={styles.ownerName}>
                    {this.state.propertyDetailsData?.user?.name}
                  </Text>
                  <View style={styles.copyView}>
                    <Image
                      style={styles.locationIcon}
                      source={images.location_pin}
                    />
                    <Text numberOfLines={1} style={styles.placeText}>
                      {this.state.propertyDetailsData?.user?.city}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginRight: 5}}>
                <TouchableOpacity
                  onPress={() => {
                    this.onChateClick();
                  }}>
                  <Image
                    style={[styles.socialIcon, {marginRight: 10}]}
                    source={images.chat_circle_btn}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.onCall();
                  }}>
                  <Image
                    style={styles.socialIcon}
                    source={images.call_circle_btn}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}

          <View style={styles.borderView} />
          <View>
            <Properydescription
              rent={this.state.propertyDetailsData?.rent}
              type={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.type
                  : this.state.propertyDetailsData?.type
              }
              date={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.available_date
                  : this.state.propertyDetailsData?.available_date
              }
              address={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.location
                  : this.state.propertyDetailsData?.location
              }
              title={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.title
                  : this.state.propertyDetailsData?.title
              }
              discription={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.description
                  : this.state.propertyDetailsData?.description
              }
              floor={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.floor
                  : this.state.propertyDetailsData?.floor
              }
              totalfloor={
                this.props.route.params?.bookingID
                  ? this.state.propertyDetailsData?.property?.total_floor
                  : this.state.propertyDetailsData?.total_floor
              }
            />
          </View>
          {this.props.route.params?.bookingID ? null : (
            <View>
              <Text style={styles.headerText}>{translate("AMENITIES")}</Text>
              <View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.amenitiesData}
                  renderItem={this.AmenitiesListing}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>
          )}

          {this.props.route.params?.bookingID ? null : (
            <View>
              <Text style={styles.headerText}>{translate("FURNISHING")}</Text>
              <View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.furnishingData}
                  renderItem={this.Furnishing}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>
          )}

          <View>
            <HouseRoules
              houserule={this.state.propertyDetailsData?.home_rules}
              othercharge={this.state.propertyDetailsData?.other_charges}
            />
          </View>
          {Helper.userType == 'OWNER' || this.state.bookingID ? null : this
              .state.propertyDetailsData?.request_status == 0 ? (
            <TouchableOpacity
              style={styles.btnView}
              onPress={() => {
                this.requestToApplay();
              }}>
              <View
                style={{
                  backgroundColor: Colors.whiteFive,
                  height: 1,
                }}></View>
              <Text style={styles.btnText}>{translate("RequesttoApply")}</Text>
            </TouchableOpacity>
          ) : this.state.propertyDetailsData?.request_status == 1 ? (
            <TouchableOpacity style={styles.btnView} disabled={true}>
              <View
                style={{
                  backgroundColor: Colors.whiteFive,
                  height: 1,
                }}></View>
              <Text style={styles.btnText}>{translate("Requested")}</Text>
            </TouchableOpacity>
          ) : this.state.propertyDetailsData?.request_status == 2 ? (
            <TouchableOpacity style={styles.btnView} disabled={true}>
              <View
                style={{
                  backgroundColor: Colors.whiteFive,
                  height: 1,
                }}></View>
              <Text style={styles.btnText}>{translate("Accepted")}</Text>
            </TouchableOpacity>
          ) : this.state.propertyDetailsData?.request_status == 3 ? (
            <TouchableOpacity
              style={styles.btnView}
              onPress={() => {
                this.requestToApplay();
              }}>
              <View
                style={{
                  backgroundColor: Colors.whiteFive,
                  height: 1,
                }}></View>
              <Text style={styles.btnText}>{translate("RequesttoApply")}</Text>
            </TouchableOpacity>
          ) : this.state.propertyDetailsData?.request_status == 4 ? (
            <TouchableOpacity
              style={styles.btnView}
              onPress={() => {
                this.requestToApplay();
              }}>
              <View
                style={{
                  backgroundColor: Colors.whiteFive,
                  height: 1,
                }}></View>
              <Text style={styles.btnText}>{translate("RequesttoApply")}</Text>
            </TouchableOpacity>
          ) : null}
        </ScrollView>
      </View>
      // </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
    containView: {
        flex: 1,
        backgroundColor: Colors.whiteTwo,
        marginBottom: Helper.hasNotch ? 15 : 0
    },
    view_LikeBtn: { position: 'absolute',bottom:-25,right:10  },
    likeIcon: { height: 30, width: 30, resizeMode: 'contain' ,tintColor:Colors.cherryRed},
    bathroomImg: { height: 24, width: 30, resizeMode: 'contain' },
    countText: { fontSize: 30, fontFamily: Fonts.segoeui, color: Colors.blackThree, left: 6 },
    countFlex: { flex: 0.33, },
    flexText: { fontSize: 10, fontFamily: Fonts.segoeui, color: Colors.pinkishGrey, top: -10 },
    borderView: { height: 0.5, backgroundColor: Colors.warmGrey, marginTop: 3, opacity: 0.5, marginHorizontal: 10 },
    contactView: { marginHorizontal: 13, flexDirection: 'row', alignItems: 'center', paddingVertical: 10 },
    copyView: { flex: 1, flexDirection: 'row', alignItems: 'center' },
    ownerImg: { height: 30, width: 30, resizeMode: 'contain', borderRadius: 100 },
    ownerName: { fontSize: 14, fontFamily: Fonts.segui_semiBold, lineHeight: 22 },
    locationIcon: { height: 7, width: 5, resizeMode: 'contain' },
    placeText: { fontSize: 10, fontFamily: Fonts.segoeui, color: Colors.brownishGrey, left: 5, lineHeight: 16 },
    socialIcon: { height: 30, width: 30, resizeMode: 'contain' },
    headerText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.pinkishGrey, marginTop: 26, marginHorizontal: 12 },
    btnView: {
        marginBottom: Helper.hasNotch ? 7 : 0,
        marginTop: 30, shadowOffset: {
            width: 0, height: 3,
        }, shadowOpacity: 0.4, shadowRadius: 4.65, elevation: 6
    },
    btnText: { textAlign: 'center', paddingVertical: 10, fontSize: 18, fontFamily: Fonts.segoeui, color: Colors.darkSeafoamGreen },
    backBtn: { height: 20, width: 20, resizeMode: 'contain', tintColor: Colors.whiteTwo },
    btnViewback: { position: 'absolute', top: Helper.hasNotch ? 35 : 25, marginHorizontal: 10, }
})
