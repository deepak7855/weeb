import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import Colors from '../../../Lib/Colors'
import Fonts from '../../../Lib/Fonts'
import images from '../../../Lib/Images'
import moment from 'moment';
import { translate } from '../../../Language'

export default class Properydescription extends Component {
    render() {
        console.log('date', this.props.date)
        return (
            <View>
                <View style={styles.rateView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Text style={styles.dolorText}>KWD {this.props.rent}</Text>
                        <Text style={styles.monthText}>/ {translate("month")}</Text>
                    </View>
                    <View style={styles.btnView}>
                        <Text numberOfLines={1} style={styles.btnText}>{this.props.type}</Text>
                    </View>
                </View>
                <View style={{ marginTop: 19, marginHorizontal: 12 }}>
                    <Text style={styles.immmediteBtn}>{this.props.date ? moment(this.props.date).format('DD/MM/YYYY') : translate("Immediately")}</Text>
                </View>
                <View style={{ marginHorizontal: 12, marginTop: 22 }}>
                    <Text numberOfLines={2} style={styles.disrptionText}>{this.props.title}</Text>
                </View>
                <View style={styles.addressView}>
                    <Image style={styles.locationIcon} source={images.location} />
                    <Text style={styles.addressText}>{this.props.address}</Text>
                </View>
                <View style={{ marginHorizontal: 12, marginTop: 20 }}>
                    <Text style={styles.longText}>{this.props.discription}
                        {/* <Text style={{ color: Colors.darkSeafoamGreen }}>See More</Text> */}
                    </Text>
                </View>
                <View style={{ marginHorizontal: 12, marginTop: 28 }}>
                    <Text numberOfLines={1} style={styles.florText}>{translate("FLOORDETAILS")}</Text>
                    <Text style={{ fontSize: 15, fontFamily: Fonts.segoeui }}>{this.props.floor} {translate("FloorOutof")} {this.props.totalfloor}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rateView: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 12, marginTop: 15 },
    dolorText: { fontSize: 26, fontFamily: Fonts.segui_semiBold },
    monthText: { fontSize: 14, fontFamily: Fonts.segoeui, left: 5 },
    btnText: { fontSize: 15, fontFamily: Fonts.segui_semiBold, color: Colors.cherryRed, },
    btnView: { paddingHorizontal: 20, paddingVertical: 2, borderWidth: 1, borderColor: Colors.whiteFive, borderRadius: 2 },
    disrptionText: { fontSize: 18, fontFamily: Fonts.segui_semiBold, width: '85%', },
    immmediteBtn: { fontSize: 14, fontFamily: Fonts.segoeui, backgroundColor: Colors.dustyOrange, width: '35%', textAlign: 'center', paddingVertical: 5, color: Colors.whiteTwo, borderRadius: 3 },
    addressView: { marginHorizontal: 12, marginTop: 10, flexDirection: 'row', alignItems: 'center' },
    locationIcon: { height: 11, width: 11, resizeMode: 'contain' },
    addressText: { width: '95%', fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.brownishGreyTwo, left: 5 },
    longText: { fontSize: 15, fontFamily: Fonts.segoeui, lineHeight: 21 },
    florText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.pinkishGrey }
})