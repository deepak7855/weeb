import { fromPairs } from 'lodash'
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Colors from '../../../Lib/Colors'
import Fonts from '../../../Lib/Fonts'
import { translate } from '../../../Language'

export default class HouseRoules extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        let check = this.props.othercharge ? this.props.othercharge.split(',') : ''
        return (
            <View style={{ marginHorizontal: 12 }}>
                {this.props.othercharge
                    ?
                    <View>
                        <Text style={[styles.otherText, { marginTop: 30, }]} numberOfLines={1}>{translate("OTHERCHARGES")}</Text>
                        <View style={{ marginTop: 18 }}>
                            <View style={styles.positionView}>
                                <View style={styles.pointView} />
                                <Text style={styles.roulsText} numberOfLines={1}>{check[0] ? translate("ElectricityCharge") : ''}</Text>
                            </View>
                            {check[1] ?
                                <View style={[styles.positionView, { marginTop: 5 }]}>
                                    <View style={styles.pointView} />
                                    <Text style={styles.roulsText} numberOfLines={1}>{check[1] ? translate("MaintenanceCharge") : ''}</Text>
                                </View>
                                :
                                null
                            }

                        </View>
                    </View>
                    :
                    null
                }

                {this.props.houseRule ?
                    <View>
                        <Text style={[styles.otherText, { marginTop: 25, }]} numberOfLines={1}>{translate("HOUSEROULES")}</Text>
                        <View style={[styles.positionView,]}>
                            <View style={styles.pointView} />
                            <Text style={[styles.roulsText, { marginTop: 15 }]} numberOfLines={2}>{this.props.houseRule}</Text>
                        </View>
                        {/* <View style={[styles.positionView,]}>
                            <View style={styles.pointView} />
                            <Text style={[styles.roulsText, { marginTop: 15 }]} numberOfLines={2}>It is a long established fact that a reader will be distracted by the readable .</Text>
                        </View>
                        <View style={[styles.positionView,]}>
                            <View style={styles.pointView} />
                            <Text style={[styles.roulsText, { marginTop: 15 }]} numberOfLines={2}>It is a long established fact that a reader will be distracted by the readable .</Text>
                        </View> */}
                    </View>
                    :
                    null
                }

            </View>
        )
    }
}
const styles = StyleSheet.create({
    roulsText: { fontSize: 14, fontFamily: Fonts.segoeui, marginLeft: 10, width: '80%', },
    otherText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.pinkishGrey, marginTop: 30, width: '35%', },
    positionView: { flexDirection: 'row', alignItems: 'center', },
    pointView: { height: 5, width: 5, borderRadius: 10, backgroundColor: Colors.black }
})