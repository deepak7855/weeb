import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  DeviceEventEmitter,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import {handleNavigation} from '../navigation/routes';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, ApiCallForProperty, LoaderForList} from '../Api/index';
import {ProgressiveImage} from '../Components/Common/index';
import { translate } from '../Language';
export default class ActiveProperty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataOfAreement: [
        {
          id: 1,
          icon: images.propert_requested,
          Title: 'Lighthouse Excursions London ...',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 1,
          route: 'Maintenance_16_2',
        },
        {
          id: 2,
          icon: images.propert_requested_t,
          Title: 'Royal Treasure',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 0,
          route: '',
        },
        {
          id: 3,
          icon: images.propert_requested3,
          Title: 'Star Green Zenith',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 0,
          route: '',
        },
        {
          id: 4,
          icon: images.propert_requested,
          Title: 'Lighthouse Excursions London ...',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 1,
          route: '',
        },
        {
          id: 5,
          icon: images.propert_requested,
          Title: 'Star Green Zenith',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 1,
          route: '',
        },
        {
          id: 6,
          icon: images.propert_requested_t,
          Title: 'Star Green Zenith',
          address: '16545 Et. Solve Ave, Suite 54564, London...',
          rate: '$250',
          rateType: '/m',
          propertyType: 'Apartment',
          request: 'Requests : 150',
          incomplete: 1,
          route: '',
        },
      ],
      activeProperty: '',
      address: '',
      lat: '',
      long: '',
      modalVisible: false,
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
    };
  }

  componentDidMount() {
    this.listner = DeviceEventEmitter.addListener(
      'owner-property-location',
      (data) => {
      //  console.log('device event', data);
        this.setState(
          {
            lat: data?.lat,
            long: data?.long,
            currentPage: 1,
          },
          () => {
            this.getInactiveData(true);
          },
        );
      },
    );

    this.listner2 = DeviceEventEmitter.addListener(
      'Listed-Property-Filter',
      (data) => {
       // console.log('device event', data);
        this.setState(
          {
            lat: data?.lat,
            long: data?.long,
            currentPage: 1,
          },
          () => {
            if (data?.type) {
              this.getInactiveData(true, data?.type);
            } else {
              this.getInactiveData(true, '');
            }
          },
        );
      },
    );

    this.listner3 = DeviceEventEmitter.addListener(
      'Call-Active-Api',
      (data) => {
       // console.log('device event', data);
        this.setState(
          {
            lat: data?.lat,
            long: data?.long,
            currentPage: 1,
          },
          () => {
            this.getInactiveData(true, '');
          },
        );
      },
    );
    // alert(this.props.route.params?.text);
    this.getInactiveData();
  }
  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
      this.listner2.remove();
      this.listner3.remove();
    }
  }
  getInactiveData = async (already, type) => {
    this.setState({
      isLoading: true,
    });
    const requestData = await ApiCallForProperty.getPropertyList({
      status: 1,
      currentPage: this.state.currentPage,
      lat: this.state.lat,
      long: this.state.long,
      type: type,
    });
    //console.log('request data acive screen', requestData?.data?.data);
    if (requestData?.status) {
      if (already) {
        this.setState({
          activeProperty: '',
        });
      }
      if (requestData?.data?.data && requestData?.data?.data.length > 0) {
        this.setState({
          activeProperty: this.state.activeProperty
            ? [...this.state.activeProperty, ...requestData?.data?.data]
            : requestData?.data?.data,
          isLoading: false,
          next_page_url: requestData?.data?.next_page_url,
          refreshing: false,
          emptymessage: false,
        });
      } else {
        this.setState({
          isLoading: false,
          next_page_url: requestData?.data?.next_page_url,
          currentPage: 1,
          refreshing: false,
          emptymessage: true,
        });
      }
    } else {
      this.setState({
        isLoading: false,
        next_page_url: requestData?.data?.next_page_url,
        currentPage: 1,
        refreshing: false,
        emptymessage: true,
      });
    }
  };
  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: 1, refreshing: false, lat: '', long: '', address: ''},
        () => {
          this.getInactiveData(true);
          //this.getTenantBookingRequestData({ loader: true });
        },
      );
    }, 2000);
  };

  onScroll = () => {
    // console.log(
    //   'next page url',
    //   this.state.next_page_url,
    //   this.state.isLoading,
    // );
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState({currentPage: this.state.currentPage + 1}, () => {
        this.getInactiveData();
        //  this.getTenantBookingRequestData({ loader: true });
      });
    }
  };

  onProperty = (index) => {
    Helper.propertyData = this.state.activeProperty[index];
    handleNavigation({
      type: 'pushTo',
      page: 'SelectPropertyType',
      passProps: {title: 'Edit Property'},
      navigation: this.props.navigation,
    });
  };

  deleteProperty = async (index) => {
    const id = this.state.activeProperty[index]?.id;
    let deleteData = await ApiCallForProperty.deleteProperty(id);
  //  console.log('delete data', deleteData);
    if (deleteData.status) {
      this.setState({currentPage: 1, refreshing: false}, () => {
        this.getInactiveData(true);
        //this.getTenantBookingRequestData({ loader: true  });
      });
    }
  };
  InActiveProperty = async (index) => {
    //console.log('id for inactive', this.state.activeProperty[index]);
    const id = this.state.activeProperty[index]?.id;
    let activeData = await ApiCallForProperty.ActiveAndDeactiveProperty(id, 2);
   // console.log('inactive  data', activeData);
    if (activeData || activeData?.status) {
      this.setState({currentPage: 1, refreshing: false}, () => {
        this.getInactiveData(true);
        DeviceEventEmitter.emit('Call-Active-Api', {type: 'Call-Active-Api'});
        //this.getTenantBookingRequestData({ loader: true  });
      });
    }
  };
  goDetails = (index) => {
    handleNavigation({
      type: 'pushTo',
      page: 'PropertyDetails',
      passProps: {propertyID: this.state.activeProperty[index]?.id},
      navigation: this.props.navigation,
    });
  };

  toSelectSpecializatio = (index) => {
    let NewVar = [...this.state.dataOfFilter];
    NewVar[index].selected = !NewVar[index].selected;
    this.setState({dataOfFilter: NewVar});
  };

  onAddProperty = () => {
    Helper.propertyData = {};
    handleNavigation({
      type: 'pushTo',
      page: 'SelectPropertyType',
      navigation: this.props.navigation,
    });
  };

  // goDetails=()=>{
  //     handleNavigation({ type: 'pushTo', page: 'PropertyDetails', navigation: this.props.navigation });
  // }

  renderOfProperty = ({item, index}) => {
    //console.log('active poperty', item)
    return (
      <TouchableOpacity
        onPress={() => {
          this.goDetails(index);
        }}>
        <View style={styles.view1}>
          <ProgressiveImage
            source={{
              uri: item?.propertyphotos ? item?.propertyphotos[0]?.imgurl : '',
            }}
            style={styles.iconHome}
            resizeMode="cover"
          />
          {/* <Image style={styles.iconHome} source={item.icon} /> */}
          <View style={{marginLeft: 13}}>
            <Text style={styles.textTitle}>{item?.title}</Text>
            <View style={styles.view3}>
              <Image style={styles.iconLocation} source={images.location} />
              <Text
                numberOfLines={3}
                style={[styles.textAddress, {width: '90%'}]}>
                {item?.location}
              </Text>
            </View>
            <View style={styles.view4}>
              <Text style={styles.textRate}>{`$${item?.rent}`}</Text>
              <Text style={[styles.textRate, {fontFamily: Fonts.segoeui}]}>
                {item.rateType}
              </Text>
              <View style={styles.dot} />
              <Text style={styles.textPropType}>{item?.type}</Text>
            </View>
          </View>
        </View>
        <View style={styles.ViewTouch}>
          <TouchableOpacity
            onPress={() => {
              this.onProperty(index);
            }}>
            <Image style={styles.iconEditProp} source={images.property_edit} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchMid}
            onPress={() => this.deleteProperty(index)}>
            <Image
              style={styles.iconEditProp}
              source={images.property_delete}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewInactive}
            onPress={() => this.InActiveProperty(index)}>
            <Text style={styles.textInactive}>{translate("Inactive")}</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };
  renderOfFilter = ({item, index}) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.toSelectSpecializatio(index)}
          style={styles.ViewRenderFilter}>
          <Image
            style={item.selected ? styles.checkIcon : styles.uncheckIcon}
            source={item.iconChecked}
          />
          <Text
            style={item.selected ? styles.selectedText : styles.unselectText}>
            {item.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    // console.log('recieved state', this.props.propertyListData)
    return (
      <View style={styles.container}>
        <View style={styles.viewTextTop}>
          <Text style={styles.textTop}>{translate("AllProperty")}</Text>
          <View style={styles.dotTop} />
          <Text style={styles.text180}>{this.state.activeProperty.length}</Text>
          <TouchableOpacity
            onPress={() => {
              this.onAddProperty();
            }}>
            <Image style={styles.iconAddPro} source={images.addproperty} />
          </TouchableOpacity>
        </View>
        <View style={styles.line1} />
        <View>
          <FlatList
            data={this.state.activeProperty}
            renderItem={this.renderOfProperty}
            extraData={this.state}
            ItemSeparatorComponent={() => (
              <View style={styles.seperator}></View>
            )}
            ListFooterComponent={() => {
              return this.state.isLoading ? <LoaderForList /> : null;
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
              />
            }
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false
                    ? null
                    : 'No active property.'}
                </Text>
              </View>
            )}
          />
        </View>
        {this.state.opener ? (
          <View style={styles.filteView}>
            <View style={styles.viewTopTextOfFilter}>
              <Text style={styles.textPropTypeOfFilter}>{translate("PropertyType")}</Text>
            </View>
            <FlatList
              data={this.state.dataOfFilter}
              renderItem={this.renderOfFilter}
              ItemSeparatorComponent={() => (
                <View style={styles.filterSeperator} />
              )}
            />
          </View>
        ) : null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 60},
  textIncomp: {
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.cherryRed,
  },
  textIncomp1: {
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.brownishGreyTwo,
  },
  viewTextTop: {
    paddingLeft: 12,
    paddingRight: 9,
    paddingTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchIcon: {width: 11, height: 11, resizeMode: 'contain'},
  textInputStyl: {fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40},
  iconHome: {width: 67, height: 57, resizeMode: 'contain'},
  textTitle: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.blackTwo,
    fontFamily: Fonts.segoeui_bold,
  },
  view3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 6,
  },
  iconLocation: {width: 7, height: 9, resizeMode: 'contain'},
  textAddress: {
    fontSize: 12,
    lineHeight: 16,
    color: Colors.brownishGreyTwo,
    fontFamily: Fonts.segoeui,
    marginLeft: 5,
  },
  view4: {flexDirection: 'row', alignItems: 'center'},
  textRate: {
    fontSize: 12,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.darkSeafoamGreen,
  },
  dot: {
    width: 2,
    height: 2,
    borderRadius: 1,
    backgroundColor: Colors.pinkishGrey,
    marginHorizontal: 7,
  },
  textPropType: {
    fontSize: 12,
    color: Colors.marigold,
    fontFamily: Fonts.segui_semiBold,
  },
  view1: {flexDirection: 'row', marginTop: 15, paddingLeft: 12},
  textReq: {
    fontSize: 12,
    color: Colors.dustyOrange,
    fontFamily: Fonts.segui_semiBold,
    flex: 0.65,
    marginBottom: 10,
  },
  viewTextInput: {
    flexDirection: 'row',
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
    paddingLeft: 13,
  },
  textTop: {
    fontSize: 16,
    lineHeight: 16,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  line1: {
    marginHorizontal: 12,
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginTop: 15,
  },
  seperator: {
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginHorizontal: 12,
    marginTop: 15,
  },
  dotTop: {
    width: 4,
    height: 4,
    backgroundColor: Colors.pinkishGreyTwo,
    borderRadius: 2,
    marginHorizontal: 7,
  },
  text180: {
    fontSize: 16,
    lineHeight: 16,
    color: Colors.pinkishGrey,
    fontFamily: Fonts.segui_semiBold,
    flex: 1,
  },
  iconAddPro: {width: 100, height: 20, resizeMode: 'contain'},
  ViewTouch: {flexDirection: 'row', marginLeft: 12, marginTop: 15},
  iconEditProp: {width: 68, height: 22, resizeMode: 'contain'},
  touchMid: {marginLeft: 10, flex: 0.93},
  viewInactive: {
    marginLeft: 10,
    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: Colors.darkSeafoamGreen,
    paddingHorizontal: 25,
  },
  textInactive: {
    top: Helper.hasNotch ? 1 : Platform.OS === 'ios' ? 0 : -2,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
  },
  ViewRenderFilter: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 23,
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
  },
  checkIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.darkSeafoamGreen,
  },
  uncheckIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.pinkishGrey,
  },
  unselectText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.pinkishGrey,
  },
  selectedText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.black,
  },
  viewTextInput: {
    flexDirection: 'row',
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
    paddingLeft: 13,
    borderRadius: 4,
    marginTop: 0,
  },
  viewTop: {
    paddingLeft: 13,
    paddingRight: 10,
    paddingTop: 18,
    backgroundColor: Colors.lightMint,
    height: 60,
  },
  searchIcon: {width: 11, height: 11, resizeMode: 'contain'},
});
