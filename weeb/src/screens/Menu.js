import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import Helper from '../Lib/Helper';
import { translate } from '../Language'
export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'Tenant',
      selectedAvtar: Helper.userData?.profile_picture ? Helper.userData?.profile_picture : '',
      name: Helper.userData?.name ? Helper.userData?.name : '',
      email: Helper.userData?.email ? Helper.userData?.email : '',
      phoneNumber: Helper.userData?.mobile_number ? Helper.userData?.mobile_number : '',
      city: Helper.userData?.city ? Helper.userData?.city : ''
    };
  }

  activetab(type) {
    this.setState({ active: type });
  }
  componentDidMount() {
    console.log('user data in drawer', Helper.userData)
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
      // ApiCall.ApiMethod('get-profile', 'GET').then((response) => {
      //   //  console.log('user profile data', response)
      // })
      this.setState({
        selectedAvtar: Helper.userData?.profile_picture ? Helper.userData?.profile_picture : '',
        name: Helper.userData?.name ? Helper.userData?.name : '',
        email: Helper.userData?.email ? Helper.userData?.email : '',
        phoneNumber: Helper.userData?.mobile_number ? Helper.userData?.mobile_number : '',
        city: Helper.userData?.city ? Helper.userData?.city : ''
      })
    });

    // ApiCall.ApiMethod('get-profile','GET').then((response) => {
    //     //console.log('user profile data', response)
    // })
  }

  componentWillUnmount() {
    this._unsubscribe();
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewTopMain}>
          <View style={{ flexDirection: 'row' }}>
            <Image style={styles.img_Profile} source={images.chat_profile} />
            <View style={styles.userDetail}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.txt_Name}>{this.state.name}</Text>
                <Image style={styles.icon_Close} source={images.close} />
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.txt_Id}>{this.state.email}</Text>
                <Text style={styles.txt_Verify}>{translate("Verify")}</Text>
              </View>
              <Text style={styles.txt_ModileNumber}>{this.state.phoneNumber}</Text>
              <View style={styles.boder_darkGreen}></View>

              <Text style={styles.txt_SwitchAccount}>{translate("SwitchAccount")}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 6,
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    borderColor: Colors.warmGrey,
                    borderWidth: 1,
                    borderRadius: 3,
                  }}>
                  <TouchableOpacity onPress={() => this.activetab('Tenant')}>
                    <Text
                      style={
                        this.state.active == 'Tenant'
                          ? styles.activeTxt
                          : styles.unactiveTxt
                      }>
                      {translate("TENANT")}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.activetab('Owner')}>
                    <Text
                      style={
                        this.state.active == 'Owner'
                          ? styles.activeTxt
                          : styles.unactiveTxt
                      }>
                      {translate("OWNER")}
                    </Text>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.btn_Chat}>
                  <Image style={styles.icon_chat} />
                  <Text style={styles.txt_Chat}>{translate("Chat")}</Text>
                  <Text style={styles.txt_ReceiveChat}>4</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <ScrollView style={{ paddingTop: 10 }}>
          {this.state.active == 'Tenant' ? (
            <TouchableOpacity style={styles.view_Menu}>
              <Image
                style={styles.icon_menu}
                source={images.my_rent_property}
              />
              <View style={{ flex: 1, marginLeft: 21 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.txt_Label}>{translate("MyRentProperty")}</Text>
                  <Text style={styles.rentProperty}>10</Text>
                </View>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : (
            <View>
              <TouchableOpacity style={styles.view_Menu}>
                <Image style={styles.icon_menu} source={images.list_Property} />
                <View style={{ flex: 1, marginLeft: 21 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.txt_Label}>{translate("ListedProperty")}</Text>
                    <Text style={styles.rentProperty}>250</Text>
                  </View>
                  <View style={styles.boder_grey}></View>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={styles.view_Menu}>
                <Image
                  style={styles.icon_menu}
                  source={images.my_rent_property}
                />
                <View style={{ flex: 1, marginLeft: 21 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.txt_Label}>{translate("MyRentals")}</Text>
                    <Text style={styles.rentProperty}>150</Text>
                  </View>
                  <View style={styles.boder_grey}></View>
                </View>
              </TouchableOpacity>
            </View>
          )}

          <TouchableOpacity style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.booking_request} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.txt_Label}>{translate("BookingRequest")}</Text>
                {this.state.active == 'Tenant' ? (
                  <Text style={styles.rentProperty}>20</Text>
                ) : (
                  <Text style={styles.BookRequest}>5</Text>
                )}
              </View>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.doctounts} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <Text style={styles.txt_Label2}>{translate("Documents")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          {this.state.active == 'Tenant' ? (
            <TouchableOpacity style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.my_transactions} />
              <View style={{ flex: 1, marginLeft: 21 }}>
                <Text style={styles.txt_Label2}>{translate("MyTransactions")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.view_Menu}>
              <Image style={styles.icon_menu} source={images.add_tanant} />
              <View style={{ flex: 1, marginLeft: 21 }}>
                <Text style={styles.txt_Label2}>{translate("AddTenant")}</Text>
                <View style={styles.boder_grey}></View>
              </View>
            </TouchableOpacity>
          )}

          <TouchableOpacity style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.about_weeb} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <Text style={styles.txt_Label2}>{translate("AboutWeeb")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.customer_support} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <Text style={styles.txt_Label2}>{translate("CustomerSupport")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.view_Menu}>
            <Image style={styles.icon_menu} source={images.terms} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <Text style={styles.txt_Label2}>{translate("Terms&Conditions")}</Text>
              <View style={styles.boder_grey}></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.view_Menu, { marginBottom: 50 }]}>
            <Image style={styles.icon_menu} source={images.privacy} />
            <View style={{ flex: 1, marginLeft: 21 }}>
              <Text style={styles.txt_Label2}>{translate("PrivacyPolicy")}</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.whiteTwo },
  viewTopMain: {
    backgroundColor: Colors.lightMint,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  img_Profile: { width: 56, height: 56, resizeMode: 'contain', marginTop: 4 },
  userDetail: { flex: 1, marginLeft: 15, justifyContent: 'center' },
  txt_Name: {
    flex: 1,
    fontFamily: Fonts.segui_semiBold,
    fontSize: 18,
    color: Colors.blackThree,
    lineHeight: 24,
  },
  icon_Close: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    tintColor: Colors.black,
  },
  txt_Id: {
    flex: 1,
    fontFamily: Fonts.segoeui,
    fontSize: 13,
    color: Colors.brownishGrey,
    lineHeight: 17,
  },
  txt_Verify: {
    fontFamily: Fonts.segoeui,
    fontSize: 12,
    color: Colors.dustyOrange,
  },
  txt_ModileNumber: {
    fontFamily: Fonts.segoeui,
    fontSize: 13,
    color: Colors.brownishGrey,
    lineHeight: 17,
  },
  boder_darkGreen: {
    backgroundColor: Colors.darkSeafoamGreen,
    height: 0.5,
    marginTop: 13,
  },
  txt_SwitchAccount: {
    fontFamily: Fonts.segoeui,
    fontSize: 12,
    color: Colors.black,
    marginTop: 10,
  },
  activeTxt: {
    fontFamily: Fonts.segoeui,
    fontSize: 11,
    color: Colors.whiteTwo,
    paddingVertical: 2,
    paddingHorizontal: 14,
    backgroundColor: Colors.darkSeafoamGreen,
  },
  unactiveTxt: {
    fontFamily: Fonts.segoeui,
    fontSize: 11,
    color: Colors.greyishBrown,
    paddingVertical: 2,
    paddingHorizontal: 14,
    backgroundColor: Colors.whiteFive,
  },
  btn_Chat: {
    flexDirection: 'row',
    backgroundColor: Colors.whiteTwo,
    borderColor: Colors.darkSeafoamGreen,
    borderWidth: 1,
    paddingHorizontal: 8,
    alignItems: 'center',
    borderRadius: 2,
  },
  icon_chat: { width: 19, height: 17, resizeMode: 'contain' },
  txt_Chat: {
    fontFamily: Fonts.segoeui,
    fontSize: 11,
    color: Colors.darkSeafoamGreen,
    marginLeft: 5,
  },
  txt_ReceiveChat: {
    height: 20,
    width: 20,
    textAlign: 'center',
    fontFamily: Fonts.segoeui,
    fontSize: 10,
    color: Colors.whiteTwo,
    backgroundColor: Colors.cherryRed,
    borderRadius: 20,
    position: 'absolute',
    right: 4,
    top: -15,
  },
  view_Menu: { flexDirection: 'row', marginHorizontal: 15, marginTop: 15 },
  icon_menu: { width: 29, height: 29, resizeMode: 'contain' },
  txt_Label: {
    flex: 1,
    fontFamily: Fonts.segoeui,
    fontSize: 16,
    color: Colors.black,
    lineHeight: 25,
  },
  rentProperty: {
    height: 22,
    fontFamily: Fonts.segoeui,
    fontSize: 10,
    color: Colors.pinkishGrey,
    borderColor: Colors.pinkishGrey,
    borderRadius: 15,
    borderWidth: 1,
    paddingHorizontal: 24,
  },
  boder_grey: {
    backgroundColor: Colors.warmGrey,
    height: 0.5,
    marginTop: 17,
    opacity: 0.5,
  },
  BookRequest: {
    width: 26,
    height: 26,
    textAlign: 'center',
    paddingVertical: 3,
    fontFamily: Fonts.segoeui,
    fontSize: 10,
    color: Colors.whiteTwo,
    backgroundColor: Colors.cherryRed,
    borderRadius: 40,
  },
  txt_Label2: {
    fontFamily: Fonts.segoeui,
    fontSize: 16,
    color: Colors.black,
    lineHeight: 25,
  },
});
