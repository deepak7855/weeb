import React, { Component } from 'react';
import {
  Text,
  View,
  RefreshControl,
  StyleSheet,
  Image,
  FlatList,
  Keyboard,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  DeviceEventEmitter,
  Dimensions,
  Animated,
} from 'react-native';
import Colors from '../../Lib/Colors';
import images from '../../Lib/Images';
import Swiper from 'react-native-swiper';
import WillasList from '../../Components/WillasList';
import Fonts from '../../Lib/Fonts';
import { handleNavigation } from '../../navigation/routes';
import PropertyType from '../../Components/PropertyType';
import AmenitiesList from '../../Components/AmenitiesList';
import Amenities from './Components/Amenities';
import AvailablefromRooms from './Components/AvailablefromRooms';
import AppHeader from '../../Components/AppHeader';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Helper, Network, AlertMsg ,CallTenant} from '../../Lib/index';
import { ApiCall, ActivityIndicatorApp, LoaderForList } from '../../Api/index';
import { GoogleApiAddressList,ProgressiveImage } from '../../Components/Common/index';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Constant } from '../../Lib/Constant';
import { geoCurrentLocation } from '../../Lib/LocationsPermission';
import messaging from '@react-native-firebase/messaging';
import ChatController from '../../Lib/SocketManager';
import { EventRegister } from 'react-native-event-listeners'
import {updateDeviceToken}from '../../Lib/FirebaseHelper'
let selectedFurniture = [];
let selectedAmenties = [];

const { width, height } = Dimensions.get('window');
import { translate } from '../../Language';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: Helper.userData?.city,
      PropirtList: [
        {
          propertyTypeIcon: images.full_house_green,
          routh: images.chalet_active,
          sarviceName: 'Full House',
          id: 1,
          type: 'Full House',
        },
        {
          propertyTypeIcon: images.apartment_green,
          routh: images.apartmentin_active,
          sarviceName: 'Apartment \nin House',
          id: 2,
          type: 'Apartment in House',
        },
        {
          propertyTypeIcon: images.chalet_green,
          routh: images.chalet_active,
          sarviceName: 'Chalet',
          id: 3,
          type: 'Chalet',
        },
        {
          propertyTypeIcon: images.apartmentbuilding_green,
          routh: images.apartment_active,
          sarviceName: 'Apartment \nin Building',
          id: 4,
          type: 'Apartment in Building',
        },
      ],
      PropirtSelect: 0,
      propertyName: '',
      barRoomList: [
        { badCount: 1, id: 1 },
        { badCount: 2, id: 2 },
        { badCount: 3, id: 3 },
        { badCount: 4, id: 4 },
        { badCount: 5, id: 5 },
        { badCount: '5+', id: '5+' },
      ],
      bathRoomList: [
        { badCount: 1, id: 1 },
        { badCount: 2, id: 2 },
        { badCount: 3, id: 3 },
        { badCount: 4, id: 4 },
        { badCount: 5, id: 5 },
        { badCount: 'Any', id: '5+' },
      ],
      furnishingList: '',
      amenitiesList: '',
      selectBaddRoom: 0,
      selectTotalBadRoom: 0,
      selectfurnishing: 0,
      modalVisible: false,
      value: 25,
      amenitiesSingle: 3,
      propertyList: '',
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      modalVisibleForLocation: false,
      address: '',
      monthlyPrice: '',
      selectedTotalBathroom: 0,
      filterDate: '',
      immedtily: '',
      showDataButton: false,
      resetButton: false,
      currentlat: '',
      currentlong: '',
      userSelectedFurniture: [],
      userSelectedAmenties: [],
      selectedType: '',
      emptymessage: false,
      notificationCount: 0,
      statusNow: 'not started',
      result: '',
      scrolly:new Animated.Value(0),
      bannerList:'',
    };
    
    {
      selectedFurniture;
    }
    {
      selectedAmenties;
    }
    AppHeader({
      ...this,
      leftHeide: true,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      searchClick: () => {
        this.goNotification();
      },
      searchIcon: images.notification_goup,
      search: true,
      location: true,
      addBtn: Helper.userType == 'OWNER' ? true : null,
      onAdd: () => {
        this.addPropartyScreen();
      },
      addItem: images.addproperty,
      menuClick: () => {
        this.menuClick();
      },
      city: this.state.city,
    });
  }

  componentDidMount() {
    ChatController.socketInit();
    this.getNotificationList();
    this.messageListener();
    this.getBanner();
    updateDeviceToken()
   // console.log('user data',Helper.userData)
    if(!Helper.userData?.customer_id){
      Helper.showToast('Please Complete your profile then processed further')
      this.props.navigation.navigate('EditProfile');
      return true;
    }
    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        // console.log('device event', data?.type);
        AppHeader({
          ...this,
          leftHeide: true,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          searchClick: () => {
            this.goNotification();
          },
          searchIcon:
            this.state.notificationCount > 0
              ? images.notification_goup
              : images.notification_icon,
          search: true,
          location: true,
          addBtn: data?.type == 'OWNER' ? true : null,
          onAdd: () => {
            this.addPropartyScreen();
          },
          addItem: images.addproperty,
          menuClick: () => {
            this.menuClick();
          },
          city: Helper.userData?.city,
        });
      },
    );

    

    geoCurrentLocation(1, (data) => {
      if (data.latitude && data.longitude) {
        Helper.setData('currentLocation', data);
        this.setState(
          { currentlat: data.latitude, currentlong: data.longitude },
          () => {
            this.getPropertiesData(true);
          },
        );
      }
    });
    this.getFurnitureAndAmenties();
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getNotificationList();
    });
    this._unsubscribefocusHome = this.props.navigation.addListener('focus', () => {
      updateDeviceToken();
  });
  }
 
  
  onChateClick = (index) => {
    if (this.state.propertyList[index]) {
      handleNavigation({
        type: 'push',
        page: 'Chat',
        passProps: {userId: this.state.propertyList[index]?.user?.id},
        navigation: this.props.navigation,
      });
    }
  };
  onCall = (index) => {
    // console.log('call item', this.state.propertyDetailsData?.user?.mobile_number);
  
   if (this.state.propertyList[index]) {
    CallTenant(this.state.propertyList[index]?.user?.mobile_number);
  }
 };
  messageListener = async () => {
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      // console.log(
      //   'setBackgroundMessageHandler-----------  ',
      //   remoteMessage,
      //   remoteMessage?.data?.type,
      //   remoteMessage?.data?.dictionery,
      // );
    });

    messaging().onNotificationOpenedApp((remoteMessage) => {
      let x = JSON.parse(remoteMessage?.data?.dictionary);
      console.log(
        'Notification caused app to open from background state:',
        x.sender_id
        ,
        remoteMessage?.data,
      );
      if (remoteMessage?.data?.type == 'chat') {
        handleNavigation({
          type: 'push',
          page: 'Chat',
          passProps: { userId: x.sender_id },
          navigation: this.props.navigation,
        });
      }
      if (remoteMessage?.data?.type == 'add_tenant') {
        handleNavigation({
          type: 'push',
          page: 'MyRentProperty',
          navigation: this.props.navigation,
          passProps: {type: 'document'},
        });
      }
      if (remoteMessage?.data?.type == 'property_request_status') {
        handleNavigation({
          type: 'push',
          page: 'BookingRequestTopTab',
          navigation: this.props.navigation,
          passProps: { type: 'document' },
        });
      }
      if (remoteMessage?.data?.type == 'property_request') {
        handleNavigation({
          type: 'push',
          page: 'BookingRequestTopTab',
          navigation: this.props.navigation,
          passProps: { type: 'document' },
        });
      }
      if(remoteMessage?.data?.type == 'property_rent_notification'){
        console.log('remoteMessage?.data?.dictionary?.property_id',remoteMessage?.data?.dictionary);
      }

      if (remoteMessage?.data?.type == 'property_terminate_notification') {
        handleNavigation({
          type: 'push',
          page: 'BookingRequestTopTab',
          navigation: this.props.navigation,
          passProps: { type: 'document' },
        });
      }
    
      // navigation.navigate(remoteMessage.data.type);
    });
    // notification app kill
    messaging().getInitialNotification().then(async notification => {
      console.log(notification, "notificationnotification321")
       let x = JSON.parse(notification?.data?.dictionary);
       console.log(
         'Notification caused app to open from background state:',
         x.sender_id,
         notification?.data?.type,
       );
       if (notification?.data?.type == 'chat') {
         handleNavigation({
           type: 'push',
           page: 'Chat',
           passProps: {userId: x.sender_id},
           navigation: this.props.navigation,
         });
       }
       if (notification?.data?.type == 'add_tenant') {
         handleNavigation({
           type: 'push',
           page: 'MyRentProperty',
           navigation: this.props.navigation,
           passProps: {type: 'document'},
         });
       }
       if (notification?.data?.type == 'property_request_status') {
         handleNavigation({
           type: 'push',
           page: 'BookingRequestTopTab',
           navigation: this.props.navigation,
           passProps: {type: 'document'},
         });
       }
       if (notification?.data?.type == 'property_request') {
         handleNavigation({
           type: 'push',
           page: 'BookingRequestTopTab',
           navigation: this.props.navigation,
           passProps: {type: 'document'},
         });
       }
       if (remoteMessage?.data?.type == 'property_terminate_notification') {
        handleNavigation({
          type: 'push',
          page: 'BookingRequestTopTab',
          navigation: this.props.navigation,
          passProps: { type: 'document' },
        });
      }
    });
    messaging().onMessage(async (remoteMessage) => {
      let obj = JSON.parse(remoteMessage?.data?.data);
      console.log('onMessage------   ', obj?.type);

    });

    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        console.log('remotemessgae',remoteMessage)
      });
  };

  getBanner = () =>{
    Network.isNetworkAvailable().then((network) => {
      if(network){
        ApiCall.ApiMethod({
          Url: 'get-banner',
          method: 'GET',
        }).then((res)=>{
            console.log('bannaer list',res)
            if(res?.status){
              this.setState({
                bannerList:res?.data
              })
              return true
            }
        }).catch((err)=>{
            console.log(err,'err------------')
        })
      }
    })
  }
  
  getNotificationList() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          ApiCall.ApiMethod({
            Url: 'get-unread-notification-count' + '?page=' + this.state.currentPage,
            method: 'GET',
          })
            .then((res) => {
              if (res?.status) {
               
                // DeviceEventEmitter.emit(
                //     Constant.NOTIFICATION_COUNT,
                //     res?.data ? res?.data?.data?.length : 0,
                //   );
                EventRegister.emit(Constant.NOTIFICATION_COUNT, 0)
                Helper.notificationCount = res?.data ? res?.data.data.length : 0
                this.setState(
                  {
                    notificationCount: res?.data ? res?.data.data.length : 0,
                  },
                  () => {
                    // DeviceEventEmitter.emit(
                    //   'notificaionCount',
                    //   this.state.notificationCount,
                    // );
                    AppHeader({
                      ...this,
                      leftHeide: true,
                      backgroundColor: Colors.lightMint10,
                      leftIcon: images.black_arrow_btn,
                      leftClick: () => {
                        this.goBack();
                      },
                      searchClick: () => {
                        this.goNotification();
                      },
                      searchIcon:
                        this.state.notificationCount > 0
                          ? images.notification_goup
                          : images.notification_icon,
                      search: true,
                      location: true,
                      //addBtn: data?.type == 'OWNER' ? true : null,
                      onAdd: () => {
                        this.addPropartyScreen();
                      },
                      addItem: images.addproperty,
                      menuClick: () => {
                        this.menuClick();
                      },
                      city: Helper.userData?.city,
                    });
                  },
                );

                return;
              }
              this.setState({
                notificationCount: 0,
              });
            })
            .catch((err) => {
              this.setState({
                notificationCount: 0,
              });
            });
        } else {
          // Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        //Helper.showToast(AlertMsg.error.NETWORK);
      });
  }
  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
    }
    this._unsubscribe();
    this._unsubscribefocusHome();
  }

  getPropertiesData = (loader) => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (!this.state.refreshing) {
          this.setState({isLoading: loader});
        }

        let data = {
          latitude: this.state.currentlat,
          longitude: this.state.currentlong,
          status: 1,
          type: this.state.propertyName,
          rent: this.state.monthlyPrice,
          rooms: this.state.selectBaddRoom,
          bathrooms:
            this.state.selectedTotalBathroom == 'Any'
              ? ''
              : this.state.selectedTotalBathroom,
          available_types:
            this.state.selectedType == 'immediately'
              ? this.state.selectedType
              : this.state.selectedType == 'date' && this.state.filterDate
              ? this.state.selectedType
              : '',
          available_date: this.state.filterDate ? this.state.filterDate : '',
          furnishing_id: this.state.userSelectedFurniture.join(','),
          amenity_id: this.state.userSelectedAmenties.join(','),
        };

        ApiCall.ApiMethod({
          Url: 'get-property-list-others' + '?page=' + this.state.currentPage,
          method: 'POST',
          data: data,
        })
          .then((res) => {
            Helper.mainApp.hideLoader();
            // console.log('datatata', res);
            if (res?.status) {
              if (res?.data?.data && res?.data?.data.length > 0) {
                this.setState({
                  propertyList: this.state.propertyList
                    ? [...this.state.propertyList, ...res?.data?.data]
                    : res?.data?.data,
                  next_page_url: res?.data?.next_page_url,
                  isLoading: false,
                  refreshing: false,
                  emptymessage: false,
                });
              } else {
                this.setState({
                  next_page_url: res?.data?.next_page_url,
                  isLoading: false,
                  refreshing: false,
                  currentPage: 1,
                  emptymessage: true,
                });
              }
            }
          })
          .catch((err) => {
            this.setState({
              next_page_url: res?.data?.next_page_url,
              isLoading: false,
              refreshing: false,
              currentPage: 1,
              emptymessage: true,
            });
            // console.log('home screen get prperty list', err);
          });
      }
    });
  };

  getFurnitureAndAmenties = () => {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          //  Helper.mainApp.showLoader();
          ApiCall.ApiMethod({Url: 'get-amenities', method: 'GET'})
            .then((res) => {
              let arr = [];
              if (res?.status) {
                res?.data.forEach((element) => {
                  arr.push({
                    id: element?.id,
                    isSelect: false,
                    image: element?.image,
                    title: element?.title,
                  });
                });

                // console.log('furniture data', arr);
                this.setState({amenitiesList: arr});
              }
            })
            .catch((err) => {
              console.log('amenties error', err);
              Helper.mainApp.hideLoader();
            });

          ApiCall.ApiMethod({Url: 'get-furnishings', method: 'GET'})
            .then((res) => {
              let arr = [];
              if (res?.status) {
                res?.data.forEach((element) => {
                  arr.push({
                    id: element?.id,
                    isSelect: false,
                    image: element?.image,
                    title: element?.title,
                  });
                });

                this.setState({furnishingList: arr});
              }
            })
            .catch((err) => {
              console.log('furniture api err', err);
              Helper.mainApp.hideLoader();
            });
        }
      })
      .catch((err) => {
        console.log('network err', err);
        Helper.mainApp.hideLoader();
      });
  };

  onRefresh = () => {
    this.setState({propertyList: [], refreshing: true});
    setTimeout(() => {
      this.setState(
        {currentPage: 1, refreshing: false, emptymessage: false},
        () => {
          this.getPropertiesData(false);
        },
      );
    }, 2000);
  };

  addPropartyScreen = () => {
    Helper.propertyData = {};
    handleNavigation({
      type: 'push',
      page: 'SelectPropertyType',
      navigation: this.props.navigation,
    });
  };

  goNotification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  openDrawer = () => {
    Keyboard.dismiss();
    handleNavigation({type: 'drawer', navigation: this.props.navigation});
  };
  menuClick = () => {
    this.props.navigation.openDrawer();
  };

  onAmenitiesSingle = (item) => {
    this.setState({amenitiesSingle: item.id});
  };

  amenitiesSingleSelect = ({item}) => {
    return (
      <Amenities
        item={item}
        onAmenitiesSingle={() => this.onAmenitiesSingle(item)}
        amenitiesSingle={this.state.amenitiesSingle}
      />
    );
  };

  selectBadCounting = (item) => {
    this.setState({
      selectBaddRoom: item.id,
      selectTotalBadRoom: item?.selectTotalBadRoom,
    });
  };

  furnishing = (item) => {
    this.setState({
      selectfurnishing: item.id,
      selectedTotalBathroom: item?.badCount,
    });
  };

  badModalList = ({item}) => {
    return (
      <AmenitiesList
        item={item}
        badCount={this.props.badCount}
        Select={() => this.selectBadCounting(item)}
        selectBaddRoom={this.state.selectBaddRoom}
      />
    );
  };
  furnishingList = ({item}) => {
    return (
      <AmenitiesList
        item={item}
        badCount={this.props.badCount}
        Select={() => this.furnishing(item)}
        selectBaddRoom={this.state.selectfurnishing}
      />
    );
  };

  onSelectProperty = (item) => {
    this.setState({PropirtSelect: item.id, propertyName: item?.type});
  };

  modalPropirtList = ({item}) => {
    return (
      <PropertyType
        item={item}
        onSelectProperty={() => this.onSelectProperty(item)}
        routh={this.props.routh}
        PropirtSelect={this.state.PropirtSelect}
      />
    );
  };

  onSelectAmenities = (index) => {
    let selected = [...this.state.amenities];
    selected[index].isSelect = !selected[index].isSelect;
    this.setState({amenities: selected});
  };

  onFurnitureSelect(index) {
    let selected = [...this.state.furnishingList];

    selected[index].isSelect = !selected[index].isSelect;
    this.setState({furniture: selected});
    if (selected[index].isSelect) {
      selectedFurniture.push(selected[index].id);
      this.state.userSelectedFurniture.push(selected[index].id);
    } else {
      if (selectedFurniture.indexOf(selected[index].id) > -1) {
        selectedFurniture.splice(
          selectedFurniture.indexOf(selected[index].id),
          1,
        );
        this.state.userSelectedFurniture.splice(
          this.state.userSelectedFurniture.indexOf(selected[index].id),
          1,
        );
      }
    }
  }

  onAmentiesSelect(index) {
    let selected = [...this.state.amenitiesList];

    selected[index].isSelect = !selected[index].isSelect;
    this.setState({amenities: selected});

    if (selected[index].isSelect) {
      selectedAmenties.push(selected[index].id);
      this.state.userSelectedAmenties.push(selected[index].id);
    } else {
      if (selectedAmenties.indexOf(selected[index].id) > -1) {
        selectedAmenties.splice(
          selectedAmenties.indexOf(selected[index].id),
          1,
        );
        this.state.userSelectedAmenties.splice(
          this.state.userSelectedAmenties.indexOf(selected[index].id),
          1,
        );
      }
    }
  }

  renderFuritures = (data) => {
    return (
      <TouchableOpacity
        style={{marginHorizontal: 15, marginTop: 10}}
        onPress={() => this.onFurnitureSelect(data.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{uri: data.item?.image}}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.darkSeafoamGreen,
              borderWidth: 1.5,
            }}
          />
        ) : (
          <Image
            source={{uri: data.item?.image}}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.white,
              borderWidth: 1.5,
            }}
          />
        )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  renderAmenities = (data) => {
    // console.log('amenities data in flatlist',data?.item)
    return (
      <TouchableOpacity
        style={{marginHorizontal: 15, marginTop: 10}}
        onPress={() => this.onAmentiesSelect(data?.index)}>
        {data?.item?.isSelect ? (
          <Image
            source={{uri: data?.item?.image}}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.darkSeafoamGreen,
              borderWidth: 1.5,
            }}
          />
        ) : (
          <Image
            source={{uri: data?.item?.image}}
            style={{
              height: 44,
              width: 44,
              resizeMode: 'contain',
              borderColor: Colors.white,
              borderWidth: 1.5,
            }}
          />
        )}
        <Text
          style={{
            marginTop: 5,
            textAlign: 'center',
            fontSize: 10,
            fontFamily: Fonts.segui_semiBold,
            color: Colors.greyishBrown,
          }}>
          {data?.item?.title}
        </Text>
      </TouchableOpacity>
    );
  };

  amenitiesList = ({item, index}) => {
    return (
      <AmenitiesList
        socialIcon={this.props.socialIcon}
        socialText={this.props.socialText}
        onMultiSelect={this.onSelectAmenities}
        routh={this.props.routh}
        item={item}
        index={index}
        height={53}
        width={53}
      />
    );
  };

  handleAddress = (data) => {
    this.setState({
      address: data?.addressname,
      currentlat: data?.lat,
      currentlong: data?.long,
    });
  };

  filterModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({modalVisible: false});
        }}>
        <SafeAreaView style={{backgroundColor: Colors.white, flex: 1}}>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps={'handled'}
            bounces={false}
            showsVerticalScrollIndicator={false}
            style={{flex: 1, backgroundColor: Colors.whiteTwo}}>
            <StatusBarCustom
              backgroundColor={Colors.white}
              translucent={true}
            />
            <View style={styles.filteView}>
              <Text style={styles.filterText}>{translate("Filter")}</Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({modalVisible: false});
                }}>
                <Image style={styles.closeIcon} source={images.close} />
              </TouchableOpacity>
            </View>
            <Text style={styles.propertyText}>{translate("PROPERTYTYPE")}</Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.PropirtList}
                renderItem={this.modalPropirtList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={{marginHorizontal: 12, marginTop: 13}}>
              <Text style={styles.locationModalText}>{translate("SELECTLOCATION")}</Text>
              {/* <TouchableOpacity style={styles.searchView} onPress={() => this.setState({ modalVisibleForLocation: true })}>
                                <Image style={styles.searchIcon} source={images.search_icon} />
                                <TextInput
                                    style={styles.inputStyle}
                                    placeholderTextColor={Colors.pinkishGrey}
                                    onChangeText={(text) => { this.setState({ search: text }) }}
                                    value={this.state.address}
                                    placeholder="Search by Location or Address"
                                    editable={false}
                                />
                            </TouchableOpacity> */}
              <TouchableOpacity
                hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                style={styles.searchView}
                onPress={() => this.setState({modalVisibleForLocation: true})}>
                <Image style={styles.searchIcon} source={images.search_icon} />
                <View
                  style={styles.inputStyle}
                  placeholderTextColor={Colors.pinkishGrey}
                  onChangeText={(text) => {
                    this.setState({search: text});
                  }}
                  value={this.state.address}
                  placeholder="Search by Location or Address"
                  editable={false}
                  // onPressOut={() => this.setState({ modalVisibleForLocation: true })}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: Fonts.segoeui,
                      color: Colors.pinkishGrey,
                    }}>
                    {' '}
                    {this.state.address
                      ? this.state.address
                      : 'Search by Location or Address'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <GoogleApiAddressList
              modalVisible={this.state.modalVisibleForLocation}
              hideModal={() => {
                this.setState({modalVisibleForLocation: false});
              }}
              onSelectAddress={this.handleAddress}
            />
            <View>
              <Text
                style={[
                  styles.locationModalText,
                  {marginHorizontal: 12, marginTop: 30},
                ]}>
                {translate("PRICEMONTHLY")}
              </Text>
              {/* <View style={{ justifyContent: 'center', alignItems: 'center',marginHorizontal: 12,backgroundColor: Colors.darkSeafoamGreen,borderRadius:10,width:'13%'}}>
                            <Text style={{  fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.black }}>{this.state.monthlyPrice + '$'}</Text>
                            </View> */}
              <View style={{width: '95%', marginHorizontal: 20}}>
                <MultiSlider
                  selectedStyle={{backgroundColor: Colors.darkSeafoamGreen}}
                  unselectedStyle={{backgroundColor: 'silver'}}
                  markerStyle={{
                    backgroundColor: Colors.darkSeafoamGreen,
                    height: 15,
                    width: 15,
                  }}
                  trackStyle={{borderRadius: 7, height: 3}}
                  // values={[this.state.filteredData.distance]}
                  sliderLength={width - width / 9.3}
                  touchDimensions={{height: 100, width: 100, borderRadius: 15}}
                  min={0}
                  max={500000}
                  value={this.state.monthlyPrice}
                  onValuesChangeFinish={(value) =>
                    this.setState({monthlyPrice: Math.round(value)})
                  }
                  step={1}
                  enableLabel
                  enabledOne
                />
              </View>
            </View>
            <Text
              style={[
                styles.locationModalText,
                {marginHorizontal: 12, marginTop: 30},
              ]}>
              {translate("BEDROOMS")}
            </Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.barRoomList}
                renderItem={this.badModalList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <Text
              style={[
                styles.locationModalText,
                {marginHorizontal: 12, marginTop: 30},
              ]}>
              {translate("BATHROOMS")}
            </Text>
            <View>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={this.state.bathRoomList}
                renderItem={this.furnishingList}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View>
              <Text
                style={[
                  styles.locationModalText,
                  {marginHorizontal: 12, marginTop: 30},
                ]}>
                {translate("FURNISHING")}
              </Text>
              <View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={this.state.furnishingList}
                  renderItem={this.renderFuritures}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>
            <View style={{marginHorizontal: 6, marginTop: 25}}>
              <Text style={[styles.availbleText, {marginHorizontal: 6}]}>
                {translate("AMENITIES")}
              </Text>
              <FlatList
                numColumns={5}
                data={this.state.amenitiesList}
                renderItem={this.renderAmenities}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <View style={{marginTop: 25}}>
              <Text style={[styles.availbleText, {marginHorizontal: 12}]}>
                {translate("AVAILABLEFROM")}
              </Text>
              <AvailablefromRooms
                onApplayFilter={() => this.onApplayFilter()}
                resetFilter={() => this.resetFilter()}
                filterDate={(val) => this.setState({filterDate: val})}
                resetButton={this.state.resetButton}
                onSelect={(type) => this.setState({selectedType: type})}
              />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </Modal>
    );
  };

  onApplayFilter = () => {
    this.setState(
      {
        modalVisible: false,
        currentPage: 1,
        propertyList: [],
      },
      () => {
        this.getPropertiesData(true);
      },
    );
  };

  resetFilter = () => {
    this.state.userSelectedFurniture = [];
    this.state.userSelectedAmenties = [];
    selectedAmenties = [];
    selectedFurniture = [];
    this.setState({
      address: '',
      lat: '',
      long: '',
      monthlyPrice: '',
      selectBadCounting: 0,
      selectBaddRoom: 0,
      selectTotalBadRoom: 0,
      PropirtSelect: 0,
      propertyName: '',
      selectedTotalBathroom: 0,
      selectfurnishing: 0,
      resetButton: true,
      immedtily: '',
      selectedType: '',
      currentlat: '',
      currentlong: '',
      filterDate: '',
    });
  };
  propertyDetails = (index) => {
   // console.log('selected property', index, this.state.propertyList[index])
    if (this.state.propertyList[index]) {
      handleNavigation({
        type: 'pushTo',
        page: 'PropertyDetails',
        passProps: {propertyID: this.state.propertyList[index]?.id},
        navigation: this.props.navigation,
      });
    }
  };
  pageListed = ({item, index}) => {
   //('property data',item)
    return (
      <WillasList
        item={item}
        iconWillas={this.props.iconWillas}
        onPress={() => this.propertyDetails(index)}
       chat={()=>this.onChateClick(index)}
       onCall= {()=>this.onCall(index)}
      />
    );
  };

  onScroll = () => {
    if (this.state.next_page_url && !this.state.isLoading) {
      this.setState(
        {currentPage: this.state.currentPage + 1, emptymessage: false},
        () => {
          this.getPropertiesData(true);
        },
      );
    }
  };

  render() {
    // console.log('helper remeber me', Helper.rememberMe)
    return (
      <View style={styles.containView}>
        <StatusBarCustom
          backgroundColor={Colors.lightMint10}
          translucent={false}
        />
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'handled'}
          bounces={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          //contentContainerStyle={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          style={{backgroundColor: Colors.whiteTwo}}>
          <View style={{height: 150, backgroundColor: Colors.lightMint}}>
            <Swiper
              style={styles.wrapper}
              activeDot={
                <View
                  style={{
                    backgroundColor: Colors.whiteTwo,
                    height: 5,
                    width: 5,
                    borderRadius: 5 / 2,
                    margin: 2,
                  }}></View>
              }
              dot={
                <View
                  style={{
                    backgroundColor: Colors.black,
                    height: 5,
                    width: 5,
                    borderRadius: 5 / 2,
                    margin: 2,
                  }}></View>
              }>
                {this.state.bannerList.length > 0 ?
                this.state.bannerList.map((res)=>{
                  return(
                    <View style={styles.slidView}>
                <Image style={styles.swiperBanner} source={{uri:res?.imgurl}} resizeMode={'contain'} />
              </View>
                  )
                })
                :
                <View style={styles.slidView}>
                <Image style={styles.swiperBanner} source={images.banner} />
              </View>
                }
            </Swiper>
          </View>
          <View style={styles.filterMainView}>
            <Text numberOfLines={1} style={[styles.rentText, {width: '75%'}]}>
              {translate("Exclusiverent")}
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.setState({modalVisible: true, selectedType: ''});
              }}>
              <Image style={styles.filterBtn} source={images.filter} />
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 20}}>
            <FlatList
              style={{margin: 4}}
              numColumns={2}
              data={this.state.propertyList}
              renderItem={this.pageListed}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
              ListFooterComponent={() => {
                return this.state.isLoading ? <LoaderForList /> : null;
              }}
              ListEmptyComponent={() =>
                !this.state.isLoading && !this.state.refreshing ? (
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: Fonts.segoeui,
                        color: Colors.cherryRed,
                        textAlign: 'center',
                      }}>
                      {this.state.emptymessage == false
                        ? null
                        : 'Property not found.'}
                    </Text>
                  </View>
                ) : null
              }
              onEndReached={this.onScroll}
              onEndReachedThreshold={0.5}
              keyboardShouldPersistTaps={'handled'}
              //    onRefresh={}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.onRefresh()}
                />
              }
              refreshing={this.state.refreshing}
              maxToRenderPerBatch={4}
              removeClippedSubviews={true}
            />
          </View>
        </KeyboardAwareScrollView>
        {this.filterModal()}
      </View>
      // </View>
    );
  }
}
const styles = StyleSheet.create({
  containView: {
    flex: 1,
    backgroundColor: Colors.whiteTwo,
  },
  wrapper: { marginTop: 10 },
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 3,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
    borderWidth: 0.5,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  searchIcon: { height: 12, width: 12, resizeMode: 'contain', marginLeft: 14 },
  inputStyle: {
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
    width: '85%',
    height: Platform.OS === 'ios' ? 30 : 50,
    justifyContent: 'center',
  },
  slidView: { alignItems: 'center', justifyContent: 'center' },
  swiperBanner: { height: 130, width: '97%' },
  filterMainView: {
    marginHorizontal: 14,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rentText: { fontSize: 14, fontFamily: Fonts.segui_semiBold },
  filterBtn: { height: 24, width: 66, resizeMode: 'contain' },
  filteView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 11,
    marginTop: 12,
  },
  filterText: { flex: 1, fontSize: 21, fontFamily: Fonts.segoeui_bold },
  closeIcon: { height: 15, width: 15, resizeMode: 'contain' },
  propertyText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
    marginHorizontal: 11,
    marginTop: 23,
  },
  locationModalText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
  },
  locationView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    paddingVertical: 12,
    borderColor: Colors.pinkishGreyTwo,
    borderRadius: 4,
    marginTop: 13,
  },
  locationInrView: { flexDirection: 'row', marginLeft: 16, alignItems: 'center' },
  locationIcon: { height: 10, width: 10, resizeMode: 'contain' },
  locationText: { fontSize: 12, fontFamily: Fonts.segui_semiBold, marginLeft: 9 },
  availbleText: {
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.greyishBrown,
  },
});
