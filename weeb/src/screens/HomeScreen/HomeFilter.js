import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { GoogleApiAddressList } from '../../Components/Common/index';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Colors from '../../Lib/Colors';
import StatusBarCustom from '../../Components/Common/StatusBarCustom';
import Fonts from '../../Lib/Fonts';
import AvailablefromRooms from './Components/AvailablefromRooms';
import PropertyType from '../../Components/PropertyType';
import images from '../../Lib/Images';
import AmenitiesList from '../../Components/AmenitiesList';
import { translate } from '../../Language';
const { width, height } = Dimensions.get('window')


export default class HomeFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allPropertyTypes: [],
            PropertySelect: '',
            modalVisibleForLocation: false,
            propertyName: '',
            address: '',
            currentlat: '',
            currentlong: '',
            monthlyPrice: '',
            barRoomList: [
                { badCount: 1, id: 1 },
                { badCount: 2, id: 2 },
                { badCount: 3, id: 3 },
                { badCount: 4, id: 4 },
                { badCount: 5, id: 5 },
                { badCount: '5+', id: '5+' },
            ],
            bathRoomList: [
                { badCount: 1, id: 1 },
                { badCount: 2, id: 2 },
                { badCount: 3, id: 3 },
                { badCount: 4, id: 4 },
                { badCount: 5, id: 5 },
                { badCount: 'Any', id: '5+' },
            ],
            selectBaddRoom: 0,
        };
    }


    onSelectProperty = (item) => {
        let name;
        if (item.id == 1) {
            name = "Full House"
        } else if (item.id == 2) {
            name = "Apartment in House"
        } else if (item.id == 4) {
            name = "Chalet"
        } else if (item?.id == 4) {
            name = "Apartment in Building"
        }
        this.setState({ PropertySelect: item.id, propertyName: item?.type })
    }

    modalPropirtList = ({ item, }) => {
        return (
            <PropertyType
                item={item}
                onSelectProperty={() => this.onSelectProperty(item)}
                routh={this.props.routh}
                PropirtSelect={this.state.PropertySelect}
            />
        )
    }

    badModalList = ({ item }) => {
        return (
            <AmenitiesList
                item={item}
                badCount={this.props.badCount}
                Select={() => this.selectBadCounting(item)}
                selectBaddRoom={this.state.selectBaddRoom}
            />
        )
    }

    selectBadCounting = (item) => {
        this.setState({ selectBaddRoom: item.id, selectTotalBadRoom: item?.selectTotalBadRoom })
    }

    handleAddress = (data) => {
        this.setState({ address: data?.addressname, currentlat: data?.lat, currentlong: data?.long })
    }


    furnishing = (item) => {
        this.setState({ selectfurnishing: item.id, selectedTotalBathroom: item?.badCount })
    }

    furnishingList = ({ item }) => {
        return (
            <AmenitiesList
                item={item}
                badCount={this.props.badCount}
                Select={() => this.furnishing(item)}
                selectBaddRoom={this.state.selectfurnishing}
            />
        )
    }


    renderFuritures = (data) => {
        //ConsoleLog('flatlist', data.item);
        return (
            <TouchableOpacity
                style={{ marginHorizontal: 15, marginTop: 10 }}
                onPress={() => this.onFurnitureSelect(data.index)}
            >
                {data?.item?.isSelect ? (
                    <Image
                        source={{ uri: data.item?.image }}
                        style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.darkSeafoamGreen, borderWidth: 1.5 }}
                    />
                ) : (
                        <Image
                            source={{ uri: data.item?.image }}
                            style={{ height: 44, width: 44, resizeMode: 'contain', borderColor: Colors.white, borderWidth: 1.5 }}
                        />
                    )}
                <Text
                    style={{
                        marginTop: 5,
                        textAlign: 'center',
                        fontSize: 10,
                        fontFamily: Fonts.segui_semiBold,
                        color: Colors.greyishBrown,
                    }}>
                    {data.item?.title}
                </Text>
            </TouchableOpacity>
        );
    };


    render() {
        return (
            <SafeAreaView style={{ backgroundColor: Colors.white, flex: 1 }}>

                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: Colors.whiteTwo }}>
                    <StatusBarCustom backgroundColor={Colors.white} translucent={true} />
                    <View style={styles.filteView}>
                        <Text style={styles.filterText}>{translate("Filter")}</Text>
                        <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
                            <Image style={styles.closeIcon} source={images.close} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.propertyText}>{translate("PROPERTYTYPE")}</Text>
                    <View>
                        <FlatList horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.allPropertyTypes}
                            renderItem={this.modalPropirtList}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ marginHorizontal: 12, marginTop: 13 }}>
                        <Text style={styles.locationModalText}>{translate("SELECTLOCATION")}</Text>
                        <TouchableOpacity style={styles.searchView} onPress={() => this.setState({ modalVisibleForLocation: true })}>
                            <Image style={styles.searchIcon} source={images.search_icon} />
                            <TextInput
                                style={styles.inputStyle}
                                placeholderTextColor={Colors.pinkishGrey}
                                onChangeText={(text) => { this.setState({ search: text }) }}
                                value={this.state.address}
                                placeholder="Search by Location or Address"
                                editable={false}
                            />
                        </TouchableOpacity>
                    </View>
                    <GoogleApiAddressList
                        modalVisible={this.state.modalVisibleForLocation}
                        hideModal={() => { this.setState({ modalVisibleForLocation: false }) }}
                        onSelectAddress={this.handleAddress}

                    />
                    <View>
                        <Text style={[styles.locationModalText, { marginHorizontal: 12, marginTop: 30 }]}>{translate("PRICEMONTHLY")}</Text>
                        {/* <View style={{ justifyContent: 'center', alignItems: 'center',marginHorizontal: 12,backgroundColor: Colors.darkSeafoamGreen,borderRadius:10,width:'13%'}}>
                            <Text style={{  fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.black }}>{this.state.monthlyPrice + '$'}</Text>
                            </View> */}
                        <View style={{ width: '95%', marginHorizontal: 20 }}>
                            <MultiSlider
                                selectedStyle={{ backgroundColor: Colors.darkSeafoamGreen }}
                                unselectedStyle={{ backgroundColor: 'silver', }}
                                markerStyle={{ backgroundColor: Colors.darkSeafoamGreen, height: 15, width: 15 }}
                                trackStyle={{ borderRadius: 7, height: 3 }}
                                // values={[this.state.filteredData.distance]}
                                sliderLength={width - (width / 9.3)}
                                touchDimensions={{ height: 100, width: 100, borderRadius: 15 }}
                                min={0}
                                max={500000}
                                value={this.state.monthlyPrice}
                                onValuesChangeFinish={(value) => this.setState({ monthlyPrice: Math.round(value) })}
                                step={1}
                                enableLabel
                                enabledOne
                            />
                        </View>
                    </View>
                    <Text style={[styles.locationModalText, { marginHorizontal: 12, marginTop: 30 }]}>{translate("BEDROOMS")}</Text>
                    <View>
                        <FlatList horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.barRoomList}
                            renderItem={this.badModalList}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <Text style={[styles.locationModalText, { marginHorizontal: 12, marginTop: 30 }]}>{translate("BATHROOMS")}</Text>
                    <View>
                        <FlatList horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.bathRoomList}
                            renderItem={this.furnishingList}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View>
                        <Text style={[styles.locationModalText, { marginHorizontal: 12, marginTop: 30 }]}>{translate("FURNISHING")}</Text>
                        <View>
                            <FlatList horizontal
                                showsHorizontalScrollIndicator={false}
                                data={this.state.furnishingList}
                                renderItem={this.renderFuritures}
                                extraData={this.state}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={{ marginHorizontal: 6, marginTop: 25 }}>
                        <Text style={[styles.availbleText, { marginHorizontal: 6, }]}>{translate("AMENITIES")}</Text>
                        <FlatList numColumns={5}
                            data={this.state.amenitiesList}
                            renderItem={this.renderAmenities}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ marginTop: 25 }}>
                        <Text style={[styles.availbleText, { marginHorizontal: 12, }]}>{translate("AVAILABLEFROM")}</Text>
                        <AvailablefromRooms
                            onApplayFilter={() => this.onApplayFilter()}
                            resetFilter={() => this.resetFilter()}
                            filterDate={(val) => this.setState({ filterDate: val })}
                            resetButton={this.state.resetButton}
                            onSelect={(type) => this.setState({ selectedType: type })}
                        />
                    </View>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    filteView: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginHorizontal: 11, marginTop: 12 },
    filterText: { flex: 1, fontSize: 21, fontFamily: Fonts.segoeui_bold },
    closeIcon: { height: 15, width: 15, resizeMode: 'contain' },
    propertyText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.greyishBrown, marginHorizontal: 11, marginTop: 23 },
    locationModalText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.greyishBrown },
    locationView: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderWidth: 1, paddingVertical: 12, borderColor: Colors.pinkishGreyTwo, borderRadius: 4, marginTop: 13 },
    locationInrView: { flexDirection: 'row', marginLeft: 16, alignItems: 'center' },
    locationIcon: { height: 10, width: 10, resizeMode: 'contain' },
    locationText: { fontSize: 12, fontFamily: Fonts.segui_semiBold, marginLeft: 9 },
    availbleText: { fontSize: 14, fontFamily: Fonts.segoeui_bold, color: Colors.greyishBrown, },
    searchView: { flexDirection: 'row', alignItems: 'center', marginHorizontal: 3, height: 50, backgroundColor: Colors.whiteTwo, marginTop: 20, borderRadius: 4, borderWidth: 0.5 },
    inputStyle: { marginLeft: 10, fontSize: 12, fontFamily: Fonts.segoeui, color: Colors.pinkishGrey, width: '85%', height: Platform.OS === 'ios' ? 30 : 50 },
})
