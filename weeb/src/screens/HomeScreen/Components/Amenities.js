import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native'
import Colors from '../../../Lib/Colors'
import { getWidth } from '../../../Lib/Constant'
import Fonts from '../../../Lib/Fonts'
import images from '../../../Lib/Images'

const {width, height} = Dimensions.get('window')
export default class Amenities extends Component {
    constructor(props){
        super(props)
        this.state={

        }
    }
    render() {
        const {item, amenitiesSingle}= this.props
        return (
            <TouchableOpacity onPress={()=>{this.props.onAmenitiesSingle(item)}} style={{marginTop:13, alignItems:'center', margin: getWidth(5),}}>
               <Image style={styles.iconView} source={ amenitiesSingle == item.id ? item.routh : item.AmenitiesIcon}/>
               <Text style={styles.iconText}>{item.AmenitiesText}</Text>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    iconView: {height:60, width:getWidth(60), resizeMode:'contain'},
    iconText: {fontSize:12, fontFamily: Fonts.segui_semiBold, color: Colors.greyishBrown,}

})
