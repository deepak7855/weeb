import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Colors from '../../../Lib/Colors'
import { getWidth } from '../../../Lib/Constant'
import Fonts from '../../../Lib/Fonts';
import Images from '../../../Lib/Images'
import { DatePicker, CustomSquareButton } from '../../../Components/Common/index'
import moment from 'moment';
import { translate } from '../../../Language';
export default class AvailablefromRooms extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showDataButton: false,
            isAvailableSelect: false,
            selectedDate: '',
            sendSelectedDate: '',
            showImmediatelyButon: false,
            selectedType: ''
        }
    }

    componentDidMount() {
        if (this.props.resetButton) {
            this.setState({
                showDataButton: false,
                showImmediatelyButon: false
            })
        }
    }

    date = (val) => {

        console.log('date return', val, moment(val).format('DD/MM/YYYY'), moment(val).format('DD-MM-YYYY'))
        this.setState({
            selectedDate: moment(val).format('DD-MM-YYYY'),
            sendSelectedDate: moment(val).format('YYYY-MM-DD'),
        })
        this.props.filterDate(moment(val).format('YYYY-MM-DD'))
    }

    onSelect = (type) => {
        this.setState({ selectedType: type })
        this.props.onSelect(type)
    }

    render() {
        const { onHideModal } = this.props
        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 14, marginHorizontal: 12 }}>
                    <CustomSquareButton color2={Colors.whiteTwo} state={this.state.selectedType=='date'} showButton={() => this.onSelect('date')} width={'45%'} bordercolor={Colors.lightMint} label={'Date'} backgroundcolor={Colors.darkSeafoamGreen} color={Colors.darkSeafoamGreen} />

                    <CustomSquareButton color2={Colors.whiteTwo} state={this.state.selectedType == 'immediately'} showButton={() => this.onSelect('immediately')} width={'45%'} bordercolor={Colors.lightMint} label={'Immediately'} backgroundcolor={Colors.darkSeafoamGreen} color={Colors.darkSeafoamGreen} />
                </View>
                {this.state.selectedType == 'date' ?
                    <View style={{ marginHorizontal: 10 }}>
                        <DatePicker
                            width={'87%'}
                            backgroundcolor={'white'}
                            color={Colors.brownishGrey}
                            leftimage={Images.calendar_icon}
                            marginleft={-10}
                            date={(val) => this.date(val)}
                            value={this.state.selectedDate}
                        />
                    </View>
                    :
                    null
                }
                <View style={{ flexDirection: 'row', marginTop: 20, borderTopWidth: 1, borderTopColor: Colors.whiteFive }}>
                    <TouchableOpacity onPress={this.props.resetFilter} style={[styles.btnFlexView, { borderRightWidth: 1, borderRightColor: Colors.whiteFive }]}>
                        <Text style={{ fontSize: 18, fontFamily: Fonts.segui_semiBold, color: Colors.brownishGreyTwo }}>{translate("Reset")}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.props.onApplayFilter} style={styles.btnFlexView}>
                        <Text style={styles.applyTetx}>{translate("Apply")}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    nonActiveBnt: { width: getWidth(164), paddingVertical: 10, alignItems: 'center', borderWidth: 1, borderColor: Colors.lightMint, borderRadius: 4 },
    btnText: { fontSize: 16, fontFamily: Fonts.segoeui, },
    activeBtn: { width: getWidth(164), backgroundColor: Colors.darkSeafoamGreen, paddingVertical: 10, alignItems: 'center', borderWidth: 1, borderColor: Colors.lightMint, borderRadius: 4 },
    btnFlexView: { flex: 0.5, alignItems: 'center', paddingVertical: 12 },
    applyTetx: { fontSize: 18, fontFamily: Fonts.segui_semiBold, color: Colors.darkSeafoamGreen }
})
