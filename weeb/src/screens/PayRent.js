import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  DeviceEventEmitter,
  Image,
  TextInput,
  Keyboard,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  RefreshControl
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import {Constant} from '../Lib/Constant';
import {handleNavigation} from '../navigation/routes';
import {Helper, Network, AlertMsg} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';

import { translate } from '../Language';
import {ProgressiveImage,FlootingButton,GoogleApiAddressList} from '../Components/Common/index';
export default class PayRent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: Helper.userData?.city,
      dataOfAreement: [],
      opener: false,
      dataOfFilter: [
        {iconChecked: images.filter_check, text: 'Full House', selected: true},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in House ',
          selected: true,
        },
        {iconChecked: images.filter_check, text: 'Chalet', selected: true},
        {
          iconChecked: images.filter_check,
          text: 'Apartment in Building',
          selected: true,
        },
      ],
      propertyType: '',
      lat: '',
      long: '',
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
      userType: Helper.userType,
      opener: false,
      checkInd: -1,
      modalVisibleForLocation:false,
    };
    AppHeader({
      ...this,
      leftHeide: true,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      searchClick: () => {
        this.goNotification();
      },
      searchIcon: Helper.notificationCount == 0 ?images.notification_icon:images.notification_goup ,
      search: true,
      location: true,
      addBtn: Helper.userType == 'OWNER' ? true : null,
      onAdd: () => {
        this.addPropartyScreen();
      },
      addItem: images.addproperty,
      menuClick: () => {
        this.menuClick();
      },
      city: this.state.city,
    });
  }

  componentDidMount() {
    // console.log('user data payrent', Helper.userData)
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getRentedProperty();
    });
    
    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        AppHeader({
          ...this,
          leftHeide: true,
          backgroundColor: Colors.lightMint10,
          leftIcon: images.black_arrow_btn,
          leftClick: () => {
            this.goBack();
          },
          searchClick: () => {
            this.goNotification();
          },
          searchIcon: images.notification_goup,
          search: true,
          location: true,
          addBtn: data?.type == 'OWNER' ? true : null,
          onAdd: () => {
            this.addPropartyScreen();
          },
          addItem: images.addproperty,
          menuClick: () => {
            this.menuClick();
          },
          city: Helper.userData?.city,
        });
        this.setState(
          {
            userType: data?.type,
          },
          () => {
            this.getRentedProperty();
          },
        );
      },
    );
  }

  getRentedProperty() {
    Network.isNetworkAvailable()
      .then((isConnected) => {
        if (isConnected) {
          this.setState({
            isLoading: true,
          });
          //  Helper.mainApp.showLoader();
          let url;
          let data = {};
          if (this.state.userType == 'OWNER') {
            url = 'owner-rented-properties' + '?page=' + this.state.currentPage;
          } else {
            url =
              'tenant-rented-properties' + '?page=' + this.state.currentPage;
          }
          ApiCall.ApiMethod({
            Url: url,
            method: 'POST',
            data: {
              type: this.state.propertyType,
              lat: this.state.lat,
              lng: this.state.long,
            },
          })
            .then((res) => {
              console.log('get  list rented property', res);
              Helper.mainApp.hideLoader();
              // Helper.showToast(res?.message)
              if (res?.status) {
                this.setState({
                  dataOfAreement: res?.data?.data,
                  isLoading: false,
                  emptymessage: false,
                  next_page_url: res?.data?.next_page_url,
                });
                return;
              }
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
                next_page_url: res?.data?.next_page_url,
                currentPage: 1,
              });
            })
            .catch((err) => {
              this.setState({
                isLoading: false,
                emptymessage: true,
                dataOfAreement: [],
                currentPage: 1,
              });
              Helper.mainApp.hideLoader();
              console.log('api err', err);
            });
        } else {
          Helper.showToast(AlertMsg.error.NETWORK);
        }
      })
      .catch(() => {
        Helper.showToast(AlertMsg.error.NETWORK);
      });
  }

  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
    }
    //   this._unsubscribe();
  }

  addPropartyScreen = () => {
    Helper.propertyData = {};
    handleNavigation({
      type: 'push',
      page: 'SelectPropertyType',
      navigation: this.props.navigation,
    });
  };

  goNotification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  openDrawer = () => {
    Keyboard.dismiss();
    handleNavigation({type: 'drawer', navigation: this.props.navigation});
  };
  menuClick = () => {
    this.props.navigation.openDrawer();
  };

  openFilter = () => {
    this.setState({opener: !this.state.opener});
  };
  toSelectSpecializatio = (index) => {
    let NewVar = [...this.state.dataOfFilter];
    NewVar[index].selected = !NewVar[index].selected;
    this.setState({dataOfFilter: NewVar});
  };
  selectItemGroup(value) {
    if (this.state.checkInd == value) {
      this.setState({checkInd: -1});
      this.setState({opener: false});
    } else {
      this.setState({checkInd: value});
      this.setState(
        {
          opener: false,
          propertyType: this.state.dataOfFilter[value]?.text,
        },
        () => {
          this.getRentedProperty()
        },
      );
    }
  }
//   openFilter = () => {
//     this.setState({opener: !this.state.opener});
//   };

  renderOfFilter = ({item, index}) => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.toSelectSpecializatio(index)}
          style={styles.ViewRenderFilter}>
          <Image
            style={item.selected ? styles.checkIcon : styles.uncheckIcon}
            source={item.iconChecked}
          />
          <Text
            style={item.selected ? styles.selectedText : styles.unselectText}>
            {item.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  onPay = (item) => {
    console.log('owner property', item);
    handleNavigation({
      type: 'push',
      page: 'PaymentDetails',
      navigation: this.props.navigation,
      passProps: {propertyData: item},
    });
  };

  renderTenantProperty = ({item}) => {
    console.log('tenant item', item);
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.onPay(item);
          }}>
          <View style={styles.view1}>
            <ProgressiveImage
              source={{
                uri: item?.property?.propertyphotos
                  ? item?.property?.propertyphotos[0]?.imgurl
                  : '',
              }}
              style={styles.iconHome}
              resizeMode="cover"
            />
            <View style={{marginLeft: 13, width: '100%'}}>
              <Text style={styles.textTitle}>{item.property?.title}</Text>
              <View style={styles.view3}>
                <Image style={styles.iconLocation} source={images.location} />
                <Text style={[styles.textAddress, {width: '75%'}]}>
                  {item?.property?.location}
                </Text>
              </View>
              <View style={styles.view4}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 0.6,
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textRate}>KWD {item.rent}</Text>
                  <Text style={[styles.textRate, {fontFamily: Fonts.segoeui}]}>
                    {item.rateType}
                  </Text>
                  <View style={styles.dot} />
                  <Text style={[styles.textPropType, {flex: 1}]}>
                    {item?.property?.type}
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: Fonts.segui_semiBold,
                      color: Colors.cherryRed,
                      right: 0,
                    }}>
                    {item?.unpaid_amount > 0 ?'Unpaid ' + 'KWD ' + item?.unpaid_amount:null}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderOwnerProperty = ({item}) => {
    console.log('owner item', item);
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.onPay(item);
          }}>
          <View style={styles.view1}>
            <ProgressiveImage
              source={{
                uri: item?.propertyphotos
                  ? item?.propertyphotos[0]?.imgurl
                  : '',
              }}
              style={styles.iconHome}
              resizeMode="cover"
            />
            <View style={{marginLeft: 13, width: '100%'}}>
              <Text style={styles.textTitle}>{item?.title}</Text>
              <View style={styles.view3}>
                <Image style={styles.iconLocation} source={images.location} />
                <Text style={styles.textAddress}>{item?.location}</Text>
              </View>
              <View style={styles.view4}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 0.6,
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textRate}>KWD {item.rent}</Text>
                  <Text style={[styles.textRate, {fontFamily: Fonts.segoeui}]}>
                    {item.rateType}
                  </Text>
                  <View style={styles.dot} />
                  <Text style={[styles.textPropType, {flex: 1}]}>
                    {item?.type}
                  </Text>
                </View>
                {item?.bookings ? (
                  <View>
                    <Text
                      style={{
                        fontSize: 12,
                        fontFamily: Fonts.segui_semiBold,
                        color: Colors.cherryRed,
                        right: 0,
                      }}>
                      {item?.bookings[0]?.unpaid_amount > 0? 'Unpaid ' + 'KWD ' + item?.bookings[0]?.unpaid_amount:null}
                    </Text>
                  </View>
                ) : null}
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(() => {
      this.setState(
        {
          currentPage: 1,
          refreshing: false,
          propertyType: '',
          address: '',
          lat: '',
          long: '',
        },
        () => {
          this.getRentedProperty();
        },
      );
    }, 2000);
  };
  handleAddress = (data) => {
    this.setState({
      address: data?.addressname,
      lat: data?.lat,
      long: data?.long,
    },()=>{
      this.getRentedProperty();
    });
  };
  render() {
    return (
      <View style={styles.container}>
        {/* <StatusBarCustom backgroundColor={Colors.lightMint10} translucent={false} /> */}
        <View style={{height: 90, backgroundColor: Colors.lightMint}}>
          <View style={styles.searchView}>
            <Image style={styles.searchIcon} source={images.search_icon} />
            <TouchableOpacity
            onPress={() => this.setState({modalVisibleForLocation: true})}
            style={[styles.inputStyle,{justifyContent: 'center'}]}>
              <Text style={{color:Colors.pinkishGrey}}>{translate('SearchbyLocationorAddress')}</Text>
            </TouchableOpacity>
          </View>
          <GoogleApiAddressList
              modalVisible={this.state.modalVisibleForLocation}
              hideModal={() => {
                this.setState({modalVisibleForLocation: false});
              }}
              onSelectAddress={this.handleAddress}
            />
        </View>
        
        {this.state.userType == 'OWNER' ? null : (
          <View style={styles.viewTextTop}>
            <View style={styles.viewTextTop1}>
              <Text style={styles.textTop}>{translate('SelectPropertyForPayRent')}</Text>
              <Text style={styles.text16}>
                {this.state.dataOfAreement?.length == 0
                  ? 0
                  : this.state.dataOfAreement?.length}
              </Text>
            </View>
            <View style={styles.lineTop} />
          </View>
        )}

        <FlatList
          data={this.state.dataOfAreement}
          renderItem={
            this.state.userType == 'OWNER'
              ? this.renderOwnerProperty
              : this.renderTenantProperty
          }
          extraData={this.state}
          ListFooterComponent={() => {
            return this.state.isLoading ? <LoaderForList /> : null;
          }}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: Fonts.segoeui,
                  color: Colors.cherryRed,
                  textAlign: 'center',
                }}>
                {!this.state.emptymessage  ?  translate('Noproperty'):null }
              </Text>
            </View>
          )}
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          keyboardShouldPersistTaps={'handled'}
          refreshing={this.state.refreshing}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
        />
        {/* <TouchableOpacity
          onPress={() => this.openFilter(true)}
          style={styles.filterTouch}>
          <Image style={styles.iconFilter} source={images.property_filter} />
        </TouchableOpacity> */}
        {/* {this.state.opener ? (
          <View style={styles.filteView}>
            <View
              style={[
                styles.viewTopTextOfFilter,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.textPropTypeOfFilter}>Property Type</Text>
              <TouchableOpacity
                onPress={() => {
                  this.openFilter(false);
                }}
                style={{
                  height: 20,
                  width: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{height: 10, width: 10, resizeMode: 'contain'}}
                  source={images.close}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              data={this.state.dataOfFilter}
              renderItem={this.renderOfFilter}
              ItemSeparatorComponent={() => (
                <View style={styles.filterSeperator} />
              )}
            />
             
          </View>
        ) : null} */}
            <FlootingButton
          toSelectSpecializatio={(index) => this.selectItemGroup(index)}
          opener={this.state.opener}
          openFilter={() => this.openFilter()}
          closeFilter={() => {
            this.openFilter();
          }}
          filterlist={this.state.dataOfFilter}
          checkInd={this.state.checkInd}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo, paddingBottom: 10},
  ViewTopSearch: {
    flexDirection: 'row',
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
    paddingLeft: 13,
  },
  viewTop: {
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 13,
    paddingRight: 10,
    paddingVertical: 18,
    borderColor: Colors.whiteTwo,
    backgroundColor: Colors.lightMint,
  },
  searchIcon: {width: 11, height: 11, resizeMode: 'contain'},
  textInputStyl: {fontSize: 12, lineHeight: 16, marginLeft: 10, height: 40},
  iconHome: {width: 67, height: 57, resizeMode: 'contain'},
  textTitle: {
    fontSize: 14,
    lineHeight: 16,
    color: Colors.blackTwo,
    fontFamily: Fonts.segoeui_bold,
  },
  view3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 6,
  },
  iconLocation: {width: 7, height: 9, resizeMode: 'contain'},
  textAddress: {
    fontSize: 12,
    lineHeight: 16,
    color: Colors.brownishGreyTwo,
    fontFamily: Fonts.segoeui,
    marginLeft: 5,
  },
  view4: {flexDirection: 'row', alignItems: 'center'},
  textRate: {fontSize: 12, fontFamily: Fonts.segoeui_bold},
  dot: {
    width: 2,
    height: 2,
    borderRadius: 1,
    backgroundColor: Colors.pinkishGrey,
    marginHorizontal: 7,
  },
  textPropType: {
    fontSize: 12,
    color: Colors.brownishGrey,
    fontFamily: Fonts.segui_semiBold,
  },
  filterTouch: {position: 'absolute', bottom: 20, right: 12},
  iconFilter: {width: 56, height: 56, resizeMode: 'contain'},
  view1: {flexDirection: 'row', marginVertical: 7, paddingLeft: 13},
  viewTextTop: {paddingLeft: 13, paddingRight: 11, paddingTop: 10},
  viewTextTop1: {flexDirection: 'row', justifyContent: 'space-between'},
  textTop: {
    fontSize: 16,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  text16: {
    fontSize: 16,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  lineTop: {
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginTop: 10,
  },
  Seperator: {
    borderRadius: 0.5,
    backgroundColor: Colors.warmGrey,
    height: 1,
    opacity: 0.18,
    marginLeft: 13,
    marginRight: 11,
    marginVertical: 10,
  },
  filteView: {
    position: 'absolute',
    bottom: 90,
    right: 13,
    backgroundColor: Colors.whiteTwo,
    elevation: 5,
    width: 200,
    borderRadius: 10,
  },
  viewTopTextOfFilter: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: Colors.whiteNine,
    borderBottomColor: Colors.whiteFive,
    borderBottomWidth: 1,
  },
  textPropTypeOfFilter: {
    fontSize: 14,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
  },
  filterSeperator: {
    borderWidth: 0.5,
    backgroundColor: Colors.warmGrey,
    opacity: 0.05,
  },
  ViewRenderFilter: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 23,
    backgroundColor: Colors.whiteTwo,
    alignItems: 'center',
  },
  checkIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.darkSeafoamGreen,
  },
  uncheckIcon: {
    width: 12,
    height: 8,
    resizeMode: 'contain',
    tintColor: Colors.pinkishGrey,
  },
  unselectText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.pinkishGrey,
  },
  selectedText: {
    fontSize: 14,
    paddingVertical: 10,
    fontFamily: Fonts.segoeui,
    marginLeft: 11,
    color: Colors.black,
  },
  searchIcon: {height: 12, width: 12, resizeMode: 'contain', marginLeft: 14},
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 12,
    height: 50,
    backgroundColor: Colors.whiteTwo,
    marginTop: 20,
    borderRadius: 4,
  },
  inputStyle: {
    width: '85%',
    height: Platform.OS === 'ios' ? 30 : 50,
    marginLeft: 10,
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.pinkishGrey,
  },
});
