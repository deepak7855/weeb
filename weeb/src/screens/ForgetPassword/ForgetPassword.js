import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
} from 'react-native';
import Images from '../../Lib/Images';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
const { height, width } = Dimensions.get('window');
import { TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Inputs, CircleButton } from '../../Components/Common/index';
import { handleNavigation } from '../../navigation/routes';
import { getWidth } from '../../Lib/Constant';
import { ApiCall } from '../../Api/index'
import { Constant } from '../../Lib/Constant';
import { Validation, Network, AlertMsg, Helper, RegexValid } from '../../Lib/index'
import { translate } from '../../Language';
export default class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    };
  }

  anNotAccount = () => {
    handleNavigation({ type: 'push', page: "Signup", navigation: this.props.navigation });
  }

  afterOtpFeel = () => {
    handleNavigation({ type: 'push', page: "VerifyOtp", navigation: this.props.navigation });
  }
  userForgetPassword = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        if (Validation.checkEmail('Email', this.state.email)) {
          Helper.resetEmail = this.state.email;
          Helper.mainApp.showLoader()
          let forgetEmail = {
            email:this.state.email
          }
          ApiCall.ApiMethod({ Url: 'forgot', method: 'POST', data: forgetEmail }).then((res) => {
            console.log('forget password in data', res)

            if (res.status) {
              Helper.tempValue = res?.data?.otp
              Helper.showToast(res?.message ? res?.message : AlertMsg?.success?.LOGIN)
              Helper.mainApp.hideLoader()
              this.props.navigation.navigate('VerifyOtp', { screenName: 'forget' })
              return
            } else {
              Helper.showToast(res?.message ? res?.message : AlertMsg.error.NETWORK);
              Helper.mainApp.hideLoader()
              return
            }


          }).catch((err) => {
            Helper.mainApp.hideLoader()
            // Helper.showToast(err?.data.message ? err?.data.message : AlertMsg.error.NETWORK);
            console.log('forget password error', err)
            return
          });
        }
      } else {
        Helper.showToast(AlertMsg.error.NETWORK);
      }
    }).catch((err) => {
      Helper.showToast(AlertMsg.error.NETWORK);
    })


  }
  render() {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps={'handled'}>
          <Image source={Images.signup_bg} style={styles.signUpBg} />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={Images.sign_up_logo} style={styles.logo} />
            <Text style={styles.title}>{translate("ForgotPassword")}</Text>
          </View>
          <View style={[styles.authBox, { backgroundColor: Colors.whiteFive }]}>
            <View
              style={{
                margin: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.authTitleText}>{translate("DontWorry")}</Text>
            </View>

            <View
              style={{
                margin: 0,
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 6
              }}>
              <Text style={styles.authMessageText}>
                {translate("Justenteryouremail")}.{' '}
              </Text>
            </View>

            <View
              style={{
                margin: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.authSendText}>
                {translate("Wellsendyouanemail")}.{' '}
              </Text>

              <View style={{ marginTop: 10 }}>
                <Inputs
                  width={'85%'}
                  backgroundcolor={'white'}
                  color={Colors.brownishGrey}
                  rightimage={Images.email}
                  placeholder={'Email address'}
                  marginleft={10}
                  marginright={0}
                  right={5}
                  keyboard={'email-address'}
                  leftimage={RegexValid.EmailRegex.test(this.state.email) ? Images.tick : null}
                  onChangeText={(text) => { this.setState({ email: text }) }}
                  value={this.state.email}
                />
              </View>
            </View>
            <CircleButton
              navigate={() => this.userForgetPassword()}

              fontsize={18} labelfonts={Fonts.segoeui_bold} colors={Colors.lightMint} arrowimage={Images.arrow_green} label={'Send'} />
          </View>
          <View style={{ marginBottom:Helper.hasNotch ? 10:0,position: 'absolute', bottom: 10, width: '100%', marginTop:30 }}>
            <TouchableOpacity onPress={() => { this.anNotAccount() }}>
              <Text style={{ fontSize: 14, fontFamily: Fonts.segoeui, textAlign: 'center' }}>{translate("Donthaveanaccount")}<Text style={{ color: Colors.darkSeafoamGreen, textAlign: "center", textDecorationLine:'underline' }}>{translate("SignUp")}</Text></Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  signUpBg: {
    height: 500,
    width: '100%',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    overflow: 'hidden',
    position: 'absolute',
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  authBox: {
    width: getWidth(340),
    borderRadius: 15,
    alignSelf: 'center',
    paddingHorizontal: 14,
    marginTop: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    //elevation: 5,
    //borderWidth: 1,
  },
  logo: {
    resizeMode: 'contain',
    width: 90,
    height: 100,
    marginTop: 50,
    borderColor: 'black',
  },
  title: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 18,
  },
  authTitleText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.black,
  },
  authMessageText: {
    fontSize: 16,
    fontFamily: Fonts.segoeui,
    color: Colors.black,
    textAlign: 'center',
  },
  authSendText: {
    fontSize: 15,
    fontFamily: Fonts.segoeui,
    color: Colors.brownishGreyTwo,
    textAlign: 'center',
  },
  borderStyleBase: {
    width: 44,
    height: 52,
  },

  borderStyleHighLighted: {
    borderColor: Colors.pinkishGrey,
  },

  underlineStyleBase: {
    width: 44,
    height: 52,
    borderWidth: 1,
    //borderBottomWidth: 1,
    color: Colors.black,
    borderRadius: 12,
    margin: 5,
  },

  underlineStyleHighLighted: {
    borderColor: Colors.pinkishGrey,
  },
  signUpCircle: {
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: Colors.lightMint,
    justifyContent: 'center',
  },
  verifyImageIcon: {
    height: 13,
    width: 19,
    resizeMode: 'contain',
    transform: [{ rotate: '180deg' }],
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    //  marginLeft: -10,
    //marginRight: 3,
  },
});
