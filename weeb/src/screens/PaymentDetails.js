import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  Text,
  SafeAreaView,
  DeviceEventEmitter,
} from 'react-native';
import AppHeader from '../Components/AppHeader';
import StatusBarCustom from '../Components/Common/StatusBarCustom';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import images from '../Lib/Images';
import {Constant} from '../Lib/Constant';
import {handleNavigation} from '../navigation/routes';
import {Helper, Network, AlertMsg,downloadFile} from '../Lib/index';
import {ApiCall, LoaderForList} from '../Api/index';
import {ProgressiveImage} from '../Components/Common/index';
import { translate } from '../Language';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import FileViewer from 'react-native-file-viewer';
export default class PaymentDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PaymentDetailsData: '',
      userType: Helper.userType == 'OWNER' ?'OWNER':'TENANT',
      propertyData: this.props.route.params?.propertyData,
      propertyId: this.props.route.params?.propertyData?.property_id ?this.props.route.params?.propertyData?.property_id:this.props.route.params?.propertyData?.id,
      unPaidMonth: '',
      next_page_url: '',
      currentPage: 1,
      refreshing: false,
      isLoading: false,
      emptymessage: false,
      year:new Date().getFullYear()
    };
    AppHeader({
      ...this,
      leftHeide: false,
      backgroundColor: Colors.lightMint10,
      leftIcon: images.black_arrow_btn,
      leftClick: () => {
        this.goBack();
      },
      title: 'Payment Details',
      searchClick: () => {
        this.notification();
      },
      searchIcon: images.notification_goup,
      search: true,
    });
  }

  componentDidMount() {
    this.getPaymentHistory();
    this.listner = DeviceEventEmitter.addListener(
      Constant.USER_TYPE,
      (data) => {
        //console.log('payment details',data)
        this.setState({
          userType: data?.type,
        });
      },
    );
  }
  
  getPaymentHistory = () => {
    Network.isNetworkAvailable().then((isConnected) => {
      if (isConnected) {
        let data = {
          property_id: this.state.propertyId,
          year: this.state.year,
        };
        this.setState({
          isLoading: true,
        });
        ApiCall.ApiMethod({
          Url: 'get-payment-history',
          method: 'POST',
          data: data,
        }).then((res) => {
          console.log('payment history---------------',res);
          if (res?.status) {
            let tempData = [];
            res?.data.map((item) => {
              if (item?.paid == 0) {
                tempData.push({
                  month: item?.month,
                  rent: item?.rent ,
                  serviceCharge:item?.service_charge,
                });
              }
            });
            this.setState({
              PaymentDetailsData: res?.data,
              unPaidMonth: tempData,
              isLoading: false,
              emptymessage: false,
              next_page_url: res?.data?.next_page_url,
            });
          }else{
            this.setState({
              PaymentDetailsData: [],
              isLoading: false,
              emptymessage: true,
            });
          }
        });
      }
    });
  };
  componentWillUnmount() {
    if (this.listner) {
      this.listner.remove();
    }
  }

  goBack = () => {
    handleNavigation({type: 'pop', navigation: this.props.navigation});
  };

  notification = () => {
    handleNavigation({
      type: 'push',
      page: 'Notification',
      navigation: this.props.navigation,
    });
  };

  payNow = () => {
    handleNavigation({
      type: 'push',
      page: 'Payment',
      navigation: this.props.navigation,
      passProps: {
        unPaidMonths: this.state.unPaidMonth,
        propertyData: this.state.propertyData,
        year: this.state.year,
      },
    });
  };
   createPDF = async(item) => {
    let options = {
      html: `<h1>Pay slip</h1>
              <h1>rent</h1>  <h1>${item?.rent}</h1>
              <h1>service charge</h1>  <h1>${item?.service_charge}</h1>
              <h1>total</h1>  <h1>${item?.rent + item?.service_charge}</h1>
              <h1>property name</h1> <h1>${this.state.userType == 'OWNER' ?this.state.propertyData?.title:this.state.propertyData?.property?.title}</h1>
              `,
      fileName: 'test',
      directory: 'Documents',
    };

    let file = await RNHTMLtoPDF.convert(options)
     console.log(file.filePath);
    //downloadFile(file.filePath);
    FileViewer.open(file.filePath)
   // alert(file.filePath);
  }
  PaymentDetailsList = ({item}) => {
    return (
      <View style={{marginHorizontal: 12}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.txt_month}>{item.month}</Text>
          <Text
            style={[
              styles.txt_pay,
              {
                backgroundColor: item.paid
                  ? Colors.darkSeafoamGreen
                  : Colors.cherryRed,
              },
            ]}>
            {item.paid ? 'Paid' : 'Unpaid'}
          </Text>
        </View>
        <View style={[styles.boderBlack, {marginHorizontal: 0}]}></View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.txt_Label}>{translate("MonthlyRent")}</Text>
          <Text style={styles.txt_LabelPrice}>KWD {item?.rent}</Text>
        </View>
        <View style={styles.boderGrey}></View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.txt_Label}>{translate("ServiceFee")}</Text>
          <Text style={styles.txt_LabelPrice}>KWD {item?.service_charge}</Text>
        </View>
        <View style={styles.boderGrey}></View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.txt_Total}>{translate("Total")}</Text>
          <Text style={styles.txt_TotalPrice}>KWD {item?.rent + item?.service_charge}</Text>
        </View>
        {item.pdf == '' ? (
          <View>
            <View style={styles.boderGrey}></View>
            <TouchableOpacity
            onPress={()=>this.createPDF(item)}
            style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.txt_downloadReceipt}>{translate("DownloadReceipt")}</Text>
              <Image
                style={{width: 58, height: 18, resizeMode: 'contain'}}
                source={images.pdf}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={[styles.boderBlack, {marginHorizontal: 0}]}></View>
      </View>
    );
  };

  previousYear = () =>{
    let lastyear = new Date();
    this.setState({
        year:this.state.year-1,
        PaymentDetailsData:'',
        unPaidMonth:''
    },()=>{
      this.getPaymentHistory()
    })
  }

  nextYear = () =>{
    let lastyear = new Date();
    this.setState({
        year:this.state.year+1,
        PaymentDetailsData:'',
        unPaidMonth:''
    },()=>{
      this.getPaymentHistory()
    })
  }
  sendNotificationToTenant = () =>{
    Network.isNetworkAvailable().then((isConnected) => {
      if(isConnected){
        Helper.mainApp.showLoader();
        ApiCall.ApiMethod({Url: 'notify-tenant-rent', method: 'POST',data:{property_id:this.state.propertyId}}).then((res) => {
          Helper.mainApp.hideLoader();
          if(res?.status){
            Helper.mainApp.hideLoader();
              Helper.showToast(res?.message)
          }else{
            Helper.showToast(translate('Somethingwentwrong,pleasetryagainlater'));
          }
        }).catch((err)=>{
          Helper.mainApp.hideLoader();
            console.log('err',err)
        })
      }
    })
  }
  render() {
    console.log('user type',this.state.userType)
    return (
      <SafeAreaView style={styles.container}>
        <StatusBarCustom
          backgroundColor={Colors.lightMint10}
          translucent={false}
        />
        <View style={{flex: 1}}>
          <View style={{backgroundColor: Colors.lightMint,paddingVertical:10}}>
            <View style={[styles.mainView, {marginTop: 10}]}>
              {this.state.userType == 'OWNER' ?
               <ProgressiveImage
                source={{
                  uri: this.state.propertyData?.propertyphotos
                    ? this.state.propertyData?.propertyphotos[0]
                        ?.imgurl
                    : '',
                }}
                style={styles.img_Property}
                resizeMode="cover"
              /> 
              :
                 <ProgressiveImage
                source={{
                  uri: this.state.propertyData?.property?.propertyphotos
                    ? this.state.propertyData?.property?.propertyphotos[0]
                        ?.imgurl
                    : '',
                }}
                style={styles.img_Property}
                resizeMode="cover"
              /> 
              }
              <View style={{marginLeft: 13,justifyContent:"space-between"}}>
                <Text style={styles.txt_Lighhouse}>
                  {this.state.userType == 'OWNER' ?this.state.propertyData?.title:this.state.propertyData?.property?.title}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Image
                    style={styles.icon_Location}
                    source={images.profile_location}
                  />
                  <View style={{width:'90%'}}>
                  <Text  style={styles.txt_PropertyLocation}>
                    {this.state.userType == 'OWNER' ?this.state.propertyData?.location:this.state.propertyData?.property?.location}
                  </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text style={styles.txtPrice}>
                    KWD{this.state.propertyData?.rent}
                  </Text>
                  <Text style={styles.txt_Property}>
                    {this.state.propertyData?.property?.type}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 10,
              marginTop: 13,
              marginBottom: 15,
              justifyContent:'space-between',
              alignItems: 'center',
            }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.txt_UnpaidAmount}>{translate("UnpaidAmount")}</Text>
            <Text style={styles.txt_UnpaidPrice}>KWD {this.state.userType == 'OWNER' ?this.state.propertyData?.bookings[0]?.unpaid_amount:this.state.propertyData?.unpaid_amount}</Text>
            </View>
            {this.state.userType == 'OWNER'
            ?
              <TouchableOpacity
              onPress={() =>this.sendNotificationToTenant()}
              style={{flexDirection: 'row', alignItems: 'center',justifyContent:"space-around",borderWidth:1,borderColor: Colors.darkSeafoamGreen,width:"40%",height:30,paddingHorizontal:7}}>
                  <Image source={images.requested_active} style={{height:10,width:10,tintColor: Colors.darkSeafoamGreen}} resizeMode={'contain'}/>
                  <Text style={{color: Colors.darkSeafoamGreen,fontSize:12,fontFamily: Fonts.segoeui}}>{translate("SendNotification")}</Text>
              </TouchableOpacity>
            :
            null
            }
          </View>

          <View style={styles.boderBlack}></View>

          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 12,
              alignItems: 'center',
            }}>
              <TouchableOpacity
              onPress={() =>this.previousYear()}
              >
            <Image
              style={styles.icon_LeftArrow}
              source={images.year_left_active}
            />
            </TouchableOpacity>
            <Text style={styles.txt_Year}>{translate("YEAR")} {this.state.year}</Text>
            <TouchableOpacity
             onPress={() =>this.nextYear()}
            >
            <Image style={styles.icon_RightArrow} source={images.year_right} />
            </TouchableOpacity>
          </View>

          <View style={styles.boderBlack}></View>

          <FlatList
            style={{flex: 1, marginBottom: 20}}
            showsVerticalScrollIndicator={false}
            data={this.state.PaymentDetailsData}
            renderItem={this.PaymentDetailsList}
            extraData={this.state}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={() => {
              return this.state.isLoading ? <LoaderForList /> : null;
            }}
            ListEmptyComponent={() => (
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop:10,
                }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: Fonts.segoeui,
                    color: Colors.cherryRed,
                    textAlign: 'center',
                  }}>
                  {this.state.emptymessage == false ? null : 'No Rent'}
                </Text>
              </View>
            )}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            keyboardShouldPersistTaps={'handled'}
            refreshing={this.state.refreshing}
          />
        </View>

        {this.state.userType == 'TENANT' && !this.state.isLoading && this.state.PaymentDetailsData.length >0 && this.state.unPaidMonth?.length >0
        ? 
        (
          <TouchableOpacity
            onPress={() => {
              this.payNow();
            }}
            style={{width: '100%', height: 50}}>
            <Text style={styles.btn_PaymentNow}>{translate("PaymentNow")}</Text>
          </TouchableOpacity>
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: Colors.whiteTwo},
  img_Property: {width: 67, height: 57, resizeMode: 'contain'},
  txt_Lighhouse: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 14,
    color: Colors.blackTwo,
  },
  txt_PropertyLocation: {
    color: Colors.brownishGreyTwo,
    fontSize: 12,
    marginLeft: 6,
    fontFamily: Fonts.segoeui,
  },
  icon_Location: {
    height: 8,
    width: 8,
    resizeMode: 'contain',
    tintColor: Colors.brownishGreyTwo,
  },
  txtPrice: {
    fontFamily: Fonts.segoeui_bold,
    fontSize: 12,
    color: Colors.darkSeafoamGreen,
  },
  txt_Property: {
    fontFamily: Fonts.segui_semiBold,
    fontSize: 12,
    color: Colors.marigold,
    marginLeft: 16,
  },
  txt_UnpaidAmount: {
    fontFamily: Fonts.segoeui,
    fontSize: 16,
    color: Colors.brownishGrey,
  },
  txt_UnpaidPrice: {
    fontFamily: Fonts.segui_semiBold,
    fontSize: 16,
    color: Colors.cherryRed,
    marginLeft: 5,
  },
  boderBlack: {
    backgroundColor: Colors.blackThree,
    height: 0.5,
    marginHorizontal: 12,
    opacity: 0.6,
  },
  icon_LeftArrow: {width: 18, height: 18, resizeMode: 'contain'},
  txt_Year: {
    flex: 1,
    fontFamily: Fonts.segoeui_bold,
    fontSize: 16,
    color: Colors.blackTwo,
    textAlign: 'center',
    paddingVertical: 14,
  },
  icon_RightArrow: {width: 18, height: 18, resizeMode: 'contain'},
  txt_month: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.segoeui_bold,
    color: Colors.blackTwo,
    paddingVertical: 12,
  },
  txt_pay: {
    fontSize: 12,
    fontFamily: Fonts.segoeui,
    color: Colors.whiteTwo,
    paddingHorizontal: 11,
    borderRadius: 2,
  },
  txt_Label: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.blackTwo,
    paddingVertical: 11,
  },
  txt_LabelPrice: {
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.blackTwo,
  },
  boderGrey: {backgroundColor: Colors.warmGrey, height: 0.5, opacity: 0.5},
  txt_Total: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
    paddingVertical: 11,
  },
  txt_TotalPrice: {
    fontSize: 14,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.blackTwo,
    paddingVertical: 11,
  },
  txt_downloadReceipt: {
    flex: 1,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    color: Colors.darkSeafoamGreen,
    paddingVertical: 11,
  },
  btn_PaymentNow: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    fontFamily: Fonts.segui_semiBold,
    color: Colors.darkSeafoamGreen,
    paddingTop: 10,
    paddingBottom: 15,
  },
  mainView: {flexDirection: 'row', paddingHorizontal: 12, alignItems: 'center'},
});
