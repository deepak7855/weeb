import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Animated,TouchableOpacity, Dimensions } from 'react-native'
import images from '../Lib/Images'
import image from '../Lib/Images'
import Fonts from '../Lib/Fonts'
import Colors from '../Lib/Colors'
import {ProgressiveImage} from '../Components/Common/index'
import moment from 'moment'
const { width, height } = Dimensions.get('window')
export default class WillasList extends React.PureComponent {
    render() {
        const { item, iconWillas } = this.props
      // console.log('imag', item?.propertyphotos[0]?.imgurl);
        return (
                <Animated.View style={styles.boxMainView}>
                <TouchableOpacity onPress={() => { this.props.onPress() }}>
                    <ProgressiveImage
                        source={{uri: item?.propertyphotos[0]?.imgurl} }
                        style={styles.willasImg}
                        resizeMode="cover"
                    />
                    {/* <Image  style={styles.willasImg} source={{url:item.iconWillas} /> */}
                  </TouchableOpacity>
                    <View style={{ alignItems: 'flex-end', }}>
                        <View style={styles.textView}>
                        <Text numberOfLines={1} style={styles.immedText}>{item?.available_date ? moment(item?.available_date).format('DD/MM/YYYY') : 'Immediately'} </Text>
                        </View>
                    </View>
                    <View style={styles.dolorMainView}>
                    <Text numberOfLines={1} style={styles.dolorText}>KWD { item?.rent}/m</Text>
                    <Text numberOfLines={1} style={[styles.fullButton, { width: 55, textAlign: 'center' }]}>{ item?.type}</Text>
                    </View>
                    <View style={[styles.locationMainView, {left:5}]}>
                        <Image style={styles.locationIcon} source={images.location}/>
                    <Text numberOfLines={1} style={styles.addressText}>{item?.location} </Text>
                    </View>
                    <View style={styles.callView}>
                        <TouchableOpacity onPress={() =>this.props.onCall()}>
                           <Image style={styles.btnMain} source={images.Call_now}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() =>this.props.chat()}>
                        <Image style={styles.btnMain} source={images.chat_now}/>
                        </TouchableOpacity>
                    </View>
                </Animated.View>
        )
    }
}
const styles = StyleSheet.create({
    willasImg: { height: 149, width:(width-40)/2,alignSelf:'center', borderRadius:10 },
    immedText: { fontSize: 8, fontFamily: Fonts.segoeui, color: Colors.white, textAlign: 'center' },
    textView: { backgroundColor: Colors.dustyOrange, width: 50, marginRight: 15, top: -7, borderRadius: 2, alignItems: 'center' },
    fullButton: {fontSize:8, fontFamily: Fonts.segui_semiBold, color: Colors.cherryRed, borderWidth:0.5,  borderColor: Colors.whiteFive},
    dolorText: {width:'47%',fontSize:14, fontFamily: Fonts.segui_semiBold, color: Colors.darkSeafoamGreen, right:5},
    dolorMainView: {flexDirection:'row', justifyContent:'space-around', alignItems:'center'},
    addressText: {fontSize:10, fontFamily: Fonts.segoeui, width:130, marginLeft:5 },
    locationIcon: {height:10, width:10, resizeMode:'contain',tintColor:Colors.brownishGreyTwo},
    locationMainView: {flexDirection:'row', alignItems:'center'},
    boxMainView: { width:(width-40)/2, backgroundColor: Colors.whiteTwo, paddingBottom:20, margin:8, borderWidth:0.5, borderRadius:10, borderColor: Colors.whiteTwo, shadowOffset: {
        width: 0,
        height: 4,
    },
    elevation:4,
    shadowOpacity: 0.30,
    shadowRadius: 4.65, },
    callView: {flexDirection:'row', alignItems:'center', justifyContent:'space-evenly', marginTop:8},
    btnMain: {height:18, width:72, resizeMode:'contain'}
})
