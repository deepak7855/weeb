import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images'

export default class PropertyType extends Component {
    constructor(props){
        super(props)
        this.state={
        }
    }
    render() {
        const {item, index, PropirtSelect} = this.props
        return (
            <View style={{marginHorizontal:12,}}>
                <TouchableOpacity onPress={()=>{this.props.onSelectProperty(item)}} >
                    {
                        PropirtSelect == item.id ?
                        <Image style={styles.selectService} source={item.routh}/>
                        :
                        <View style={styles.propertyView}>
                            <Image style={styles.unSelectIcon} source={item.propertyTypeIcon }/>
                        </View>
                    }
                </TouchableOpacity>
                <Text style={{fontSize:12, fontFamily: Fonts.segoeui,}}>{item.sarviceName}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    propertyView: {backgroundColor:Colors.lightMint, paddingVertical:20, paddingHorizontal:20, marginTop:12, borderRadius:4 },
    unSelectIcon: {height:30, width:30, resizeMode:'contain' },
    selectService: {height:70, width:70, resizeMode:'contain', marginTop:12,}
})

// : item.routh
// item.isSelect ?