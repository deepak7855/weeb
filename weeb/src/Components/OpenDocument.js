import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';
import Swiper from 'react-native-swiper'
import images from '../Lib/Images';
import { getWidth } from '../Lib/Constant';
import {ProgressiveImage} from '../Components/Common/index';
export default class OpenDocument extends React.PureComponent {
        constructor(props) {
                super(props)
                this.state={}
        }
        render() {
                const {maodalHeid} = this.props
                return (
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: Colors.whiteTwo,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{height: 240, width: '100%'}}>
                      <Swiper
                        style={{}}
                        dotColor={'rgba(0,0,0,.2)'}
                        activeDot={
                          <View
                            style={{
                              backgroundColor: Colors.blackThree,
                              height: 5,
                              width: 5,
                              borderRadius: 5 / 2,
                              top: 75,
                              margin: 2,
                            }}></View>
                        }
                        dot={
                          <View
                            style={{
                              backgroundColor: Colors.black,
                              height: 5,
                              width: 5,
                              borderRadius: 5 / 2,
                              top: 75,
                              margin: 2,
                            }}></View>
                        }>
                        <View style={styles.slidView}>
                          {/* <Image style={{ height: 212, width: 328, }} source={images.id_card_view} /> */}
                          <ProgressiveImage
                            source={{
                              uri: this.props.front_id
                                ? this.props.front_id
                                : '',
                            }}
                            style={{height: 212, width: 328}}
                            resizeMode="cover"
                          />
                        </View>
                        <View style={styles.slidView}>
                          {/* <Image
                            style={{height: 212, width: 328}}
                            source={images.id_card_view}
                          /> */}
                          <ProgressiveImage
                            source={{
                              uri: this.props.back_id
                                ? this.props.back_id
                                : '',
                            }}
                            style={{height: 212, width: 328}}
                            resizeMode="cover"
                          />
                        </View>
                      </Swiper>
                    </View>
                    <Text style={{fontSize: 16}}>Front ID</Text>

                    <View>
                      <TouchableOpacity
                        onPress={() => {
                          maodalHeid();
                        }}
                        style={{marginTop: 100}}>
                        <Image
                          style={{height: 16, width: 16, resizeMode: 'contain'}}
                          source={images.close}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                );
        }
}

const styles = StyleSheet.create({
        slidView: {width:'100%', alignItems:'center'}
})