import React from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import images from '../Lib/Images';
import Fonts from '../Lib/Fonts';
import Colors from '../Lib/Colors';
import Helper from '../Lib/Helper';
import { ProgressiveImage } from '../Components/Common/index';
import moment from 'moment';
export default class BookingRequestViews extends React.PureComponent {
    render() {
        const {
          item,
          title,
          subTitle,
          onClickAgreement,
          onClick1,
          onClick2,
          onChat,
          onCall,
          titleOwner,
          RejectProperty,
          subTitleOwner,
          AcceptProperty,
          onClickOwner,
          onClickOwner1,
          OwnerTypeButton,
          ViewClickOwner,
          addAgreementOwner,
          viewAgreementOwner,
          viewDocBtn,
          adAgreementBtn,
          viewAgreementTenant,
        } = this.props;
        //  console.log('booking data', item?.property?.user);
        // console.log('okkk', item?.property?.created_at.split(" "))
        let timeAndDate = item?.property?.created_at ? item?.property?.created_at.split(" ") : [];
        return (
          <View style={styles.container}>
            <View style={styles.dateTimeViewCss}>
              <Text style={styles.samTextCss}>
               { moment(timeAndDate[0]).format('YYYY-MM-DD')}
              </Text>
              <View style={styles.samFlexView}>
                <Image source={images.clock} style={styles.samCssOfIcon} />
                <Text style={[styles.samTextCss]}>{ moment(timeAndDate[1]).format('h:mm a')}</Text>
              </View>
            </View>
            <View style={styles.cardViewOfCss}>
              {this.props.AgreementBtt && Helper.userType == 'OWNER' ? (
                <View
                  style={[
                    styles.userOfMainView,
                    {backgroundColor: Colors.ice},
                  ]}>
                  <Image
                    source={{uri: item?.user?.profile_picture}}
                    style={styles.userImageCss}
                  />
                  <View style={{marginLeft: 10, flex: 1}}>
                    <Text style={styles.userNameOfCss}>{item?.user?.name}</Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        top: -3,
                      }}>
                      <Image
                        source={images.location_pin}
                        style={styles.samCssOfIcon}
                      />
                      <Text
                        style={[
                          styles.samTextCss,
                          {color: Colors.brownishGrey},
                        ]}>
                        {item?.user?.city}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.samFlexView}>
                    <TouchableOpacity
                      onPress={() => {
                        onChat();
                      }}
                      style={{
                        backgroundColor: Colors.whiteTwo,
                        borderRadius: 35,
                      }}>
                      <Image
                        source={images.owner_chat}
                        style={styles.callChatIconCss}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        onCall();
                      }}
                      style={{
                        backgroundColor: Colors.whiteTwo,
                        borderRadius: 35,
                        marginLeft: 6,
                      }}>
                      <Image
                        source={images.owner_call}
                        style={styles.callChatIconCss}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <View style={[styles.userOfMainView]}>
                  <Image
                    source={{
                      uri:
                        Helper.userType == 'OWNER'
                          ? item?.user?.profile_picture
                          : item?.property?.user?.profile_picture,
                    }}
                    style={styles.userImageCss}
                  />
                  <View style={{marginLeft: 10, flex: 1}}>
                    <Text style={styles.userNameOfCss}>
                      {Helper.userType == 'OWNER'
                        ? item?.user?.name
                        : item?.property?.user?.name}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        top: -3,
                      }}>
                      <Image
                        source={images.location_pin}
                        style={styles.samCssOfIcon}
                      />
                      <Text
                        style={[
                          styles.samTextCss,
                          {color: Colors.brownishGrey},
                        ]}>
                        {Helper.userType == 'OWNER'
                          ? item?.user?.city
                          : item?.property?.user?.city}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.samFlexView}>
                    <TouchableOpacity
                      onPress={() => {
                        onChat();
                      }}>
                      <Image
                        source={images.owner_chat}
                        style={styles.callChatIconCss}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        onCall();
                      }}>
                      <Image
                        source={images.owner_call}
                        style={[styles.callChatIconCss, {marginLeft: 6}]}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              )}

              <View style={{paddingTop: 15, paddingLeft: 15, paddingRight: 10}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  {/* <Image source={images.paymentproperty_image}
                                style={styles.houseImgCss} /> */}
                  <ProgressiveImage
                    source={{uri: item?.property?.propertyphotos[0]?.imgurl}}
                    style={styles.houseImgCss}
                    resizeMode="cover"
                  />
                  <View style={{marginLeft: 10}}>
                    <Text numberOfLines={1} style={styles.houseAddText}>
                      {item?.property?.title}
                    </Text>
                    <View style={styles.samFlexView}>
                      <Image
                        source={images.location}
                        style={styles.samCssOfIcon}
                      />
                      <Text numberOfLines={2} style={styles.samTextCss}>
                        {item?.property?.location}
                      </Text>
                    </View>
                    <View style={styles.samFlexView}>
                      <Text style={styles.priceTextCss}>
                        {item?.property?.rent}
                      </Text>
                      <View style={styles.dotCss} />
                      <Text style={styles.houseTextCss}>
                        {item?.property?.type}
                      </Text>
                    </View>
                  </View>
                </View>
                {this.props.AgreementBtt && Helper.userType == 'OWNER' ? (
                  <TouchableOpacity
                    onPress={() => {
                      onClickAgreement();
                    }}
                    style={styles.bttViewAgreement}>
                    <Text style={styles.bttTextAgreement}>View Document</Text>
                  </TouchableOpacity>
                ) : null}

                {this.props.AgreementBttTENANT &&
                Helper.userType == 'TENANT' ? 
                (
                  <TouchableOpacity
                    onPress={() => {
                       viewAgreementTenant();
                    }}
                    style={styles.bttViewAgreement}>
                    <Text style={styles.bttTextAgreement}>View Agreement</Text>
                  </TouchableOpacity>
                )
                :
                null
                }

                {this.props.Button && Helper.userType == 'OWNER' ? 
                (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    {this.props.titleOwner && (
                      <TouchableOpacity
                        onPress={() => {
                          RejectProperty();
                        }}
                        style={[styles.titleOwnerView, {width: '48%'}]}>
                        <Text style={styles.titleOwner}>{titleOwner}</Text>
                      </TouchableOpacity>
                    )}
                    {this.props.subTitleOwner && (
                      <TouchableOpacity
                        onPress={() => {
                          AcceptProperty();
                        }}
                        style={[
                          styles.bttViewAgreement,
                          {
                            width: '48%',
                            backgroundColor: Colors.darkSeafoamGreen,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.bttTextAgreement,
                            {color: Colors.white},
                          ]}>
                          {subTitleOwner}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                )
                :(
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        onClick1();
                      }}
                      style={[styles.bttViewAgreement, {width: '48%'}]}>
                      <Text style={styles.bttTextAgreement}>{title}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        RejectProperty();
                      }}
                      style={[
                        styles.bttViewAgreement,
                        {
                          width: '48%',
                          backgroundColor: Colors.darkSeafoamGreen,
                        },
                      ]}>
                      <Text
                        style={[
                          styles.bttTextAgreement,
                          {color: Colors.white},
                        ]}>
                        {subTitle}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) }

                
              </View>
            </View>
            {Helper.userType == 'OWNER' ? (
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {this.props.viewDocBtn && (
                  <TouchableOpacity
                    onPress={() => {
                      ViewClickOwner();
                    }}
                    style={[styles.bttViewAgreementOwner, {width: '48%'}]}>
                    <Text style={[styles.bttTextAgreementOwner]}>
                      {viewDocBtn}
                    </Text>
                  </TouchableOpacity>
                )}
                {this.props.adAgreementBtn &&
                item?.property?.agreements &&
                item?.property?.agreements?.length ? (
                  <TouchableOpacity
                    onPress={() => {
                      viewAgreementTenant();
                    }}
                    style={[styles.titleOwnerView, {width: '48%'}]}>
                    <Text style={styles.titleOwner}>{'View Agreement'}</Text>
                  </TouchableOpacity>
                ) : null}
                {this.props.adAgreementBtn &&
                item?.property?.agreements?.length == 0 ? (
                  <TouchableOpacity
                    onPress={() => {
                      addAgreementOwner();
                    }}
                    style={[styles.titleOwnerView, {width: '48%'}]}>
                    <Text style={styles.titleOwner}>{adAgreementBtn}</Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            ) : null}
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { marginHorizontal: 10, marginBottom: 20, backgroundColor: Colors.whiteTwo },
    dateTimeViewCss: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10 },
    samTextCss: { color: Colors.brownishGreyTwo, fontSize: 12, fontFamily: Fonts.segoeui, width: '80%' },
    samFlexView: { flexDirection: 'row', alignItems: 'center' },
    samCssOfIcon: { height: 10, width: 10, resizeMode: "contain", top: 1, marginRight: 4 },
    cardViewOfCss: { borderWidth: 1, borderColor: Colors.whiteFive, borderRadius: 5, paddingBottom: 20, },
    userOfMainView: { flexDirection: 'row', alignItems: 'center', borderBottomColor: Colors.whiteFive, borderBottomWidth: 1, paddingVertical: 10, paddingLeft: 20, paddingHorizontal: 10 },
    userImageCss: { height: 35, width: 35, resizeMode: "contain" },
    userNameOfCss: { color: Colors.black, fontFamily: Fonts.segui_semiBold, fontSize: 14 },
    callChatIconCss: { height: 35, width: 35, resizeMode: "contain" },
    houseImgCss: { height: 60, width: 70, resizeMode: "contain" },
    houseAddText: { color: Colors.blackTwo, fontFamily: Fonts.segoeui_bold, fontSize: 14 },
    priceTextCss: { color: Colors.darkSeafoamGreen, fontFamily: Fonts.segoeui, fontSize: 12 },
    dotCss: { backgroundColor: Colors.pinkishGrey, height: 3, borderRadius: 3 / 1, width: 3, top: 2, marginHorizontal: 6 },
    houseTextCss: { color: Colors.marigold, fontFamily: Fonts.segui_semiBold, fontSize: 12 },
    bttViewAgreement: { borderWidth: 1, borderColor: Colors.darkSeafoamGreen, borderRadius: 4, padding: 4, marginTop: 15 },
    bttTextAgreement: { color: Colors.darkSeafoamGreen, fontSize: 14, fontFamily: Fonts.segoeui, textAlign: 'center' },
    agreementView: { marginTop: 20, borderWidth: 1, paddingVertical: 3, alignItems: 'center', borderRadius: 2, borderColor: Colors.darkSeafoamGreen },
    agreementText: { fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.darkSeafoamGreen },
    titleOwner: { fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.cherryRed, textAlign: 'center' },
    titleOwnerView: { borderWidth: 1, borderColor: Colors.cherryRed, borderRadius: 4, padding: 4, marginTop: 15 },
    bttViewAgreementOwner: { borderWidth: 1, borderColor: Colors.darkSeafoamGreen, borderRadius: 4, padding: 4, marginTop: 15 },
    bttTextAgreementOwner: { color: Colors.darkSeafoamGreen, fontSize: 14, fontFamily: Fonts.segoeui, textAlign: 'center' },
})
