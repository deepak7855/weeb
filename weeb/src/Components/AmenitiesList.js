import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Colors from '../Lib/Colors'
import Fonts from '../Lib/Fonts'
import images from '../Lib/Images'
export default class AmenitiesList extends Component {
    render() {
        const { item, index, selectBaddRoom, Select, height, width, onMultiSelect, disabled = false, items} = this.props
        return (
            <View>
                {items ?
                    <TouchableOpacity disabled={disabled} onPress={() => { onMultiSelect(index) }} style={{ marginTop: 19, paddingHorizontal: 10 }}>
                        {items.socialIcon ? <Image style={[styles.listIcon, { height: height ? height : 57, width: width ? width : 57 }]} source={{ uri: items?.socialIcon }} /> : null}
                        {items.socialText ? <Text style={styles.bottomText} numberOfLines={1}>{items.socialText}</Text> : null}
                    </TouchableOpacity>
                    :
                    null
                }
               
                { item  ? <TouchableOpacity onPress={() => { Select(item) }} style={[styles.badView, { backgroundColor: selectBaddRoom == item.id ? Colors.darkSeafoamGreen : Colors.whiteTwo }]}>
                    {item.badCount ? <Text style={[styles.badText, { color: selectBaddRoom == item.id ? Colors.whiteTwo : Colors.darkSeafoamGreen }]}>{item.badCount}</Text> : null}
                </TouchableOpacity> : null}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    listIcon: { width: 30, resizeMode: 'contain', },
    bottomText: { textAlign: 'center', marginTop: 7, fontSize: 10, fontFamily: Fonts.segui_semiBold, },
    badText: { fontSize: 16, fontFamily: Fonts.segoeui, },
    badView: {width:60, height:60 ,alignItems:'center',justifyContent:'center',marginHorizontal:7,borderWidth: 1, borderColor: Colors.lightMint, borderRadius: 4 }
})
