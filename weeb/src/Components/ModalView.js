import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Colors from '../Lib/Colors';
import Fonts from '../Lib/Fonts';

export default class ModalView extends React.PureComponent {
    render() {
        const { icon, title, subTitle, color, onClickNo, onClickYess, onClick, buttonTitle, singleButton, bttNoYess, width, height } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.modalMainView}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={[styles.iconCss, {height: height ? height:80, width: width ? width :80}]}
                            source={icon} />
                    </View>
                    {/* height: 80, width: 80, */}
                    {title &&
                        <Text style={[styles.titleTextOffCss, { color: color, }]}>{title}</Text>
                    }
                    <Text style={styles.subTitleOffCss}>{subTitle}</Text>
                    {buttonTitle &&
                        <TouchableOpacity onPress={() => { onClick() }}
                            style={{ backgroundColor: Colors.lightMint, borderRadius: 4, padding: 4, width: "43%", alignSelf: 'center', marginTop: 15 }}>
                            <Text style={[styles.bttTextAgreement, { color: Colors.black }]}>{buttonTitle}</Text>
                        </TouchableOpacity>
                    }
                    {bttNoYess &&
                        < View style={styles.twoBttMainView}>
                            <TouchableOpacity onPress={() => { onClickNo() }}
                                style={styles.bttViewAgreement}>
                                <Text style={styles.bttTextAgreement}>NO</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { onClickYess() }}
                                style={{ backgroundColor: Colors.lightMint, borderRadius: 4, padding: 4, width: "43%" }}>
                                <Text style={[styles.bttTextAgreement, { color: Colors.black }]}>Yes</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: 'center', alignItems: 'center' },
    modalMainView: { backgroundColor: Colors.white, paddingVertical: 20, paddingHorizontal: 25, borderRadius: 7, width: "80%", },
    iconCss: { height: 80, width: 80, resizeMode: 'contain' },
    titleTextOffCss: { fontSize: 20, fontFamily: Fonts.segoeui, textAlign: 'center', marginBottom:10 },
    subTitleOffCss: { fontSize: 14, fontFamily: Fonts.segoeui, color: Colors.black, textAlign: 'center', marginBottom: 10 },
    bttViewAgreement: { borderWidth: 1, borderColor: Colors.pinkishGrey, borderRadius: 4, padding: 4, width: "43%" },
    bttTextAgreement: { color: Colors.brownishGrey, fontSize: 16, fontFamily: Fonts.segui_semiBold, textAlign: 'center' },
    twoBttMainView: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 8, marginTop: 15 },
})