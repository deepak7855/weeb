import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
export default class StepLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      nextStep: 1,
      totalStep: 6,
      value: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text
          style={{
            fontSize: 12,
            fontFamily: Fonts.segoeui,
            color: Colors.blackThree,
            margin: 7,
          }}>
          {this.props.step}
        </Text>

        <View style={{flexDirection: 'row', margin: 5}}>
          <View style={styles.stepGreenLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
          <View style={styles.stepRedLine} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  stepGreenLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.darkSeafoamGreen,
    margin: 1,
  },
  stepRedLine: {
    width: '16%',
    height: 2,
    backgroundColor: Colors.cherryRed,
    margin: 1,
  },
});
