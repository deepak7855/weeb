import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image, View, Text, FlatList } from 'react-native';
import Images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
export default class FlootingButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    renderOfFilter = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.toSelectSpecializatio(index)}
                    style={styles.ViewRenderFilter}>
                    <Image style={this.props?.checkInd == index ? styles.checkIcon : styles.uncheckIcon} source={item.iconChecked} />
                    <Text style={this.props?.checkInd == index ? styles.selectedText : styles.unselectText}>{item.text}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    render() {


        return (
            <View>
                <TouchableOpacity
                    onPress={this.props.openFilter}
                    style={styles.filterTouch}>
                    <Image style={styles.iconFilter} source={Images.property_filter} />
                </TouchableOpacity>
                {
                    this.props.opener ? <View style={styles.filteView}>
                        <View style={[styles.viewTopTextOfFilter, {flexDirection:'row', justifyContent:'space-between', alignItems:'center'}]}>
                            <Text style={styles.textPropTypeOfFilter}>Property Type</Text>
                            <TouchableOpacity onPress={()=>{this.props.closeFilter()}}>
                                <Image style={{height:10, width:10, resizeMode:'contain'}} source={images.close}/>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.props.filterlist}
                            renderItem={this.renderOfFilter}
                            ItemSeparatorComponent={() => <View style={styles.filterSeperator} />} />
                    </View> : null
                }
            </View>
        );
    }
}


const styles = StyleSheet.create({
    flottingButton: {
        // position: "absolute",
        bottom: 10,
        height: 57,
        width: 57,
        right: 10,
    },
    ViewRenderFilter: { flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 23, backgroundColor: Colors.whiteTwo, alignItems: 'center' },
    filteView: { position: 'absolute', bottom: 90, right: 12, backgroundColor: Colors.whiteTwo, elevation: 5, width: 200, borderRadius: 10 },
    filterTouch: { position: 'absolute', bottom: 20, right: 12 },
    iconFilter: { width: 56, height: 56, resizeMode: 'contain' },
    filteView: { position: 'absolute', bottom: 90, right: 12, backgroundColor: Colors.whiteTwo, elevation: 5, width: 200, borderRadius: 10 },
    viewTopTextOfFilter: { paddingVertical: 10, paddingHorizontal: 20, backgroundColor: Colors.whiteNine, borderBottomColor: Colors.whiteFive, borderBottomWidth: 1 },
    textPropTypeOfFilter: { fontSize: 14, fontFamily: Fonts.segui_semiBold, color: Colors.blackTwo },
    checkIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.darkSeafoamGreen },
    uncheckIcon: { width: 12, height: 8, resizeMode: 'contain', tintColor: Colors.pinkishGrey },
    unselectText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.pinkishGrey },
    selectedText: { fontSize: 14, paddingVertical: 10, fontFamily: Fonts.segoeui, marginLeft: 11, color: Colors.black },
    filterSeperator: { borderWidth: 0.5, backgroundColor: Colors.warmGrey, opacity: 0.05 },
})