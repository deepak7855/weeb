import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  Pressable,
  Image,
  Platform,
  Modal,
  TouchableOpacity,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import {
  check,
  request,
  PERMISSIONS,
} from 'react-native-permissions';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Colors from '../../Lib/Colors'
const GooglePlacesInput = () => {
  return (
    <GooglePlacesAutocomplete
      placeholder='Name, Keyword and Category'
      onPress={(data, details = null) => {
        //   console.log("+++++++++++++++++++",data, details);
      }}
      query={{
        key: 'AIzaSyADKJ0J87cRHB5MjXjRHyzk6V7SGvBifuk',
        language: 'en',
      }}
    />
  );
};

export default class GooglelocationTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLocationPermission: true,
    };
  }


  render() {
    return (
      <View style={styles.mainContiner}>
        <Text
          style={[
            styles.labelStyle,
            {
              color: this.props.labelcolor,
              fontSize: this.props.labelsize,
              fontFamily: this.props.labelfontfamily,
            },
          ]}>
          {' '}
          {this.props.labels}{' '}
        </Text>
        <TouchableOpacity
          style={[
            styles.inputContiner,
            {
              backgroundColor: this.props.backgroundcolor,
              borderColor: Colors.whiteEight,
              borderWidth: 1,
            },
          ]}
          onPress={() => this.props.showModal()}>
          {this.props.rightimage ? (
            <Pressable>
              <Image
                source={this.props.rightimage}
                style={[
                  styles.rightImageIcon,
                  { marginLeft: this.props.marginleft },
                ]}
              />
            </Pressable>
          ) : null}

          <Text
            style={[
              styles.inputView,
              {
                width: this.props.width,
                color: this.props.value? Colors.blackThree: Colors.pinkishGrey,
                paddingLeft: this.props.rightimage ? 15 : 0
              },
            ]}>
            {this.props.value ? this.props.value : this.props.placeholder}
          </Text>
          {this.props.leftimage ? (
            <Pressable>
              <Image
                source={this.props.leftimage}
                style={[
                  styles.leftImageIcon,
                  { marginLeft: this.props.leftmarginleft },
                ]}
              />
            </Pressable>
          ) : null}
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContiner: {
    margin: 10,
    flex: 1,
  },
  labelStyle: {
    fontSize: 12,
    marginBottom: 10,
    fontFamily: Fonts.segoeui,
  },
  inputContiner: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    width: '100%',
    borderRadius: 5,
  },
  inputView: {
    fontSize: 14,
    paddingVertical: 5,
    paddingLeft: 15,
    fontFamily: Fonts.segoeui,
    // borderWidth: 1,
  },
  rightImageIcon: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    left:5
  },
  leftImageIcon: {
    height: 14,
    width: 14,
    resizeMode: 'contain',
    //  marginLeft: -10,
    //marginRight: 3,
  },
  backImageIcon: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    //  marginLeft: -10,
    //marginRight: 3,
    marginTop: 10,
    marginRight: 7,
    //  borderWidth: 1,
    padding: 5,
  },
});
