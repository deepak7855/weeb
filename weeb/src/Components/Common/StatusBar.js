import React from 'react';
import {StatusBar} from 'react-native';

const StatusBarScreen = () => {
  return <StatusBar animated={true} hidden={false} />;
};

export default StatusBarScreen;
