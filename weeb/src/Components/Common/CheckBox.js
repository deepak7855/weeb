import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, TouchableOpacityBase } from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import Images from '../../Lib/Images';
export default class CheckBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onpress} style={{ margin: 10, flexDirection: 'row', alignItems: 'center' }}>
                {this.props.state
                    ?

                    <Image style={styles.checkboxImage} source={Images.payment_green_check} />

                    :

                    <Image style={styles.checkboxImage} source={Images.uncheck_charges} />

                }

                <Text style={{ textAlign: 'center', fontSize: this.props.fontsize, fontFamily: this.props.fontfamily, color: this.props.color, marginLeft: 15 }}>{this.props.label}</Text>

            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    checkboxImage: {
        height: 18,
        width: 18,
        resizeMode: 'contain',
       // borderWidth: 1
    }
})