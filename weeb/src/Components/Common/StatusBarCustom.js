// import React from 'react';
// import { StatusBar } from 'react-native'

// const StatusBarScreen = () => {
//   return <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>;
// };

// export default StatusBarScreen;
import React, { Component } from 'react'
import { StyleSheet, Text, View,StatusBar } from 'react-native'

export default class StatusBarCustom extends Component {
  render() {
    const {backgroundColor, translucent} = this.props
    return (
      <StatusBar  barStyle = 'dark-content' hidden = {false} backgroundColor ={backgroundColor}  translucent = {translucent}/>
    )
  }
}
const styles = StyleSheet.create({
})