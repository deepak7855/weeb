import {Platform} from 'react-native'
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import Helper from '../../Lib/Helper';
import Config from '../../Lib/Config'; 

export function configureGoogleLogin() {
    try {
        GoogleSignin.configure({
            webClientId: Config.googleClientId,
            offlineAccess: true,
            hostedDomain: '',
            loginHint: '',
            forceConsentPrompt: true,
            accountName: '',
        });
    } catch ({ message }) {
    }
}

export function googleLogin(cb) {
    try {
        GoogleSignin.hasPlayServices().then(() => {
            GoogleSignin.signOut().then(() => {
                GoogleSignin.signIn().then((result) => {

                    let formdata = new FormData();
                    formdata.append("social_type", 'GOOGLE');
                    formdata.append("social_id", result.user.id);
                    formdata.append("device_type", Helper.device_type);
                    formdata.append("device_id", Helper.device_id);

                    if (result.user.email) {
                        formdata.append("email", result.user.email);
                    }
                    if (result.user.name) {
                        formdata.append("name", result.user.name);
                    }
                    if (result.user.photo) {
                        formdata.append("profile_picture", result.user.photo);
                    }
                   
                    cb(formdata)
                    GoogleSignin.signOut()
                }).catch((error) => {
                   // console.log(error, "error")
                    cb(false)

                })
            })
        })

    } catch (error) {
       // console.log(error, "error")
        cb(false);
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
        } else {
            // some other error happened
        }
    }
}

export function FacebookLogin(cb) {
    LoginManager.logOut();
    try {
       // LoginManager.setLoginBehavior('native_with_fallback');
         LoginManager.setLoginBehavior(Platform.OS == 'android' ? 'web_only' : 'browser');
        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    cb(false);
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let token = data.accessToken.toString();
                            fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + token)
                                .then((response) => response.json())
                                .then((json) => {
                                    //console.log(JSON.stringify(json));

                                    let formdata = new FormData();
                                    formdata.append("social_type", 'FACEBOOK');
                                    formdata.append("social_id", json.id);
                                    formdata.append("device_type", Helper.device_type);
                                    formdata.append("device_id", Helper.device_id);

                                    if (json.email) {
                                        formdata.append("email", json.email);
                                    }
                                    if (json.name) {
                                        formdata.append("name", json.name);
                                    }
                                    if (json.picture.data.url) {
                                        formdata.append("profile_picture", json.picture.data.url);
                                    }
                                    
                                    cb(formdata);
                                    LoginManager.logOut();
                                })
                                .catch((error) => {
                                    //console.log('fb error 1', error)
                                    cb(false);
                                })
                        },
                        function (error) {
                            //console.log('fb error 2', error)
                            cb(false);
                        }
                    )
                }
            },
            function (error) {
                //console.log('fb error 3', error)
                cb(false);
            }
        );
    } catch ({ message }) {
    }
}
