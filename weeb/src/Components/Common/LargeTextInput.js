import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
export default class LargeTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{
                margin: 10, }}>
                <Text
                    style={{
                        fontSize: this.props.fontsize ? 12 : 16,
                        color: Colors.blackThree,
                        fontFamily: this.props.fontfamily ? Fonts.segoeui : Fonts.segoeui_bold,
                    }}>
                    {this.props.discription}
                </Text>
                {/* <View> */}
                <TextInput
                    placeholderTextColor={Colors.pinkishGrey} 
                    style={{
                        backgroundColor: this.props.backgroundcolor,
                        borderColor: Colors.whiteEight,
                        borderWidth: 1.5,
                        borderRadius: 4,
                      //  borderWidth: 1,
                        height: this.props.height,
                        marginTop: 10,
                       // borderColor: Colors.pinkishGrey,
                        textAlignVertical: 'top',
                        fontFamily: Fonts.segoeui,
                       // color: Colors.pinkishGrey,
                        fontSize: 14,
                        padding:7,
                    }}
                    placeholder={this.props.placeholder}
                    multiline={true}
                    maxLength={this.props.maxlength}
                    onChangeText={(val) => this.props.changetext(val)}
                    value={this.props.value}
                />
                {!this.props.maxlengthtext
                    ?
                    <Text
                        style={{
                            fontSize: 10,
                            color: Colors.cherryRed,
                            fontFamily: Fonts.segoeui,
                        }}>
                        Max 500 Words
            </Text>
                    :
                    null
                }

            </View>
        );
    }
}
