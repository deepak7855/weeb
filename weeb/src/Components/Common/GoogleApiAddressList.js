import React from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  AppState,
  Image,
  StyleSheet,
  Platform,
  View,
} from 'react-native';
navigator.geolocation = require('@react-native-community/geolocation');
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Config from '../../Lib/Config';
import Helper from '../../Lib/Helper';
import {
  check,
  request,
  PERMISSIONS,
  openSettings,
} from 'react-native-permissions';
import images from '../../Lib/Images';
import Colors from '../../Lib/Colors';
export default class GoogleApiAddressList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      language: '',
      long: this.props.long,
      lat: this.props.lat,
      addressname: this.props.addressname,
      isLocationPermission: true,
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    this.checkPermission();
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      this.checkPermission();
    }
    this.setState({appState: nextAppState});
  };

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  async checkPermission() {
    await check(
      Platform.select({
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      }),
    ).then((result) => {
      if (result == 'granted') {
        this.setState({isLocationPermission: true});
      } else {
        this.setState({isLocationPermission: false});
      }
    });
  }

  enableLocationService() {
    request(
      Platform.select({
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      }),
    ).then((status) => {
      if (status === 'granted') {
        this.setState({isLocationPermission: true}, () => {});
      } else if (status === 'blocked') {
        Helper.permissionConfirm(
          'Please go to Settings and enable location services for weeb.',
          (status) => {
            if (status) {
              openSettings().catch(() => {
                console.warn('cannot open settings');
              });
            }
          },
        );
      }
    });
  }

  handleAddress = (address) => {
    this.props.onSelectAddress(address);
  };

  gotoBack = () => {
    this.props.hideModal();
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={this.props.hideModal}>
        <View
          style={{
            marginTop: Platform.OS == 'ios' ? 40 : 5,
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            onTouchStart={() => {
              this.gotoBack();
            }}>
            <TouchableOpacity
              hitSlop={{left: 30, right: 30, bottom: 30, top: 30}}
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              <Image
                resizeMode="contain"
                style={{height: 20, width: 20, marginTop: 3, marginRight: 8}}
                source={images.black_arrow_btn}
              />
            </TouchableOpacity>
          </View>
          <View>
            <View style={styles.headTitleView}>
              <Text style={styles.headertitle}>Search location</Text>
            </View>
          </View>
          <View></View>
        </View>
        <View style={{flex: 1, flexDirection: 'column'}}>
          <View
            style={{
              elevation: 5,
              top: 5,
              width: '100%',
              position: 'absolute',
              backgroundColor: 'transparent',
              flex: 1,
              justifyContent: 'center',
            }}>
            <GooglePlacesAutocomplete
              placeholder={'Enter location'}
              minLength={2} // minimum length of text to search
              autoFocus={true}
              returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
              listViewDisplayed={true} // true/false/undefined
              fetchDetails={true}
              onFail={(error) => console.error(error)}
              renderDescription={(row) => row.description || row.vicinity} // custom description render
              onPress={(data, details = null) => {
                // alert('bbgbbbbb')
                // console.log(data, 'data');
                // console.log(details, 'details');

                let tempform;
                if (data.description) {
                  tempform = {
                    addressname: data.description,
                    lat: details.geometry.location.lat,
                    long: details.geometry.location.lng,
                  };
                } else if (details.formatted_address) {
                  tempform = {
                    addressname: details.formatted_address,
                    lat: details.geometry.location.lat,
                    long: details.geometry.location.lng,
                  };
                }

                // alert(tempform)
                this.handleAddress(tempform);

                this.props.hideModal();
              }}
              getDefaultValue={() => {
                return ''; // text input default value
              }}
              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: Config.googleApiKey,
                // types: '(cities)', // default: 'geocode'
              }}
              styles={{
                description: {
                  fontWeight: 'bold',
                  color: '#000',
                },
                textInputContainer: {
                  borderTopWidth: 0,
                  borderBottomWidth: 0,
                },
                textInput: {
                  backgroundColor: 'rgba(0,0,0,0.5)',
                  textAlign: 'left',
                  height: 38,
                  color: '#fff',
                  fontSize: 16,
                },
                predefinedPlacesDescription: {
                  color: '#1faadb',
                  textAlign: 'left',
                },
                loader: {
                  paddingRight: 10,
                },
              }}
              currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
              enableHighAccuracyLocation={false}
              currentLocationLabel={'Get Current Location'}
              nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              GoogleReverseGeocodingQuery={
                {
                  // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }
              }
              // GooglePlacesSearchQuery={{
              //     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
              //     rankby: 'distance',
              //     types: 'food',
              // }}
              filterReverseGeocodingByTypes={[
                'locality',
                'administrative_area_level_3',
              ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
              debounce={200}

              // /keyboardShouldPersistTaps={'handled'}
            />
            {/* {!this.state.isLocationPermission &&
                            <View style={styles.turnRow}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: '#fff' }}
                                        tintColor={'#fff'}
                                        source={images.location_pin}
                                    />
                                    <Text style={[styles.enable, { color: '#fff', fontSize: 16 }]}>Enable Location Services</Text>
                                </View>
                                <TouchableOpacity hitSlop={{ left: 30, right: 30, top: 30, bottom: 30 }} activeOpacity={0.7} onPress={() => alert('ok')} style={{ backgroundColor: '#fff', borderRadius: 5 }}>
                                    <Text style={styles.enable}>TURN ON</Text>
                                </TouchableOpacity>
                            </View>
                        } */}
          </View>
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  headTitleView: {
    flex: 5,
    justifyContent: 'center',
    textAlign: 'center',
    paddingVertical: Platform.OS == 'ios' ? 0 : 15,
  },
  headertitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
  },
  turnRow: {
    backgroundColor: Colors.cherryRed,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'space-between',
    position: 'absolute',
    top: 60,
    width: '100%',
  },
  enable: {
    fontFamily: 'Avenir',
    fontSize: 14,
    paddingHorizontal: 20,
    fontWeight: '500',
    paddingVertical: 5,
    color: '#e2003f',
  },
});
