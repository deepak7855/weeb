import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Pressable,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import RNPickerSelect from 'react-native-picker-select';
import Helper from '../../Lib/Helper';
export default class TwoPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countrydata: [
        {
          label: '1',
          value: '2',
        },
        {
          label: '2',
          value: '2',
        },
        {
          label: '3',
          value: '3',
        },
        {
          label: '4',
          value: '4',
        },
        {
          label: '5',
          value: '5',
        },
      ],
    };
  }

  render() {
    return (
      <View style={styles.mainContiner}>
        <Text style={[styles.labelStyle, { color: this.props.color, fontSize: this.props.fontSize, fontFamily: this.props.fontfamily }]}>
          {' '}
          {this.props.labels}{' '}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={[
              styles.inputContiner,
              {
                backgroundColor: this.props.backgroundcolor,
                borderColor: Colors.pinkishGrey,
                borderWidth: 1,
                width: this.props.leftviewWidth,
              },
            ]}>
            <RNPickerSelect
              onValueChange={(value) => this.props.selectedFloorValue(value)}
              items={this.state.countrydata}
              useNativeAndroidPickerStyle={false}
              style={pickerSelectStyles}
              placeholder={{ label: 'Select Floor', value: false }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={this.props.rightimage}
                    style={[styles.dropdownIcon, {top:Helper.isIOS ? -7 : 0}]}
                  />
                );
              }}
              value={this.props.selectfloor}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.inputContiner,
              {
                backgroundColor: this.props.backgroundcolor,
                borderColor: Colors.pinkishGrey,
                borderWidth: 1,
                width: this.props.rightviwewidth,
              },
            ]}>
            <RNPickerSelect
              onValueChange={(value) => this.props.selectedTotalFloorValue(value)}
              items={this.state.countrydata}
              useNativeAndroidPickerStyle={false}
              style={pickerSelectStyles}
              placeholder={{ label: 'Total Floor', value: false }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={this.props.rightimage}
                    style={[styles.dropdownIcon, {top:Helper.isIOS ? -7 : 0}]}
                  />
                );
              }}
              value={this.props.totalfloor}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContiner: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 12,
    marginBottom: 10,
    fontFamily: Fonts.segoeui,
  },
  dropdownIcon: {
    width: 12,
    height: 12,
    marginTop: 22,
    tintColor: 'black',
  },
  inputContiner: {
    height: 39,
    flex:0.5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    borderBottomColor: Colors.pinkishGrey,
    borderTopColor: Colors.pinkishGrey,
    borderEndColor: Colors.pinkishGrey,
    borderLeftColor: Colors.pinkishGrey,
    borderRightColor: Colors.pinkishGrey,
    borderColor: Colors.pinkishGrey,
    margin:8
  },
  inputView: {
    fontSize: 14,
    paddingVertical: 10,
    paddingLeft: 15,
    fontFamily: Fonts.segui_semiBold,
  },
  rightImageIcon: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
  },
  arrowImageIcon: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '90%',
    minWidth: '90%',
    height: 40,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    alignItems:'center',
  },
  inputAndroid: {
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '90%',
    minWidth: '90%',
    height: 54,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    borderColor: Colors.black,
  },
});
