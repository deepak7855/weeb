import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

export default class CircleButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    margin: 30,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <Text
                    style={{
                        fontSize: this.props.fontsize,
                        fontFamily: this.props.labelfonts,
                        marginRight: 10,
                    }}>
                    {this.props.label}
                </Text>
                <TouchableOpacity
                    onPress={
                        this.props.navigate
                    }
                //    style={[styles.signUpCircle, { backgroundColor: this.props.colors }]}
                >
                    <Image
                        source={this.props.arrowimage}
                        style={styles.signUpImageIcon}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    signUpImageIcon: {
        height: 47,
        width: 47,
        resizeMode: 'contain',
        // transform: [{ rotate: '180deg' }],
        // marginLeft: 10,
        // justifyContent: 'center',
        // alignItems: 'center',
        //  marginLeft: -10,
        //marginRight: 3,
        //alignSelf: 'center'
    },
    signUpCircle: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
        justifyContent: 'center',
        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        //alignContent: 'center',
        elevation: 6,
    },
})
