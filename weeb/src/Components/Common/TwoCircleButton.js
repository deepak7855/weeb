import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

export default class TwoCircleButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const {disabled, rightnavigation, leftnavigation, rightimage, leftimage, tintColor} = this.props
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 30,
                }}>
                <TouchableOpacity disabled={disabled} onPress={()=>{leftnavigation()}}>
                    <Image source={rightimage} style={[styles.iconPro, {tintColor: tintColor}]} />
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{rightnavigation()}}>
                    <Image
                        source={leftimage}
                        style={{ width: 46, height: 46, marginLeft: 23 }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}
const styles =StyleSheet.create({
    iconPro: { width: 46, height: 46 }
})
