import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Pressable,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import RNPickerSelect from 'react-native-picker-select';
const countrydata = require('../../Components/Utils/countries.json');
export default class MobileInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countrydata: [
        {
          label: '+965',
          value: '+965',
        },
      ],
    };
  }
  render() {
    return (
      <View style={styles.mainContiner}>
        <Text style={[styles.labelStyle, { color: this.props.color }]}>
          {' '}
          {this.props.labels}{' '}
        </Text>
        <View
          style={{
            //   borderWidth: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            //backgroundColor: this.props.backgroundcolor,
          }}>
          <TouchableOpacity
            style={[
              styles.inputContiner,
              {
                backgroundColor: this.props.backgroundcolor,
                borderColor: 'white',
                borderWidth: 0.5,
                width: this.props.leftviewWidth,
              },
            ]}>
            {this.props.leftimage ? (
              <Pressable>
                <Image
                  source={this.props.leftimage}
                  style={[
                    styles.rightImageIcon,
                    { marginLeft: this.props.marginleft },
                  ]}
                />
              </Pressable>
            ) : null}
            <RNPickerSelect
              onValueChange={(value) => console.log(value)}
              items={this.state.countrydata}
              useNativeAndroidPickerStyle={false}
              style={pickerSelectStyles}
              placeholder={{ label: '+91', value: '+91' }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={this.props.rightimage}
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 22,
                      tintColor: 'black',
                      //marginRight: 15,
                      marginLeft: 10,
                    }}
                  />
                );
              }}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.inputContiner,
              {
                backgroundColor: this.props.backgroundcolor,
                borderColor: 'white',
                borderWidth: 0.5,
                width: this.props.rightviwewidth,
              },
            ]}>
            <TextInput
              style={[
                styles.inputView,
                {
                  width: this.props.width,
                },
              ]}
              placeholder={this.props.placeholder}
              keyboardType={'number-pad'}
              maxLength={this.props.maxlength}
              onChangeText={() => { this.props.onChangeText() }}
              value={this.props.value}
              returnKeyType={this.props.returnKeyType}
              ref={this.props.setfocus}
              onSubmitEditing={this.props.getfocus}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContiner: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 12,
    marginBottom: 10,
    fontFamily: Fonts.segoeui,
  },
  inputContiner: {
    height: 39,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    width: '100%',
    borderRadius: 5,
  },
  inputView: {
    fontSize: 14,
    paddingVertical: 10,
    paddingLeft: 15,
    fontFamily: Fonts.segui_semiBold,
    //    / borderWidth: 1,
    //  width: '90%',
  },
  rightImageIcon: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
    //marginLeft: -25,
    //marginRight: 3,
  },
  arrowImageIcon: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
    //marginLeft: -25,
    //marginRight: 3,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    // backgroundColor: Color.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '100%',
    minWidth: '100%',
    height: 54,
    fontSize: 12,
    fontFamily: Fonts.segui_semiBold,
    // borderColor: Color.black,
    borderWidth: 2,
  },
  inputAndroid: {
    //backgroundColor: Colors.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '80%',
    minWidth: '77%',
    height: 54,
    fontSize: 12,
    // margin: -5,
    //marginLeft: -10,
    //marginRight: -5,
    fontFamily: Fonts.segui_semiBold,
    borderColor: Colors.black,
    //  borderWidth: 2,
  },
});
