import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  Pressable,
  Image,
  Platform
} from 'react-native';
import Colors from '../../Lib/Colors';
import Fonts from '../../Lib/Fonts';
import images from '../../Lib/Images';
export default class Inputs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View
        style={[styles.mainContiner, { marginBottom: this.props.marginbotom, }]}>
        {/* {this.props.labels ? */}
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text
              style={[
                styles.labelStyle,
                {
                  color: this.props.labelcolor,
                  fontSize: this.props.labelsize,
                  fontFamily: this.props.labelfontfamily,
                },
              ]}>
              {' '}
              {this.props.labels}{''}
            </Text>
            <Text
              style={{
                marginBottom: 10,
                marginLeft: 2,
                fontSize: 16,
                fontFamily: Fonts.segoeui,
              }}>
            {this.props.labeltwo}
            </Text>
            {
              this.props.icon &&
              <Image style={{height:12, width:12, resizeMode:'contain', marginRight:10, marginBottom: 6}} source={this.props.icon}/>
            }
          </View>
          

        <View
          style={[
            styles.inputContiner,
            {
              backgroundColor: this.props.backgroundcolor,
              borderColor: Colors.whiteEight,
              borderWidth: 1.5,
              borderRadius: 4
            },
          ]}>
          {this.props.rightimage ? (
            <Pressable>
              <Image
                source={this.props.rightimage}
                style={[
                  styles.rightImageIcon,
                  { marginLeft: this.props.marginleft },
                ]}
              />
            </Pressable>
          ) :
            null
          }
          <TextInput
            style={[
              styles.inputView,
              {
                fontSize: this.props.inputSize,
                width: this.props.width,
                paddingLeft: this.props.InputPaddingLeft ? this.props.InputPaddingLeft : 10,
               // color: !this.props.editable ? Colors.blackThree :'',
              },
            ]}
            blurOnSubmit={this.props.bluronsubmit}
            placeholder={this.props.placeholder}
            
            keyboardType={this.props.keyboard}
            maxLength={this.props.maxlength}
            onChangeText={(val) => this.props.onChangeText(val)}
            secureTextEntry={this.props.password}
            placeholderTextColor={this.props.placeholdercolor}
            ref={this.props.setfocus}
            onSubmitEditing={this.props.getfocus}
            value={this.props.value}
            returnKeyType={this.props.returnKeyType}
            editable={this.props.editable} 
          />
          {this.props.leftimage ? (
            <Pressable onPress={() => { this.props.onTbShow() }}>
              <Image
                source={this.props.leftimage}
                style={[
                  styles.leftImageIcon,
                  {
                    marginRight: this.props.marginright, right: this.props.right,
                    width: this.props.widthleft ? this.props.widthleft : 16,
                    height: this.props.heightleft ? this.props.heightleft : 16,
                  },
                ]}
              />
            </Pressable>
          ) : <View style={{ width: 16 }}></View>}
        </View>
        {this.props.maxlengthtext
          ?
          <Text
            style={{
              fontSize: 10,
              color: Colors.cherryRed,
              fontFamily: Fonts.segoeui,
            }}>
            Max {this.props.maxlengthtext} Words
            </Text>
          :
          null
                      }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContiner: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 12,
    marginBottom: 10,
    fontFamily: Fonts.segoeui,
  },
  inputContiner: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: Platform.OS == "ios" ?3 : 0 
  },
  inputView: {
    fontSize: 14,
    paddingVertical: 5,
    paddingLeft: 15,
    fontFamily: Fonts.segoeui,
  },
  rightImageIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  leftImageIcon: {
    resizeMode: 'contain',
  }
});
