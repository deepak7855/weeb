// import React from 'react';
// import {View,ActivityIndicator,StyleSheet,Image} from 'react-native'
// import Colors from '../../Lib/Colors'
// export default class ProgressiveImage extends React.Component {
//     render() {
//         const {
//             thumbnailSource,
//             source,
//             style,
//             ...props
//         } = this.props;
//         return (
//             <View style={styles.container}>
//                 <Image
//                     {...props}
//                     source={thumbnailSource}
//                     style={style}
//                 />
//                 <Image
//                     {...props}
//                     source={source}
//                     style={[styles.imageOverlay, style]}
//                 />
//             </View>
//         );
//     }
// }
// const styles = StyleSheet.create({
//     imageOverlay: {
//         position: 'absolute',
//         left: 0,
//         right: 0,
//         bottom: 0,
//         top: 0,
//     },
//     container: {
//        // backgroundColor: '#e1e4e8',
//     },
// });

import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image'
 import Colors from '../../Lib/Colors'
export default class ProgressiveImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            defaultImage: this.props.source
        }
    }

    loadStart = (e) => { 
       // console.log('fast imag load start', e)
        this.setState({ loading: true })
    }


    loadEnd = (e) => {
      //  console.log('fast imag load end', e)
        this.setState({ loading: false })    
        // setTimeout(() => {
        //     this.setState({ loading: false })    
        // }, 2000);
        
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={this.props.style}>
                    <FastImage
                        style={this.props.style}
                        resizeMode={this.props.resizeMode}
                        source={this.props.source}
                        onLoadStart={(e) => this.loadStart(e)}
                        onLoadEnd={(e) =>this.loadEnd(e)}
                    />
                </View >
                {this.state.loading &&
                    <ActivityIndicator style={[this.props.style, styles.activityIndicator]} animating={true} size="small" color={Colors.cherryRed} />
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    activityIndicator: {
        position: 'absolute',
        zIndex: 1,
        backgroundColor: "transparent",
        left: 0,
        right: 0,
        bottom: 0,
    }
});