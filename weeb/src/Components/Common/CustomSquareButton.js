import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

export default class CustomSquareButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.showButton}
                style={[styles.DataButton, { width: this.props.width, borderColor: this.props.bordercolor, backgroundColor: this.props.state ? this.props.backgroundcolor : null }]}>
                <Text style={{ fontSize: 16, fontFamily: this.props.fontfamily, color: this.props.state ? this.props.color2 : this.props.color }}>{this.props.label}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    DataButton: {
        //width: '45%',
        borderWidth: 1,
        height: 40,
        justifyContent: "center",
        alignItems: 'center',
        // borderColor: Colors.lightMint,
        //backgroundColor: Colors.darkSeafoamGreen
    }
})