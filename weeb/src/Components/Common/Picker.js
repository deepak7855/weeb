import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Platform,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import Fonts from '../../Lib/Fonts';
import Colors from '../../Lib/Colors';
import RNPickerSelect from 'react-native-picker-select';
import Helper from '../../Lib/Helper';
export default class Picker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countrydata: [
        {
          label: '1',
          value: '1',
        },
        { 
          label: '2',
          value: '2',
          
        },
        {
          label: '3',
          value: '3',
        },
        {
          label: '4',
          value: '4',

        },
        {
          label: '5',
          value: '5',
        },
        {
          label: '5+',
          value: '5+',
        },
      ],
    };
  }

  render() {
    return (
      <View style={styles.mainContiner}>
        <Text
          style={[
            styles.labelStyle,
            {
              color: this.props.color,
              fontSize: this.props.labelsize,
              fontFamily: this.props.labelfontfamily,
            },
          ]}>
          {' '}
          {this.props.labels}{' '}
        </Text>
        <View
          style={{
            //   borderWidth: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            //backgroundColor: this.props.backgroundcolor,
          }}>
          <TouchableOpacity
            style={[
              styles.inputContiner,
              {
                backgroundColor: this.props.backgroundcolor,
                borderColor: Colors.pinkishGrey,
                borderWidth: 1,
                width: this.props.leftviewWidth,
              },
            ]}>
            <RNPickerSelect
              onValueChange={(value) => this.props.selectedPickerValue(value)}
              items={this.props.data?this.props.data:this.state.countrydata}
              useNativeAndroidPickerStyle={false}
              style={pickerSelectStyles}
              placeholder={{ label: this.props.placeholder, value: false }}
              Icon={() => {
                return (
                  <Image
                    resizeMode="contain"
                    source={this.props.leftimage}
                    style={[styles.dropdownIcon,{top: Helper.isIOS ? -7 : 0}]}
                  />
                );
              }}
              value={this.props.value}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContiner: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 12,
    marginBottom: 10,
    fontFamily: Fonts.segoeui,
  },
  inputContiner: {
    height: 39,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    width: '100%',
    borderRadius: 5,
    borderBottomColor: Colors.pinkishGrey,
    borderTopColor: Colors.pinkishGrey,
    borderEndColor: Colors.pinkishGrey,
    borderLeftColor: Colors.pinkishGrey,
    borderRightColor: Colors.pinkishGrey,
    borderColor: Colors.pinkishGrey,
  },
  rightImageIcon: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
    //marginLeft: -25,
    //marginRight: 3,
  },
  dropdownIcon: {
    width: 12,
    height: 12,
    marginTop: 22,
    tintColor: 'black',
    marginRight: 10,
  },
  arrowImageIcon: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
    //marginLeft: -25,
    //marginRight: 3,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    // backgroundColor: Color.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '98%',
    minWidth: '98%',
    height: 40,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    borderColor: Colors.black,
    alignItems:'center'
    // borderWidth: 2,
  },
  inputAndroid: {
    //backgroundColor: Colors.white,
    paddingHorizontal: 20,
    color: Colors.black,
    maxWidth: '98%',
    minWidth: '98%',
    height: 54,
    fontSize: 14,
    fontFamily: Fonts.segoeui,
    borderColor: Colors.black,
  },
});
