import React from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Image,
    View,
    Text,
    Keyboard,
} from "react-native";
import Fonts from "../Lib/Fonts";
import Colors from "../Lib/Colors";
import images from "../Lib/Images";
import Helper from "../Lib/Helper";

const AppHeader = (prop) => {
    return prop.props.navigation.setOptions({
        headerLeft: () =>
            !prop.leftHeide ? (
                <TouchableOpacity onPress={() => { Keyboard.dismiss(); prop.leftClick(); }}
                    hitSlop={{
                        top: 0,
                        bottom: 20,
                        left: 50,
                        right: 50,
                    }}
                    style={{ position: "absolute", left: 20, flexDirection: "row" }}
                >
                    <Image
                        source={prop.leftIcon}
                        style={{ height: 20, width: 20, resizeMode: "contain" }}
                    />
                </TouchableOpacity>
            ) : (
                    <View style={{flexDirection: 'row', alignItems: 'center', }}>
                        <TouchableOpacity onPress={() => { prop.menuClick() }}>
                            <Image style={styles.menuIcon} source={images.menu} />
                        </TouchableOpacity>
                        {
                            prop.location == true ?
                                <Text style={{ fontSize: 18, fontFamily: Fonts.segoeui, left: 15 }}>{prop.city}</Text>
                                :
                                null
                        }
                    </View>
                ),

        headerRight: () =>
            !prop.rightHide ? (
                <View style={{ flexDirection: 'row' }}>
                    {prop.search ? (
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {
                                prop.addBtn == true ?
                                    <TouchableOpacity onPress={() => { prop.onAdd() }}>
                                        <Image style={styles.addIcon} source={prop.addItem} />
                                    </TouchableOpacity>
                                    :
                                    null
                            }
                            <TouchableOpacity onPress={() => { prop.searchClick() }} style={{ right: 15, padding: 5 }}>
                                <Image style={styles.searchIcon} source={prop.searchIcon} />
                            </TouchableOpacity>
                        </View>
                    ) : null}
                </View>
            ) : (
                    <View style={{ width: 50 }} />
                ),
        headerTitle: () => (
            <Text
                style={{
                    fontSize: 20,
                    fontFamily: Fonts.segoeui_bold,
                    color: Colors.black,
                    textAlign: 'center'
                }}
            >
                {prop.title}

            </Text>
        ),
        headerStyle: (styles.headerShadow, {backgroundColor : prop.backgroundColor ? prop.backgroundColor : Colors.whiteTwo}),
        headerTitleStyle: {
            alignSelf: "center",
            fontFamily: Fonts.segoeui_bold,
            color: Colors.white,
        },
        headerLayoutPreset: "center",
    });

};
export default AppHeader;
const styles = StyleSheet.create({
    headerShadow: {
    },
    searchIcon: { height: 20, width: 20, resizeMode: 'contain', left: 5 },
    menuIcon: { height: 16, width: 22, resizeMode: 'contain', marginLeft: 20 },
    addIcon: { height: 17, width: 80, resizeMode: 'contain', borderRadius: 4, right: 25 }
});
