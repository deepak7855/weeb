import memoize from "lodash.memoize";
import i18n from "i18n-js";
import en from './en.json';
import ar from './ar.json';
export const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
);


export const setI18nConfig = (lang) => {
    // fallback if no available language fits
    // clear translation cache
    translate.cache.clear();

    i18n.translations = {
        en,
        ar,
    };
    i18n.locale = lang;
}



