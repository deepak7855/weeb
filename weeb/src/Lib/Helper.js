
import * as React from 'react';
import Toast from 'react-native-root-toast';
import { Alert, Platform, Dimensions, Keyboard } from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import DeviceInfo from 'react-native-device-info'
import Config from './Config';
import { Constant } from '../Lib/Constant'
const { width, height } = Dimensions.get('window'); 
import PushNotification from 'react-native-push-notification';
export default class Helper extends React.Component {
  url = '';
  static mainApp;
  static toast;
  static userData = {};
  static navigationRef;
  static Loader;
  static isIOS = Platform.OS == 'ios';
  static device_type = Platform.OS == 'android' ? 'ANDROID' : 'IOS'; 
  static device_id = 'SIMULATOR';
  static hasNotch = DeviceInfo.hasNotch();
  static country_list = '';
  static userType = 'TENANT'; //TENANT , OWNER
  static token = '';
  static showDocument = false;
  static resetEmail = '';
  static tempValue;
  static propertyData = {};
  static rememberMe = {};
  static isSocketConnected;
  static socket = null;
  static ownerPaymentDatails={};
  static tenantPaymentDetails = {};
  static notificationCount = 0;
  static currentLanguage ='en';
  static getFormData(obj) {
    let formData = new FormData();
    for (let i in obj) {
      formData.append(i, obj[i]);
    }
    return formData;
  }

  constructor(props) {
    super(props);
  }
  static registerNavigator(ref) {
    Helper.navigationRef = ref;
  }
  static registerLoader(mainApp) {
    Helper.mainApp = mainApp;
  }

  static registerLoged(mainApp) {
    Helper.mainApp = mainApp;
  }

  // static trim(value) {
  //     Helper.trim =
  // }

  static showLoader() {
    Keyboard.dismiss();
    Helper.mainApp.setState({loader: true});
  }

  static hideLoader() {
    Helper.mainApp.setState({loader: false});
  }

  static registerToast(toast) {
    Helper.toast = toast;
  }

  static showToast(msg) {
    if (msg) {
      Toast.show(msg, {
        duration: 2000,
        position: Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
    }
  }
  static alert(alertMessage, cb) {
    Alert.alert(
      Config.app_name,
      alertMessage,
      [
        {
          text: 'OK',
          onPress: () => {
            if (cb) cb(true);
            console.log('OK Pressed');
          },
        },
      ],
      {cancelable: false},
    );
  }

  static confirm(alertMessage, cb) {
    Alert.alert(
      Config.app_name,
      alertMessage,
      [
        {
          text: 'OK',
          onPress: () => {
            if (cb) cb(true);
          //  console.log('OK Pressed');
          },
        },
        {
          text: 'Cancel',
          onPress: () => {
            if (cb) cb(false);
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }

  static confirmPopUp(alertMessage, cb) {
    Alert.alert(
      Config.app_name,
      alertMessage,
      [
        {
          text: 'YES',
          onPress: () => {
            if (cb) cb(true);
            //console.log('OK Pressed');
          },
        },
        {
          text: 'NO',
          onPress: () => {
            if (cb) cb(false);
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }

  static permissionConfirm(alertMessage, cb) {
    Alert.alert(
      Config.app_name,
      alertMessage,
      [
        {
          text: 'NOT NOW',
          onPress: () => {
            if (cb) cb(false);
          },
          style: 'cancel',
        },
        {
          text: 'SETTINGS',
          onPress: () => {
            if (cb) cb(true);
            //console.log('OK Pressed');
          },
        },
      ],
      {cancelable: false},
    );
  }

  static cameraAlert(
    alertMessage,
    Camera,
    Gallery,
    Cancel,
    cbCamera,
    cbGallery,
  ) {
    Alert.alert(
      Config.app_name,
      alertMessage,
      [
        {
          text: Camera,
          onPress: () => {
            if (cbCamera) cbCamera(true);
           // console.log('OK Pressed');
          },
        },
        {
          text: Gallery,
          onPress: () => {
            if (cbGallery) cbGallery(true);
           // console.log('OK Pressed');
          },
        },
        {
          text: Cancel,
          onPress: () => {
            if (cbCamera) cbCamera(false);
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }

  static async setData(key, val) {
    try {
      let tempval = JSON.stringify(val);
      await AsyncStorage.setItem(key, tempval);
    } catch (error) {
      console.error(error, 'AsyncStorage');
    }
  }

  static async getData(key) {
    try {
      let value = await AsyncStorage.getItem(key);
      //console.log('async get',value)
      if (value) {
        let newvalue = JSON.parse(value);
        return newvalue;
      } else {
        return value;
      }
    } catch (error) {
      console.error(error, 'AsyncStorage');
    }
  }

  static async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (exception) {
      return false;
    }
  }

  static LogOutMethod() {
    Helper.removeItemValue(Constant.USER_DATA);
    Helper.removeItemValue(Constant.TOKEN);
    Helper.removeItemValue(Constant.USER_TYPE);
    PushNotification.removeAllDeliveredNotifications();
    Helper.userData = {};
    Helper.token = {};
    Helper.userType = '';
    Helper.propertyData = {};
    Helper.navigationRef.reset({
      index: 0,
      routes: [{name: 'Signin'}],
    });
  }
  static async makeRequest({url, data, method = 'POST', loader = true}) {
    console.log('0000000000', data);
    let finalUrl = Config.baseurl + url;
    //console.log(finalUrl, 'finalUrl');
    let form;
    let methodnew;
    let token = await this.getData('token');

    //console.log(token, 'tokentoken');
    //console.log(data, 'form');
    let varheaders;
    if (method == 'POSTUPLOAD') {
      methodnew = 'POST';
      /* if (data !== "" && data !== undefined && data !== null) {
                for (var property in data) {
                        form.append(property, data[property]);
                }
            } */

      varheaders = {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + token,
      };
      form = data;

      /* form = new FormData();
            for (let i in data)
                form.append(i, data[i]); */ // you can append anyone.
    } else if (method == 'POST') {
      methodnew = 'POST';
      if (token) {
        varheaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        };
      } else {
        varheaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        };
      }
      form = JSON.stringify(data);
    } else {
     // console.log('yes get');
      methodnew = 'GET';
      if (token) {
        varheaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        };
      } else {
        varheaders = {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        };
      }
    }

    return fetch(finalUrl, {
      body: form,
      method: methodnew,
      headers: varheaders,
    })
      .then((response) => {
        return response.json();
      })
      .then((responseJson) => {
       // console.log(responseJson, 'responseJson');
        if (responseJson.hasOwnProperty('status_code')) {
          if (responseJson.status_code === 401) {
            AsyncStorage.removeItem('userdata');
            AsyncStorage.removeItem('token');
            AsyncStorage.removeItem('is_social');

            Helper.navigationRef.reset({
              index: 0,
              routes: [{name: 'Welcome'}],
            });
            this.showToast(responseJson.error);
          }
        } else return responseJson;
      })
      .catch((error, a) => {
        this.showToast('Please check your internet connection.');
        //console.log('errorerror', error);
      });
  }

  static getImageUrl(url) {
    return (finalUrl = Config.imageUrl + '/' + url);
  }

  static checkNull(txt) {
    if (txt == null) {
      return '';
    } else {
      return txt;
    }
  }

  static Size(value) {
    return (value / 375) * width;
  }

  static getYearsOld(date) {
    const date1 = new Date();
    const date2 = new Date(date);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    if (diffDays > 365) {
      //year
      if (diffDays < 730) {
        return {num: 1, type: 'Year'};
      } else {
        return {num: diffDays / 365, type: 'Years'};
      }
    } else if (diffDays > 30) {
      if (diffDays < 60) {
        return {num: 1, type: 'Month'};
      } else {
        return {num: diffDays / 30, type: 'Months'};
      }
    } else {
      return {num: diffDays, type: 'Days'};
    }
  }

  static GetAddressFromLatLong(latitude, longitude, cb) {
    //console.log('lat --- ' + latitude, 'long --- ' + longitude);
    fetch(
      'https://maps.googleapis.com/maps/api/geocode/json?address=' +
        latitude +
        ',' +
        longitude +
        '&key=' +
        Config.google_place_api_key,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        return cb(responseJson.results[0].formatted_address);
      })
      .catch((err) => {
        Helper.alert('Unable to get your location');
      });
  }
}


