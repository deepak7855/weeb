import {translate} from '../Language/index'
export default {
    success: {
        LOGIN: 'Login Success!',
        SIGNUP: 'Signup Success!',
        PROFILEUPDATE:'PROFILE UPDATE SUCCESFULLY!',
        ADDBANK:'BANK ACCOUNT ADDED SUCCESFULLY!'
    },
    error: {
        NETWORK: translate('Internetconnectionerror!'),
        EmailAddress: "Enter email address.",
        EmailAddressValid: "Enter valid email address.",
        Password: "Enter password.",
        PasswordChar: "Password should be minimum 8 characters.",
        ApiFail: 'Something Went Wrong',
    }

}