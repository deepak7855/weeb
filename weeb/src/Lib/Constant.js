import { Dimensions } from 'react-native'

const screenDimensions = {
    ScreenWidth: Dimensions.get('screen').width,
    ScreenHeight: Dimensions.get('screen').height,
    WindowWidth: Dimensions.get('window').width,
    WindowHeight: Dimensions.get('window').height,
}
const getWidth = (value) => {
    return (value / 375) * Dimensions.get('window').width
}

const Constant = {
    Base_Url: 'http://dev9server.com/weeb/api/',
    USER_DATA: 'user_data',
    TOKEN: 'token',
    REMEMBER_ME_EMAIL: 'remember_me_email',
    REMEMBER_ME_PASSWORD: 'remember_me_password',
    REMEMBER_ME: 'REMEMBER_ME',
    USER_TYPE: 'USER_TYPE',
    NOTIFICATION_COUNT:'NOTIFICATION_COUNT_EVENT'
}

export { screenDimensions, getWidth, Constant }