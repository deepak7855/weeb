import * as React from 'react';
import SocketIOClient from 'socket.io-client';
//import { DeviceEventEmitter } from 'react-native-event-listeners';
import {NetInfo, DeviceEventEmitter} from 'react-native';
import Config from './Config';
import Helper from './Helper';
export default class ChatController extends React.Component {
  static events = {
    send_message: 'send_message',
    get_message_history: 'get_message_history',
    get_message_history_response: 'get_message_history_response',
    get_user_thread: 'get_user_thread',
    get_user_thread_response: 'get_user_thread_response',
    clear_chat: 'clear_chat',
    search: 'search',
    send_message_by_self: 'multi_user',
    recevied_message_by_others: 'receive_message',
    read_message_update: 'read_message_update',
    read_message_update_response: 'read_message_update_response',
    user_block_unblock: 'user_block_unblock',
    user_block_unblock_response: 'user_block_unblock_response',
    check_user_status: 'check_user_status',
    check_user_status_response: 'check_user_status_response',
    search_response: 'search_response',
    get_other_profile: 'get_other_profile',
    get_other_profile_response:'get_other_profile_response',
  };
  static isSockectConnected = 0;
  static tempEventName;
  static tempEventData;
  isConnected = false;
  constructor() {}
  static socketInit() {
    if (ChatController.isSockectConnected == 0) {
      this.socket = SocketIOClient(Config.socket_url, {
        forceNew: true,
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionDelayMax: 3000,
        transports: ['websocket'],
        allowUpgrades: false,
        pingTimeout: 30000,
        query: {user_id: Helper.userData?.id},
      });
      // console.log(
      //   '*********UserID---------------------------',
      //   Config.socket_url,
      //   {query: 'user_id=' + Helper.userData?.id},
      // );
      this.reConnect();
      this.socket.on('connect', (data) => {
       // console.log('-------------Socket connected-------    ', data);
      });

      this.socket.on('disconnect', (data) => {
        !Helper.userData ? null : this.socket?.connect();
        //console.log('-------------Socket disconnected---    ', data, '---- ');
      });
      this.socket.on('connect_error', (data) => {
        //console.log('-------------connect_error-------    ', data);
      });
      this.socket.on('error', (data) => {
        this.socketInit();
        //console.log('-------------error-------    ', data);
      });
      this.socket.on('multi_user', (data) => {
       // console.log('send_message_by_self----------', data);
        DeviceEventEmitter.emit(this.events.send_message_by_self, data);
      });
      this.socket.on('receive_message', (data) => {
       // console.log('recevied_message_by_others----------', data);
        DeviceEventEmitter.emit(this.events.recevied_message_by_others, data);
      });
      this.socket.on(this.events.get_message_history, (data) => {
       // console.log('get_message_history----------', data);
        DeviceEventEmitter.emit(this.events.get_message_history, data);
      });
      this.socket.on('get_message_history_response', (data) => {
        //console.log('get_message_history_response----------', data);
        DeviceEventEmitter.emit(this.events.get_message_history_response, data);
      });
      this.socket.on('get_user_thread', (data) => {
       // console.log(this.events.get_user_thread, '----------', data);
        DeviceEventEmitter.emit(this.events.get_user_thread, data);
      });
      this.socket.on('get_user_thread_response', (data) => {
        //console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.get_user_thread_response, data);
        //  this.callbackSocket('read_message_update', {msg_id:msg_id});
      });
      this.socket.on('get_other_profile', (data) => {
       // console.log(this.events.get_other_profile, '----------', data);
        DeviceEventEmitter.emit(this.events.get_other_profile, data);
      });
      this.socket.on('get_other_profile_response', (data) => {
        //console.log(this.events.get_other_profile_response, '----------', data);
        DeviceEventEmitter.emit(this.events.get_other_profile_response, data);
         DeviceEventEmitter.emit('get_other_profile_response_other', data);
        //  this.callbackSocket('read_message_update', {msg_id:msg_id});
      });
      this.socket.on('read_message_update_response', (data) => {
        // console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.read_message_update_response, data);
      });
      this.socket.on('read_message_update', (data) => {
        // console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.read_message_update, data);
      });
      this.socket.on('user_block_unblock', (data) => {
        // console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.user_block_unblock, data);
      });
      this.socket.on('user_block_unblock_response', (data) => {
        console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.user_block_unblock_response, data);
      });

      this.socket.on('check_user_status', (data) => {
        // console.log(this.events.get_user_thread_response, '----------', data);
        DeviceEventEmitter.emit(this.events.check_user_status, data);
      });
      this.socket.on('check_user_status_response', (data) => {
       // console.log(this.events.check_user_status_response, '----------', data);
        DeviceEventEmitter.emit(this.events.check_user_status_response, data);
      });
      // this.socket.on('search', (data) => {
      //   console.log(this.events.search, '----------', data);
      //   DeviceEventEmitter.emit(this.events.search, data);
      // });
      this.socket.on('search_response', (data) => {
       // console.log(this.events.search_response, '----------', data);
        DeviceEventEmitter.emit(this.events.search_response, data);
      });
    } else {
      this.reConnect();
    }
    // this.socket.on('connect', (data) => {
    //   ChatController.isSockectConnected = 1;
    //   console.log('*********-------------Socket connected');
    // });
    // this.socket.on('disconnect', (data) => {
    //   ChatController.isSockectConnected = -1;
    //   console.log('*********-------------Socket disconnected', data);
    //   this.reConnect();
    // });
  }
  static reConnect() {
    //this.socket.connect();
    if (this.socket.connected == false) {
    // console.log('reconnect call -------------Socket connected');
      this.socket.connect();
        // if(this.tempEventName == this.events.get_user_thread){
          
        //   this.callbackSocket(this.tempEventName,this.tempEventData);
        //   this.tempEventName = '';
        //   this.tempEventData = '';
        // }
    } else {
     // console.log('old call -------------Socket connected');
    }
  }
  static disconnectUser() {
    this.socket.disconnect();
  }
  static callbackSocket(eventname, formdata) {
   // console.log('-----Socket emit===== ', eventname, ' -->  ', formdata);
    //  this.reConnect();
this.tempEventName = eventname;
this.tempEventData = formdata;
    if (this.socket.connected == false) {
      this.reConnect();
    }
      //console.log('-----------event---calling------',this.tempEventData,this.tempEventName)
    this.socket.emit(eventname, formdata);
  }
}
