import React from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import FileViewer from 'react-native-file-viewer';

import RNFS from 'react-native-fs';

export function FileDownload(url) {
  //  console.log('file extasion', url.split('.').pop());
    const localFile = `${RNFS.DocumentDirectoryPath}/${'agreement'}.${url
      .split('.')
      .pop()}`;
  const options = {
    fromUrl: url,
    toFile: localFile,
  };
  RNFS.downloadFile(options)
    .promise.then(() => FileViewer.open(localFile))
    .then((res) => {
      // success
      console.log('report res', res);
    })
    .catch((error) => {
      console.log('report error', error);
      // error
    });
};

export async function downloadFile(url) {
    console.log('file url', url);
    if(Platform.OS == 'ios'){
      FileDownload(url);
      return
    }
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      FileDownload(url);
    } else {
      Alert.alert(
        'Permission Denied!',
        'You need to give storage permission to download the file',
      );
    }
  } catch (err) {
    console.warn(err);
  }
};

