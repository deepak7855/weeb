import Helper from './Helper'
const Fonts  = {
    segoeui: Helper.isIOS ? 'SegoeUI' : 'segoeui',
    segoeui_bold: Helper.isIOS ? 'SegoeUI-Semibold' : 'segoeui_bold',
    segui_semiBold: Helper.isIOS ? 'SegoeUI-Bold' : 'segui_semiBold',
     
}
export default Fonts;