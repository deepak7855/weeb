import React from 'react';
import { Linking,Platform} from 'react-native';
 export function CallTenant(number) {
    let phoneNumber = '';
    if (Platform.OS == 'android') {
      phoneNumber = `tel:$${+number}`;
    } else {
      phoneNumber = `telprompt:$${+number}`;
    }
    Linking.openURL(phoneNumber);
  }
