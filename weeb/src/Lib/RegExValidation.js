import Toast from 'react-native-root-toast'
import { Helper } from '.';
export const RegexValid = {
    EmailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    NameRegex: /^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z ]*)*$/,
    OTP: /^\d{6}$/,
    ALPHABET_ONLY: /^[a-zA-Z \s]*$/,
    MOBILE: /^[0-9]{1,20}$/,
    ALPHANUMERIC: /^[A-Za-z0-9_ ]+$/,
    ONLYNUMBER: /^[0-9\b]+$/
}

export const Validation = {
    checkAlphabat: (name, min, max, value) => {
        let minValue = min || 3;
        let maxValue = max || 25;

        if (value) {
            if (!RegexValid.NameRegex.test(value)) { Helper.showToast(name + ' is Invalid'); return false }
            else if (value.length < minValue || value.length > maxValue) { Toast.show(`${name} must be between ${minValue} or ${maxValue} Characters.`); return false }
            return true;
        }
        else {
            Helper.showToast(name + ' is Required');
            return false;
        }
    },
    checkAlphaNumeric: (name, min, max, value) => {
        let minValue = min || 3;
        let maxValue = max || 25;

        if (value) {
            if (!RegexValid.ALPHANUMERIC.test(value)) { Helper.showToast(name + ' is Invalid'); return false }
            else if (value.length < minValue || value.length > maxValue) { Helper.showToast(`${name} must be between ${minValue} or ${maxValue} Characters.`); return false }
            return true;
        }
        else {
            Helper.showToast(name + ' is Required');
            return false;
        }
    },

    checkEmail: (name, value, val) => {
       
        if (value) {
            if (!RegexValid.EmailRegex.test(value)) {
              
                Helper.showToast(name + ' is Invalid');
                return false
            }
        } else {
            
            Helper.showToast(name + ' is Required')
            return false
        }

        return true;
    },

    checkNotNull: (name, min, max, value) => {
        if (value) {
            if (value.length < min || value.length > max) { Helper.showToast(`${name} must be between ${min} to ${max} Characters.`); return false }
        } else {
            Helper.showToast(name + ' is Required')
            return false
        }

        return true
    },
    checkAddress: (name,min ,value) => {
        if (value) {
            if (value.length < min) { Helper.showToast(`${name} is Required`); return false }
        } else {
            Helper.showToast(name + ' is Required')
            return false
        }

        return true
    },
    checkPicker: (name, min, value) => {
        if (value) {
            
            if (value.length < min && Number(value) == 0) { Helper.showToast(`${name} is Required`); return false }
        } else {
            Helper.showToast(name + ' is Required')
            return false
        }
        return true
    },
    checkAmount: (name, min, value) => {
        if (value) {
            if (!RegexValid.ONLYNUMBER.test(value)) {
                Helper.showToast(`${name} is invalid`); return false
            }
        } else {
            Helper.showToast(name + ' is Required')
            return false
        }
        return true
    },
    checkMatch: (name, value, value2) => {
        if (value === value2) {
            return true
        }
        else { Helper.showToast(`${name}  should be same.`); return false }
    },

    checkShouldNotBeMatch: (name, value, value2) => {
        if (value == value2) {
            Helper.showToast(`${name}  should not be same.`);
            return false
        }
        else {  return true }
    },
    checkPhoneNumber: (name, min, max, value) => {
        var min = min || 7;
        var max = max || 15;
        if (value) {
            if (!RegexValid.MOBILE.test(value)) { Toast.show(`${name} is Invalid.`); return false }
            else if (value.length < min || value.length > max) { Toast.show(`${name} should be greater than ${min - 1} digits.`); return false }
            return true
        }
        else { Helper.showToast(`${name} is Required.`); return false }

    },
    checkOtp: (name, value) => {
        if (value) {
            if (!RegexValid.OTP.test(value)) {
                Helper.showToast(name + ' is Invalid');
                return false
            } else if (value.length < 6) {
                Helper.showToast(name + ' is Invalid');
                return false
            }
        } else {
            Helper.showToast(name + ' is Required')
            return false
        }

        return true;
    },
    checkTrue: (name, value) => {
        if (!value) {
            Helper.showToast(`${name} is Required!`);
            return false
        } else {
            return true
        }
    },
}


