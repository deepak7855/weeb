import { Platform } from "react-native";
import Helper from "../Lib/Helper";
import messaging from '@react-native-firebase/messaging';
// import { ApiEndpoints } from "./ApiEndpoints";
import { ApiCall, ActivityIndicatorApp, LoaderForList } from '../Api/index';
export async function checkPermission() {
    let enabled = await messaging().hasPermission();
    if (enabled) {
        getDeviceToken();
    } else {
        requestPermission();
    }
}

export async function requestPermission() {
    try {
        await messaging().requestPermission();
        getDeviceToken();
    } catch (error) {
        console.log('permission rejected');
    }
}

export function getDeviceId() {
    if (Platform.OS === 'android') {
        getDeviceToken();
    }
    else {
        checkPermission()
    }
}

export async function updateDeviceToken() {
    let fcmToken = null;
    fcmToken = await messaging().getToken();
    if (fcmToken) {
        updateTokenInDb(fcmToken)
    }
}

export async function updateTokenInDb(fcmToken) {
    Helper.device_id = fcmToken;
    if (Helper.userData) {
        let formdata = new FormData();
        formdata.append("device_type", Helper.device_type);
        formdata.append("device_id", fcmToken);

        ApiCall.ApiMethod({Url:'update-device',method:'POSTUPLOAD',data:formdata}).then((res)=>{
            console.log('------------fcm-------token',res)
            if(res?.status){
              console.log('fcm token is update device',res)
              return true;
            }
        })
    }
}
