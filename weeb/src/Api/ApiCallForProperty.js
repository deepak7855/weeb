import { Helper, Network, AlertMsg } from '../Lib/index';
import { ApiCall } from '../Api/index'; 
const getPropertyList = async ({ lat = '', long = '', status,currentPage ,type='' }) => {
  return  Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
            let data = {
                latitude: lat,
                longitude: long,
                status_user: status,
                type:type
            }
            return ApiCall.ApiMethod({ Url: 'get-properties' + "?page=" + currentPage, method: 'POST', data: data }).then( ( res)  =>  {
               // console.log('active property', res, 'status', status)
                if (res?.status) {
                    // if (status == 2) {
                    //     this.setState({
                    //         InActivePropertyDataState: res?.data?.data
                    //     })
                    //     return;
                    // } if (status == 1) {
                    //     this.setState({
                    //         ActivePropertyDataState: res?.data?.data
                    //     })
                    //     return;
                    // }


                    return res
                }
            }).catch((err) => {
               // console.log('adad', err)
                return false
            })
        }
    })
}

const deleteProperty = (id) => {
    let data = {
        property_id: id
    };

  return  Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
            Helper.mainApp.showLoader();
          return  ApiCall.ApiMethod({ Url: 'delete-property', method: 'POST', data: data }).then((res) => {
                Helper.mainApp.hideLoader();
                //console.log('delete res', res);
                if (res?.status) {
                    Helper.showToast('Property Deleted successfully')
                    return res;
                 //   getPropertyList({ status: 1,currentPage:1 })
                } else {
                    Helper.showToast(res?.message)
                }

            }).catch((err) => {
                Helper.mainApp.hideLoader();
                //console.log('delete api err', err)
            })
        }
    }).catch((err) => {
        Helper.mainApp.hideLoader();
       // console.log('err', err)
    })
    // }
}
 
const ActiveAndDeactiveProperty = (id,status) => {
    let data = {
        property_id: id,
        status_user: status,

    };

    return Network.isNetworkAvailable().then((isConnected) => {
        if (isConnected) {
            Helper.mainApp.showLoader();
            return ApiCall.ApiMethod({ Url: 'property-status-update', method: 'POST', data: data }).then((res) => {
                Helper.mainApp.hideLoader();
                //console.log('active.inactive res', res);
                if (res?.status) {
                    Helper.showToast(res?.message)
                    return res;
                } else {
                    Helper.showToast(res?.message)
                    return res;
                }

            }).catch((err) => {
                Helper.mainApp.hideLoader();
                //console.log('active/inactive api err', err)
            })
        }
    }).catch((err) => {
        Helper.mainApp.hideLoader();
       // console.log('err', err)
    })
    // }
}

export default { getPropertyList, deleteProperty, ActiveAndDeactiveProperty}