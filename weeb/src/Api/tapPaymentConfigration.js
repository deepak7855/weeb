import React, { Component } from 'react';
import {Platform} from 'react-native'
import RNGoSell from '@tap-payments/gosell-sdk-react-native';
import {Helper, Network, AlertMsg} from '../Lib/index';
const {
    Languages,
    PaymentTypes,
    AllowedCadTypes,
    TrxMode,
    SDKMode
} = RNGoSell.goSellSDKModels;

const transactionCurrency = 'kwd';
const shipping = [
    {
        name: 'shipping 1',
        description: 'shiping description 1',
        amount: 10.0,
    },
];

const paymentitems = [
    {
        amount_per_unit: 1,
        description: 'Item 1 Apple',
        discount: {
            type: 'F',
            value: 10,
            maximum_fee: 10,
            minimum_fee: 0,
        },
        name: 'item1',
        quantity: {
            value: 1,
        },
        taxes: [
            {
                name: 'tax1',
                description: 'tax describtion',
                amount: {
                    type: 'F',
                    value: 10,
                    maximum_fee: 10,
                    minimum_fee: 0,
                },
            },
        ],
        total_amount: 100,
    },
];

const taxes = [
    {
        name: 'tax1',
        description: 'tax describtion',
        amount: { type: 'F', value: 10.0, maximum_fee: 10.0, minimum_fee: 1.0 },
    },
    {
        name: 'tax1',
        description: 'tax describtion',
        amount: { type: 'F', value: 10.0, maximum_fee: 10.0, minimum_fee: 1.0 },
    },
];
const customer = {
    isdNumber: Helper.userData?.customer_id,
    number: Helper.userData?.mobile_number,
    customerId: '',
    first_name: Helper.userData?.name,
    middle_name: 'kumar',
    last_name: 'mahehswari',
    email:Helper.userData?.email,
};
const paymentReference = {
    track: 'track',
    payment: 'payment',
    gateway: 'gateway',
    acquirer: 'acquirer',
   transaction: 'trans_910101',
    order: 'order_262625',
    gosellID: null,
};


const appCredentials = {
    production_secrete_key: (Platform.OS == 'ios') ? 'iOS-Live-KEY' : 'Android-Live-KEY',
    language: Languages.EN,
    sandbox_secrete_key: (Platform.OS == 'ios') ? 'sk_test_qYadfvseIAjoi41OXcnrG3Bt' : 'sk_test_lAiWOcSI7zBojU2N31HhqywE',
    bundleID: (Platform.OS == 'ios') ? 'org.reactjs.native.example.testingtapaymentmethod' : 'com.weeb',
}

const allConfigurations = {
    appCredentials: appCredentials,
    sessionParameters: {
        paymentStatementDescriptor: 'paymentStatementDescriptor',
        transactionCurrency: 'kwd',
        isUserAllowedToSaveCard: true,
        paymentType: PaymentTypes.ALL,
        amount:Helper.ownerPaymentDatails && Helper.ownerPaymentDatails.totalGrandTotal ? Helper.ownerPaymentDatails.totalGrandTotal.toString():'',
        //shipping: shipping,
        allowedCadTypes: AllowedCadTypes.CREDIT,
       // paymentitems: paymentitems,
        paymenMetaData: { a: 'a meta', b: 'b meta' },
        applePayMerchantID: 'applePayMerchantID',
        authorizeAction: { timeInHours: 10, time: 10, type: 'CAPTURE' },
        cardHolderName: 'Card Holder NAME',
        editCardHolderName: false,
        postURL: 'https://tap.company',
        paymentDescription: 'paymentDescription',
        destinations: {
            "destination": [
              {
                "id":Helper.ownerPaymentDatails?.ownerDestinationId ,
                "amount":Helper.ownerPaymentDatails?.ownerMoney ,
                "currency": "KWD",
                "description":"testing",
              },
            ]
          },
        trxMode: TrxMode.PURCHASE,
       // taxes: taxes,
        merchantID: '',
        SDKMode: SDKMode.Sandbox,
        customer: customer,
        isRequires3DSecure: true,
        receiptSettings: { id: null, email: false, sms: true },
        allowsToSaveSameCardMoreThanOnce: false,
        paymentReference: paymentReference,
    },
};

export default allConfigurations