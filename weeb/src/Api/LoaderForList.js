

import React, { Component } from "react";
import { Platform, ActivityIndicator, View, } from 'react-native'
import Helper from '../Lib/Helper';
import Colors from '../Lib/Colors';
export default class LoaderForList extends Component {
    constructor(props) {
        super(props);
        
    }
    render() {
        return  (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color={Colors.darkSeafoamGreen} />
            </View>
        ) 
    }

}