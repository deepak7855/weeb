const Apiurl = {
    SOCIAL: 'userSocial',
    TermAndPrivacy:'http://dev9server.com/weeb/terms-and-condition',
    PrivacyPolicy:'http://dev9server.com/weeb/privacy-policy',
    AboutUs:'http://dev9server.com/weeb/about-us',
    HelpAndSupport:'http://dev9server.com/weeb/support',
}
export default Apiurl;