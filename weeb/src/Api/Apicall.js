import * as React from 'react';
import { Network } from '../Lib/index'
import axios from 'axios';
import { ApiUrl, CommonApiMethod } from './index';
import { Helper, AlertMsg } from '../Lib/index'
import {Constant} from '../Lib/Constant'
export default class Apicall extends React.Component {
    static async ApiMethod({ Url, data, method = "POST", loader = true }) {
        let completeUrl;
        let form;
        let methodnew;
        let token;
               // if (Helper.token) token = 'Bearer ' + Helper.token
               token = await Helper.token
               //console.log(token, "tokentoken")
               let varheaders;
               completeUrl = CommonApiMethod.BaseUrl(Url)


       


if(method == "TAP"){
    let key = 'sk_test_NUKx2SJj6m3otYDI7infTXuL'
    completeUrl = Url;
    methodnew = "POST";
    varheaders = {
        // Accept: 'application/json',
        'Content-Type': 'multipart/form-data;',
        Authorization: 'Bearer ' + key,
        // Host:'www.api.tap.company'
    }
    form = data
}
        else if (method == "POSTUPLOAD") {
            methodnew = "POST";
            /* if (data !== "" && data !== undefined && data !== null) {
                for (var property in data) {
                        form.append(property, data[property]);
                }
            } */

            varheaders = {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data;',
                Authorization: 'Bearer ' + token
            }
            form = data//SON.stringify(data);

            /* form = new FormData();
            for (let i in data)
                form.append(i, data[i]); */ // you can append anyone.

        }
        else if (method == "POST") {

            methodnew = "POST";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
            form = JSON.stringify(data);
        }
        else if(method == "GET"){
            console.log('yes get');
            methodnew = "GET";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }


        }


        //if (loader) this.showLoader();

      //  console.log(completeUrl, "finalUrl",'method',method,methodnew,'data',JSON.stringify(data),await Helper.token);
        

       // console.log("Api chali", form, "form");
//console.log('---------var header',varheaders)
        return fetch(completeUrl, {
            body: form,
            method: methodnew,
            headers: varheaders,
        })
            .then((response) => {

                //console.log(response, "response")

                return response.json()

            })
            .then((responseJson) => {
                //console.log('hello test response');
               // console.log(responseJson, "responseJson")
                if (responseJson?.status == 'unauthenticated') {
                    Helper.showToast(responseJson?.message)
                    Helper.LogOutMethod();

                    return 
                }
                return responseJson;

            })
            .catch((error, a) => {
                //if (loader) this.hideLoader();
                Helper.showToast(AlertMsg.error.NETWORK);
                console.log('errorerror', error);
            });
    }

    static socialLogin = async (data) => {
        return await Apicall.ApiMethod({ Url: ApiUrl.SOCIAL, method: "POSTUPLOAD", data: data,isImage:true}).then(
            (response) => {
                if (response.status) {
                    return response;
                } else {
                    Helper.showToast(response.message);
                    return false
                }
            },
        );
    }

    static  getPropertyList =  ({ lat = '', long = '', status  }) => {
        Network.isNetworkAvailable().then((isConnected) => {
            if (isConnected) {
                let data = {
                    latitude: lat,
                    longitude: long,
                    status: status,
                }
                return Apicall.ApiMethod({ Url: 'get-properties', method: 'POST', data: data }).then((res) => {
                  //  console.log('active property', res, 'status', status)
                    if (res?.status) {
                        // if (status == 2) {
                        //     this.setState({
                        //         InActivePropertyDataState: res?.data?.data
                        //     })
                        //     return;
                        // } if (status == 1) {
                        //     this.setState({
                        //         ActivePropertyDataState: res?.data?.data
                        //     })
                        //     return;
                        // }


                         return res?.data?.data
                    }
                }).catch((err) => {
                    return false
                    console.log('adad', err)
                })
            }
        })
    }
}