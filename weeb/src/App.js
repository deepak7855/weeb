import React from 'react';
import { StyleSheet, View,Platform, StatusBar } from 'react-native';
import Routes from './navigation/routes';
import ActivityIndicatorApp from './Api/ActivityIndicatorApp'
import Helper from './Lib/Helper'

import Colors from '../src/Lib/Colors'
import { setI18nConfig } from './Language';
export default class App extends React.Component {

constructor(props) {
  super(props)

  this.state = {
     
  }
  setI18nConfig('en')
}


  render() {
    return (
      <View style={{ flex: 1, }}>
        {/* <View style={{backgroundColor:Colors.lightMint,height:Platform.OS === 'ios'?50:0}}>

<StatusBar barStyle={'dark-content'} backgroundColor={'blue'}/>
        </View> */}
        <Routes />
        <ActivityIndicatorApp onRef={ref => { Helper.mainApp = ref }} />
      </View>
    )
  }
}

const styles = StyleSheet.create({

});
